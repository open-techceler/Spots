//
//  XMPPMessage+XEP_0033.m
//  Maply
//
//  Created by steven on 22/7/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "XMPPMessage+XEP_0033.h"
#import "XMPPJID.h"
#import "XMPPStream.h"

#import "NSXMLElement+XMPP.h"
#if TARGET_OS_IPHONE
#import "DDXML.h"
#endif


static NSString *const xmlns_multicast = @"http://jabber.org/protocol/address";

@implementation XMPPMessage (XEP_0033)

+ (XMPPMessage *)multicastMessageWithType:(NSString *)type jids:(NSArray *)jids module:(NSString*)module
{
    return [[XMPPMessage alloc] initWithType:type jids:jids module:module];
}

+ (XMPPMessage *)multicastMessageWithBody:(NSString *)body to:(NSArray *)jids module:(NSString*)module
{
    return [[XMPPMessage alloc] initChatMessageWithBody:body toJids:jids module:module];
}

+ (XMPPMessage *)multicaseFlashMessageWithData:(NSDictionary*)data to:(NSArray *)jids module:(NSString*)module{
    return [[XMPPMessage alloc] initFlashMessageWithBody:data toJids:jids module:module];
}

- (id)initFlashMessageWithBody:(NSDictionary *)data toJids:(NSArray *)to_jids module:(NSString*)module
{
    if ((self = [super initWithName:@"message"]))
    {
        [self addAttributeWithName:@"type" stringValue:@"chat"];
        [self addAttributeWithName:@"to" stringValue:module];
        
        NSXMLElement *customMsgMark = [NSXMLElement elementWithName:@"stevencustommsg"];
        [customMsgMark setStringValue:@"true"];
        [self addChild:customMsgMark];
        
        NSXMLElement *multicast = [NSXMLElement elementWithName:@"addresses" xmlns:xmlns_multicast];
        [self addChild:multicast];
        
        for (id recipient in to_jids)
        {
            NSString *jidStr = nil;
            if ([recipient isKindOfClass:[XMPPJID class]])
                jidStr = [(XMPPJID *)recipient full];
            else if ([recipient isKindOfClass:[NSString class]])
                jidStr = (NSString *)recipient;
            
            if (jidStr)
            {
                NSXMLElement *address =  [NSXMLElement elementWithName:@"address"];
                [address addAttributeWithName:@"type" stringValue:@"to"];
                [address addAttributeWithName:@"jid" stringValue:jidStr];
                [multicast addChild:address];
            }
        }
        
        [data enumerateKeysAndObjectsUsingBlock:^(NSString* key, NSString* content, BOOL * _Nonnull stop) {
            NSXMLElement* customData = [NSXMLElement elementWithName:key];
            [customData setStringValue:content];
            [self addChild:customData];
        }];
        
    }
    return self;
}

- (id)initChatMessageWithBody:(NSString *)body toJids:(NSArray *)to_jids module:(NSString*)module
{
    if ((self = [super initWithName:@"message"]))
    {
        [self addAttributeWithName:@"type" stringValue:@"chat"];
        [self addAttributeWithName:@"to" stringValue:module];
        
        NSXMLElement *multicast = [NSXMLElement elementWithName:@"addresses" xmlns:xmlns_multicast];
        [self addChild:multicast];
        
        for (id recipient in to_jids)
        {
            NSString *jidStr = nil;
            if ([recipient isKindOfClass:[XMPPJID class]])
                jidStr = [(XMPPJID *)recipient full];
            else if ([recipient isKindOfClass:[NSString class]])
                jidStr = (NSString *)recipient;
            
            if (jidStr)
            {
                NSXMLElement *address =  [NSXMLElement elementWithName:@"address"];
                [address addAttributeWithName:@"type" stringValue:@"to"];
                [address addAttributeWithName:@"jid" stringValue:jidStr];
                [multicast addChild:address];
            }
        }
        
        NSXMLElement *body_child = [NSXMLElement elementWithName:@"body"];
        [body_child setStringValue:body];
        
        [self addChild:body_child];
    }
    return self;
}

- (id)initWithType:(NSString *)type jids:(NSArray *)jids module:(NSString *)module
{
    if ((self = [super initWithName:@"message"]))
    {
        if (type)
            [self addAttributeWithName:@"type" stringValue:type];
        
        if (module)
            [self addAttributeWithName:@"to" stringValue:module];
        
        
        NSXMLElement *multicast = [NSXMLElement elementWithName:@"addresses" xmlns:xmlns_multicast];
        [self addChild:multicast];
        
        for (id recipient in jids)
        {
            NSString *jidStr = nil;
            if ([recipient isKindOfClass:[XMPPJID class]])
                jidStr = [(XMPPJID *)recipient full];
            else if ([recipient isKindOfClass:[NSString class]])
                jidStr = (NSString *)recipient;
            
            if (jidStr)
            {
                NSXMLElement *address =  [NSXMLElement elementWithName:@"address"];
                [address addAttributeWithName:@"type" stringValue:@"to"];
                [address addAttributeWithName:@"jid" stringValue:jidStr];
                [multicast addChild:address];
            }
        }
    }
    return self;
}

- (BOOL)isMulticast
{
    return ([[self elementsForXmlns:xmlns_multicast] count] > 0);
}

- (NSArray *)jids
{
    NSMutableArray *jids = nil;
    
    NSXMLElement *multicast = [self elementForName:@"addresses"];
    if (multicast)
    {
        NSArray *addresses = [multicast elementsForName:@"address"];
        
        jids = [[NSMutableArray alloc] initWithCapacity:[addresses count]];
        for (NSXMLElement* address in  addresses)
        {
            NSString *jidStr = [[address attributeForName:@"jid"] stringValue];
            XMPPJID *jid = [XMPPJID jidWithString:jidStr];
            if (jid) {
                [jids addObject:jid];
            }
        }
    }
    
    return jids;
}

- (NSArray *)jidStrings
{
    NSMutableArray *jids = nil;
    
    NSXMLElement *multicast = [self elementForName:@"addresses"];
    if (multicast)
    {
        NSArray *addresses = [multicast elementsForName:@"address"];
        jids = [[NSMutableArray alloc] initWithCapacity:[addresses count]];
        
        for (NSXMLElement *address in addresses)
        {
            NSString *jid = [[address attributeForName:@"jid"] stringValue];
            if (jid) {
                [jids addObject:jid];
            }
        }
    }
    
    return jids;
}

@end
