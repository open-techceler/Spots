//
//  XMPPMessage+XEP_0033.h
//  Maply
//
//  Created by steven on 22/7/2016.
//  Copyright © 2016 admin. All rights reserved.
//

/**
 * This file is targeted for submission to the XMPPFramework project (open source, BSD license).
 **/

#import <Foundation/Foundation.h>
#import "XMPPMessage.h"


@interface XMPPMessage (XEP_0033)

+ (XMPPMessage *)multicastMessageWithType:(NSString *)type jids:(NSArray *)jids module:(NSString*)module;
+ (XMPPMessage *)multicastMessageWithBody:(NSString *)body to:(NSArray *)jids module:(NSString*)module;
+ (XMPPMessage *)multicaseFlashMessageWithData:(NSDictionary*)data to:(NSArray *)jids module:(NSString*)module;

- (BOOL)isMulticast;
- (NSArray *)jids;
- (NSArray *)jidStrings;

@end
