//
//  AddInfoCell.h
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddInfoCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *lblTitle;
@property(nonatomic,weak) IBOutlet UITextField *txtField;
-(void)setTitle:(NSString*)title value:(NSString*)value;
@end
