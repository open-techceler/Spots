//
//  LocationTrackEngine.m
//  Maply
//
//  Created by steven on 22/7/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "LocationTrackEngine.h"
#import "NotificationDefinitions.h"

@implementation LocationTrackEngine

+(LocationTrackEngine*)sharedInstance {
    static LocationTrackEngine* sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LocationTrackEngine alloc] init];
    });
    
    return sharedInstance;
}

-(instancetype)init {
    if (self = [super init]) {
        [StevenXMPPHelper sharedInstance].delegate = self;
        _track_interval_in_seconds = FOREGROUND_TRACK_INTERVAL;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUIApplicationBecomeActive) name:kUIApplicationDidBecomeActive object:nil];
        
        return self;
    }
    return nil;
}

-(void)didEnterBackground {
    if ([_timer isValid]) {
        [self stopLocationTrackEngine];
        _track_interval_in_seconds = BACKGROUND_TRACK_INTERVAL;
        [self startLocationTrackEngine];
    }
}

-(void)didUIApplicationBecomeActive {
    if ([_timer isValid]) {
        [self stopLocationTrackEngine];
        _track_interval_in_seconds = FOREGROUND_TRACK_INTERVAL;
        [self startLocationTrackEngine];
    }
}

-(void)sendMyLocation:(CLLocation*)location andMyUserId:(NSInteger)user_id to:(NSString*)jid {
    
    NSDictionary* data = @{@"latitude": [NSString stringWithFormat:@"%f", location.coordinate.latitude],
                           @"longitude":[NSString stringWithFormat:@"%f", location.coordinate.longitude],
                           @"userId":   [NSString stringWithFormat:@"%d", (int)user_id]};
    
    [[StevenXMPPHelper sharedInstance] sendFlashMessage:data to:jid completion:nil];
}

-(void)broadcastWithMyLocation:(CLLocation*)locationInfo andMyUserId:(NSInteger)userId toFriendIds:(NSArray*)friendIds {
    
    NSString* latitude = [NSString stringWithFormat:@"%f", locationInfo.coordinate.latitude];
    NSString* longitude = [NSString stringWithFormat:@"%f", locationInfo.coordinate.longitude];
    NSString* myId = [NSString stringWithFormat:@"%d", (int)userId];
    
    NSString* message = [NSString stringWithFormat:@"%@###%@###%@", latitude, longitude, myId];
    
    NSMutableArray* friendsJids = [[NSMutableArray alloc] init];
    for (NSString* friendId in friendIds) {
        [friendsJids addObject:[NSString stringWithFormat:@"%@@%@", friendId, XmppVHOST]];
    }
    
    [[StevenXMPPHelper sharedInstance] sendMultcastMesssage:message to:friendsJids completion:^(XMPPStream *stream, XMPPMessage *message) {
        
    }];

}

-(void)startLocationTrackEngine {
    [self timerFired];
    
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:_track_interval_in_seconds
                                                  target:self
                                                selector:@selector(timerFired)
                                                userInfo:nil
                                                 repeats:YES];
    }
}

-(void)timerFired {
    
    [[INTULocationManager sharedInstance] requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse timeout:3 delayUntilAuthorized:YES block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
       
        if (achievedAccuracy >= INTULocationAccuracyBlock) {
            if (self.currentLocation) {
                float deltaInLati = fabs(self.currentLocation.coordinate.latitude - currentLocation.coordinate.latitude);
                float deltaInLongi = fabs(self.currentLocation.coordinate.longitude - currentLocation.coordinate.longitude);
                if (deltaInLati > AllowDeltaInLatiLongi || deltaInLongi > AllowDeltaInLatiLongi)
                {
                    NSLog(@"latitude ===> %f, longitude ===> %f ", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
                    
                    self.currentLocation = currentLocation;
                    
                    //if the location is updated, then broadcast my location
                    if (self.delegate)
                        [self.delegate broadcastToFriendsWithMyLocation:currentLocation];
                    
                }
            }
            else {
                self.currentLocation = currentLocation;
                
                if ([LocationTrackEngine sharedInstance].delegate)
                    [[LocationTrackEngine sharedInstance].delegate broadcastToFriendsWithMyLocation:currentLocation];
            }
            //@added by steven
            
        }
        else {
            if (status == INTULocationStatusError) {
                [[NSNotificationCenter defaultCenter] postNotificationName:kLocationErrorNotification object:nil];
            }
        }
        
    }];
}

-(void)stopLocationTrackEngine {
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
}

-(CLLocation*)lastLocationWithFriendId:(NSInteger)friendId {
    NSArray* history = [[StevenXMPPHelper sharedInstance] loadArchivedMessagesFrom:[NSString stringWithFormat:@"%d@%@", (int)friendId, XmppVHOST] isOutgoing:false];
    
    if ([history count] == 0) {
        return nil;
    }
    
    NSArray *components = [(NSString*)[history lastObject] componentsSeparatedByString:@"###"];
    
    double latitude = [[components objectAtIndex:0] doubleValue];
    double longitude = [[components objectAtIndex:1] doubleValue];
    
    CLLocation* location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    return location;
}

#pragma mark - StevenXMPPHelperDeleagte
-(void)xmppHelper:(XMPPStream *)stream didReceiveFlashMessage:(NSDictionary *)data {
    double latitude = [(NSString*)[data objectForKey:@"latitude"] doubleValue];
    double longitude = [(NSString*)[data objectForKey:@"longitude"] doubleValue];
    NSInteger userId = [(NSString*)[data objectForKey:@"userId"] integerValue];
    
    if (_delegate) {
        [_delegate didReceiveUpdatedLocation:[[CLLocation alloc] initWithLatitude:latitude longitude:longitude] fromUserId:userId];
    }
}

-(void)xmppHelper:(XMPPStream *)stream didReceiveMessage:(NSString *)message {
    NSArray *components = [message componentsSeparatedByString:@"###"];
    
    double latitude = [[components objectAtIndex:0] doubleValue];
    double longitude = [[components objectAtIndex:1] doubleValue];
    NSString* userId = [components objectAtIndex:2];
    
    CLLocation* location = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
    
    if (_delegate)
        [_delegate didReceiveUpdatedLocation:location fromUserId:[userId integerValue]];
}

@end
