//
//  LocationClass.h
//  Pradip
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "INTULocationManager.h"

@protocol LocationClassDelegate <NSObject>

@optional
-(void) locationFound:(CLLocation*)location;
-(void) failedToFetchLocation:(NSString*)str_error;

@end

@interface LocationClass : NSObject <CLLocationManagerDelegate>

@property (nonatomic,assign) id<LocationClassDelegate> delegate;
@property (nonatomic, strong) CLLocation* currentLocation;
@property (nonatomic) double track_interval_in_seconds;

+(LocationClass*)sharedLoctionManager;
-(void)startFetchingLocation;
//-(void)stopFetchingLocation;
-(void)updateLocationToServer;
- (void)stopUpdateLocationTimer;
@end
