//
//  HomeViewViewController.m
//  Maply
//
//  Created by admin on 2/9/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "HomeViewViewController.h"
#import <MapKit/MapKit.h>
#import "CameraMainViewController.h"
#import "MessagesMainViewController.h"
#import "JourneysViewController.h"
#import "AddFriendsViewController.h"
#import "SelectLocationViewController.h"
#import "SendToViewController.h"
#import "FriendsMainViewController.h"
#import "LGSideMenuController.h"
#import "UserAPI.h"
#import "user.h"
#import "ZFModalTransitionAnimator.h"
#import "FBClusteringManager.h"
#import "FBAnnotation.h"
#import "ClusterAnnotationView.h"
#import "MFSideMenu.h"
#import "UserAPI.h"
#import "Friend.h"
#import "FriendsListWrapper.h"
#import "LocationClass.h"
#import "NotificationDefinitions.h"
#import "Reachability.h"
#import "DistortionViewController.h"
#import "HomeCallOutView.h"
#import "SMCalloutView.h"
#import "MyNavigationController.h"
#import "StevenXMPPHelper.h"
#import "LocationTrackEngine.h"
#import "LiveFeedVennerViewController.h"
#import "FriendPopupViewController.h"

#define LIVE 0 
#define JOURNEY 1
#define OfflineDurationTime @"offline_duration_time"
#define OfflineStartTimeStamp @"offline_start_time_stamp"
#define LocalNotificationID @"go_online_notification"

typedef void(^AlertActionBlock)(UIAlertAction * _Nonnull action, int hour);

@interface CustomMapView : MKMapView
@property (nonatomic, strong) SMCalloutView *mapCalloutView;
@end

@interface HomeViewViewController ()<FBClusteringManagerDelegate,LocationClassDelegate,UIGestureRecognizerDelegate,SMCalloutViewDelegate, LiveFeedVennerViewControllerDelegate, FriendPopupViewControllerDelegate> {
    UIViewController* currentPopupViewCon;
}

@property(nonatomic,strong)IBOutlet CustomMapView *mapView;
@property(nonatomic,weak)IBOutlet UIButton *btnMessageCount,*btnJourney;
@property(nonatomic,weak)IBOutlet UIButton *btnOnline,*btnLocation;

@property (weak, nonatomic) IBOutlet UILabel *lblOfflineTime;
@property (strong, nonatomic) NSTimer *offlineTimer;

@property(nonatomic,weak)IBOutlet UILabel *lblLive;
@property(nonatomic,strong)IBOutlet UIView *vwPopUp,*vwTop,*vwBottom;

@property(nonatomic,weak)IBOutlet UILabel *lblName,*lblAddress;
@property(nonatomic,weak)IBOutlet UILabel *lblHey,*lblMessage,*lblMeet;
@property (nonatomic, strong) ZFModalTransitionAnimator *animator;

@property (nonatomic, strong) FBClusteringManager *clusteringManager;
@property(nonatomic,strong) NSArray *arrayFriends,*arrayTotalFriends,*arrayMyJourney,*arrayEvents;
@property(nonatomic,strong) NSMutableArray *arrayList;
@property(nonatomic, weak) Friend *clickedUser;
@property(nonatomic, strong) NSString* clickedUserImgUrl;

@property(nonatomic) BOOL isLocationClicked;

@property(nonatomic,weak)IBOutlet UIView *vwNotification;
@property(nonatomic,weak)IBOutlet UIButton *btnMsgHey,*btnMsgMeet;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint *vwMsgPopUpHeight,*vwPopUpHeight;
@property (nonatomic, strong) SMCalloutView *calloutView;
@property(nonatomic,weak)IBOutlet UIButton *btnLiveTab,*btnJourneyTab;
@property(nonatomic) NSInteger selectedTab;

@property (nonatomic) BOOL bShowHaloAnimation;

@end

@implementation HomeViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    currentPopupViewCon = nil;
    
    [self initOfflineView];
    [[self.btnJourneyTab imageView] setContentMode: UIViewContentModeScaleAspectFit];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    self.vwOffline.connectivity_type=ConnectivityTypeHome;
    [self.vwOffline doUIForHome];
   
    [self setFontAndColor];
    [self initLoaderActivityWithFrame:CGRectMake((SCREEN_SIZE.width/2.0)-10,(SCREEN_SIZE.height/2.0)-(40+kStatusBarHeight), 20, 20)];

    if([Utility isIOS9])
        self.mapView.showsCompass = NO;
   // else
       // self.mapView.rotateEnabled = NO;
    
    [self addObservers];
   
    // create our custom callout view
    self.calloutView = [SMCalloutView platformCalloutView];
    self.calloutView.delegate = self;
    // tell our custom map view about the callout so it can send it touches
    self.mapView.mapCalloutView = self.calloutView;
    
    //LocationTrackEngine on XmppProtocol
    [LocationTrackEngine sharedInstance].delegate = self;
    
}

-(void)addObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityDidChange:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getReloadLiveFriends) name:kReloadLiveFriendsListNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popToMapView) name:kpopToMapViewNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getNearEventsAndMyJourney) name:kreloadMyJourneyTabNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkLocationService) name:kcheckLocationServiceNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshHome:) name:MFSideMenuStateNotificationEvent object:nil];
    
    UIPanGestureRecognizer* panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [self.mapView addGestureRecognizer:panRec];
    
    UITapGestureRecognizer* tapRec = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapMap:)];
    [tapRec setDelegate:self];
    [self.mapView addGestureRecognizer:tapRec];

    UISwipeGestureRecognizer* swipeRec = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSlideDownMap:)];
    swipeRec.direction = UISwipeGestureRecognizerDirectionDown;
    [self.mapView addGestureRecognizer:swipeRec];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshPage) name:kUIApplicationDidBecomeActive object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goOnlineFromNotification) name:kUIApplicationGoOnline object:nil];
}

-(void)checkLocationService
{
    if([CLLocationManager locationServicesEnabled] &&
      [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        [self hideLocationView];
    }
    else
    {
        [self showLocationView];
    }

}

- (void) removeCurrentPopupView {
    if (currentPopupViewCon != nil) {
        [currentPopupViewCon willMoveToParentViewController:nil];
        [currentPopupViewCon.view removeFromSuperview];
        [currentPopupViewCon removeFromParentViewController];
        
        currentPopupViewCon = nil;
    }
    
    self.selectedTab=LIVE;
    self.bShowHaloAnimation = NO;
    [self showLiveFriendsOnMap];
    
    UIAppDelegate.mainController.swipeEnabled=YES;
    self.menuContainerViewController.panMode =  MFSideMenuPanModeDefault;
}

- (void) didSlideDownMap:(UIGestureRecognizer *) gestureRecognizer {
    [self removeCurrentPopupView];
}

- (void)didTapMap:(UIGestureRecognizer*)gestureRecognizer {
    [self removePopUpWithAnimation:NO];

    CGPoint point = [gestureRecognizer locationInView:self.mapView];
    
    bool bTappedPin = false;
    
    for (id<MKAnnotation> annotation in self.mapView.annotations){
        MKAnnotationView* anView = [self.mapView viewForAnnotation: annotation];
        if ([anView isKindOfClass:[ClusterAnnotationView class]]) {
            CGRect rectPin = [self.mapView convertRect:anView.frame toView:self.mapView];
            if (CGRectContainsPoint(rectPin, point))
                bTappedPin = true;
        }
    }
    
    if (!bTappedPin)
        [self removeCurrentPopupView];
}

- (void)didDragMap:(UIGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        if(self.calloutView)
            [self.calloutView dismissCalloutAnimated:YES];

        [self resetMapTrackingMode];
    }
}

-(void)resetMapTrackingMode
{
    if(self.btnLocation.tag!=0)
    {
        self.btnLocation.tag=0;
        [self.btnLocation setImage:[UIImage imageNamed:@"navi"] forState:UIControlStateNormal];
        [self.mapView setUserTrackingMode:MKUserTrackingModeNone animated:NO];
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)reachabilityDidChange:(NSNotification*)notification
{
    Reachability* reachability = (Reachability*)[notification object];
    if ([reachability isReachable] == NO) {
        [self showOfflineView:YES error:nil];
    }
}

-(void)showGoOfflineView{
    if(self.vwOffline.hidden)
    {
        [self stopLoadingActivity];
        [self resetMapTrackingMode];
        self.btnLocation.enabled=NO;
        self.mapView.showsUserLocation=NO;
        [self showOfflineView:YES error:nil];
        self.arrayFriends=nil;
        self.arrayMyJourney=nil;
        self.arrayList=nil;
        [self updateLiveCount];
        [self removeAnnotations];
        
        [self.view bringSubviewToFront:self.vwTop];
        [self.view bringSubviewToFront:self.btnOnline];
        [self.view bringSubviewToFront:self.vwBottom];
        
        //https://trello.com/c/BkqDRI8P
        /*double delayInSeconds = 1.0f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            if(self.menuContainerViewController.menuState==MFSideMenuStateClosed)
            {
                [self.menuContainerViewController toggleRightSideMenuCompletion:^{
                }];
            }
            
        });*/
    }
}

#pragma mark - Offline Timer
int initDurationInSeconds;
int currentElapsedTimeInSeconds;
-(void)startOfflineTimerWithInitDurationInSeconds:(int)initSeconds andWithCurrentElapsedTimeInSeconds:(int)elapsedSeconds{
    self.lblOfflineTime.hidden = false;
    [self.view bringSubviewToFront:self.lblOfflineTime];
    
    int remainTime = initSeconds - elapsedSeconds;
    
    int hour = remainTime / 3600;
    int min = (remainTime - hour * 3600) / 60;
    int seconds = remainTime - hour * 3600 - min * 60;
    
    self.lblOfflineTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hour, min, seconds];
    
    if (!_offlineTimer) {
        initDurationInSeconds = initSeconds;
        currentElapsedTimeInSeconds = elapsedSeconds;
        _offlineTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateOfflineTimeLabel:) userInfo:nil repeats:YES];
    }
}

-(void)updateOfflineTimeLabel:(NSTimer*)timer {
    currentElapsedTimeInSeconds++;
    
    if (currentElapsedTimeInSeconds > initDurationInSeconds) {
        [self clickRetry:nil];
        [self stopOfflineTimer];
    }
    
    int remainTime = initDurationInSeconds - currentElapsedTimeInSeconds;
    
    int hour = remainTime / 3600;
    int min = (remainTime - hour * 3600) / 60;
    int seconds = remainTime - hour * 3600 - min * 60;
    
    self.lblOfflineTime.text = [NSString stringWithFormat:@"%02d:%02d:%02d", hour, min, seconds];
}
-(void)stopOfflineTimer {
    @synchronized(_offlineTimer) {
        if (_offlineTimer != nil && [_offlineTimer isValid]) {
            [_offlineTimer invalidate];
        } else {
            return;
        }
        _offlineTimer = nil;
        self.lblOfflineTime.hidden = true;
    }
}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        self.mapView.showsUserLocation=YES;

        if(self.btnOnline.selected == NO)
        {
            [self clickOnline:nil];
        }
        
    }
}

-(void)popToMapView
{
    if(UIAppDelegate.mainController.currentPageIndex!=kHomeViewViewController)
    {
        [UIAppDelegate.mainController popToMapView];
  
    //https://trello.com/c/n3qxu1kB
   /* if(self.menuContainerViewController.menuState!=MFSideMenuStateClosed)
    {
        self.menuContainerViewController.panMode =  MFSideMenuPanModeDefault;
        UINavigationController *navLeft=(UINavigationController *)self.menuContainerViewController.leftMenuViewController;
    
        UINavigationController *navRight=(UINavigationController *)self.menuContainerViewController.rightMenuViewController;
    
        [navLeft popToRootViewControllerAnimated:NO];
        [navRight popToRootViewControllerAnimated:NO];
        
        
        self.menuContainerViewController.menuSlideAnimationEnabled=NO;
        self.menuContainerViewController.menuSlideAnimationFactor=0;
        self.menuContainerViewController.menuAnimationDefaultDuration=0;
        
        __weak typeof(self) weakSelf = self;
        // dispatch_async(dispatch_get_main_queue(), ^{
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{
          //  weakSelf.menuContainerViewController.menuSlideAnimationEnabled=YES;
            weakSelf.menuContainerViewController.menuSlideAnimationFactor=3.0f;
            weakSelf.menuContainerViewController.menuAnimationDefaultDuration=0.2f;
        }];*/
      //  });

    }
    
    
}

-(void)refreshHome:(NSNotification *)noti
{
    NSDictionary *dict=noti.userInfo;
    MFSideMenuStateEvent eventtype=[[dict objectForKey:@"eventType"] intValue];
    if(eventtype==MFSideMenuStateEventMenuDidClose)
    {
        double delayInSeconds = 1.f;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self getLiveFriends:nil];
        });
    }
    else
    {
        [self resetMapTrackingMode];
    }
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    [self refreshPage];
}

-(void)goOnlineFromNotification {
    [VVBaseUserDefaults setIntForKey:OfflineDurationTime value:0];
    [self refreshPage];
}

-(void)refreshPage {
    User *obj=[User currentUser];
    if([obj.status isEqualToString:@"Live"])
    {
        self.mapView.showsUserLocation=YES;
        self.btnOnline.selected=YES;
        [self getLocationAndFindLives];
    }
    else
    {
        self.btnOnline.selected=NO;
        [self showGoOfflineView];
        int initDurationTime = (int)[VVBaseUserDefaults getIntForKey:OfflineDurationTime];
        int elapsedTime = (int)[[NSDate date] timeIntervalSince1970] - (int)[VVBaseUserDefaults getIntForKey:OfflineStartTimeStamp];
        if ([VVBaseUserDefaults getIntForKey:OfflineDurationTime] == 0 || initDurationTime < elapsedTime) {
            [self clickRetry:nil];
            [self stopOfflineTimer];
            [VVBaseUserDefaults setInt:0 forKey:OfflineDurationTime];
        }
        else {
            [self stopOfflineTimer];
            [self startOfflineTimerWithInitDurationInSeconds:initDurationTime andWithCurrentElapsedTimeInSeconds:elapsedTime];
        }
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self getNearEventsAndMyJourney];
    [self getReloadLiveFriends];
    [self getUnReadMessageCount];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removePopUpWithAnimation:NO];
}

-(void)setFontAndColor
{
    /*vwPopUP
     Avenir Medium  46 - name
     Avenir Light 42 - address
     Avenir Roman -hey-message-meet
     50pt
     #666666
     #888888
     */
    self.lblLive.font=FONT_AVENIR_PRO_DEMI_H2;
    
    self.lblName.font= FONT_AVENIR_MEDIUM_H3; //FONT_AVENIR_PRO_DEMI_H3;
    self.lblName.textColor=UICOLOR_NOTIFICATION_TITLE;
    
    self.lblAddress.font=FONT_AVENIR_LIGHT_H1;
    self.lblAddress.textColor=UICOLOR_LIGHT_TEXT;
    
    self.lblHey.font=FONT_AVENIR_ROMAN_H3;//FONT_AVENIR_PRO_REGULAR_H3;
    self.lblHey.textColor=UICOLOR_NOTIFICATION_TITLE;
    
    self.lblMessage.font=FONT_AVENIR_ROMAN_H5;
    self.lblMessage.textColor=UICOLOR_NOTIFICATION_TITLE;
    
    self.lblMeet.font=FONT_AVENIR_ROMAN_H5;
    self.lblMeet.textColor=UICOLOR_NOTIFICATION_TITLE;
    
    self.btnMessageCount.titleLabel.font=FONT_AVENIR_PRO_DEMI_H0;
    
    [self updateLiveCount];
    
    self.btnMsgHey.titleLabel.font=FONT_AVENIR_ROMAN_H1;
    self.btnMsgMeet.titleLabel.font=FONT_AVENIR_ROMAN_H1;
    
    self.btnMsgHey.layer.cornerRadius=4.0;
    self.btnMsgMeet.layer.cornerRadius=4.0;
    self.btnMsgHey.contentEdgeInsets=UIEdgeInsetsMake(5, 9, 5, 9);
    self.btnMsgMeet.contentEdgeInsets=UIEdgeInsetsMake(5, 9,5, 9);
    
}

-(void)updateLiveCount
{
    self.lblLive.attributedText=[self getAttributedString:self.arrayFriends.count];
}

-(NSMutableAttributedString *)getAttributedString:(NSInteger)count
{
    NSString *subString = [NSString stringWithFormat:@"%ld",(long)count];
    NSString *fullString =[NSString stringWithFormat:MPLocalizedString(@"live_xxx", nil),count];
    
    NSMutableAttributedString* att_string = [[NSMutableAttributedString alloc] initWithString:fullString];
    NSRange atRange = [fullString rangeOfString:subString];
    
    if (atRange.location != NSNotFound) {
        [att_string addAttribute:NSForegroundColorAttributeName value:[UIColor darkGrayColor] range:atRange];
        //[att_string addAttribute:NSFontAttributeName value:FONT_MEDIUM_H3 range:atRange];

    }
    return att_string;
}

-(void)getLocationAndFindLives
{
    [self startLoadingActivity];
    if([LocationClass sharedLoctionManager].currentLocation==nil){
        [[LocationClass sharedLoctionManager] setDelegate:self];
        [[LocationClass sharedLoctionManager] startFetchingLocation];
        
        [[LocationTrackEngine sharedInstance] startLocationTrackEngine];
    }
    else
    {
        [self getLiveFriends:nil];
   }
    
}

-(void) locationFound:(CLLocation*)location
{
    [[LocationClass sharedLoctionManager] setDelegate:nil];
    
    [self getNearEventsAndMyJourney];
}

-(void) failedToFetchLocation:(NSString*)str_error
{
    [[LocationClass sharedLoctionManager] setDelegate:nil];
   // [ToastMessage showErrorMessageAppTitleWithMessage:str_error];
     [self stopLoadingActivity];
     [self showLocationView];
    //set timer to update location after 5 min
    [[LocationClass sharedLoctionManager] updateLocationToServer];
}

- (void) dismissFeedVennerViewCon:(LiveFeedVennerViewController *)viewCon {
    [viewCon willMoveToParentViewController:nil];
    [viewCon.view removeFromSuperview];
    [viewCon removeFromParentViewController];
    
    self.selectedTab=LIVE;
    self.bShowHaloAnimation = NO;
    [self showLiveFriendsOnMap];
    
    UIAppDelegate.mainController.swipeEnabled=YES;
    self.menuContainerViewController.panMode =  MFSideMenuPanModeDefault;
    
    currentPopupViewCon = nil;
}

- (void) dismissFriendPopupViewCon:(FriendPopupViewController *)viewCon {
    [viewCon willMoveToParentViewController:nil];
    [viewCon.view removeFromSuperview];
    [viewCon removeFromParentViewController];
    
    self.selectedTab=LIVE;
    self.bShowHaloAnimation = NO;
    [self showLiveFriendsOnMap];
    
    UIAppDelegate.mainController.swipeEnabled=YES;
    self.menuContainerViewController.panMode =  MFSideMenuPanModeDefault;
    
    currentPopupViewCon = nil;
}

- (void) removeCurrentPopupViewCon {
    if (currentPopupViewCon) {
        [currentPopupViewCon willMoveToParentViewController:nil];
        [currentPopupViewCon.view removeFromSuperview];
        [currentPopupViewCon removeFromParentViewController];
    }
    
    currentPopupViewCon = nil;
}

-(IBAction)clickLiveTab:(id)sender
{
    [self removePopUpWithAnimation:NO];
    
    if (currentPopupViewCon != nil) {
        if ([currentPopupViewCon isKindOfClass:[LiveFeedVennerViewController class]]) {
            return;
        }
        
        [self removeCurrentPopupViewCon];
    }
    
    self.selectedTab=LIVE;

    self.bShowHaloAnimation = YES;
    [self showLiveFriendsOnMap];
    
    LiveFeedVennerViewController* viewCon = [[LiveFeedVennerViewController alloc] initWithNibName:@"LiveFeedVennerViewController" bundle:nil];
    viewCon.delegate = self;
    viewCon.arrayTotalFriends = self.arrayTotalFriends;
    
    [self addChildViewController:viewCon];
    [self.view addSubview:viewCon.view];
    [viewCon didMoveToParentViewController:self];
    
    currentPopupViewCon = viewCon;
    
    viewCon.view.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
    float fShowHeight = (CGRectGetWidth(self.view.bounds) - 12.f) / 77.f * 43.f + 12.f;
    [UIView animateWithDuration:0.4f animations:^(void) {
        viewCon.view.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) - fShowHeight, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
    }];
    
    UIAppDelegate.mainController.swipeEnabled=NO;
}


-(IBAction)clickMyJourneyTab:(id)sender
{
    UIAppDelegate.mainController.currentIndex=kFriendsMainViewController;
//    [self removePopUpWithAnimation:NO];
//    self.selectedTab=JOURNEY;
//    [self showMyJourneyOnMap];
}

-(void)getNearEventsAndMyJourney
{
    if([UIAppDelegate isInternetAvailable])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
            [[UserAPI new]getEventsAndMyJourneySuccess:^(FriendsListWrapper *objWrapper)
            {
                NSArray *arrMyJourney = [objWrapper.list filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type = %@",@"JOURNEY"]];
                self.arrayMyJourney=arrMyJourney;
                
                NSArray *arrEvents = [objWrapper.list filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"type = %@",@"EVENT"]];
                self.arrayEvents=arrEvents;
                
                [self showMyJourneyOnMap];
                [self showLiveFriendsOnMap];
            } failure:^(NSError *error) {
            }];
        });
    }
}

-(void)getReloadLiveFriends {
    [self getLiveFriends:nil];
}

-(void)getLiveFriends:(void (^)(NSArray *friendList, NSError* error))completion
{
    if (self.btnOnline.selected)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[UserAPI new] getFriends:0 keyWord:@"" success:^(FriendsListWrapper *objWrapper) {
                
                self.mapView.showsUserLocation = YES;

                //self.arrayTotalFriends = objWrapper.list;

                NSArray *liveFriends = [objWrapper.list filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(status_type = %@) AND (status = %@)", @"Live", @"Live"]];
                
                bool isRefreshFriendsLocation = NO;
                if (self.arrayFriends.count != liveFriends.count) {
                    isRefreshFriendsLocation = YES;
                }
                
                self.arrayFriends=liveFriends;//objWrapper.list;//
                self.arrayTotalFriends = liveFriends;
                
                //@updated by steven
                //update friend's location according xmppStorage in the arrayFriends
                for (int i=0; i< self.arrayFriends.count; i++) {
                    Friend *friend = [self.arrayFriends objectAtIndex:i];
                    CLLocation* lastLocation = [[LocationTrackEngine sharedInstance] lastLocationWithFriendId:friend.friend_id];
                    if (lastLocation == nil || lastLocation.coordinate.latitude == 0 || lastLocation.coordinate.longitude == 0) {
                        continue;
                    }
                    friend.latitude = lastLocation.coordinate.latitude;
                    friend.longitude = lastLocation.coordinate.longitude;
                }
                
                [self updateLiveCount];
                
                if (isRefreshFriendsLocation) {
                    [self showLastUpdatedLocationsForFriends];
                }
                //@updated by steven
                
                [self stopLoadingActivity];
                [self showOfflineView:NO error:nil];
                
                if (completion) {
                    completion(self.arrayFriends, nil);
                }
                
            } failure:^(NSError *error) {
                [self stopLoadingActivity];
                
                [self removeAnnotations];
                [self showOfflineView:YES error:nil];
                
                if (completion) {
                    completion(nil, error);
                }
            }];
        });
    }
    else {
        [self showGoOfflineView];
        [self stopLoadingActivity];
    }
}

-(void)showMyJourneyOnMap
{
    if(self.selectedTab==JOURNEY)
    {
        self.arrayList=[[NSMutableArray alloc]initWithArray:self.arrayMyJourney];
        //MPLog(@"myJourney count>>%lu",(unsigned long)self.arrayList.count);
        NSMutableArray *array = [self LocationsFromArray:self.arrayList];
        [self loadClusterdMap:array];
    }
}

//@called every time the getNearbyUsers is called to show live Friends on the map
//@to archive xmpp protocol, here, get rid of module that loadClusterMap
-(void)showLiveFriendsOnMap
{
    if(self.selectedTab==LIVE)
    {
        self.arrayList=[[NSMutableArray alloc]initWithArray:self.arrayFriends];
        [self.arrayList addObjectsFromArray:self.arrayEvents];
        MPLog(@"Live count, Event count>>%lu %lu",(unsigned long)self.arrayList.count,(long)self.arrayEvents.count);
        NSMutableArray *array = [self LocationsFromArray:self.arrayList];
        [self loadClusterdMap:array];
        [self updateLiveCount];
    }
   
}

-(void)getUnReadMessageCount
{
    /*if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }*/
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[UserAPI new]getMessagesCount:0 success:^(NSInteger msgCount) {
                [self updateTotalMessageCount:msgCount];
        
            } failure:^(NSError *error) {
        
            }];
        });
 
}

-(void)updateTotalMessageCount:(NSInteger)count
{
    
    if(count)
    {
        [self.btnMessageCount setTitle:[NSString stringWithFormat:@"%ld",(long)count] forState:UIControlStateNormal];
        [self.btnMessageCount setBackgroundImage:[UIImage imageNamed:@"badges"] forState:UIControlStateNormal];
    }
    else
    {
        [self.btnMessageCount setTitle:@"" forState:UIControlStateNormal];
        [self.btnMessageCount setBackgroundImage:[UIImage imageNamed:@"chk"] forState:UIControlStateNormal];
    }
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:count];
}

//from datasource arrayList, take the array of FBAnnotation to be displayed on the map
- (NSMutableArray *)LocationsFromArray:(NSArray *)list
{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < list.count; i++) {
        Friend *obj=[list objectAtIndex:i];
        FBAnnotation *a = [[FBAnnotation alloc] init];
        
        //@updated by steven
        CLLocation *location = [[LocationTrackEngine sharedInstance] lastLocationWithFriendId:(int)obj.friend_id];
        if (!obj.type) //Friend
        {
            if (location != nil) {
                a.coordinate = location.coordinate;
            }
            else {
                a.coordinate = CLLocationCoordinate2DMake(obj.latitude, obj.longitude);
            }
        }
        else {
            a.coordinate = CLLocationCoordinate2DMake(obj.latitude, obj.longitude);
        }
        
        //@updated by steven
        a.image=obj.image;
        a.index=i;
        a.badge_count=obj.badge_count;
        a.type=obj.type;
        if(location != nil || (obj.latitude != 0 && obj.longitude != 0)) {
            [array addObject:a];
        }
    }
    return array;
}

-(IBAction)clickCamera:(id)sender
{
    [self showCameraWithClickedUserid:0];
}

-(void)showCameraWithClickedUserid:(NSInteger)reply_userId
{
    [self resetMapTrackingMode];

    CameraMainViewController *cnt=[[CameraMainViewController alloc]initWithNibName:@"CameraMainViewController" bundle:nil];
    cnt.comefrom= (reply_userId ==0?CameraMainComeFromHome:CameraMainComeFromReplyUser);
    cnt.reply_userId=reply_userId;
    cnt.reply_userImgUrl = _clickedUser.image;
    MyNavigationController *modalVC=[[MyNavigationController alloc]initWithRootViewController:cnt];
    modalVC.navigationBarHidden=YES;
    
   /* modalVC.modalPresentationStyle = UIModalPresentationCustom;
    self.animator = [[ZFModalTransitionAnimator alloc] initWithModalViewController:modalVC];
    self.animator.dragable = NO;
    self.animator.bounces = NO;
    self.animator.behindViewAlpha = 0.5f;
    self.animator.behindViewScale = 0.5f;
    
    modalVC.transitioningDelegate = self.animator;*/
    [UIAppDelegate.window.rootViewController presentViewController:modalVC animated:NO completion:nil];
}


-(IBAction)clickMessageCount:(id)sender
{
    UIAppDelegate.mainController.currentIndex=kMessagesMainViewController;
//    [self updateTotalMessageCount:0];
   /* [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
        //[self updateTotalMessageCount:0];
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadMessageListNotification object:nil];
  
    }];*/
    //[kLGSideMenuController showLeftViewAnimated:YES completionHandler:nil];
}

-(IBAction)clickJourney:(id)sender
{
    UIAppDelegate.mainController.currentIndex=kJourneysViewController;

 /*  if(self.menuContainerViewController.menuState==MFSideMenuStateClosed)
   {
       [self.menuContainerViewController toggleRightSideMenuCompletion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:kReloadJourneyListNotification object:nil];

       }];
   }*/
    //[kLGSideMenuController showRightViewAnimated:YES completionHandler:nil];
}

-(IBAction)clickOnline:(id)sender
{
    [self removePopUpWithAnimation:NO];
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    self.btnOnline.selected=!self.btnOnline.selected;
    
    if (!self.btnOnline.selected) {
        //means go to offline
        NSString *message = MPLocalizedString(@"you_are_away_now",nil);
        
        AlertActionBlock block = ^(UIAlertAction * _Nonnull action, int hour){
            
            [[UserAPI new] updateUserStatusSuccess:[NSNumber numberWithInt:hour] success:^(BOOL status) {
                int interval = hour * 3600;
                NSDate *now = [NSDate date];
                [VVBaseUserDefaults setInt:interval forKey:OfflineDurationTime];
                [VVBaseUserDefaults setInt:((int)[now timeIntervalSince1970]) forKey:OfflineStartTimeStamp];
                [self startOfflineTimerWithInitDurationInSeconds:interval andWithCurrentElapsedTimeInSeconds:0];
                
                [self showGoOfflineView];
                
                [ToastMessage showSuccessMessage:MPLocalizedString(message, nil)];
            } failure:^(NSError *error) {
                [ToastMessage showErrorMessageOppsTitleWithMessage:[error description]];
                self.btnOnline.selected = TRUE;
            }];
        };
        
        //https://trello.com/c/m73M60pk/210-setup-a-pop-up-like-this-asking-for-how-long-you-want-to-be-offline-this-should-pop-up-when-you-tap-the-green-button-in-map-view
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Set the duration" message:nil preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *twoHoursAction = [UIAlertAction actionWithTitle:@"2 Hours" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(action, 2);
        }];
        UIAlertAction *eightHoursAction = [UIAlertAction actionWithTitle:@"8 Hours" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(action, 8);
        }];
        UIAlertAction *twentyFourHoursAction = [UIAlertAction actionWithTitle:@"24 Hours" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(action, 24);
        }];
        UIAlertAction *fortyEightHoursAction = [UIAlertAction actionWithTitle:@"48 Hours" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            block(action, 48);
        }];
        [alertController addAction:twoHoursAction];
        [alertController addAction:eightHoursAction];
        [alertController addAction:twentyFourHoursAction];
        [alertController addAction:fortyEightHoursAction];
        
        [self presentViewController:alertController animated:YES completion:nil];
    
    }
    else {
        //means go to online
        [self goOnline];
    }
}

-(void) goOnline {
    [[UserAPI new] updateUserStatusSuccess:[NSNumber numberWithInt:0] success:^(BOOL status) {
        NSString *message = MPLocalizedString(@"you_are_live_now",nil);
        [self getLiveFriends:^(NSArray *friendList, NSError *error) {
            [self showLastUpdatedLocationsForFriends];
        }];
        [self getUnReadMessageCount];
        self.btnLocation.enabled=YES;
        [VVBaseUserDefaults setInt:0 forKey:OfflineDurationTime];
        [self stopOfflineTimer];
        
        [ToastMessage showSuccessMessage:MPLocalizedString(message, nil)];
        
    } failure:^(NSError *error) {
        [ToastMessage showErrorMessageOppsTitleWithMessage:[error description]];
        self.btnOnline.selected = FALSE;
    }];
}

-(IBAction)clickLocation:(id)sender
{
    [self removePopUpWithAnimation:NO];
    if(self.mapView.showsUserLocation==NO)
        return;
    if(self.btnLocation.tag==0)
    {
        self.isLocationClicked=YES;
        self.btnLocation.tag=1;
        [self.btnLocation setImage:[UIImage imageNamed:@"navi2"] forState:UIControlStateNormal];
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];


       /* MKCoordinateRegion mapRegion;
        mapRegion.center = self.mapView.userLocation.coordinate;
        mapRegion.span.latitudeDelta = 0.02;
        mapRegion.span.longitudeDelta = 0.02;
        [self.mapView setRegion:mapRegion animated: YES];*/
        
      //  [self.mapView setUserTrackingMode:MKUserTrackingModeNone];
       
    }
    else if(self.btnLocation.tag==1)
    {
         self.btnLocation.tag=2;
        [self.btnLocation setImage:[UIImage imageNamed:@"navi1"] forState:UIControlStateNormal];
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading animated:YES];

    }
    else if(self.btnLocation.tag==2)
    {
          self.btnLocation.tag=0;
        [self.btnLocation setImage:[UIImage imageNamed:@"navi"] forState:UIControlStateNormal];
        [self.mapView setUserTrackingMode:MKUserTrackingModeNone animated:YES];

    }
    
   /* //https://trello.com/c/fUyTFq2c
    if(self.isLocationClicked)
    {
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollowWithHeading];
        self.isLocationClicked=NO;

    }
    else
    {

        // http://stackoverflow.com/questions/18936839/programatically-rotating-a-mkmapview-in-ios7
        if ([self.mapView respondsToSelector:@selector(camera)]) {
            // MKMapCamera *newCamera = [[self.mapView  camera] copy];
            //[newCamera setHeading:360.0]; // or newCamera.heading + 90.0 % 360.0
            //[self.mapView  setCamera:newCamera animated:YES];
        
            [[self.mapView  camera] setHeading:360.0];
        }
    
        MKCoordinateRegion mapRegion;
        mapRegion.center = self.mapView.userLocation.coordinate;
        mapRegion.span.latitudeDelta = 0.02;
        mapRegion.span.longitudeDelta = 0.02;
        [self.mapView setRegion:mapRegion animated: YES];
      
        [self.mapView setUserTrackingMode:MKUserTrackingModeNone];
        self.isLocationClicked=YES;


    }*/
    
}

-(void)rotateMapToNorthIfNeeded
{
    if(self.mapView.camera.heading!=360)
    {
        // http://stackoverflow.com/questions/18936839/programatically-rotating-a-mkmapview-in-ios7
        if ([self.mapView respondsToSelector:@selector(camera)])
        {
            MKMapCamera *newCamera = [[self.mapView  camera] copy];
            [newCamera setHeading:360.0]; // or newCamera.heading + 90.0 % 360.0
            [self.mapView  setCamera:newCamera animated:YES];
            
            //[[self.mapView  camera] setHeading:360.0];
        }
    }
    
    self.isLocationClicked=NO;
}


-(IBAction)clickHey:(id)sender
{
    [self sendMessageToFriend:@"said hey!"];
    [self removePopUpWithAnimation:YES];

}

-(IBAction)clickMessage:(id)sender
{
    [self showCameraWithClickedUserid:self.clickedUser.friend_id];
    [self removePopUpWithAnimation:YES];
}

-(IBAction)clickMeet:(id)sender
{
    [self sendMessageToFriend:@"wants to Meet up!"];
    [self removePopUpWithAnimation:YES];
}

-(void)sendMessageToFriend:(NSString *)msg
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    [[UserAPI new]replyToFriendWithImage:nil withFriendId:self.clickedUser.friend_id withVenue:nil withText:msg success:^(NSString *image_url) {
       /* [KSToastView ks_setAppearanceOffsetBottom:SCREEN_SIZE.height-120];
        [KSToastView ks_showToast:MPLocalizedString(@"message_send_successfully", nil) duration:1.0f completion:^{
        }];*/
        
      //  [ToastMessage showSuccessMessage:MPLocalizedString(@"message_send_successfully", nil)];

    } failure:^(NSError *error) {
        [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
    }];
}

-(void)removePopUpWithAnimation:(BOOL)animated
{
    self.clickedUser=nil;
    if(animated)
    {
      
    [UIView animateWithDuration:0.05
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.vwPopUp.backgroundColor=[UIColor clearColor];
                     }
                     completion:nil];
    
    CGRect optionsFrame = self.vwPopUp.frame;
    
    [UIView beginAnimations:nil context:nil];
    
    optionsFrame.origin.y += SCREEN_SIZE.height;
    self.vwPopUp.frame = optionsFrame;
    
    [UIView commitAnimations];
    
    [self.vwPopUp
        performSelector:@selector(removeFromSuperview)
        withObject:nil
        afterDelay:.5];
    }
    else
    {
        if(self.vwPopUp.superview)
            [self.vwPopUp removeFromSuperview];
    }
}

- (void) moveMapToSelectedLocation:(CLLocationCoordinate2D) coordinate {
    MKCoordinateRegion region;
    MKCoordinateSpan span;
    span.latitudeDelta = 16;
    span.longitudeDelta = 16;
    region.span = span;
    region.center = coordinate;
    
    [self.mapView setRegion:region animated:YES];
}

- (void)moveCenterByOffset:(CGPoint)offset from:(CLLocationCoordinate2D)coordinate
{
   // dispatch_async(dispatch_get_main_queue(), ^(void){

        /*CGPoint point = [self.mapView convertCoordinate:coordinate toPointToView:self.mapView];
        //point.x += offset.x;
        point.y += offset.y;
        CLLocationCoordinate2D center = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];*/
        
        MKCoordinateRegion region = self.mapView.region;
        region.center = coordinate;
        region.center.latitude -= 0.000622;
        region.span.latitudeDelta = 0.008;//0.029954 * 4;
        region.span.longitudeDelta = 0.006;//0.017767 * 4;
    
        [self.mapView setRegion:region animated:YES];
        
        //0.002092   //0.005423
        MPLog(@"origin.center-->%f %f",coordinate.latitude,coordinate.longitude);
        MPLog(@"Newcenter-->%f %f",region.center.latitude,region.center.longitude);
    
}

-(IBAction)clickUser:(NSInteger)index
{
    [self removePopUpWithAnimation:NO];
    Friend *obj=[self.arrayList objectAtIndex:index];
    self.vwPopUp.backgroundColor=[UIColor clearColor];
    
    //https://trello.com/c/V6xpNiqK
   /* MKCoordinateRegion region ;
    MPLog(@"self.mapView.region>> span-->lat-%f lng-%f",region.span.latitudeDelta,region.span.longitudeDelta);
    region.center = CLLocationCoordinate2DMake(obj.latitude, obj.longitude);
    region.span.latitudeDelta = 0.029954;
    region.span.longitudeDelta = 0.017767;
    [self.mapView setRegion:region animated:YES];*/
    
    [self moveCenterByOffset:CGPointMake(0, 50) from:CLLocationCoordinate2DMake(obj.latitude, obj.longitude)];

    dispatch_async(dispatch_get_main_queue(), ^(void){
        self.lblName.text=obj.name.uppercaseString;
        self.lblAddress.text= obj.full_address;
        self.clickedUser=obj;

        //obj.arrNotific=[NSMutableArray new];
        //[obj.arrNotific addObject:@"Martin said Hey!"];
        //[obj.arrNotific addObject:@"Martin want to meet up!"];
    if(obj.arrNotific.count)
    {
        self.vwNotification.hidden=NO;
        self.btnMsgHey.hidden=NO;
       
        NSString *msgHey=[obj.arrNotific firstObject];
        [self.btnMsgHey setTitle:msgHey.capitalizedString forState:UIControlStateNormal];
        if(obj.arrNotific.count>1)
        {
            self.btnMsgMeet.hidden=NO;
             NSString *msgMeet=[obj.arrNotific objectAtIndex:1] ;
            [self.btnMsgMeet setTitle:msgMeet.capitalizedString forState:UIControlStateNormal];
            self.vwMsgPopUpHeight.constant=60;
            self.vwPopUpHeight.constant=210;
        }
        else
        {
            self.btnMsgMeet.hidden=YES;
            self.vwMsgPopUpHeight.constant=35;
            self.vwPopUpHeight.constant=185;
            [self.btnMsgMeet setTitle:nil forState:UIControlStateNormal];
        }
        obj.arrNotific=nil;
    }
    else
    {
        self.vwNotification.hidden=YES;

        self.vwMsgPopUpHeight.constant=0;
        self.vwPopUpHeight.constant=150;
        self.btnMsgHey.hidden=YES;
        self.btnMsgMeet.hidden=YES;
        [self.btnMsgHey setTitle:nil forState:UIControlStateNormal];
        [self.btnMsgMeet setTitle:nil forState:UIControlStateNormal];
    }
    
 
    CGRect optionsFrame = self.vwPopUp.frame;
    optionsFrame.origin.x = 0;
    optionsFrame.size.width = SCREEN_SIZE.width;
    optionsFrame.size.height =self.vwPopUpHeight.constant;
    optionsFrame.origin.y =SCREEN_SIZE.height- self.vwPopUpHeight.constant;
  
    // For the animation, move the view up by its own height.
    
   optionsFrame.origin.y += optionsFrame.size.height;
    
    self.vwPopUp.frame = optionsFrame;
    [self.view addSubview: self.vwPopUp];
    
    [UIView beginAnimations:nil context:nil];
    
    optionsFrame.origin.y -= optionsFrame.size.height;
    self.vwPopUp.frame = optionsFrame;
    [UIView commitAnimations];
    
   [UIView animateWithDuration:0.5
                          delay:0.1
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
//                          self.vwPopUp.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.2];
                     }
                     completion:nil];
    
  });
    
}

-(void)removeAnnotations
{
    if (self.mapView.annotations.count > 0) {
        [self.mapView removeAnnotations:[self.mapView annotations]];
        self.clusteringManager=nil;
    }
}

-(void)loadClusterdMap:(NSMutableArray *)annotations
{
    [self removeAnnotations];
    
    self.clusteringManager = [[FBClusteringManager alloc] initWithAnnotations:annotations];
    self.clusteringManager.delegate = self;
   // self.mapView.centerCoordinate = CLLocationCoordinate2DMake(0, 0);
    [self mapView:self.mapView regionDidChangeAnimated:YES];
   // [self zoomMapTo5Km];
   
}

-(void)zoomMapTo5Km
{
    if(self.mapView.userLocation.coordinate.latitude>0&&self.mapView.userLocation.coordinate.longitude)
    {

    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
            // dispatch_async(dispatch_get_main_queue(), ^(void){
            // CLLocationCoordinate2D noLocation=CLLocationCoordinate2DMake(18.49967418668279, 73.86488839991787);
            CLLocationCoordinate2D noLocation=self.mapView.userLocation.coordinate;
            
           // NSLog(@"noLocation>>%f>> %f",noLocation.latitude,noLocation.longitude);
            
            MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(noLocation, 5000, 5000);
            MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
            [self.mapView setRegion:adjustedRegion animated:YES];
            // });

    });
    }

}

#pragma mark - MKMapViewDelegate
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation      {
    CLLocationAccuracy accuracy = userLocation.location.horizontalAccuracy;
    if (accuracy) {
        [self zoomMapTo5Km];
    }
}
- (void)mapViewDidFinishLoadingMap:(MKMapView *)mapView
{
  
}
- (void)mapViewDidFinishRenderingMap:(MKMapView *)mapView fullyRendered:(BOOL)fullyRendered
{
    //if(!_isCurrentRegionSet&&fullyRendered&&self.mapView.userLocation.coordinate.latitude>0)
   
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    [[NSOperationQueue new] addOperationWithBlock:^{
        double scale = self.mapView.bounds.size.width / self.mapView.visibleMapRect.size.width;
        NSArray *annotations = [self.clusteringManager clusteredAnnotationsWithinMapRect:mapView.visibleMapRect withZoomScale:scale];
        
        [self.clusteringManager displayAnnotations:annotations onMapView:mapView];
    }];
    
    if(self.isLocationClicked==YES && self.mapView.userTrackingMode==MKUserTrackingModeFollow)
    {
            [self rotateMapToNorthIfNeeded];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    MKAnnotationView *annotationView;
    
    static NSString *identifier = @"clusterAnnotation";
    
    ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];

    if (clusterAnnotationView) {
        clusterAnnotationView.annotation = annotation;
    } else {
        clusterAnnotationView = [[ClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
    }
    
    if ([annotation isKindOfClass:FBAnnotationCluster.class])
    {
        FBAnnotationCluster *cluster = (FBAnnotationCluster *)annotation;
        clusterAnnotationView.count = cluster.annotations.count;
        clusterAnnotationView.badge_count=0;
    }
    else{
        FBAnnotation * obj_MyLocation = annotation;
        clusterAnnotationView.type=obj_MyLocation.type;
        clusterAnnotationView.count =0;
        clusterAnnotationView.imgUrl=obj_MyLocation.image;
        clusterAnnotationView.badge_count=obj_MyLocation.badge_count;
        //clusterAnnotationView.imgUrl=@"https://graph.facebook.com/544996062350039/picture?type=large&return_ssl_resources=1";
    }
    
    [clusterAnnotationView showHaloAnimation:self.bShowHaloAnimation];
    
    annotationView = clusterAnnotationView;
    annotationView.canShowCallout = NO;
    return annotationView;
 

}

- (void)disclosureTapped:(id)sender {
    [self.calloutView dismissCalloutAnimated:YES];

    Friend *obj=[self.arrayList objectAtIndex:self.calloutView.tag];

    [self resetMapTrackingMode];
    if([obj.type isEqualToString:@"JOURNEY"])
    {
        [self clickJourney:nil];
    }
    else
    {
        DistortionViewController *cnt=[[DistortionViewController alloc]initWithNibName:@"DistortionViewController" bundle:nil];
        cnt.objFriend = obj;
        UINavigationController *modalVC=[[UINavigationController alloc]initWithRootViewController:cnt];
        [UIAppDelegate.window.rootViewController presentViewController:modalVC animated:NO completion:nil];
    }

}

- (BOOL)calloutViewShouldHighlight:(SMCalloutView *)calloutView
{
  
    return YES;
}

/// Called when the callout view is clicked. Not honored by @c SMClassicCalloutView.
- (void)calloutViewClicked:(SMCalloutView *)calloutView
{
    
}
- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    [self.calloutView dismissCalloutAnimated:YES];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [mapView deselectAnnotation:view.annotation animated:YES];
    
    if ([view.annotation isKindOfClass:[MKUserLocation class]]) {
        return;
    }
    
    if ([view.annotation isKindOfClass:[FBAnnotationCluster class]]) {
        /*  FBAnnotationCluster * obj_MyLocation = view.annotation;
       MKCoordinateRegion mapRegion;
        mapRegion.center = obj_MyLocation.coordinate;
        
        
      //  NSLog(@"mapView.region.span.latitudeDelta>>%f",mapView.region.span.latitudeDelta);
      //   NSLog(@"mapView.region.span.longitudeDelta>>%f",mapView.region.span.longitudeDelta);
        float latitudeDelta = mapView.region.span.latitudeDelta/2.0;
        float longitudeDelta = mapView.region.span.longitudeDelta/2.0;
        
        if(latitudeDelta>0.02)
            latitudeDelta=0.02;
        
        if(longitudeDelta>0.02)
            longitudeDelta=0.02;
        
        mapRegion.span.latitudeDelta = latitudeDelta;
        mapRegion.span.longitudeDelta = longitudeDelta;
        [self.mapView setRegion:mapRegion animated: YES];*/
        
        FBAnnotationCluster *clusterAnnotation = (FBAnnotationCluster *)view.annotation;
        MKMapRect mapRect = [self mapRect:clusterAnnotation];
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(100, 100, 100, 100);
        [mapView setVisibleMapRect:mapRect edgePadding:edgeInsets animated:YES];
        
    }
    else if ([view.annotation isKindOfClass:[FBAnnotation class]]) {
        FBAnnotation * obj_MyLocation = view.annotation;
        Friend *obj=[self.arrayList objectAtIndex:obj_MyLocation.index];

        //remove badge icon on read
        if(obj.badge_count>0)
        {
            ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)view;
            clusterAnnotationView.badge_count=0;
            obj_MyLocation.badge_count=0;
            obj.badge_count=0;
        }
        
        if([obj.type isEqualToString:@"EVENT"]||[obj.type isEqualToString:@"JOURNEY"])
        {
      
            self.calloutView.title = obj.name;
            self.calloutView.subtitle =obj.location;
         
            
            // Apply the MKAnnotationView's desired calloutOffset (from the top-middle of the view)
            self.calloutView.calloutOffset = view.calloutOffset;
            self.calloutView.tag=obj_MyLocation.index;
            // create a disclosure button for comparison
            UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeCustom];
            disclosure.frame=CGRectMake(0, 0, 30, 44);;
            [disclosure setTitle:@">" forState:UIControlStateNormal];
            [disclosure setTitleColor:UICOLOR_NAV_TITLE forState:UIControlStateNormal];
           
            [self.calloutView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disclosureTapped:)]];
            
            self.calloutView.userInteractionEnabled=YES;
            
           // [disclosure addGestureRecognizer:tap];
            self.calloutView.rightAccessoryView = disclosure;
            

            // iOS 7 only: Apply our view controller's edge insets to the allowable area in which the callout can be displayed.
            if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
                self.calloutView.constrainedInsets = UIEdgeInsetsMake(self.topLayoutGuide.length, 0, self.bottomLayoutGuide.length, 0);
            view.userInteractionEnabled=YES;
            // This does all the magic.
            [self.calloutView presentCalloutFromRect:view.bounds inView:view constrainedToView:self.view animated:YES];
           
        }
        else
        {
            Friend* selectFriend = self.arrayList[obj_MyLocation.index];
            NSInteger nSelectFriendIdx = [self.arrayFriends indexOfObject:selectFriend];
            if (nSelectFriendIdx >= 0) {
                UIAppDelegate.mainController.swipeEnabled=NO;

                if (currentPopupViewCon != nil) {
                    if ([currentPopupViewCon isKindOfClass:[FriendPopupViewController class]]) {
                        FriendPopupViewController* viewCon = (FriendPopupViewController *)currentPopupViewCon;
                        viewCon.nSelectedFriendIdx = nSelectFriendIdx;
                        [viewCon updateSelectedFriend];
                        
                        return;
                    }
                    
                    [self removeCurrentPopupViewCon];
                }

                [self selectUserOnMap:selectFriend];

                self.selectedTab=LIVE;
                self.bShowHaloAnimation = YES;
                [self showLiveFriendsOnMap];
                
                FriendPopupViewController* viewCon = [[FriendPopupViewController alloc] initWithNibName:@"FriendPopupViewController" bundle:nil];
                viewCon.delegate = self;
                viewCon.arrayTotalFriends = self.arrayFriends;
                viewCon.nSelectedFriendIdx = nSelectFriendIdx;
                
                [self addChildViewController:viewCon];
                [self.view addSubview:viewCon.view];
                [viewCon didMoveToParentViewController:self];
                
                currentPopupViewCon = viewCon;
                
                viewCon.view.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
                float fShowHeight = (CGRectGetWidth(self.view.bounds) - 12.f) / 77.f * 43.f + 12.f;
                [UIView animateWithDuration:0.4f animations:^(void) {
                    viewCon.view.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) - fShowHeight, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
                }];
            }

             //[self clickUser:obj_MyLocation.index];
        }
    }
    //NSLog(@"didSelectAnnotationView");
}

- (void) selectUserOnMap:(Friend *) obj {
    //[self moveMapToSelectedLocation:CLLocationCoordinate2DMake(obj.latitude, obj.longitude)];
    [self moveCenterByOffset:CGPointMake(0, 50) from:CLLocationCoordinate2DMake(obj.latitude, obj.longitude)];
    
    self.clickedUser = obj;
}

- (void) tappedLiveFriend:(LiveFeedVennerViewController *) viewCon selectedFriend:(Friend *) obj {
    [viewCon willMoveToParentViewController:nil];
    [viewCon.view removeFromSuperview];
    [viewCon removeFromParentViewController];

    NSInteger nSelectFriendIdx = [self.arrayFriends indexOfObject:obj];
    if (nSelectFriendIdx >= 0) {
        [self selectUserOnMap:obj];
        
        FriendPopupViewController* viewCon = [[FriendPopupViewController alloc] initWithNibName:@"FriendPopupViewController" bundle:nil];
        viewCon.delegate = self;
        viewCon.arrayTotalFriends = self.arrayFriends;
        viewCon.nSelectedFriendIdx = nSelectFriendIdx;
        
        currentPopupViewCon = viewCon;

        [self addChildViewController:viewCon];
        [self.view addSubview:viewCon.view];
        [viewCon didMoveToParentViewController:self];
        
        viewCon.view.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds), CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
        float fShowHeight = (CGRectGetWidth(self.view.bounds) - 12.f) / 77.f * 43.f + 12.f;
        [UIView animateWithDuration:0.4f animations:^(void) {
            viewCon.view.frame = CGRectMake(0, CGRectGetHeight(self.view.bounds) - fShowHeight, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds));
        }];
        
        UIAppDelegate.mainController.swipeEnabled=NO;
    }
}

- (void) sendHeyToFriend:(FriendPopupViewController *)viewCon withFriendInfo:(Friend *)friendObj {
    [self dismissFriendPopupViewCon:viewCon];
    
    [self selectUserOnMap:friendObj];
    
    [self sendMessageToFriend:@"said hey!"];
}

- (void) sendMessageToFriend:(FriendPopupViewController *)viewCon withFriendInfo:(Friend *)friendObj {
    [self dismissFriendPopupViewCon:viewCon];
    
    [self selectUserOnMap:friendObj];
    
    [self showCameraWithClickedUserid:self.clickedUser.friend_id];
}

- (void) sendMeetUpToFriend:(FriendPopupViewController *)viewCon withFriendInfo:(Friend *)friendObj {
    [self dismissFriendPopupViewCon:viewCon];
    
    [self selectUserOnMap:friendObj];
    
    [self sendMessageToFriend:@"wants to Meet up!"];
}

- (void) selectFriendUser:(FriendPopupViewController *)viewCon withFriendInfo:(Friend *)friendObj {
    [self moveCenterByOffset:CGPointMake(0, 50) from:CLLocationCoordinate2DMake(friendObj.latitude, friendObj.longitude)];
}

- (MKMapRect)mapRect:(FBAnnotationCluster * )clusterAnnotation
{
    MKMapPoint clusterPoint = MKMapPointForCoordinate(clusterAnnotation.coordinate);
    MKMapRect mapRect = MKMapRectMake(clusterPoint.x, clusterPoint.y, 0.1, 0.1);
    for (id<MKAnnotation> annotation in clusterAnnotation.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        mapRect = MKMapRectUnion(mapRect, pointRect);
    }
    
    return mapRect;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    //DDAnnotation *annotationView = (DDAnnotation*)view.annotation;

}
#pragma mark - FBClusterManager delegate - optional

- (CGFloat)cellSizeFactorForCoordinator:(FBClusteringManager *)coordinator
{
    return 1.5;
}

-(void)handlePushNotification:(NSDictionary *)userInfo
{
   //[ToastMessage showErrorMessageAppTitleWithMessage:userInfo.description];
    NSDictionary *dict=userInfo[@"aps"];
    NSInteger unread=[dict[@"unread"] integerValue];
   // NSInteger badge=[dict[@"badge"]integerValue];
   // [ToastMessage showErrorMessageAppTitleWithMessage:[NSString stringWithFormat:@"badge>>>%ld",(long)badge]];
    
   // NSInteger existing_count=self.btnMessageCount.titleLabel.text.integerValue;
    [self updateTotalMessageCount:unread];
    [self updateBadgeForUser:dict];
}

-(void)updateBadgeForUser:(NSDictionary*)dict
{
    NSInteger user_id=[dict[@"user_id"] integerValue];
    //filter friend from list
    NSArray *resultArray = [self.arrayList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"friend_id = %ld",user_id]];
    
    if(resultArray.count)
    {
        Friend *objFriend=[resultArray firstObject];
        objFriend.badge_count=objFriend.badge_count+1;
        if(objFriend.arrNotific==nil)
            objFriend.arrNotific=[NSMutableArray new];
        
        [objFriend.arrNotific addObject:[dict objectForKey:@"alert"]];
        NSMutableArray *array = [self LocationsFromArray:self.arrayList];
        [self loadClusterdMap:array];
    }
}

#pragma mark - Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[LocationClass sharedLoctionManager] setDelegate:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

#pragma mark - LocationTrackEngine Delegate

//update the friend location with the lasted received updated location,
-(void)showLastUpdatedLocationsForFriends {
    @synchronized (self.arrayList) {
        
        self.arrayList=[[NSMutableArray alloc]initWithArray:self.arrayFriends];
        [self.arrayList addObjectsFromArray:self.arrayEvents];
        
        //locations from an array
        if (self.arrayList == nil) {
            return;
        }
        
        NSMutableArray *array = [self LocationsFromArray:self.arrayList];
        [self loadClusterdMap:array];
    }
}

-(void)didReceiveUpdatedLocation:(CLLocation *)location fromUserId:(NSInteger)user_id {
    
    if (!self.btnOnline.selected) {
        return;
    }
    
    @synchronized (self.arrayList) {
        //locations from an array
        if (self.arrayList == nil) {
            return;
        }
        
        //get last updated location of friend
        CLLocation* lastLocation = [[LocationTrackEngine sharedInstance] lastLocationWithFriendId:user_id];
        //update friendList&arrayList array
        for (int i=0; i< self.arrayFriends.count; i++) {
            Friend *friend = [self.arrayFriends objectAtIndex:i];
            if (friend.friend_id == user_id) {
                friend.latitude = lastLocation.coordinate.latitude;
                friend.longitude = lastLocation.coordinate.longitude;
                break;
            }
        }
        
        self.arrayList=[[NSMutableArray alloc]initWithArray:self.arrayFriends];
        [self.arrayList addObjectsFromArray:self.arrayEvents];
        
        NSMutableArray *array = [self LocationsFromArray:self.arrayList];
        [self loadClusterdMap:array];
    }
}

-(void)broadcastToFriendsWithMyLocation:(CLLocation*)location {
    
    if (!self.btnOnline.selected) {
        return;
    }
    
    NSMutableArray* friendList;
    @synchronized (self.arrayFriends) {
        if ([self.arrayFriends count] == 0)
            return;
        
        friendList = [[NSMutableArray alloc] initWithArray:self.arrayFriends];
    }
    
//    NSLog(@"gps location => %@", location.description);
//    NSLog(@"mapView location => %@", self.mapView.userLocation.location);
    
    NSMutableArray* friendIds = [[NSMutableArray alloc] init];
    for (Friend* obj in friendList) {
        [friendIds addObject: [NSString stringWithFormat:@"%d", (int)obj.friend_id]];
    }
    
    [[LocationTrackEngine sharedInstance] broadcastWithMyLocation:location andMyUserId:[User currentUser].user_id toFriendIds:friendIds];
}

@end

#pragma mark - CustomMapView

@interface MKMapView (UIGestureRecognizer)

// this tells the compiler that MKMapView actually implements this method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;

@end

@implementation CustomMapView

// override UIGestureRecognizer's delegate method so we can prevent MKMapView's recognizer from firing
// when we interact with UIControl subclasses inside our callout view.
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIControl class]])
        return NO;
    else
        return [super gestureRecognizer:gestureRecognizer shouldReceiveTouch:touch];
}

// Allow touches to be sent to our calloutview.
// See this for some discussion of why we need to override this: https://github.com/nfarina/calloutview/pull/9
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    UIView *calloutMaybe = [self.mapCalloutView hitTest:[self.mapCalloutView convertPoint:point fromView:self] withEvent:event];
    if (calloutMaybe)
    {
        return self.mapCalloutView;
    }
   
    return [super hitTest:point withEvent:event];
}
@end
