//
//  Friend.m
//  Maply
//
//  Created by admin on 2/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "Friend.h"

@implementation Friend
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // override this in the model if property names in JSON don't match model
    NSDictionary *mapping = [NSDictionary mtl_identityPropertyMapWithModel:self];
    return [mapping mtl_dictionaryByAddingEntriesFromDictionary:
  @{@"friend_id":@"id",
    @"event_description":@"description",
    }];
}
@end
