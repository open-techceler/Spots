//
//  FriendsMainViewController.h
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MyNavigationViewController.h"

@interface FriendsMainViewController : MyNavigationViewController
-(void)refreshFriendsList;
@end
