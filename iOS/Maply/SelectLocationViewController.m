//
//  SelectLocationViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SelectLocationViewController.h"
#import "LocationCell.h"
#import "Foursquare2.h"
#import <CoreLocation/CoreLocation.h>
#import "FSConverter.h"
#import "FSVenue.h"
#import "LocationClass.h"
#import "Utility.h"

@interface SelectLocationViewController ()<LocationClassDelegate>
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,weak)IBOutlet UIView *vwSearch;
@property(nonatomic,weak)IBOutlet UISearchBar *searchBar;
@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UIView *vwContainer;
@property (strong, nonatomic) NSMutableArray *nearbyVenues;
@property (nonatomic, weak) NSOperation *lastSearchOperation;
@property(nonatomic,strong)NSString *myLocationName,*streetName;
@property(nonatomic,strong)IBOutlet MKMapView *mapView;
@property(nonatomic)BOOL isSearching;
@end

@implementation SelectLocationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setFontAndColor];
    [self setUpNavigationBar];
    [self initLoaderActivityWithFrame:CGRectMake((SCREEN_SIZE.width/2.0)-10,(SCREEN_SIZE.height/2.0)-40, 20, 20)];
    
    [self initOfflineViewWithFrame:CGRectMake(0,44, SCREEN_SIZE.width-20, SCREEN_SIZE.height-64)];
    [self.vwOffline removeFromSuperview];
    [self.vwContainer addSubview:self.vwOffline];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    
    self.searchBar.barTintColor = [UIColor clearColor];
    self.searchBar.backgroundImage = [UIImage new];
    self.tableView.tableHeaderView=self.vwSearch;
    
    [self startLoadingActivity];
    
    self.mapView.showsUserLocation=YES;
    self.tableView.hidden=YES;
    self.mapView.hidden=YES;
    [self getLocation];
    
    
  
}



-(void)getLocation
{
    if([LocationClass sharedLoctionManager].currentLocation==nil){
        
        [[LocationClass sharedLoctionManager] setDelegate:self];
        // Force location manager to start finding current location
        [[LocationClass sharedLoctionManager] startFetchingLocation];
    }
    else
    {
        [self getAddressFromLatLon];
        [self getVenuesForLocation:nil];
        [self zoomMapTo5Km];
    }
    
}

-(void) locationFound:(CLLocation*)location
{
    [[LocationClass sharedLoctionManager] setDelegate:nil];
    [self getLocation];
}
-(void) failedToFetchLocation:(NSString*)str_error
{
    [[LocationClass sharedLoctionManager] setDelegate:nil];
    [ToastMessage showErrorMessageAppTitleWithMessage:str_error];
    
}
-(void)setUpNavigationBar
{
    self.lblTitle.text=MPLocalizedString(@"where_are_you", nil);
    self.lblTitle.font=FONT_AVENIR_MEDIUM_H4;
    self.lblTitle.textColor=UICOLOR_LOCATION_TITLE;
}

-(void)setFontAndColor
{
    self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:.5];
    //self.tableView.backgroundColor=UICOLOR_APP_BG;
    self.searchBar.placeholder=MPLocalizedString(@"find_or_create_location", nil);
    
    self.vwContainer.layer.masksToBounds=YES;
    self.vwContainer.layer.cornerRadius=4.0;
}

-(IBAction)clickBack:(id)sender
{
   
    /*NSString *customLocation=self.searchBar.text;
    if(![Utility isEmpty:customLocation])
    {
        FSVenue *obj=[FSVenue new];
        obj.venueId=[NSString stringWithFormat:@"%@_%@",customLocation,[VVBaseUserDefaults userId]];
        obj.name=customLocation;
        [self.delegate hideExactLocation:obj];
        [self dismissViewControllerAnimated:YES completion:nil];
    }*/

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)getVenuesForLocation:(NSString *)string {
    
    if(![Utility isEmpty:string])
        _isSearching=YES;
    else
        _isSearching=NO;
    
    double lat=[LocationClass sharedLoctionManager].currentLocation.coordinate.latitude;
    double lng=[LocationClass sharedLoctionManager].currentLocation.coordinate.longitude;
    
    [self.lastSearchOperation cancel];
    self.lastSearchOperation=[Foursquare2 venueSearchNearByLatitude:@(lat)
                                 longitude:@(lng)
                                     query:string
                                     limit:nil
                                    intent:intentCheckin
                                    radius:@(500)
                                categoryId:nil
                                  callback:^(BOOL success, id result){
                                       MPLog(@"dic>>%@",result);
                                      if (success) {
                                          NSDictionary *dic = result;
                                          MPLog(@"dic>>%@",dic);
                                          NSArray *venues = [dic valueForKeyPath:@"response.venues"];
                                          FSConverter *converter = [[FSConverter alloc]init];
                                          NSArray *allVenues = [converter convertToObjects:venues];
                                          
                                          //sort by popularity
                                          NSSortDescriptor *sortDisc = [NSSortDescriptor sortDescriptorWithKey:@"checkinsCount" ascending:NO];
                                         NSArray *arrayPopular =[allVenues sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDisc]];
                                          
                                          //get top 10 popular records
                                          NSInteger top_records=(arrayPopular.count>=10?10:arrayPopular.count);
                                          NSArray *arratTop10 = [arrayPopular subarrayWithRange:NSMakeRange(0, top_records)];

                                           //sort by distance
                                          NSSortDescriptor *sortDist = [NSSortDescriptor sortDescriptorWithKey:@"location.distance" ascending:YES];
                                          NSArray *arrayDistance =[allVenues sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDist]];
                                          
                                          //https://trello.com/c/fSqLRqau
                                          //show top 10 records by popularity and remaining by distance
                                          self.nearbyVenues=[[NSMutableArray alloc]initWithArray:arratTop10];
                                          for (FSVenue *item in arrayDistance){
                                              if ([arratTop10 containsObject:item])
                                              {
                                                  MPLog(@"venue exitst in Top 10");
                                              }
                                              else
                                                  [ self.nearbyVenues addObject:item];
                                          }
                                          
                                          [self.tableView reloadData];
                                          
                                          [self stopLoadingActivity];
                                          self.tableView.hidden=NO;
                                          self.mapView.hidden=NO;
                                          
                                      }
                                      else
                                      {
                                          [self stopLoadingActivity];
                                          [self showOfflineView:YES error:nil];
                                      }
                                  }];
}
-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        self.tableView.hidden=YES;
        self.mapView.hidden=YES;
        self.searchBar.text=@"";
        [self.searchBar resignFirstResponder];
        [self startLoadingActivity];
        [self getLocation];
        [self getAddressFromLatLon];
    }
}
- (MKAnnotationView *) mapView: (MKMapView *) mapView viewForAnnotation:(id<MKAnnotation>) annotation
{
    if (annotation != mapView.userLocation)
    {
        return nil;
    }
    else
    {
        MKAnnotationView *pinView = nil;
        static NSString *defaultPinID = @"com.cabwala.pin";
        pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if (pinView == nil )
        {
            pinView = [[MKAnnotationView alloc]
                       initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        }
       
        pinView.image = [UIImage imageNamed:@"fill_small"];
        
        return pinView;
    }
}
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{

}

-(void)zoomMapTo5Km
{


    CLLocationCoordinate2D currentLocation=[LocationClass sharedLoctionManager].currentLocation.coordinate;
    if(currentLocation.latitude>0&&currentLocation.longitude>0)
    {
      //  CLLocationCoordinate2D noLocation=self.mapView.userLocation.coordinate;

            MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(currentLocation, 3000, 3000);
            MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:viewRegion];
            [self.mapView setRegion:adjustedRegion animated:NO];
    }
    
}

#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==1)
        return 32;
    
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vw =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 30)];
    vw.backgroundColor=[UIColor whiteColor];
    
    UILabel *lbl =[[UILabel alloc] initWithFrame:CGRectMake(16, 10, SCREEN_SIZE.width-16, 20)];
    lbl.textColor=UICOLOR_LIGHT_TEXT;
    lbl.font=FONT_HELVETICA_REGULAR_H3;//FONT_AVENIR_PRO_DEMI_H1;
    lbl.textAlignment=NSTextAlignmentLeft;
    lbl.text=MPLocalizedString(@"all_nearby_locations", nil);
    [vw addSubview:lbl];
    
    return vw;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
    {
        if (_isSearching) {
             return 3;
        }
        else
             return 2;
    }
    
    return self.nearbyVenues.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* MyIdentifier = @"LocationCell";
    LocationCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"LocationCell" owner:self options:nil] objectAtIndex:0];
    }
    
    if(indexPath.section==0)
    {
    if(indexPath.row==0)
    {
        cell.lblTitle.text = self.streetName;
        cell.lblAddress.text=[NSString stringWithFormat:@"%@ - current location",self.myLocationName];
        cell.imgUser.image=[UIImage imageNamed:@"fill"];
        cell.imgUser.contentMode=UIViewContentModeScaleAspectFit;
        cell.lblKm.hidden=YES;
    }
    else if(indexPath.row==1)
    {
        cell.lblTitle.text = self.myLocationName;
        cell.lblAddress.text=@"Neighborhood - Hides exact location";
        cell.imgUser.image=[UIImage imageNamed:@"skyblue-2"];
        cell.lblKm.hidden=YES;
    }
    else
    {
            cell.lblTitle.text = [NSString stringWithFormat:@"Create Location \"%@\"",_searchBar.text];
            cell.lblAddress.text=[NSString stringWithFormat:@"%@ - current location",self.myLocationName];
            cell.imgUser.image=nil;;
            cell.imgUser.backgroundColor=[PICKER_COLOR_PINK colorWithAlphaComponent:0.5];
            cell.imgUser.contentMode=UIViewContentModeScaleAspectFit;
            cell.lblKm.hidden=YES;
    }
    }
    else{
        cell.lblKm.hidden=NO;
        FSVenue *venue = self.nearbyVenues[indexPath.row];
        cell.lblTitle.text = venue.name.capitalizedString;
        cell.lblAddress.text=venue.location.formattedAddress;
        cell.lblKm.text=[NSString stringWithFormat:@"%@m",
                         venue.location.distance];
        MPLog(@"venue.icon>>%@",venue.icon);
        [cell.imgUser getImageWithURL:venue.icon comefrom:ImageViewComfromUser];

    }
    
    

   /* if (venue.location.address) {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@m, %@",
                                     venue.location.distance,
                                     venue.location.address];
    } else {
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@m",
                                     venue.location.distance];
    }*/

    
   // [cell.imgUser getImageWithURL:@"https://graph.facebook.com/478446222347605/picture?type=large" comefrom:ImageViewComfromUser];

    return cell;
    
}

-(void)getAddressFromLatLon
{
    [Utility getAddressFromLatLngSuccess:^(CLPlacemark *placemark) {
        
        MPLog(@"placemark.name %@",placemark.name);
        MPLog(@"placemark.ISOcountryCode %@",placemark.ISOcountryCode);
        MPLog(@"locality %@",placemark.locality);
        MPLog(@"postalCode %@",placemark.postalCode);
        MPLog(@"country %@",placemark.country);
        
        NSString *address= [Utility getCityCountryFromPlacemark:placemark];
        self.myLocationName=address;
        self.streetName=placemark.thoroughfare;
        [self.tableView reloadData];
    }];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section==0)
    {
    if(indexPath.row==0)
    {
        if(![Utility isEmpty:self.streetName])
        {
            FSVenue *obj=[FSVenue new];
            obj.venueId=[NSString stringWithFormat:@"%@_%@",self.streetName,[VVBaseUserDefaults userId]];
            obj.name=self.streetName;
             if(![Utility isEmpty:self.myLocationName])
             {
                 obj.location=[FSLocation new];
                 obj.location.address=self.myLocationName;
             }
        
            [self.delegate hideExactLocation:obj];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
            return;
    }
    else if(indexPath.row==1)
    {
        if(![Utility isEmpty:self.myLocationName])
        {
            FSVenue *obj=[FSVenue new];
            obj.venueId=[NSString stringWithFormat:@"%@_%@",self.myLocationName,[VVBaseUserDefaults userId]];
            obj.name=self.myLocationName;
            
            // [self.delegate didSelectLocation:obj];
            [self.delegate hideExactLocation:obj];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        return;
    }
    else if(indexPath.row==2)
    {
        if(![Utility isEmpty:self.searchBar.text])
        {
            FSVenue *obj=[FSVenue new];
            obj.venueId=[NSString stringWithFormat:@"%@_%@",self.searchBar.text,[VVBaseUserDefaults userId]];
            obj.name=self.searchBar.text;
            if(![Utility isEmpty:self.myLocationName])
            {
                obj.location=[FSLocation new];
                obj.location.address=self.myLocationName;
            }
            // [self.delegate didSelectLocation:obj];
            [self.delegate hideExactLocation:obj];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        return;
    }
    }
    else
    {
        [self.delegate didSelectLocation:[self.nearbyVenues objectAtIndex:indexPath.row]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }

    
}
#pragma mark -- Load More methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.searchBar isFirstResponder])
        [self.searchBar resignFirstResponder];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self getVenuesForLocation:searchText];

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
