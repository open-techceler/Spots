//
//  FriendsListWrapper.m
//  Maply
//
//  Created by admin on 2/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "FriendsListWrapper.h"
#import "Friend.h"

@implementation FriendsListWrapper
+ (NSValueTransformer*)listJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Friend class]];
}

+ (NSValueTransformer*)live_requestJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Friend class]];
}

+ (NSValueTransformer*)friend_reqeustJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Friend class]];
}

+ (NSValueTransformer*)top_friendsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Friend class]];
}

+ (NSValueTransformer*)friendsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Friend class]];
}

@end
