//
//  LiveFeedCustomView.m
//  Maply
//
//  Created by Admin on 28/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "LiveFeedCustomView.h"
#import "Friend.h"
#import "BaseImageViewWithData.h"

@implementation LiveFeedCustomView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) layoutSubviews {
    [super layoutSubviews];

    for (int nIdx = 0; nIdx < 4; nIdx++) {
        UIView* viewCanvas = (UIView *)[self viewWithTag:10 + nIdx];
        viewCanvas.hidden = true;
        
        [viewCanvas setNeedsLayout];
        [viewCanvas layoutIfNeeded];
        
        BaseImageViewWithData* imgAvatar = (BaseImageViewWithData *)[self viewWithTag:20 + nIdx];
        imgAvatar.userInteractionEnabled = YES;
        
        imgAvatar.backgroundColor=UICOLOR_APP_BG;
        imgAvatar.layer.borderColor=[UIColor whiteColor].CGColor;
        imgAvatar.layer.borderWidth=2.0;
        imgAvatar.layer.cornerRadius=CGRectGetHeight(imgAvatar.bounds) / 2.f;
        imgAvatar.clipsToBounds = YES;

    }
    
    if (arraySelectedFriends.count == 0)
        return;
    
    NSLog(@"view width - %f", self.frame.size.width);
    NSLog(@"view height - %f", self.frame.size.height);
  
    for (int nIdx = 0; nIdx < arraySelectedFriends.count; nIdx++) {
        Friend *obj=[arraySelectedFriends objectAtIndex:nIdx];
        
        UIView* viewCanvas = (UIView *)[self viewWithTag:10 + nIdx];
        viewCanvas.hidden = false;
        NSLog(@"sub view - %f", viewCanvas.frame.size.width);
        
        BaseImageViewWithData* imgAvatar = (BaseImageViewWithData *)[self viewWithTag:20 + nIdx];
        imgAvatar.userInteractionEnabled = YES;
        /*
        imgAvatar.backgroundColor=UICOLOR_APP_BG;
        imgAvatar.layer.borderColor=[UIColor whiteColor].CGColor;
        imgAvatar.layer.borderWidth=2.0;
//        imgAvatar.layer.cornerRadius=(viewCanvas.frame.size.height / 8.f * 5.f - 10.f) / 2.f;
        imgAvatar.layer.cornerRadius=(viewCanvas.frame.size.height / 8.f * 5.f) / 2.f;
        imgAvatar.clipsToBounds = YES;
        */
        
        [self addTapGesture:imgAvatar];
        
        NSLog(@"image view - %f", imgAvatar.frame.size.width);

        UILabel* lblName = (UILabel *)[self viewWithTag:30 + nIdx];
        UIImageView* imgOnlineStatus = (UIImageView *)[self viewWithTag:40 + nIdx];
        
        [imgAvatar getImageWithURL:obj.image comefrom:ImageViewComfromUser];
        
        NSArray* arrayNames = [obj.name componentsSeparatedByString:@" "];
        lblName.text = arrayNames[0];
        //lblName.text = obj.username;
        
        if ([obj.status_type compare:@"Live"] == NSOrderedSame)
            imgOnlineStatus.hidden = false;
        else
            imgOnlineStatus.hidden = true;
    }
}

- (void) loadUsers:(NSArray *) arrayFriends {
    arraySelectedFriends = [arrayFriends mutableCopy];
}

- (void) addTapGesture: (BaseImageViewWithData *) imageView {
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedImageView:)];
    [imageView addGestureRecognizer:tapGesture];
}

- (void) tappedImageView:(UITapGestureRecognizer *) gesture {
    BaseImageViewWithData* imageView = (BaseImageViewWithData *)gesture.view;
    NSInteger nTag = imageView.tag - 20;
 
    Friend *obj=[arraySelectedFriends objectAtIndex:nTag];

    if ([obj.status_type compare:@"Live"] == NSOrderedSame)
    {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(tappedLiveFriend:)])
            [self.delegate tappedLiveFriend:obj];
    }
}

@end
