//
//  SendToViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SendToViewController.h"
#import "SendToCell.h"
#import "AddFriendsViewController.h"
#import "UserAPI.h"
#import "Friend.h"
#import "FriendsListWrapper.h"
#import "FriendsManager.h"
#import "User.h"

@interface SendToViewController ()
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,weak)IBOutlet UIView *vwSearch,*vwSend;
@property(nonatomic,weak)IBOutlet UISearchBar *searchBar;
@property(nonatomic,weak)IBOutlet UILabel *lblSendTo;
@property (nonatomic,strong) NSMutableArray  *lsSections;
@property (nonatomic,strong) FriendsListWrapper  *objWrapper;
@property (nonatomic,strong) NSMutableArray  *selectedIds;
@property(nonatomic)BOOL isMyJourney;
@property(nonatomic)NSInteger uploadedMsgId;
@property BOOL isSearching;
@property (nonatomic,strong) NSArray  *searchArray;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint *vwSendHieght;

@end

@implementation SendToViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //self.navigationItem.hidesBackButton=YES;
    [self setFontAndColor];
    [self setUpNavigationBar];
    [self initLoaderActivity];
    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    
    self.selectedIds=[NSMutableArray new];
    self.searchBar.barTintColor = [UIColor clearColor];
    self.searchBar.backgroundImage = [UIImage new];
    self.tableView.hidden=YES;
    
    double delayInSeconds = 0.2f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.tableView.tableHeaderView=self.vwSearch;
        _lsSections =[Utility getSectionTitleArray];
        [self getFriendList];
    });
   
    
    if((self.camera_mode==CameraModeVideo || self.camera_mode==CameraModeGIF) && _recordSession)
    {
        SCAssetExportSession *assetExportSession = [[SCAssetExportSession alloc] initWithAsset:_recordSession.assetRepresentingSegments];
        assetExportSession.outputUrl = _recordSession.outputUrl;
        assetExportSession.outputFileType = AVFileTypeMPEG4;
        assetExportSession.videoConfiguration.filter = self.selectedFilter;
        assetExportSession.videoConfiguration.preset = SCPresetHighestQuality;
        assetExportSession.audioConfiguration.preset = SCPresetLowQuality;
        
        //render text & drawing on video
        assetExportSession.videoConfiguration.watermarkImage=self.finalSnap;
        assetExportSession.videoConfiguration.watermarkFrame=[UIScreen mainScreen].bounds;
      
       
        [assetExportSession exportAsynchronouslyWithCompletionHandler: ^{
            if (assetExportSession.error == nil) {
                 self.videoUrl=_recordSession.outputUrl;
                MPLog(@"_recordSession>>%@",_recordSession.outputUrl);
                // We have our video and/or audio file
            } else {
                // Something bad happened
            }
        }];
      /*  [_recordSession mergeSegmentsUsingPreset:AVAssetExportPresetLowQuality completionHandler:^(NSURL *url, NSError *error) {
        if (error == nil) {
            self.videoUrl=url;
            MPLog(@"url>>%@",self.videoUrl);
            _bannerImg=[Utility thumbnailImageFromURL:self.videoUrl];;
            
        } else {
            NSLog(@"Bad things happened: %@", error);
        }
    }];*/
    }


    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
}

-(void)getFriendList
{
    [self startLoadingActivity];
    
    [[FriendsManager sharedInstance]getSendToFriendsList:^(FriendsListWrapper *objWrapper) {
        self.objWrapper=objWrapper;

        [self sortRecordAscending];
        [self.tableView reloadData];
        [self stopLoadingActivity];
        self.tableView.hidden=NO;
    } failure:^(NSError *error) {
        [self stopLoadingActivity];
        self.tableView.hidden=YES;
        [self showOfflineView:YES error:error];
    }];
   /* [[UserAPI new]getSendToFriendSuccess:^(FriendsListWrapper *objWrapper) {
        self.objWrapper=objWrapper;
        [self sortRecordAscending];
        [self.tableView reloadData];
        [self stopLoadingActivity];
           self.tableView.hidden=NO;
    } failure:^(NSError *error) {
          [self stopLoadingActivity];
           self.tableView.hidden=YES;
          [self showOfflineView:YES error:error];
    }];*/
}

-(void)sortRecordAscending
{
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"username" ascending:YES];
    
    self.objWrapper.top_friends=[self.objWrapper.top_friends sortedArrayUsingDescriptors:@[sort]];
    self.objWrapper.friends=[self.objWrapper.friends sortedArrayUsingDescriptors:@[sort]];

}

-(void)searchFriend:(NSString *)keyword
{
    MPLog(@"keyword>>%@",keyword);
    if(keyword.length)
    {
        self.isSearching=YES;

        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", keyword];
        
        NSMutableArray *allFriends=[NSMutableArray new];
        [allFriends addObjectsFromArray:self.objWrapper.top_friends];
        [allFriends addObjectsFromArray:self.objWrapper.friends];
        
       self.searchArray = [allFriends filteredArrayUsingPredicate:predicate];
    
        [self.tableView reloadData];
    }
    else
    {
        self.isSearching=NO;
        [self.tableView reloadData];
    }
}

-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"send_to", nil);
    [self.Nav setBackButtonAsLeftButton];
     [self.Nav.btnLeft addTarget:self action:@selector(clickBack:)forControlEvents:UIControlEventTouchUpInside];
    
    [self.Nav setRightButtonWithImageName:@"addf" withTitle:nil font:nil];
    [self.Nav.btnRight addTarget:self action:@selector(clickAdd:)forControlEvents:UIControlEventTouchUpInside];

    [self.Nav setNavigationLayout:NO];
    
}
-(IBAction)clickSendTo:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    [self uploadSnapToServer];
}

-(void)uploadSnapToServer
{
    if(self.uploadedMsgId==0)
    {
//        [self showLoader]; //commented by steven
        
        /*if(self.camera_mode==CameraModeGIF)
        {
            self.videoUrl=self.gifUrl;
             _bannerImg=[Utility thumbnailImageFromURL:self.videoUrl];;
        }*/
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[UserAPI new]uploadSnapToServer:self.finalSnap
                              withBannerImag:self.bannerImg
                                    withText:self.imageText
                                withVideoUrl:self.videoUrl
                                 isMyJourney:self.isMyJourney
                                   withVenue:self.objVenue
                                  cameraMode:self.camera_mode
                                     success:^(NSInteger uploaded_id) {
                                         
                                         if(self.selectedIds.count)
                                         {
                                             //         [self showLoader]; //updated by steven
                                             
                                             dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                                 [[UserAPI new]sendMessageToFriends:self.selectedIds withMsgId:uploaded_id success:nil failure:^(NSError *error) {
                                                     [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
                                                 }];
                                             });
                                         }
                                         
                                     } failure:^(NSError *error) {
                                         [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
//                                         [self stopLoader];
                                     }];
        });
        
        [self dismissViewControllerAnimated:NO completion:^{
            [self removeAllRecordedVideos];
        }];
    }
    else
       [self sendMessageToFriends];
}

-(void)sendMessageToFriends
{
    if(self.selectedIds.count)
    {
//         [self showLoader]; //updated by steven
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [[UserAPI new]sendMessageToFriends:self.selectedIds withMsgId:self.uploadedMsgId success:nil failure:^(NSError *error) {
                [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
                [self stopLoader];
            }];
        });
        
        [self dismissViewControllerAnimated:NO completion:^{
            [self removeAllRecordedVideos];
        }];
    }
    else
    {
        [self dismissViewControllerAnimated:NO completion:^{
            [self removeAllRecordedVideos];
        }];
    }

}

-(void)removeAllRecordedVideos
{
    //remove all videos from disk
    if(_recordSession)
        [_recordSession removeAllSegments];
}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        
        self.tableView.hidden=YES;
        [self startLoadingActivity];
        [self getFriendList];
        
    }
}

-(void)showLoader
{
    [self startLoadingActivity];
    self.tableView.hidden=YES;
    self.vwSend.hidden=YES;
}

-(void)stopLoader
{
    [self stopLoadingActivity];
    self.tableView.hidden=NO;
    self.vwSend.hidden=NO;
}

-(IBAction)clickAdd:(id)sender
{
    AddFriendsViewController  *cnt=[[AddFriendsViewController alloc]initWithNibName:@"AddFriendsViewController" bundle:nil];
    [self.navigationController pushViewController:cnt animated:YES];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG;
    self.vwSend.backgroundColor=[UIColor colorWithRed:254/255.0f green:77/255.0f blue:93/255.0f alpha:1.0f];
    
    self.searchBar.placeholder=MPLocalizedString(@"search", nil);
    self.lblSendTo.font=FONT_AVENIR_MEDIUM_H6;//FONT_AVENIR_PRO_DEMI_H3;
    [self updateSendToLabel];
    
    UITextField *searchField = [self.searchBar valueForKey:@"_searchField"];
    [searchField setFont:FONT_AVENIR_MEDIUM_H3];
}

-(void)updateSendToLabel
{
    if(self.selectedIds.count || self.isMyJourney)
    {
        self.vwSendHieght.constant=44;
        if(self.selectedIds.count)
            self.lblSendTo.text=[NSString stringWithFormat:MPLocalizedString(@"send_to_xxx_friends", nil),self.selectedIds.count];
        else
             self.lblSendTo.text=MPLocalizedString(@"send_to_my_journey", nil);

    }
    else
    {
        self.vwSendHieght.constant=0;
    }
}

-(IBAction)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(self.isSearching)
        return 1;
    return 3;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==1 && self.objWrapper.top_friends.count==0)
        return 0;
    else if(section==2 && self.objWrapper.friends.count==0)
        return 0;
    
    return 32;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vw =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 30)];
    vw.backgroundColor=[UIColor whiteColor];
    
    UILabel *lbl =[[UILabel alloc] initWithFrame:CGRectMake(16, 10, SCREEN_SIZE.width-16, 20)];
    lbl.textColor=UICOLOR_LIGHT_TEXT;
    lbl.font=FONT_HEADER_TITLE;//FONT_AVENIR_PRO_DEMI_H1;
    lbl.textAlignment=NSTextAlignmentLeft;
    
    NSString *title=nil;
    if(self.isSearching){
        
    }
    else
    {
        if(section==0)
            title= MPLocalizedString(@"my_journey", nil);
        else if(section==1)
            title= [NSString stringWithFormat:MPLocalizedString(@"top_xxx", nil),self.objWrapper.top_friends.count];
        else
            title=MPLocalizedString(@"friends", nil);
    }

    lbl.text=title;
    [vw addSubview:lbl];
    
    return vw;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.isSearching)
        return self.searchArray.count;
    
    
    if(section==0)
        return 1;
    else if(section==1)
        return self.objWrapper.top_friends.count;
    else if(section==2)
           return self.objWrapper.friends.count;
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString* MyIdentifier = @"SendToCell";
    SendToCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SendToCell" owner:self options:nil] objectAtIndex:0];
    }
    
    if(self.isSearching)
    {
        Friend *obj=[self.searchArray objectAtIndex:indexPath.row];
        
        cell.lblName.text=obj.name.capitalizedString;
        [cell.imgUser getImageWithURL:obj.image comefrom:ImageViewComfromUser];
         cell.btnSelect.selected=[self.selectedIds containsObject:[NSString stringWithFormat:@"%ld",(long)obj.friend_id]];
    }
    else
    {
        if(indexPath.section==0)
        {
            cell.lblName.text=MPLocalizedString(@"my_journey", nil);
            
            User *objUser=[User currentUser];
            [cell.imgUser getImageWithURL:objUser.image comefrom:ImageViewComfromUser];
            cell.btnSelect.selected=self.isMyJourney;
        }
        else{
            NSArray *array=nil;
            if(indexPath.section==1)
                array=self.objWrapper.top_friends;
            else if(indexPath.section==2)
                array=self.objWrapper.friends;
            
            Friend *obj=[array objectAtIndex:indexPath.row];
            
            cell.lblName.text=obj.name.capitalizedString;
            [cell.imgUser getImageWithURL:obj.image comefrom:ImageViewComfromUser];
            cell.btnSelect.selected=[self.selectedIds containsObject:[NSString stringWithFormat:@"%ld",(long)obj.friend_id]];
          
        }

    }
    if(cell.btnSelect.selected)
        cell.contentView.backgroundColor=UICOLOR_SELECTED_CELL;
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SendToCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.btnSelect.selected=!cell.btnSelect.selected;
  
    if(self.isSearching)
    {
        Friend *obj=[self.searchArray objectAtIndex:indexPath.row];
        
        if(cell.btnSelect.selected)
            [self.selectedIds addObject:[NSString stringWithFormat:@"%ld",(long)obj.friend_id]];
        else
            [self.selectedIds removeObject:[NSString stringWithFormat:@"%ld",(long)obj.friend_id]];
    }
    else
    {
    
        if(indexPath.section==0)
        {
            if(self.objVenue==nil)
                [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"please_add_location", nil)];
            else
                self.isMyJourney=cell.btnSelect.selected;
        }
        else if(indexPath.section==1)
        {
            Friend *obj=[self.objWrapper.top_friends objectAtIndex:indexPath.row];
            if(cell.btnSelect.selected)
                [self.selectedIds addObject:[NSString stringWithFormat:@"%ld",(long)obj.friend_id]];
            else
                [self.selectedIds removeObject:[NSString stringWithFormat:@"%ld",(long)obj.friend_id]];
        }
        else if(indexPath.section==2)
        {
            Friend *obj=[self.objWrapper.friends objectAtIndex:indexPath.row];
            if(cell.btnSelect.selected)
                [self.selectedIds addObject:[NSString stringWithFormat:@"%ld",(long)obj.friend_id]];
            else
                [self.selectedIds removeObject:[NSString stringWithFormat:@"%ld",(long)obj.friend_id]];
     }
  }
    
   [self updateSendToLabel];
   [self.tableView reloadData];

    
}
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return _lsSections;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    NSInteger sectionVal=0;
    
    for (NSInteger i=0; i<self.lsSections.count; i++) {
        NSString *sectiontitle=[self.lsSections objectAtIndex:i];
        if([sectiontitle hasPrefix:title])
        {
            sectionVal=i;
            break;
        }
    }
    
    NSString *temtitle =@"";
    NSArray *dataArray=nil;
    
    //VVLog(@"title:%@",title);
    
    if(self.objWrapper.friends.count!=0)
        dataArray = self.objWrapper.friends;
    
    for (NSInteger i = 0; i< [dataArray count]; i++)
    {
        
        //---Here you return the name i.e. Honda,Mazda and match the title for first letter of name and move to that row corresponding to that indexpath as below---//
        
        Friend *userObj  = (Friend*)[dataArray objectAtIndex:i];
        if(userObj.name.length>0)
        {
            temtitle =[userObj.name substringToIndex:1];
            if ([temtitle isEqualToString:title]) {
                [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:2] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                break;
            }
        }
    }
    
    return -1;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
  
    [self searchFriend:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
#pragma mark -- Load More methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.searchBar isFirstResponder])
        [self.searchBar resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
