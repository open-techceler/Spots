//
//  BlockedCell.h
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@interface BlockedCell : UITableViewCell
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgUser;
@property(nonatomic,weak)IBOutlet UILabel *lblFullName,*lblUserName;
@property(nonatomic,weak)IBOutlet UIButton *btnBlock;
@property(nonatomic,weak)IBOutlet NSLayoutConstraint *topContraint;

@end
