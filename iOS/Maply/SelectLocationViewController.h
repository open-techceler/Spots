//
//  SelectLocationViewController.h
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FSVenue.h"
@protocol SelectLocationDelegate

-(void) hideExactLocation:(FSVenue *)objVenue;
-(void) didSelectLocation:(FSVenue *)objVenue;
@end

@interface SelectLocationViewController : MyNavigationViewController
@property(nonatomic,weak)    id<SelectLocationDelegate>delegate;

@end
