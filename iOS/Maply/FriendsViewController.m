//
//  FriendsViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "FriendsViewController.h"
#import "BlockedCell.h"
#import "RequestLiveCell.h"
#import "UserAPI.h"
#import "FriendsListWrapper.h"
#import "Friend.h"
#import "NoInternetView.h"
#import "ActionSheetStringPicker.h"

#define LIVE @"Live"
#define FRIENDS @"Friends"

#define DISABLE_LIVE_TAG 1000
#define SELECT_HOURS_TAG 1001

@interface FriendsViewController ()<UIActionSheetDelegate>
@property(nonatomic)NSInteger next_offset;
@property(nonatomic,strong)NSMutableDictionary *dictData;
@property(nonatomic)BOOL server_request_inprogress;

@property (nonatomic,strong)UIActivityIndicatorView *activityLoadMore;

@property BOOL isSearching;

@property(nonatomic,strong)IBOutlet UIView *vwSearch;
@property(nonatomic,weak)IBOutlet UISearchBar *searchBar;
@property (nonatomic,strong) NSMutableArray  *lsSections;
@property (nonatomic) NSInteger  selectedRow;
@property (nonatomic,strong) Friend  *selectedFriend;

@end

@implementation FriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.searchBar.barTintColor = [UIColor clearColor];
    self.searchBar.backgroundImage = [UIImage new];
    
    self.tableView.tableHeaderView=self.vwSearch;

    self.arrayActions=[NSMutableArray new];

    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    [self initLoaderActivityWithFrame: CGRectMake((SCREEN_SIZE.width/2.0)-40,(SCREEN_SIZE.height/2.0)-(40+kNavBarHeight+kScopeBarHeight+kStatusBarHeight), 80, 80)];
    
    [self initEmptyDataLabelWithFrame: CGRectMake(40,NAVIGATION_HEIGHT + 30.f,SCREEN_SIZE.width-80.f,SCREEN_SIZE.height-2*(NAVIGATION_HEIGHT+120.f))];
    //[self initEmptyDataLabelWithFrame:CGRectMake(0,(SCREEN_SIZE.height/2.0)-(NAVIGATION_HEIGHT+kScopeBarHeight),SCREEN_SIZE.width,30)];
    
    
    self.dictData=[NSMutableDictionary new];
    _lsSections =[Utility getSectionTitleArray];

    [self setFontAndColor];
    
    [self startLoadingActivity];
    self.tableView.hidden=YES;
//    [self getFriendsList];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG;
   // self.tableView.sectionIndexColor=UICOLOR_LIGHT_TEXT;
    self.searchBar.placeholder=MPLocalizedString(@"search", nil);
    
    UITextField *searchField = [self.searchBar valueForKey:@"_searchField"];
    [searchField setFont:FONT_AVENIR_MEDIUM_H3];
}

-(void)getFriendsList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

    [[UserAPI new]getFriends:self.next_offset  keyWord:self.searchBar.text success:^(FriendsListWrapper *objWrapper) {
        
        if(self.next_offset==0)
            [self.dictData removeAllObjects];
        
        self.next_offset=objWrapper.next_offset;
        
        
        NSMutableArray *arrayLive=[self.dictData objectForKey:LIVE];
        if(arrayLive==nil)
            arrayLive=[NSMutableArray new];
        
        NSArray *filteredLived = [objWrapper.list filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"status_type = %@",LIVE]];
        if(filteredLived.count){
            [arrayLive addObjectsFromArray:filteredLived];
            [self.dictData setObject:arrayLive forKey:LIVE];
        }
        
        NSMutableArray *arrayFriends=[self.dictData objectForKey:FRIENDS];
        if(arrayFriends==nil)
            arrayFriends=[NSMutableArray new];

        NSArray *filteredFriends =objWrapper.list
        ;//[objWrapper.list filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"status_type != %@",LIVE]];
        if(filteredFriends.count){
            [arrayFriends addObjectsFromArray:filteredFriends];
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
            NSArray * sortedArray=[arrayFriends sortedArrayUsingDescriptors:@[sort]];
            
            //https://trello.com/c/JeWKB2Hf
            NSMutableArray *arrSorted = [[NSMutableArray alloc]initWithArray:sortedArray];
            [self.dictData setObject:arrSorted forKey:FRIENDS];
        }
        
        [self.tableView reloadData];
        
        self.tableView.hidden=NO;
        self.server_request_inprogress=NO;
        [self setActivityInTableFooter:NO];
        [self stopLoadingActivity];
        if(self.dictData.count==0)
        {
           // self.tableView.hidden=YES;
            //[self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_friends_found", nil)];
            [self showEmptyDataImageViewWithImage:[UIImage imageNamed:@"icon_empty_friends"]];
        }

        
    } failure:^(NSError *error) {
         self.server_request_inprogress=NO;
        [self setActivityInTableFooter:NO];
        [self stopLoadingActivity];
        [self showOfflineView:YES error:error];


    }];
});
}

-(void)reloadFriendList
{
    if(self.dictData.count)
    {
        //reload in background
        self.searchBar.text=nil;
        self.next_offset=0;
        [self getFriendsList];
    }
    else
    {
        self.next_offset=0;
        self.tableView.hidden=YES;
        [self startLoadingActivity];
        [self getFriendsList];
    }
  
}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        [self reloadFriendList];
    }
}

#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.dictData.allKeys.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 32;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vw =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 30)];
    vw.backgroundColor=[UIColor whiteColor];
    
    UILabel *lbl =[[UILabel alloc] initWithFrame:CGRectMake(16, 10, SCREEN_SIZE.width-16, 20)];
    lbl.textColor=UICOLOR_LIGHT_TEXT;
    lbl.font=FONT_HEADER_TITLE;//FONT_AVENIR_PRO_DEMI_H1;
    lbl.textAlignment=NSTextAlignmentLeft;
    NSString *sectionKey= [self.dictData.allKeys objectAtIndex:section];
    lbl.text=([sectionKey isEqualToString:LIVE]?MPLocalizedString(@"live_connected", nil):MPLocalizedString(@"friends", nil));
    [vw addSubview:lbl];
    
    return vw;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    NSArray *arr=[self.dictData objectForKey:[self.dictData.allKeys objectAtIndex:section]];
    return arr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionKey= [self.dictData.allKeys objectAtIndex:indexPath.section];
    if([sectionKey isEqualToString:LIVE])
          return 60;
    
    NSArray *arrFriends=[self.dictData objectForKey:sectionKey];
    Friend *obj=[arrFriends objectAtIndex:indexPath.row];
    if([obj.cellType isEqualToString:@"expand"])
        return 80;
    
    /*if([obj.status isEqualToString:LIVE]&&[obj.status_type isEqualToString:kStatusTypeLiveRequest])
    {
        return 50;
    }
    
     return 70;*/
    
    return 60; // return 60;
  
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *sectionKey= [self.dictData.allKeys objectAtIndex:indexPath.section];
    if([sectionKey isEqualToString:LIVE])
    {
    
        static NSString* MyIdentifier = @"BlockedCell";
        BlockedCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"BlockedCell" owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        NSArray *array=[self.dictData objectForKey:sectionKey];
        Friend *obj=[array objectAtIndex:indexPath.row];
        
        cell.btnBlock.titleLabel.font=FONT_AVENIR_BLACK_H0;
        if(![Utility isEmpty:obj.time])
        {
            if([obj.live_type isEqualToString:@"UntilDisconnect"])
            {
                cell.btnBlock.titleLabel.font=FONT_AVENIR_BLACK_H1;
                [cell.btnBlock setTitle:@"∞" forState:UIControlStateNormal];
            }
            else {
                NSString *time=[Utility getCountdownText:obj.time];
                [cell.btnBlock setTitle:time forState:UIControlStateNormal];
            }
        }
        else
            [cell.btnBlock setTitle:MPLocalizedString(@"live", nil).uppercaseString forState:UIControlStateNormal];
        [cell.btnBlock setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cell.btnBlock setBackgroundImage:nil forState:UIControlStateNormal];
        cell.btnBlock.titleLabel.textColor=[UIColor blackColor];
        
        cell.lblUserName.text=@"";
        cell.lblFullName.text=obj.name.capitalizedString;
        cell.topContraint.constant=19;
        [cell.imgUser getImageWithURL:obj.image comefrom:ImageViewComfromUser];
        return cell;
    }
    else
    {
        
        static NSString* MyIdentifier = @"RequestLiveCell";
        RequestLiveCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"RequestLiveCell" owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            
        }
       // cell.btnEdit.tag=indexPath.row;
        cell.btnBlock.tag=indexPath.row;
        cell.btnDelete.tag=indexPath.row;
        
       // [cell.btnEdit addTarget:self action:@selector(clickEdit:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnBlock addTarget:self action:@selector(clickBlock:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDelete addTarget:self action:@selector(clickDelete:) forControlEvents:UIControlEventTouchUpInside];
        
        NSArray *arrFriends=[self.dictData objectForKey:FRIENDS];
        Friend *obj=[arrFriends objectAtIndex:indexPath.row];
        
        cell.btnRequestLive.tag=indexPath.row;
        [cell.btnRequestLive removeTarget:nil
                                   action:NULL
                         forControlEvents:UIControlEventAllEvents];
       
        cell.lblName.text=obj.name.capitalizedString;
        [cell.imgUser getImageWithURL:obj.image comefrom:ImageViewComfromUser];
        
        if([obj.status isEqualToString:LIVE]&&[obj.status_type isEqualToString:kStatusTypeLiveRequest])
        {
              cell.lblWaiting.hidden=NO;
              cell.lblWaiting.attributedText=[self getAttributedString:MPLocalizedString(@"live_waiting", nil)];
              
              cell.vwBlock.hidden=YES;
              cell.btnRequestLive.hidden=YES;
        }
        else
        {
            cell.lblWaiting.hidden=YES;
            if([obj.status_type isEqualToString:LIVE])
            {
               /* [cell setUpDisableLiveButton];
                 [cell.btnRequestLive addTarget:self action:@selector(btnDisableLive:) forControlEvents:UIControlEventTouchUpInside];*/
                cell.btnRequestLive.hidden=YES;
            }
            else
            {
                if([obj.cellType isEqualToString:@"expand"])
                {
                    [cell setUpRequstLiveButton];
                    [cell.btnRequestLive addTarget:self action:@selector(btnRequestLive:) forControlEvents:UIControlEventTouchUpInside];
                    cell.btnRequestLive.hidden=NO;
                }
                else
                     cell.btnRequestLive.hidden=YES;
            }
        }
        
        if([obj.cellType isEqualToString:@"expand"])
        {
             cell.vwBlock.hidden=NO;
        }
        else
        {
            cell.vwBlock.hidden=YES;
        }
        return cell;
    }
    
}
-(void)clickEdit:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
}

-(void)clickBlock:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    NSMutableArray *arrFriends=[self.dictData objectForKey:FRIENDS];

    UIButton *btn=(UIButton *)sender;
    Friend *obj=[arrFriends objectAtIndex:btn.tag];
   _selectedRow=btn.tag;
    
    NSString *msg=[NSString stringWithFormat:MPLocalizedString(@"are_you_sure_you_block", nil),obj.name.capitalizedString];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:APP_NAME_TITLE message:msg delegate:self cancelButtonTitle:MPLocalizedString(@"cancel", nil) otherButtonTitles:MPLocalizedString(@"yes", nil) ,nil];
    alert.tag=BLOCK_ALERT_TAG;
    [alert show];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        if(alertView.tag==BLOCK_ALERT_TAG)
        {
            NSMutableArray *arrFriends=[self.dictData objectForKey:FRIENDS];
            Friend *obj=[arrFriends objectAtIndex:_selectedRow];
        
            Friend *objSelected=[Friend new];
            objSelected.friend_id=obj.friend_id;
            objSelected.status_type=kStatusTypeBlock;
            [self.arrayActions addObject:objSelected];
            [self updateUserAction];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedRow inSection:self.dictData.allKeys.count-1];
            [self deleteRowAtIndexpath:indexPath fromArray:arrFriends];
        }
        else if(alertView.tag==DELETE_ALERT_TAG)
        {
            NSMutableArray *arrFriends=[self.dictData objectForKey:FRIENDS];
                        Friend *obj=[arrFriends objectAtIndex:_selectedRow];
            
            Friend *objSelected=[Friend new];
            objSelected.friend_id=obj.friend_id;
            objSelected.status_type=kStatusTypeNone;
            [self.arrayActions addObject:objSelected];
            [self updateUserAction];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedRow inSection:self.dictData.allKeys.count-1];
            [self deleteRowAtIndexpath:indexPath fromArray:arrFriends];
        }
    }
}

-(void)clickDelete:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    NSMutableArray *arrFriends=[self.dictData objectForKey:FRIENDS];
    
    UIButton *btn=(UIButton *)sender;
    Friend *obj=[arrFriends objectAtIndex:btn.tag];
    
    _selectedRow=btn.tag;
    
    NSString *msg=[NSString stringWithFormat:MPLocalizedString(@"are_you_sure_you_delete_xxx", nil),obj.name.capitalizedString];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:APP_NAME_TITLE message:msg delegate:self cancelButtonTitle:MPLocalizedString(@"cancel", nil) otherButtonTitles:MPLocalizedString(@"yes", nil) ,nil];
    alert.tag=DELETE_ALERT_TAG;
    [alert show];
    
}


-(void)btnDisableLive:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
}
-(void)showActionSheetFoHours:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    NSMutableArray *arrFriends=[self.dictData objectForKey:FRIENDS];
    self.selectedFriend=[arrFriends objectAtIndex:btn.tag];
    
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:MPLocalizedString(@"How long do you want to share your location?", nil) delegate:self cancelButtonTitle:MPLocalizedString(@"cancel", nil) destructiveButtonTitle:nil otherButtonTitles:@"24 Hours",@"72 Hours",@"Until disconnected",nil];
    action.tag=SELECT_HOURS_TAG;
    [action showInView:self.view];
}

#pragma marks -- UIActionSheet
// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag==SELECT_HOURS_TAG) {
    
         if(buttonIndex==3)
         {
             MPLog(@"Cancel");
         }
         else
         {
            self.selectedFriend.status_type=kStatusTypeLiveRequest;
            self.selectedFriend.status=LIVE;

            Friend *objSelected=[Friend new];
            objSelected.friend_id=self.selectedFriend.friend_id;
            objSelected.status_type=kStatusTypeLiveRequest;

            if(buttonIndex==0)
                objSelected.time=@"24";
            else if(buttonIndex==1)
                objSelected.time=@"72";
            else if(buttonIndex==2)
                objSelected.time=@"0";
             [self.tableView reloadData];
            [self.arrayActions addObject:objSelected];
            [self updateUserAction];

         }
    }
    else if (actionSheet.tag==DISABLE_LIVE_TAG) {
        
        if(buttonIndex==1)
        {
            MPLog(@"Cancel");
        }
        else
        {
            if(![UIAppDelegate isInternetAvailable])
            {
                [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
                return;
            }
            Friend *objSelected=[Friend new];
            objSelected.friend_id=self.selectedFriend.friend_id;
            objSelected.status_type=kStatusTypeDisconnect;
            [self.arrayActions addObject:objSelected];
            [self updateUserAction];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedRow inSection:0];
            NSMutableArray *array = [self.dictData objectForKey:LIVE];
            [self deleteRowAtIndexpath:indexPath fromArray:array];
        }
    }

}

-(void) updateUserAction {
    if(self.arrayActions.count)
    {
        [[UserAPI new]updateUserAction:self.arrayActions success:^(FriendsListWrapper *objWrapper) {
            [self.arrayActions removeAllObjects];
        } failure:^(NSError *error) {
                
        }];
    }
}

-(void)btnRequestLive:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
   
    [self showActionSheetFoHours:sender];

    
}
-(void)deleteRowAtIndexpath:(NSIndexPath *)indexPath fromArray:(NSMutableArray *)array
{
    [self.tableView beginUpdates];
    [array removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
    
}

-(NSMutableAttributedString *)getAttributedString:(NSString *)full_str
{
    
    NSMutableAttributedString* att_string = [[NSMutableAttributedString alloc] initWithString:full_str];
    NSRange atRange = [full_str rangeOfString:MPLocalizedString(@"waiting", nil)];
    
    if (atRange.location != NSNotFound) {
        [att_string addAttribute:NSForegroundColorAttributeName value:UICOLOR_NAV_TITLE range:atRange];
        [att_string addAttribute:NSFontAttributeName value:FONT_AVENIR_MEDIUM_H00 range:atRange];

    }
    return att_string;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    NSString *sectionKey= [self.dictData.allKeys objectAtIndex:indexPath.section];
    if([sectionKey isEqualToString:LIVE])
    {
        NSMutableArray *arrFriends=[self.dictData objectForKey:LIVE];
        self.selectedFriend=[arrFriends objectAtIndex:indexPath.row];
        _selectedRow = indexPath.row;
        UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:MPLocalizedString(@"are_sure_to_disconnect", nil) delegate:self cancelButtonTitle:MPLocalizedString(@"cancel", nil) destructiveButtonTitle:MPLocalizedString(@"yes", nil) otherButtonTitles:nil];
        action.tag=DISABLE_LIVE_TAG;
        [action showInView:self.view];
    }
    else
    {
        NSArray *arrFriends=[self.dictData objectForKey:FRIENDS];
        Friend *obj=[arrFriends objectAtIndex:indexPath.row];
        
        NSMutableArray* arrShouldBeCollapsed = [NSMutableArray new];
        if([obj.cellType isEqualToString:@"expand"])
        {
            obj.cellType=@"";
        }
        else
        {
            obj.cellType=@"expand";
            
            //@https://trello.com/c/UDTSAFLl/180-the-menu-in-friends-does-not-automatically-scroll-down-when-you-tap-on-it-also-max-1-menu-should-be-open-at-a-time-see-video-att
            
            int i = 0;
            for (Friend *friend in arrFriends) {
                if (obj.friend_id != friend.friend_id) {
                    if ([friend.cellType isEqualToString:@"expand"]) {
                        friend.cellType = @"";
                        NSIndexPath *_indexPath = [NSIndexPath indexPathForRow:i inSection:indexPath.section];
                        [arrShouldBeCollapsed addObject:_indexPath];
                    }
                }
                i++;
            }
        }
        NSMutableArray *arrShouldBeChanged = [NSMutableArray new];
        [arrShouldBeChanged addObject:indexPath];
        [arrShouldBeChanged addObjectsFromArray:arrShouldBeCollapsed];
        
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:arrShouldBeChanged withRowAnimation:UITableViewRowAnimationFade];
        [self.tableView endUpdates];
        
        //@move the tableview to show the selected content
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    return _lsSections;
}
- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    NSInteger sectionVal=0;
    
    for (NSInteger i=0; i<self.lsSections.count; i++) {
        NSString *sectiontitle=[self.lsSections objectAtIndex:i];
        if([sectiontitle hasPrefix:title])
        {
            sectionVal=i;
            break;
        }
    }
    
    NSString *temtitle =@"";
    NSArray *dataArray=nil;
    
    //VVLog(@"title:%@",title);
    NSArray *arrFriends=[self.dictData objectForKey:FRIENDS];

    if(arrFriends.count!=0)
        dataArray = arrFriends;
    
    for (NSInteger i = 0; i< [dataArray count]; i++)
    {
        
        //---Here you return the name i.e. Honda,Mazda and match the title for first letter of name and move to that row corresponding to that indexpath as below---//
        
        Friend *userObj  = (Friend*)[dataArray objectAtIndex:i];
        if(userObj.name.length>0)
        {
            temtitle =[userObj.name substringToIndex:1];
            if ([temtitle isEqualToString:title]) {
                [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:self.dictData.allKeys.count-1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
                break;
            }
        }
    }
    
    return -1;
}


- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
     if(searchBar.text.length==0)
     {
         if(self.isSearching)
        {
            self.isSearching=NO;
            self.next_offset=0;
            [self getFriendsList];
            [searchBar resignFirstResponder];

        }
     }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.isSearching=YES;
     self.next_offset=0;
     [self getFriendsList];
    [searchBar resignFirstResponder];
}

#pragma mark -- Load More methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.searchBar isFirstResponder])
        [self.searchBar resignFirstResponder];
    
    if (self.server_request_inprogress == NO && self.next_offset!=-1) {
        CGPoint offset = self.tableView.contentOffset;
        CGRect bounds = self.tableView.bounds;
        CGSize size = self.tableView.contentSize;
        
        UIEdgeInsets inset = self.tableView.contentInset;
        
        float y = offset.y + bounds.size.height - inset.bottom;
        float h = size.height;
        
        CGFloat reload_distance = 50;
        
        if (y + reload_distance > h ) {
            [self setActivityInTableFooter:YES];
            self.server_request_inprogress = YES;
            
            double delayInSeconds = 0.5f;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self getFriendsList];
            });
            
            return;
        }
    }
}

-(void)addLoadMoreActivityasFooter
{
    self.activityLoadMore =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityLoadMore.hidden=YES;
    self.activityLoadMore.frame = CGRectMake(0, 0, SCREEN_SIZE.width, 44);
    self.tableView.tableFooterView=self.activityLoadMore;
}

-(void)setActivityInTableFooter:(BOOL)show
{
    if(show )//&& self.lsDoctors.next_offset!=-1)
    {
        self.activityLoadMore.hidden=NO;
        [self.activityLoadMore startAnimating];
        self.tableView.tableFooterView=self.activityLoadMore;
    }
    else
    {
        self.activityLoadMore.hidden=YES;
        [self.activityLoadMore stopAnimating];
        self.tableView.tableFooterView=nil;
    }
}

@end
