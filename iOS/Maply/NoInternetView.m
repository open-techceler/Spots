//
//  NoInternetView.m
//  
//
//  Created by Sphinx on 10/15/13.
//
//

#import "NoInternetView.h"
#import "SSError.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "Utility.h"
#import "NSString+StringSizeWithFont.h"
@implementation NoInternetView

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    
    return self;
}
- (void)drawSubviewsWithType:(ConnectivityScreenType)type
{

    self.backgroundColor = UICOLOR_GLOBAL_BG;
    _connectivity_type = type;
    [self loadAllComponents];
}

- (void)drawCustomSubviewsWithType:(ConnectivityScreenType)type
{
    self.backgroundColor = UICOLOR_GLOBAL_BG;
    CGRect rect = self.frame;
    
    self.frame = rect;
    _connectivity_type = type;
    
    [self loadAllComponents];
}

-(void)loadAllComponents
{
    [self addBgViewForHome];
    
    [self addImage];
    [self addLable];
    
    [self addButton];
}

- (void) addBgViewForHome {
    _bgViewForHome = [[UIView alloc] initWithFrame:CGRectMake(20, (self.frame.size.height/2.0)-300, (self.frame.size.width-20*2), 300)];
    _bgViewForHome.layer.cornerRadius = 4.f;
    _bgViewForHome.backgroundColor = [UIColor whiteColor];
    [self addSubview:_bgViewForHome];
    
    _bgViewForHome.hidden = true;
}

- (void)addImage
{
    _imgView = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width/2 - 61/2), (self.frame.size.height/2.0)-100 , 61, 43)];
    _imgView.image = [UIImage imageNamed:@"no_internet.png"];
    [self addSubview:_imgView];
}

- (void)addLable
{
    _lblMessage = [[UILabel alloc] initWithFrame:CGRectMake(10, _imgView.frame.origin.y+ _imgView.frame.size.height+5, (self.frame.size.width-10*2), 20)];
    _lblMessage.textAlignment = NSTextAlignmentCenter;
    _lblMessage.backgroundColor = [UIColor clearColor];
    _lblMessage.lineBreakMode=NSLineBreakByWordWrapping;
    _lblMessage.numberOfLines = 0;
    _lblMessage.textColor = [UIColor colorWithHexValue:0xA9A9A9];
    _lblMessage.font = FONT_AVENIR_PRO_REGULAR_H5;
   [self showConnectivityMessage:nil];
    
    [self addSubview:_lblMessage];
    
    _lblTitleForHome = [[UILabel alloc] initWithFrame:CGRectMake(10, _imgView.frame.origin.y+ _imgView.frame.size.height+5, (self.frame.size.width-10*2), 20)];
    _lblTitleForHome.textAlignment = NSTextAlignmentCenter;
    _lblTitleForHome.backgroundColor = [UIColor clearColor];
    _lblTitleForHome.lineBreakMode=NSLineBreakByWordWrapping;
    _lblTitleForHome.numberOfLines = 0;
    _lblTitleForHome.textColor = [UIColor colorWithHexValue:0xA9A9A9];
    _lblTitleForHome.font = FONT_AVENIR_PRO_REGULAR_H5;
    
    [self addSubview:_lblTitleForHome];
    _lblTitleForHome.hidden = true;
}

- (void)addButton
{
    _btnRetry = [UIButton buttonWithType:UIButtonTypeCustom];
    _btnRetry.frame = CGRectMake((self.frame.size.width/2 - 57/2), _lblMessage.frame.origin.y + _lblMessage.frame.size.height + 15, 57, 25);
    _btnRetry.titleEdgeInsets=UIEdgeInsetsMake(2, 0, 0, 0);
    [_btnRetry setTitle:[MPLocalizedString(@"retry", nil) uppercaseString] forState:UIControlStateNormal];
    _btnRetry.titleLabel.font = FONT_AVENIR_PRO_DEMI_H4;
    _btnRetry.backgroundColor=PICKER_COLOR_PINK;
    [_btnRetry setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnRetry setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [_btnRetry setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    _btnRetry.layer.cornerRadius=3.0f;
    [self addSubview:_btnRetry];
}

- (void)resetAttributedLabel
{
    if ([Utility isEmpty:_lblMessage.text] == NO) {
        [_lblMessage setText:@""];
        [_lblMessage setAttributedText:nil];
    }
}

- (void)showErrorMessageOnly:(SSError*)error
{
    _lblMessage.text = MPLocalizedString(@"error_invalid_facebook_token", nil);
    _btnRetry.hidden= YES;
    _imgView.hidden = YES;
}


- (void)showConnectivityMessage:(NSError*)error
{
    [self resetAttributedLabel];
    SSError *vverror = error.userInfo[kSSErrorObjectKey];
    NSString *message =@"";
    if (_connectivity_type == ConnectivityCommon && error!=nil) {
        message = [vverror localizedMessage];
    } else {
        message= [self getConnectivityMsgWithType:_connectivity_type];
    }

    CGSize messageSize =[message VVSizeWithFont:FONT_AVENIR_PRO_REGULAR_H5 constrainedToSize:CGSizeMake(self.frame.size.width-20, CGFLOAT_MAX) lineBreakMode:NSLineBreakByWordWrapping];
    MPLog(@"height %f",messageSize.height);
    _lblMessage.text =message;
    _lblMessage.frame = CGRectMake(10, _imgView.frame.origin.y+ _imgView.frame.size.height+10, (self.frame.size.width-10*2), messageSize.height);
    
    if (_connectivity_type == ConnectivityTypeHome) {
        return;
    }
    
    [self adjustSubViews];
}



- (NSString*)getConnectivityMsgWithType:(ConnectivityScreenType)ScreenType
{
    NSString* connectivity_title = nil;
    
    if ([UIAppDelegate isInternetAvailable] == YES) //if there is internet and server call fail
    {
        /*if (ScreenType == ConnectivityPaused) {
            connectivity_title = [NSString stringWithFormat:@"%@\n%@", MPLocalizedString(@"service_unavailable", nil), MPLocalizedString(@"we_will_match_your_wine", nil)];
            connectivity_sub_title = MPLocalizedString(@"we_will_match_your_wine", nil);
        } else if (ScreenType == ConnectivityNoStreams) {
            _btnRetry.hidden = YES;
            connectivity_title = MPLocalizedString(@"no_ratings_found", nil);
            return [[NSMutableAttributedString alloc] initWithString:connectivity_title];
        } else if (ScreenType == ConnectivityNoWines) {
            _btnRetry.hidden = YES;
            connectivity_title = MPLocalizedString(@"no_wines", nil);
            return [[NSMutableAttributedString alloc] initWithString:connectivity_title];
        } else*/
        if (ScreenType == ConnectivityTypeHome) {
            connectivity_title = MPLocalizedString(@"you_are_offline_content", nil);
        }
        else
        {
            connectivity_title = [NSString stringWithFormat:@"%@\n",
                                      [NSString stringWithFormat:MPLocalizedString(@"app_server_unavailable", nil),
                                       APP_NAME]
                                                            ];
        }
    } else {
        if (ScreenType == ConnectivityTypeHome) {
             connectivity_title =MPLocalizedString(@"you_are_offline_content", nil);
        } else
        {
            connectivity_title = [NSString stringWithFormat:@"%@",
                                                            MPLocalizedString(@"no_internet_connection", nil)];
        }
    }


    return connectivity_title;
}

-(void)adjustSubViews
{
    NSInteger img_height=0;
    if(_imgView.image)
        img_height=_imgView.frame.size.height;
 
    _imgView.frame=CGRectMake((self.frame.size.width/2 - 52/2),(self.frame.size.height/2.0)-100 , 61, img_height);
    _lblMessage.frame=CGRectMake(10, _imgView .frame.origin.y +  img_height + 15, self.frame.size.width-20, _lblMessage.frame.size.height);
    
    _btnRetry.frame = CGRectMake((self.frame.size.width/2 - (_btnRetry.frame.size.width/2)), _lblMessage.frame.origin.y + _lblMessage.frame.size.height + 30, _btnRetry.frame.size.width, 25);
}

-(void)doUIForHome
{
    self.bgViewForHome.hidden = false;
    _lblTitleForHome.hidden = false;
    
    _bgViewForHome.frame = CGRectMake(20, (self.frame.size.height/2.0)-150, (self.frame.size.width-20*2), 300);
   
    self.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    
    self.lblTitleForHome.textColor=[UIColor darkGrayColor];
    self.lblTitleForHome.text = MPLocalizedString(@"you_are_offline_title", nil);
    _lblTitleForHome.frame=CGRectMake(40, _bgViewForHome.frame.origin.y + 20.f, self.frame.size.width-80, 30.f);
    
    self.lblMessage.textColor=[UIColor darkGrayColor];
    self.lblMessage.text = MPLocalizedString(@"you_are_offline_content", nil);
    _lblMessage.frame=CGRectMake(40, _bgViewForHome.frame.origin.y + 100.f, self.frame.size.width-80, _lblMessage.frame.size.height);

    [self.btnRetry setTitle:@"Go back online" forState:UIControlStateNormal];
    _btnRetry.titleLabel.font = FONT_AVENIR_PRO_DEMI_H6;
   
    _btnRetry.frame = CGRectMake((self.frame.size.width/2 - (180/2)), self.bgViewForHome.frame.origin.y + self.bgViewForHome.frame.size.height - 80.f, 180, 44); //92 is the height of the viewBottom of the HomeViewController, and 35 is the height of the btnRetry
    _btnRetry.layer.cornerRadius=22.0f;
    
}

@end
