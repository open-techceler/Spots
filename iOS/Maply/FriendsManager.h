//
//  FriendsManager.h
//  Maply
//
//  Created by admin on 5/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
@class FriendsListWrapper;

@interface FriendsManager : NSObject

+ (FriendsManager*)sharedInstance;
-(void)fetchFriendList;
-(void)getSendToFriendsList:(void (^)(FriendsListWrapper* objWrapper))success
           failure:(void (^)(NSError* error))failure;
@end
