//
//  LocationEnableViewController.m
//  Maply
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "LocationEnableViewController.h"
#import "LocationClass.h"

@interface LocationEnableViewController () <LocationClassDelegate>

@end

@implementation LocationEnableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if ([UIAppDelegate checkLocationServicePermission] == 0) {
        self.imgBg.image = [UIImage imageNamed:@"location_access_bg"];
        self.btnAskMe.hidden = false;
    } else {
        self.imgBg.image = [UIImage imageNamed:@"location_turn_off"];
        self.btnAskMe.hidden = true;
    }

    self.btnSkip.hidden = !self.bShowSkipButton;

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServiceAuthorized) name:@"LocationServiceAuthorized" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServiceDenied) name:@"LocationServiceDenied" object:nil];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    NSLog(@"location enable view shows");
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) checkLocationService {
    if ([UIAppDelegate checkLocationServicePermission] == 1) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(enabledLocationService:withFlag:)]) {
            [self.delegate enabledLocationService:self withFlag:false];
        }
    }
}

- (IBAction)actionSkip:(id)sender {
    if (self.delegateForSkip != nil && [self.delegateForSkip respondsToSelector:@selector(tappedLocationSkipButton)])
        [self.delegateForSkip tappedLocationSkipButton];
}

- (void) locationServiceAuthorized {
    if (!self.bShowSkipButton) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(enabledLocationService:withFlag:)]) {
            [self.delegate enabledLocationService:self withFlag:true];
        }
    } else {
        [self actionSkip:self.btnSkip];
    }
}

- (void) locationServiceDenied {
    self.imgBg.image = [UIImage imageNamed:@"location_turn_off"];
    self.btnAskMe.hidden = true;
}

- (IBAction)askMe:(id)sender {
    [[LocationClass sharedLoctionManager] startFetchingLocation];
}

@end
