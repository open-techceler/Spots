//
//  AppDelegate.m
//  Maply
//
//  Created by admin on 2/9/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "FacebookManager.h"
#import "AFNetworkActivityLogger.h"
#import "UserNameViewController.h"
#import "LogInViewController.h"
#import "User.h"
#import "HomeViewViewController.h"
#import "MenuContainerViewController.h"
#import "Foursquare2.h"
#import "MessagesMainViewController.h"
#import "JourneysViewController.h"
#import "MFSideMenuContainerViewController.h"
#import "APNRegistrationHelpers.h"
#import "UserAPI.h"
#import "HomeViewViewController.h"
#import "MFSideMenu.h"
#import "TWMessageBarManagerStyleSheet.h"
#import "NotificationDefinitions.h"
#import "VVReachabilityManager.h"
#include <AVFoundation/AVFoundation.h>
#import "StevenXMPPHelper.h"
#import "LocationTrackEngine.h"
#import "LocationEnableViewController.h"

@interface AppDelegate ()
@property (nonatomic) BOOL is_Push_token_sending;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //http://stackoverflow.com/questions/28715597/allows-music-playback-during-recording-video-like-snapchat-ios/32416013#32416013
    //https://trello.com/c/rzvT0WaV
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
        AVAudioSession *session1 = [AVAudioSession sharedInstance];
        [session1 setCategory:AVAudioSessionCategoryPlayAndRecord  withOptions:AVAudioSessionCategoryOptionMixWithOthers|AVAudioSessionCategoryOptionDefaultToSpeaker|AVAudioSessionCategoryOptionAllowBluetooth error:nil];
        [session1 setActive:YES error:nil];
        [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    });
    
    NSArray *fontFamilies = [UIFont familyNames];
    
    for (int i = 0; i < [fontFamilies count]; i++)
    {
        NSString *fontFamily = [fontFamilies objectAtIndex:i];
        NSArray *fontNames = [UIFont fontNamesForFamilyName:[fontFamilies objectAtIndex:i]];
        NSLog (@"%@: %@", fontFamily, fontNames);
    }
    
    [VVReachabilityManager sharedManager];

    [TWMessageBarManager sharedInstance].styleSheet = [[TWMessageBarManagerStyleSheet alloc] init];

    
    [Foursquare2 setupFoursquareWithClientId:@"OAF5KUQHLCOEMEPOFTPIOKEJJO4TAQXK5NOJ2RVQGG411BIN"
                                      secret:@"Y0SQ1WJJQU1ITS2L2STFNOKTQMEDJ2TCV0PWJZ5BHRXAK5LQ"
                                 callbackURL:@"maply://foursquare"];
    
    [self configureLogging];
    [self applyGlobalInterfaceAppearance];
    
    //[APNRegistrationHelpers applicationDidFinishLaunching:application];

    [self setGlobalDateFormtterLocale];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];

    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:FONT_HELVETICA_LIGHT_H1];
//    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    [self initMainAppFlow];
    return YES;
}

- (UIViewController *) getTopViewController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (BOOL) checkPushNotificationSetting {
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        UIRemoteNotificationType types = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
        if (types == UIRemoteNotificationTypeNone) {
            return FALSE;
        } else {
            return TRUE;
        }
    } else {
        return [[UIApplication sharedApplication] isRegisteredForRemoteNotifications];
    }
}

- (void)registerUserNotificationSetting
{
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS7 and below
        MPLog(@"registerUserNotificationSetting IOS 7");
        UIRemoteNotificationType myTypes = [self types_7];
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
    else {
             
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert
                                                                                             | UIUserNotificationTypeBadge
                                                                                             | UIUserNotificationTypeSound) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
}

- (int) checkLocationServicePermission {
    if ([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted) {
            return 2; //denied
        } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
            return 0; //not determined
        } else {
            return 1; //enable
        }
    } else {
        return 0; //disable
    }
}

- (UIUserNotificationType)types_8
{
    return UIRemoteNotificationTypeNewsstandContentAvailability | UIUserNotificationTypeBadge | UIUserNotificationTypeAlert | UIUserNotificationTypeSound;
}

- (UIRemoteNotificationType)types_7
{
    return UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
}

-(void) getBatterStatus
{
    UIDevice *myDevice = [UIDevice currentDevice];
    [myDevice setBatteryMonitoringEnabled:YES];
    
    int state = [myDevice batteryState];
    NSLog(@"battery status: %d",state); // 0 unknown, 1 unplegged, 2 charging, 3 full
    
    double batLeft = (float)[myDevice batteryLevel] * 100;
    NSLog(@"battery left: %f", batLeft);
}

-(void)initLogInFlow
{
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nav = [story instantiateInitialViewController];
    self.window.rootViewController = nav;
}

-(void)initUserNameFlow
{
    UserNameViewController *cnt =[[UserNameViewController alloc] initWithNibName:@"UserNameViewController" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:cnt];
    nav.navigationBarHidden=YES;
    self.window.rootViewController = nav;
}

-(void)initHomeViewFlow
{
   
    _mainController=[MainViewController new];
    /* MessagesMainViewController  *cnt = [[MessagesMainViewController alloc]initWithNibName:@"MessagesMainViewController" bundle:nil];
     UINavigationController *leftnav = [[UINavigationController alloc]initWithRootViewController:cnt];
    
    JourneysViewController  *cntJourney = [[JourneysViewController alloc]initWithNibName:@"JourneysViewController" bundle:nil];
     UINavigationController *rightnav  = [[UINavigationController alloc]initWithRootViewController:cntJourney];
    
    HomeViewViewController *cntCenter =[[HomeViewViewController alloc] initWithNibName:@"HomeViewViewController" bundle:nil];
    UINavigationController *navCenter = [[UINavigationController alloc]initWithRootViewController:cntCenter];
    navCenter.navigationBarHidden=YES;

    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:navCenter
                                                    leftMenuViewController:leftnav
                                                    rightMenuViewController:rightnav];
    container.shadow.enabled=NO;
    container.menuWidth = SCREEN_SIZE.width;*/

    
   /* HomeViewViewController *cnt =[[HomeViewViewController alloc] initWithNibName:@"HomeViewViewController" bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:cnt];
    nav.navigationBarHidden=YES;
    
    MenuContainerViewController *sideMenuController = [[MenuContainerViewController alloc] initWithRootViewController:nav
                                                                                                    presentationStyle:LGSideMenuPresentationStyleSlideAbove
                                                                                                                 type:0];*/
    
   // [self pushHOme:sideMenuController];
    
  /*  [UIView transitionWithView:self.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{ self.window.rootViewController = sideMenuController; }
                    completion:nil];*/
    self.window.rootViewController = _mainController;
}

-(void)pushHOme:(UINavigationController *)viewControllerToBeShown
{
    if( self.window.rootViewController!=nil){
    viewControllerToBeShown.view.backgroundColor = [UIColor orangeColor];
    
    UIView *myView1 = self.window.rootViewController.view;
    
    UIView *myView2 = viewControllerToBeShown.view;
    
    myView2.frame = self.window.bounds;
    
    
    [self.window addSubview:myView2];
    
    
    CATransition* transition = [CATransition animation];
    transition.startProgress = 0;
    transition.endProgress = 1.0;
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    transition.duration = 0.0;
    
    // Add the transition animation to both layers
    [myView1.layer addAnimation:transition forKey:@"transition"];
    [myView2.layer addAnimation:transition forKey:@"transition"];
    
    myView1.hidden = YES;
    myView2.hidden = NO;
    
    
    self.window.rootViewController = viewControllerToBeShown;
    }
    else
        self.window.rootViewController=viewControllerToBeShown;
}

-(void)initMainAppFlow
{
    User *obj =[User currentUser];
    NSLog(@"obj>>>%@",obj.description);
    
//    obj.username=nil;
    if(obj.user_id==0)
    {
        [self initLogInFlow];
    }
    else
    {
        [[StevenXMPPHelper sharedInstance] connectWithJid:[NSString stringWithFormat:@"%d@%@", (int)obj.user_id, XmppVHOST] password:@"1" hostname:@"47.89.40.246" connectionHandler:nil authenticationHandler:nil];
        
        if([Utility isEmpty:obj.username])
        {
            [self initUserNameFlow];
        }
        else
        {
            [self initHomeViewFlow];
        }
    }
}



#pragma mark - Navigation Bar Appearance Methods

- (void)setGlobalDateFormtterLocale
{
    NSDateFormatter* dtfmt = [[NSDateFormatter alloc] init];
    [dtfmt setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    self.global_formatter = dtfmt;
    
    NSLocale* usLocale = [[NSLocale alloc] initWithLocaleIdentifier:[LanguageManager getPerferedLocaleCode]];
    [self.global_formatter setLocale:usLocale];
   
}

- (void)applyNativeInterfaceApperance
{
    [[UINavigationBar appearance] setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setBackIndicatorImage:nil];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:nil];
}

- (void)applyGlobalInterfaceAppearance
{
    UIImage* navBackgroundImage = nil;
    navBackgroundImage = [[UIImage imageNamed:@"nav.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeStretch];
    
    
    UIImage* myImage = [UIImage imageNamed:@"transparent.png"];
    myImage = [myImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [[UINavigationBar appearance] setBackIndicatorImage:myImage];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:myImage];
   
    if([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        [[UINavigationBar appearance] setTranslucent:NO];
    }
    [[UINavigationBar appearance] setBackgroundImage:navBackgroundImage forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[UIImage new]];

}

-(void)configureLogging
{
#ifdef DEBUG
    [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelDebug];
    [[AFNetworkActivityLogger sharedLogger] startLogging];
#endif
    
}
- (BOOL)isInternetAvailable
{
    return [VVReachabilityManager isReachable];
   
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    //https://trello.com/c/n3qxu1kB
    User *obj =[User currentUser];
    if(obj.user_id)
        [[NSNotificationCenter defaultCenter]postNotificationName:kpopToMapViewNotification object:nil];
    
    //for maintain socket connection
    if ([application respondsToSelector:@selector(setKeepAliveTimeout:handler:)]) {
        [application setKeepAliveTimeout:600 handler:^{
            //Do other keep alive stuff here
        }];
    }
    else {
        [[StevenXMPPHelper sharedInstance] disconnect];
    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    if ([[self getTopViewController] isKindOfClass:[LocationEnableViewController class]]) {
        [((LocationEnableViewController *)[self getTopViewController]) checkLocationService];
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     [[FacebookManager sharedInstance] facebookManagerBecomeActive];
    
    User *obj =[User currentUser];
    if(obj.user_id)
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:kcheckLocationServiceNotification object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:kreloadMyJourneyTabNotification object:nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:kUIApplicationDidBecomeActive object:nil];
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    [[StevenXMPPHelper sharedInstance] disconnect];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}


//For interactive notification only
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}

//- (void) application:(UIApplication *)application
//  performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    completionHandler(UIBackgroundFetchResultNewData);
//    
//    // Start the task
////    [task resume];
//}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    MPLog(@"My token is: %@", deviceToken);

    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationAuthorized" object:nil];

    // [ToastMessage showErrorMessageAppTitleWithMessage:@"deviceToken found"];
    [APNRegistrationHelpers didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    if( !_is_Push_token_sending )
    {
        _is_Push_token_sending = YES;
        [self registerDevice];
    }
}

- (void)registerDevice
{
    if ([[VVBaseUserDefaults userId] integerValue] > 0 &&
        [APNRegistrationHelpers deviceToken] != nil)
    {
        [[UserAPI new]sendPushToken:[APNRegistrationHelpers deviceTokenString] success:^(MessageListWrapper *objWrapper) {
              _is_Push_token_sending = NO;
            //[ToastMessage showErrorMessageAppTitleWithMessage:@"Push Token Success"];

        } failure:^(NSError *error) {
             _is_Push_token_sending = NO;
              //[ToastMessage showErrorMessageAppTitleWithMessage:@"Push Token Failed"];
        }];

    }
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    NSString *str = [NSString stringWithFormat: @"Error: %@", err];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NotificationFailed" object:nil];

    [ToastMessage showErrorMessageAppTitleWithMessage:str];
    MPLog(@"%@",str);
}



- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    MPLog(@"didReceiveRemoteNotification >>>>>%@>>>>>", userInfo);
    
    NSDictionary *dict=userInfo[@"aps"];
   /* [KSToastView ks_setAppearanceOffsetBottom:SCREEN_SIZE.height-120];
        [KSToastView ks_showToast:[dict objectForKey:@"alert"] duration:1.0f completion:^{
    }];*/
    if([[dict objectForKey:@"type"] isEqualToString:@"GO_ONLINE"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:kUIApplicationGoOnline object:nil];
    } else {
        [ToastMessage showInfoMessage:MPLocalizedString([dict objectForKey:@"alert"] , nil)];
    }
    
    if ([VVBaseUserDefaults userId].integerValue != 0) {
        [self handleDeepLinkForDict:userInfo];
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    NSDictionary *dict=userInfo[@"aps"];
    if([[dict objectForKey:@"type"] isEqualToString:@"GO_ONLINE"]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:kUIApplicationGoOnline object:nil];
    }
}

- (BOOL)handleDeepLinkForDict:(NSDictionary*)params
{
    BOOL result = NO;

    if(_mainController)
    {
        HomeViewViewController *cntHome=[_mainController HomeViewInstance];
        if (cntHome && [cntHome isKindOfClass:[HomeViewViewController class]]) {
            [cntHome handlePushNotification:params];
        } else {
        }
    }
    return result;

}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

@end
