//
//  MomentRestAPI.m
//  Maply
//
//  Created by admin on 6/28/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MomentRestAPI.h"

@implementation MomentRestAPI
- (id)init
{
    self = [super init];
    if (self) {
        [self.pathComponents addObject:@"moment"];
    }
    return self;
}

- (AFHTTPRequestOperation*)GETAction:(APIRestAction)action
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    
    return [super GETAction:action success:success failure:failure];
}

- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)action
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    
    return [super POSTAction:action success:success failure:failure];
}

- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)action
                           endDynamic:(NSString*)path
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    [self.pathComponents addObject:path];
    
    return [super POSTAction:action success:success failure:failure];
}

- (AFHTTPRequestOperation*)PUTAction:(APIRestAction)action
                          endDynamic:(NSString*)path
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self.pathComponents addObject:path];
    [self setupPathComponents:action];
    return [super PUTsuccess:success failure:failure];
}


- (AFHTTPRequestOperation*)GETAction:(APIRestAction)action
                          endDynamic:(NSString*)path
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    [self.pathComponents addObject:path];
    return [super GETAction:action success:success failure:failure];
}

- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)task
            constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> formData))block
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:task];
    return [super POSTconstructingBodyWithBlock:block success:success failure:failure];
}


- (AFHTTPRequestOperation*)PUTAction:(APIRestAction)action
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    return [super PUTsuccess:success failure:failure];
}

- (AFHTTPRequestOperation*)DELETEAction:(APIRestAction)action
                             endDynamic:(NSString*)person_id
                                success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                                failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    [self.pathComponents addObject:person_id];
    return [super DELETEsuccess:success failure:failure];
}

- (void)setupPathComponents:(APIRestAction)action
{
    NSArray *subPath;
    if(action==APIRestDeleteJourney)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        //subPath=[NSArray arrayWithObject:@"view"];
    }

    
    if(subPath.count)
        [self.pathComponents addObjectsFromArray:subPath];
}

- (void)setupPropertiesForTask:(APIRestAction)action
{
    NSArray *subPath=nil;
    if(subPath.count)
        [self.pathComponents addObjectsFromArray:subPath];
}

@end
