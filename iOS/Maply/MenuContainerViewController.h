//
//  MenuContainerViewController.h
//  Beetot
//
//  Created by admin on 1/18/16.
//  Copyright © 2016 Pradip. All rights reserved.
//

#import "LGSideMenuController.h"

@interface MenuContainerViewController : LGSideMenuController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
                         presentationStyle:(LGSideMenuPresentationStyle)style
                                      type:(NSUInteger)type;
@end
