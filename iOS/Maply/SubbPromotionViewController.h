//
//  SubbPromotionViewController.h
//  Beetot
//
//  Created by admin on 1/6/16.
//  Copyright (c) 2016 Rishi. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Message;

@interface SubbPromotionViewController : MyNavigationViewController
@property (nonatomic, strong) Message *objMessage;
@property (nonatomic)JourneyDetailsComeFrom comefrom;

@property NSUInteger pageIndex;
-(void)startTimer;
@end
