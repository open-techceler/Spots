//
//  MyJourneySubCell.m
//  Maply
//
//  Created by admin on 6/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MyJourneySubCell.h"

@implementation MyJourneySubCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblName.font=FONT_AVENIR_MEDIUM_H1;
    self.lblAddress.font=FONT_AVENIR_LIGHT_H09;
    self.lblCount.font=FONT_AVENIR_MEDIUM_H1;
   
    self.lblCount.textColor=UICOLOR_DARK_TEXT;
    self.lblName.textColor=UICOLOR_DARK_TEXT;
    self.lblAddress.textColor=UICOLOR_LIGHT_TEXT;
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
