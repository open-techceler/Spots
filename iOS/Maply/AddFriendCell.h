//
//  AddFriendCell.h
//  Maply
//
//  Created by Admin on 01/10/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Friend;

@protocol AddFriendCellDelegate;

@interface AddFriendCell : UITableViewCell

@property (nonatomic, weak) id<AddFriendCellDelegate> delegate;

@property (nonatomic, strong) NSArray* arrayFriends;

- (void) loadUsers:(NSArray *) arrayUsers;

@end

@protocol AddFriendCellDelegate <NSObject>

- (void) tappedAddButton:(AddFriendCell *) cell friend:(Friend *) obj;

@end
