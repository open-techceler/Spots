//
//  FriendsListWrapper.h
//  Maply
//
//  Created by admin on 2/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MTLModelModified.h"

@interface FriendsListWrapper : MTLModelModified
@property(nonatomic,strong)NSArray *list;
@property NSInteger count;

@property(nonatomic,strong)NSMutableArray *live_request,*friend_reqeust;

//send to
@property(nonatomic,strong)NSArray *top_friends,*friends;


@end
