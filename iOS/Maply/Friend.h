//
//  Friend.h
//  Maply
//
//  Created by admin on 2/16/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MTLModelModified.h"

@interface Friend : MTLModelModified
@property(nonatomic,strong)NSString*name,*username,*image,*status_type,*location,*full_address;
@property(nonatomic)NSInteger friend_id,badge_count;
@property(nonatomic)double latitude,longitude;
@property(nonatomic,strong)NSString *cellType,*time;
@property(nonatomic,strong)NSString *live_type;


@property(nonatomic,strong)NSString *title,*event_description;
@property(nonatomic,strong)NSString *type;
@property(nonatomic,strong)NSMutableArray *arrNotific;

/*
 "id": 2,
 "image": "http://www.sphinx-solution.com/wp-content/themes/enfold/images/layout/new-logox-sphinx.png",
 "name": "Roshan Mahajan",
 "action": "Add"
 */

@end
