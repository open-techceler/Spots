//
//  MenuContainerViewController.m
//  Beetot
//
//  Created by admin on 1/18/16.
//  Copyright © 2016 Pradip. All rights reserved.
//

#import "MenuContainerViewController.h"
#import "MessagesMainViewController.h"
#import "JourneysViewController.h"

@interface MenuContainerViewController ()
@property (strong, nonatomic) UINavigationController *leftViewController,*rightViewController;

@end

@implementation MenuContainerViewController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController
                         presentationStyle:(LGSideMenuPresentationStyle)style
                                      type:(NSUInteger)type
{
    self = [super initWithRootViewController:rootViewController];
    if (self)
    {
        
        // -----
       MessagesMainViewController  *cnt = [[MessagesMainViewController alloc]initWithNibName:@"MessagesMainViewController" bundle:nil];
      self.leftViewController = [[UINavigationController alloc]initWithRootViewController:cnt];
      [self setLeftViewEnabledWithWidth:SCREEN_SIZE.width
                            presentationStyle:style
                         alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
        self.leftViewStatusBarStyle = UIStatusBarStyleDefault;
        self.leftViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnPhonePortrait;
            
        [self.leftView addSubview:_leftViewController.view];
        
        JourneysViewController  *cntJourney = [[JourneysViewController alloc]initWithNibName:@"JourneysViewController" bundle:nil];
        self.rightViewController = [[UINavigationController alloc]initWithRootViewController:cntJourney];
        [self setRightViewEnabledWithWidth:SCREEN_SIZE.width
                        presentationStyle:style
                     alwaysVisibleOptions:LGSideMenuAlwaysVisibleOnNone];
        self.rightViewStatusBarStyle = UIStatusBarStyleDefault;
        self.rightViewStatusBarVisibleOptions = LGSideMenuStatusBarVisibleOnPhonePortrait;
        
        [self.rightView addSubview:_rightViewController.view];

        
        
        //self.leftViewSwipeGestureEnabled=NO;
       // self.gesturesCancelsTouchesInView=NO;
    }
    return self;
}

- (void)leftViewWillLayoutSubviewsWithSize:(CGSize)size
{
    [super leftViewWillLayoutSubviewsWithSize:size];
    
 /*   if (![UIApplication sharedApplication].isStatusBarHidden)
        _leftViewController.tableView.frame = CGRectMake(0.f , 20.f, size.width, size.height-20.f);
    else*/
    _leftViewController.view.frame = CGRectMake(0.f , 0.f, size.width, size.height);
}

- (void)rightViewWillLayoutSubviewsWithSize:(CGSize)size
{
    [super rightViewWillLayoutSubviewsWithSize:size];
    _rightViewController.view.frame = CGRectMake(0.f , 0.f, size.width, size.height);

}

@end
