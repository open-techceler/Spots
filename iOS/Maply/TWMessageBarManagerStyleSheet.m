//
//  TWMessageBarManagerStyleSheet.m
//  MediPock
//
//  Created by anand mahajan on 12/11/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

// Strings
NSString * const kAppDelegateDemoStyleSheetImageIconError = @"icon-error.png";
NSString * const kAppDelegateDemoStyleSheetImageIconSuccess = @"icon-success.png";
NSString * const kAppDelegateDemoStyleSheetImageIconInfo = @"icon-info.png";


#import "TWMessageBarManagerStyleSheet.h"

@implementation TWMessageBarManagerStyleSheet

#pragma mark - Alloc/Init

+ (TWMessageBarManagerStyleSheet *)styleSheet
{
    return [[TWMessageBarManagerStyleSheet alloc] init];
}

#pragma mark - TWMessageBarStyleSheet

- (UIColor *)backgroundColorForMessageType:(TWMessageBarMessageType)type
{
    UIColor *backgroundColor = nil;
    switch (type)
    {
        case TWMessageBarMessageTypeError:
            backgroundColor = PICKER_COLOR_PINK;
            break;
        case TWMessageBarMessageTypeSuccess:
            backgroundColor = PICKER_COLOR_PINK;
            break;
        case TWMessageBarMessageTypeInfo:
            backgroundColor = PICKER_COLOR_PINK;
            break;
        default:
            break;
    }
    return backgroundColor;
}

- (UIColor *)strokeColorForMessageType:(TWMessageBarMessageType)type
{
    UIColor *strokeColor = [UIColor clearColor];
 /*   switch (type)
    {
        case TWMessageBarMessageTypeError:
            strokeColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0];
            break;
        case TWMessageBarMessageTypeSuccess:
            strokeColor = [UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:1.0];
            break;
        case TWMessageBarMessageTypeInfo:
            strokeColor = [UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:1.0];
            break;
        default:
            break;
    }*/
    return strokeColor;
}

- (UIImage *)iconImageForMessageType:(TWMessageBarMessageType)type
{
    UIImage *iconImage = nil;
    switch (type)
    {
        case TWMessageBarMessageTypeError:
            iconImage = [UIImage imageNamed:kAppDelegateDemoStyleSheetImageIconError];
            break;
        case TWMessageBarMessageTypeSuccess:
            iconImage = [UIImage imageNamed:kAppDelegateDemoStyleSheetImageIconSuccess];
            break;
        case TWMessageBarMessageTypeInfo:
            iconImage = [UIImage imageNamed:kAppDelegateDemoStyleSheetImageIconInfo];
            break;
        default:
            break;
    }
    return iconImage;
}

- (UIFont *)titleFontForMessageType:(TWMessageBarMessageType)type
{
    return FONT_AVENIR_PRO_DEMI_H6;
}

- (UIFont *)descriptionFontForMessageType:(TWMessageBarMessageType)type
{
    return FONT_AVENIR_PRO_REGULAR_H6;
}

- (UIColor *)titleColorForMessageType:(TWMessageBarMessageType)type
{
    return [UIColor whiteColor];
}

- (UIColor *)descriptionColorForMessageType:(TWMessageBarMessageType)type
{
    return [UIColor whiteColor];
}
@end
