//
//  FriendsViewController.h
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol  FriendsViewDelegate <NSObject>
//-(void)pushController:(UIViewController *)cnt;
//-(void)presentViewController:(UIViewController *)cnt;
-(void)setBadgeForTab:(NSInteger)tabIndex value:(NSInteger)value;

@end
@interface FriendsViewController : MyNavigationViewController
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *arrayActions;
@property(nonatomic,weak) id<FriendsViewDelegate> delegate;
-(void)reloadFriendList;
@end
