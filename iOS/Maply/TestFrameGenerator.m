//
//  TestFrameGenerator.m
//  makeMovie
//
//  Created by Rob Mayoff on 4/25/15.
//  Donated to the public domain.
//

@import UIKit;
#import "TestFrameGenerator.h"

@implementation TestFrameGenerator {
   
    CMTime nextTime;
}

#pragma mark - NSObject overrides

- (instancetype)init {
    if (self = [super init]) {
        //baseImage = [UIImage imageNamed:@"baseImage.jpg"];
       // _totalFramesCount = self.arrayImages.count;
        nextTime = CMTimeMake(0, 45);
    }
    return self;
}

- (CGSize)frameSize {
    return  self.baseImage.size;
}

- (BOOL)hasNextFrame {
    return self.framesEmittedCount < self.totalFramesCount;
}

- (CMTime)nextFramePresentationTime {
    return nextTime;
}

- (void)drawNextFrameInContext:(CGContextRef)gc {
    CGContextTranslateCTM(gc, 0, self.baseImage.size.height);
    CGContextScaleCTM(gc, 1, -1);
    UIGraphicsPushContext(gc); {
//        NSData *pngData = [NSData dataWithContentsOfFile:self.arrayImages[_framesEmittedCount]];
       self.baseImage = self.arrayImages[_framesEmittedCount];

        //self.baseImage = [UIImage imageWithData:pngData];
        [self.baseImage drawAtPoint:CGPointZero];

       // [[UIColor redColor] setFill];
      //  UIRectFill(CGRectMake(0, 0, baseImage.size.width, baseImage.size.height * self.framesEmittedCount / self.totalFramesCount));
    } UIGraphicsPopContext();

    ++_framesEmittedCount;

    if (self.frameGeneratedCallback != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.frameGeneratedCallback();
        });
    }

   // NSLog(@" nextTime.value>>%lld", nextTime.value);
    if (self.framesEmittedCount < self.totalFramesCount / 2) {
        nextTime.value += 1.7;
    } else {
        nextTime.value += 2.0;
    }
}

@end
