//
//  FSConverter.m
//  Foursquare2-iOS
//
//  Created by Constantine Fry on 2/7/13.
//
//

#import "FSConverter.h"
#import "FSVenue.h"

@implementation FSConverter

- (NSArray *)convertToObjects:(NSArray *)venues {
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:venues.count];
    for (NSDictionary *v  in venues) {
        FSVenue *ann = [[FSVenue alloc]init];
        ann.name = v[@"name"];
        ann.venueId = v[@"id"];
        
        ann.checkinsCount= v[@"stats"][@"checkinsCount"];
        
        NSArray *categories=v[@"categories"];
        NSDictionary *cat=[categories firstObject];
        if(cat)
        {
            ann.icon = cat[@"icon"][@"prefix"];
            ann.icon=[NSString stringWithFormat:@"%@bg_32%@",ann.icon,cat[@"icon"][@"suffix"]];

        }

        ann.location.address = v[@"location"][@"address"];
        ann.location.city = v[@"location"][@"city"];
        ann.location.country = v[@"location"][@"country"];
        ann.location.distance = v[@"location"][@"distance"];
        
        if([Utility isEmpty:ann.location.address])
        {
            NSMutableArray *arr_city_country=[NSMutableArray new];
            if(![Utility isEmpty:ann.location.city])
                [arr_city_country addObject:ann.location.city];
            if(![Utility isEmpty:ann.location.country])
                [arr_city_country addObject:ann.location.country];
            
            ann.location.address=[arr_city_country componentsJoinedByString:@", "];
        }
        else
        {
            if(![Utility isEmpty:ann.location.city]&& ![ann.location.address containsString:ann.location.city])
                ann.location.address=[NSString stringWithFormat:@"%@, %@",ann.location.address,ann.location.city];
            else
                ann.location.address=ann.location.address;
        }
            
        
        NSArray *formatted = v[@"location"][@"formattedAddress"];
        ann.location.formattedAddress=[formatted componentsJoinedByString:@", "];
        [ann.location setCoordinate:CLLocationCoordinate2DMake([v[@"location"][@"lat"] doubleValue],
                                                      [v[@"location"][@"lng"] doubleValue])];
        [objects addObject:ann];
    }
    return objects;
}

@end
