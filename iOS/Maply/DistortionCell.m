//
//  DistortionCell.m
//  Maply
//
//  Created by admin on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "DistortionCell.h"

@implementation DistortionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblTitle.font=FONT_AVENIR_ROMAN_H15  ;
    self.lblDetails.font=FONT_AVENIR_LIGHT_H3 ;
    
    self.lblTitle.textColor=[UIColor whiteColor];
    self.lblDetails.textColor=[UIColor whiteColor];
    
    self.lblTitle.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.lblTitle.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    
    self.lblTitle.layer.shadowRadius = 5.0;
    self.lblTitle.layer.shadowOpacity =0.6;
    self.lblTitle.layer.masksToBounds = NO;
    self.lblTitle.layer.shouldRasterize = YES;
    
    self.lblDetails.highlightedTextColor = [UIColor whiteColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
