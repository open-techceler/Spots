//
//  TitleValueTableCell.m
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 11/23/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import "TitleValueTableCell.h"

@interface TitleValueTableCell()
@property(nonatomic,strong)IBOutlet NSLayoutConstraint *constFooterLeading;
@end
@implementation TitleValueTableCell

- (void)awakeFromNib {
    // Initialization code
    _lblTitle.preferredMaxLayoutWidth =SCREEN_SIZE.width - 146;
    _lblValue.preferredMaxLayoutWidth=SCREEN_SIZE.width -228;
    
    _lblValue.font =FONT_AVENIR_ROMAN_H4;
    _lblTitle.font =FONT_SETTING_TITLE;
    
    self.lblTitle.textColor=UICOLOR_NOTIFICATION_TITLE;
    self.lblValue.textColor=UICOLOR_LIGHT_TEXT;
    
    _lblValue.text =@"";
    _lblTitle.text =@"";
    
    self.imgUser.clipsToBounds = YES;
}

-(void)setTitle:(NSString*)title value:(NSString*)value
{
    _lblTitle.text=title;
    _lblValue.text=value;
}

-(void)hideLastCellSperatorIfNeeded:(NSInteger)row_count index:(NSInteger)index
{
    if(index==(row_count-1))
        _constFooterLeading.constant=0;
    else
        _constFooterLeading.constant=16;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
