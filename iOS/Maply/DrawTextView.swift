//
//  DrawTextView.swift
//  
//
//  Created by Remi Robert on 11/07/15.
//
//
//import Masonry

class CustomLabel: UILabel {
    override func drawTextInRect(rect: CGRect) {
        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, UIEdgeInsetsMake(0, 5, 0, 5)))
    }
}

public class DrawTextView: UIView {

    var textLabel: CustomLabel!
    
    var initialCenter: CGPoint!
    
    var text: String! {
        didSet {
            
            //**
            //if(textLabel.text==nil){
                textLabel.hidden=true;
           // }
            
            textLabel.text = text
            sizeTextLabel()
        }
    }

    init() {
        super.init(frame: CGRectZero)
        
        layer.masksToBounds = true
        backgroundColor = UIColor.clearColor()
        textLabel = CustomLabel()
        textLabel.font = textLabel.font.fontWithSize(17)  //***
        textLabel.textAlignment = NSTextAlignment.Center
        textLabel.numberOfLines = 1
        textLabel.textColor = UIColor.blackColor()
        addSubview(textLabel)
        
        textLabel.mas_makeConstraints { (make: MASConstraintMaker!) -> Void in
            
            //***https://trello.com/c/Q7V95N89
           // make.right.and().left().equalTo()(self)
            
          //  make.left.equalTo()(self.mas_left).with().offset()(15);
           // make.right.equalTo()(self.mas_right).with().offset()(-15);
            make.left.equalTo()(self.mas_left).with().offset()(0);
            make.right.equalTo()(self.mas_right).with().offset()(0);
            make.top.equalTo()(self.mas_top).with().offset()(0);
            make.bottom.equalTo()(self.mas_bottom).with().offset()(0);
            /////
            
            make.centerY.equalTo()(self)
            make.centerX.equalTo()(self)
        }
        
        //***
        textLabel.layer.cornerRadius=5;
        textLabel.clipsToBounds = true;
        textLabel.layer.masksToBounds = true
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func sizeTextLabel() {
        self.hidden=false
        let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(0.1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            let oldCenter = self.textLabel.center
            let styleText = NSMutableParagraphStyle()
            styleText.alignment = NSTextAlignment.Center
            let attributsText = [NSParagraphStyleAttributeName:styleText, NSFontAttributeName:UIFont(name: "Avenir-Light", size: 17.0)!]
            let sizeTextLabel = (NSString(string: self.textLabel.text!)).boundingRectWithSize(self.superview!.frame.size
                , options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributsText, context: nil)
           
            self.translatesAutoresizingMaskIntoConstraints = true;
            if(sizeTextLabel.width==0){
                self.textLabel.frame.size = CGSizeZero
            }
            else{
                self.frame.size = CGSizeMake(sizeTextLabel.width + 30, sizeTextLabel.height + 10)
            }
            
           
            self.textLabel.center = oldCenter
            
            
           
            //**
            self.textLabel.layoutIfNeeded();
            self.layoutIfNeeded();
            self.superview?.layoutIfNeeded();
            self.textLabel.hidden=false;
            
            //**
            let myoldCenter = self.initialCenter
           // NSLog("myoldCenter>>%f %f", myoldCenter.x,myoldCenter.y);
            self.center=myoldCenter

        }
       
        //**
      /*  dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            let oldCenter = self.textLabel.center
            let styleText = NSMutableParagraphStyle()
            styleText.alignment = NSTextAlignment.Center
            let attributsText = [NSParagraphStyleAttributeName:styleText, NSFontAttributeName:UIFont.boldSystemFontOfSize(self.textLabel.font.pointSize)]
        let sizeTextLabel = (NSString(string: self.textLabel.text!)).boundingRectWithSize(CGSizeMake(UIScreen.mainScreen().bounds.size.width, 30)
, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: attributsText, context: nil)
            self.textLabel.frame.size = CGSizeMake(sizeTextLabel.width + 10, sizeTextLabel.height + 10)
            self.textLabel.center = oldCenter
           
            self.layoutIfNeeded();
            self.textLabel.hidden=false;

        });*/
        

     
    }
}
