//
//  JourneyListWrapper.m
//  Maply
//
//  Created by admin on 2/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "JourneyListWrapper.h"
#import "Journey.h"
#import "Message.h"

@implementation JourneyListWrapper
+ (NSValueTransformer*)listJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Journey class]];
}

+ (NSValueTransformer*)my_journeyJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Message class]];
}
@end
