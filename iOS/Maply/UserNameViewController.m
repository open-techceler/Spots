//
//  UserNameViewController.m
//  Maply
//
//  Created by admin on 2/9/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "UserNameViewController.h"
#import "UserAPI.h"
#import "NSError+Status.h"
#import "AddFriendsViewController.h"
#import "InviteFriendsViewController.h"
#import "FacebookManager.h"

@interface UserNameViewController ()
@property(nonatomic,weak)IBOutlet UILabel *lblCreate,*lblGood;
@property(nonatomic,weak)IBOutlet UITextField *textfield;
@property(nonatomic,weak)IBOutlet RNLoadingButton *btnGo;
@property(nonatomic,weak)IBOutlet UIView *inputView;
@property (nonatomic,strong)  UIActivityIndicatorView *activity;
@property (nonatomic,strong)  UIImageView *imgCheck;
@property(nonatomic,strong)IBOutlet NSLayoutConstraint *topContraint;

@property BOOL isValidUserName;
@end

@implementation UserNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    if(SCREEN_SIZE.height==480)
        self.topContraint.constant=50;
    else
        self.topContraint.constant=208;
    
    [self setFontAndColor];
    
    self.imgCheck=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    self.activity=[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];

    
    self.textfield.rightView=self.activity;
    self.textfield.rightViewMode = UITextFieldViewModeAlways;
    
    self.textfield.leftView = self.imgCheck;
    self.textfield.leftViewMode = UITextFieldViewModeAlways;
    
    self.textfield.inputAccessoryView=_inputView;
    [self.textfield becomeFirstResponder];
     self.btnGo.enabled=NO;
    self.isValidUserName=NO;
    
   

}

-(void)setFontAndColor
{
    self.lblCreate.font=FONT_AVENIR_PRO_REGULAR_H8;
    self.lblCreate.text=MPLocalizedString(@"create_a_username", nil);

    self.lblGood.font=FONT_AVENIR_PRO_DEMI_H1;
    self.lblGood.text=@"";
    
    self.btnGo.titleLabel.font=FONT_AVENIR_PRO_DEMI_H5;
    [self.btnGo setTitle:MPLocalizedString(@"go", nil) forState:UIControlStateNormal];

}

-(IBAction)clickGo:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    if([Utility isEmpty:self.textfield.text])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"pls_add_username", nil)];
        return;
    }
    
    if(self.isValidUserName==NO)
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"pls_enter_valid_username", nil)];
        return;
    }
    
    [self updateUserNameOnServer];
  
}

-(void)checkIfValidUserName
{
    if(![Utility isEmpty:self.textfield.text] && self.textfield.text.length>=5)
    {
        [self.activity startAnimating];
        [[UserAPI new]checkUsername:self.textfield.text success:^(BOOL status) {
            [self stopActivity];
             self.imgCheck.image=[UIImage imageNamed:@"tick"];
            self.btnGo.enabled=YES;
            self.isValidUserName=YES;
            self.lblGood.text=MPLocalizedString(@"good_to_go", nil);
        } failure:^(NSError *error) {
            self.imgCheck.image=[UIImage imageNamed:@"icon_report"];
             [self stopActivity];
            self.btnGo.enabled=NO;
             self.isValidUserName=NO;
            self.lblGood.text=MPLocalizedString(@"pls_enter_valid_username", nil);
        }];
    }
    else
    {
        self.imgCheck.image=[UIImage imageNamed:@"icon_report"];
        self.btnGo.enabled=NO;
        self.isValidUserName=NO;
        self.lblGood.text=MPLocalizedString(@"pls_enter_valid_username", nil);
       [self stopActivity];
    }
}
-(void)stopActivity
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 1.0f * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self.activity stopAnimating];

    });

}

-(void)updateUserNameOnServer
{
    self.btnGo.loading=YES;
    [[UserAPI new]updateUsernameOnServer:self.textfield.text success:^(BOOL status) {
        self.btnGo.loading=NO;
        //[UIAppDelegate initMainAppFlow];
        
        [FacebookManager getFacebookFriendsWithSuccess:^(NSMutableArray *lsFriends) {
            if (lsFriends.count == 0)
                [self goInviteView];
            else
                [self goAddFriendsView];
        } failure:^(NSError *error) {
            [self goInviteView];
        }];

    } failure:^(NSError *error) {
          self.btnGo.loading=NO;
        [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
    }];
}

- (void) goAddFriendsView {
    //go to add friends view
    AddFriendsViewController *cnt=[[AddFriendsViewController alloc]initWithNibName:@"AddFriendsViewController" bundle:nil];
    cnt.bFirstSetup = true;
    [self.navigationController pushViewController:cnt animated:YES];
}

- (void) goInviteView {
    InviteFriendsViewController* viewCon = [[InviteFriendsViewController alloc] initWithNibName:@"InviteFriendsViewController" bundle:nil];
    [self.navigationController pushViewController:viewCon animated:YES];
}

#pragma mark- UITextfieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self
                                             selector:@selector(checkIfValidUserName)
                                               object:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.2f * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [self checkIfValidUserName];
    });
    
    return YES;

}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self clickGo:nil];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
