//
//  Utility.h
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 15/09/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationClass.h"

@interface Utility : NSObject



+ (BOOL)isEmpty:(NSString*)val;

+(BOOL)isIOS7;
+(BOOL)isIOS8;
+(BOOL)isIOS9;
+(void)logoutFromApp;
+(NSMutableArray *)getSectionTitleArray;
+(void)getAddressFromLatLngSuccess:(void (^)(CLPlacemark *address))success;
+(NSString *)getCityCountryFromPlacemark:(CLPlacemark *) placemark;
+(UIImage *)thumbnailImageFromURL:(NSURL *)videoURL;
+(NSString *)getCountdownText:(NSString*)valid_until;
+(UIImage *)fixOrientationOfImage:(UIImage *)image;
+(UIImage *)bannerImageFrom:(UIImage*)image;
+(UIImage *)cropImage:(UIImage*)image toRect:(CGRect)rect;
@end
