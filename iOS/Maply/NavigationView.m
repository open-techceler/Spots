//
//  NavigationView.m
//  VivinoV2
//
//  Created by Admin on 24/07/13.
//
//

#import "NavigationView.h"
#import "Utility.h"


@interface NavigationView ()

@property (nonatomic, strong) NSString* strMainTitle;
@property (nonatomic)BOOL isModelView;
@end

@implementation NavigationView

- (id)init
{
    self = [super init];
    _isModelView=NO;
    self.lblTitle = [[UILabel alloc] init];
    self.lblTitle.translatesAutoresizingMaskIntoConstraints=YES;
    self.lblTitle.backgroundColor=[UIColor clearColor];
    self.lblTitle.frame = TITLE_FRAME;
    self.lblTitle.font = FONT_NAV_TITLE;
    self.lblTitle.textAlignment = NSTextAlignmentCenter;
    self.lblTitle.textColor = UICOLOR_NAV_TITLE;
   [self addSubview:_lblTitle];

    self.activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activityView.hidden=YES;
    [self addSubview:self.activityView];
    
   
    return self;
}

- (void)setBtnLeft:(RNLoadingButton*)btnLeft
{
    if (_btnLeft != nil)
        [_btnLeft removeFromSuperview];

    _btnLeft = btnLeft;
    _btnLeft.exclusiveTouch = YES;

    [self addSubview:_btnLeft];
    _btnLeft.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)setBtnRight:(RNLoadingButton*)btnRight
{
    if (_btnRight != nil)
        [_btnRight removeFromSuperview];

    _btnRight = btnRight;
    _btnRight.exclusiveTouch = YES;

    [self addSubview:_btnRight];
    _btnRight.translatesAutoresizingMaskIntoConstraints = NO;

}

- (void)addSubtitleWithText:(NSString*)subTitle withFont:(UIFont*)fontSize
{
    if (_strMainTitle == nil && _lblTitle.text.length)
        self.strMainTitle = _lblTitle.text;

    [self setTitleLabelMultiLine];

  //https://tickets.vivino.com/issues/10895
    // check for strMainTitle it might be nil for some cases
    NSMutableAttributedString* str = nil;
    
  
    if ([Utility isEmpty:subTitle])
        subTitle=@"";
    
    if ([Utility isEmpty:self.strMainTitle])
        self.strMainTitle=@"";
    
    if(_strMainTitle && ![Utility isEmpty:_strMainTitle])
      str =  [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@", self.strMainTitle, subTitle]];
    else
         str =  [[NSMutableAttributedString alloc] initWithString: subTitle];
    
    
    //set username font
    NSDictionary* userAttributes =
        @{ NSForegroundColorAttributeName : [UIColor darkGrayColor],
           NSFontAttributeName : fontSize }; 

    [str setAttributes:userAttributes range:[str.string rangeOfString:subTitle]];
    _lblTitle.attributedText = str;
}

-(void)makeRightButtonTextTwolines
{
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[_btnRight(==80)]"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings(_btnRight)]];
    
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_btnRight(==40)]"
                                                                 options:0
                                                                 metrics:nil
                                                                   views:NSDictionaryOfVariableBindings(_btnRight)]];
    
    
     self.btnRight.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
     self.btnRight.titleLabel.textAlignment = NSTextAlignmentCenter;
 
    //comment out this line to make the button text right aligned
     [self.btnRight setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];

}

- (void)setNavigationLayout:(BOOL)isModelView
{
    //self.backgroundColor = [UIColor yellowColor];
    _isModelView=isModelView;
    
    _lblTitle.translatesAutoresizingMaskIntoConstraints = NO;
    _lblTitle.numberOfLines=2;
    CGFloat modelViewConstant = 5.0;
    if (!isModelView)
        modelViewConstant = [Utility isIOS9]? 47.0 :30.0; // added offset for ios 9

    if (_btnLeft.imageView.image == nil) //_btnLeft.imageView.image ---back button condition)
        modelViewConstant -= ([Utility isIOS9]?10:5);

    if (_btnLeft != nil) {
        [self.btnLeft setContentHuggingPriority:100
                                        forAxis:UILayoutConstraintAxisHorizontal];
        [self.btnLeft setContentCompressionResistancePriority:UILayoutPriorityRequired
                                                      forAxis:UILayoutConstraintAxisHorizontal];

        // btnLeft left spacing
        NSLayoutConstraint* consLeftbtnSpace = [NSLayoutConstraint constraintWithItem:self.btnLeft attribute:NSLayoutAttributeLeading
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self
                                                                            attribute:NSLayoutAttributeLeading
                                                                           multiplier:1.0
                                                                             constant:-modelViewConstant];

        [self addConstraint:consLeftbtnSpace];

        // btnLeft Y
        NSLayoutConstraint* consLeftbtnY = [NSLayoutConstraint constraintWithItem:self.btnLeft attribute:NSLayoutAttributeCenterY
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:self
                                                                        attribute:NSLayoutAttributeCenterY
                                                                       multiplier:1.0
                                                                         constant:0];

        [self addConstraint:consLeftbtnY];

      
        
        NSLayoutConstraint* conSpacingLeft = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeLeft
                                                                          relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                             toItem:self.btnLeft
                                                                          attribute:NSLayoutAttributeRight
                                                                         multiplier:1.0
                                                                           constant:5];

        [self addConstraint:conSpacingLeft];

    } else {
        NSLayoutConstraint* conSpacingRight = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeLeading
                                                                           relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                              toItem:self
                                                                           attribute:NSLayoutAttributeLeading
                                                                          multiplier:1.0
                                                                            constant:5];

        [self addConstraint:conSpacingRight];

        [self.lblTitle setContentCompressionResistancePriority:750 forAxis:UILayoutConstraintAxisHorizontal];
    }

    if (_btnRight != nil) {

        [self.btnRight setContentHuggingPriority:100
                                         forAxis:UILayoutConstraintAxisHorizontal];
        [self.btnRight setContentCompressionResistancePriority:UILayoutPriorityRequired
                                                       forAxis:UILayoutConstraintAxisHorizontal];

        // btnRight Right spacing
        NSLayoutConstraint* consRightbtnSpace = [NSLayoutConstraint constraintWithItem:self.btnRight attribute:NSLayoutAttributeTrailing
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self
                                                                             attribute:NSLayoutAttributeTrailing
                                                                            multiplier:1.0
                                                                              constant:(_btnRight.imageView.image != nil ? 5 : 0)];

        [self addConstraint:consRightbtnSpace];

        // btnRight Y
        NSLayoutConstraint* consRightbtnY = [NSLayoutConstraint constraintWithItem:self.btnRight attribute:NSLayoutAttributeCenterY
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self
                                                                         attribute:NSLayoutAttributeCenterY
                                                                        multiplier:1.0
                                                                          constant:0];

        [self addConstraint:consRightbtnY];

        
       
        
        //title width calculate with btnRight and btnLeft
        NSLayoutConstraint* conTitleWidthRight = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeRight
                                                                              relatedBy:NSLayoutRelationLessThanOrEqual
                                                                                 toItem:self.btnRight
                                                                              attribute:NSLayoutAttributeLeft
                                                                             multiplier:1.0
                                                                               constant:-5];

        [self addConstraint:conTitleWidthRight];

        ////

        [self setTitleLabelMultiLine];

    } else {
        NSLayoutConstraint* conSpacingLeft = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeTrailing
                                                                          relatedBy:NSLayoutRelationLessThanOrEqual
                                                                             toItem:self
                                                                          attribute:NSLayoutAttributeTrailing
                                                                         multiplier:1.0
                                                                           constant:0];

        [self addConstraint:conSpacingLeft];

        [self.lblTitle setContentCompressionResistancePriority:750 forAxis:UILayoutConstraintAxisHorizontal];
    }

    //  [self.lblTitle setContentHuggingPriority:99
    //                                    forAxis:UILayoutConstraintAxisHorizontal];

    // | - 5px - btnLeft ---- title ---- btnRight - 5px - |
    // to have title alined in center, need to consider this factor for available width calculation
    //  for the pushed controllers on iOS 7 and onwards we have an extra padding from the left , so consider that padding while calculating center offset
    //keep the header center align
   // **float centerConstant = isModelView ? 0 : [Utility isIOS_7] ? (modelViewConstant - (_btnLeft == nil ? 30.0 : 20.0)) : 0;
    
   /*float offset =([Utility isIOS9] ?
                  (_btnLeft == nil ? 45.0 : 32.0) :
                  (_btnLeft == nil ? 30.0 : 20.0)) ;*/
    float offset =([Utility isIOS9] ?
                   (_btnLeft == nil ? 40.0 : 27.0) :
                   (_btnLeft == nil ? 30.0 : 18.0)) ;
    
    
    float centerConstant = isModelView ? 0 : modelViewConstant - offset;

    NSLayoutConstraint* conCenter = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0
                                                                  constant:-centerConstant];
    [conCenter setPriority:250];
    [self addConstraint:conCenter];

    NSLayoutConstraint* conTitleY = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0];
    
    [self addConstraint:conTitleY];

}

- (void)setTitleLabelWidth:(BOOL)isModelView
{

    float w = [self.btnLeft systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].width + [self.btnRight systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].width;
    w = 320 - w;
    if (!isModelView)
        w -= 35.0;
    else
        w -= 10;

    self.lblTitle.preferredMaxLayoutWidth = w;
}

- (void)setTitleLabelMultiLine
{

    self.lblTitle.numberOfLines = 2;
    [self.lblTitle setContentHuggingPriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
    [self.lblTitle setContentCompressionResistancePriority:UILayoutPriorityDefaultLow forAxis:UILayoutConstraintAxisVertical];
}

- (void)setNavigationLayoutXXX:(BOOL)isModelView
{
    _isModelView=isModelView;
    _lblTitle.translatesAutoresizingMaskIntoConstraints = NO;

    CGFloat modelViewConstant = 5.0;
    if (!isModelView && [Utility isIOS7])
        modelViewConstant = 35.0;

    if (_btnLeft.imageView.image == nil) //_btnLeft.imageView.image ---back button condition)
        modelViewConstant -= 5;

    if (_btnLeft != nil) {
        // btnLeft left spacing
        NSLayoutConstraint* consLeftbtnSpace = [NSLayoutConstraint constraintWithItem:self.btnLeft attribute:NSLayoutAttributeLeading
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self
                                                                            attribute:NSLayoutAttributeLeading
                                                                           multiplier:1.0
                                                                             constant:-modelViewConstant];

        [self addConstraint:consLeftbtnSpace];

        // btnLeft Y
        NSLayoutConstraint* consLeftbtnY = [NSLayoutConstraint constraintWithItem:self.btnLeft attribute:NSLayoutAttributeCenterY
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:self
                                                                        attribute:NSLayoutAttributeCenterY
                                                                       multiplier:1.0
                                                                         constant:0];

        [self addConstraint:consLeftbtnY];

        NSLayoutConstraint* conTitleWidthLeft = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeLeft
                                                                             relatedBy:NSLayoutRelationGreaterThanOrEqual
                                                                                toItem:self.btnLeft
                                                                             attribute:NSLayoutAttributeRight
                                                                            multiplier:1.0
                                                                              constant:5];

        //  [conTitleWidthLeft setPriority:UILayoutPriorityDefaultLow];

        [self addConstraint:conTitleWidthLeft];
    }

    if (_btnRight != nil) {
        // btnRight Right spacing
        NSLayoutConstraint* consRightbtnSpace = [NSLayoutConstraint constraintWithItem:self.btnRight attribute:NSLayoutAttributeTrailing
                                                                             relatedBy:NSLayoutRelationEqual
                                                                                toItem:self
                                                                             attribute:NSLayoutAttributeTrailing
                                                                            multiplier:1.0
                                                                              constant:(_btnRight.imageView.image != nil ? 5 : 0)];

        [self addConstraint:consRightbtnSpace];

        // btnRight Y
        NSLayoutConstraint* consRightbtnY = [NSLayoutConstraint constraintWithItem:self.btnRight attribute:NSLayoutAttributeCenterY
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self
                                                                         attribute:NSLayoutAttributeCenterY
                                                                        multiplier:1.0
                                                                          constant:0];

        [self addConstraint:consRightbtnY];

        //title width calculate with btnRight and btnLeft
        NSLayoutConstraint* conTitleWidthRight = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeRight
                                                                              relatedBy:NSLayoutRelationLessThanOrEqual
                                                                                 toItem:self.btnRight
                                                                              attribute:NSLayoutAttributeLeft
                                                                             multiplier:1.0
                                                                               constant:5];

        //   [conTitleWidthRight setPriority:UILayoutPriorityDefaultLow];
        [self addConstraint:conTitleWidthRight];

        [self.btnLeft setContentHuggingPriority:100
                                        forAxis:UILayoutConstraintAxisHorizontal];

        [self.btnRight setContentHuggingPriority:100
                                         forAxis:UILayoutConstraintAxisHorizontal];
    }

    //keep the header center ali
    float centerConstant = isModelView ? 0 : [Utility isIOS7] ? modelViewConstant - 20.0 : 0;

    NSLayoutConstraint* conCenter = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeCenterX
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self
                                                                 attribute:NSLayoutAttributeCenterX
                                                                multiplier:1.0
                                                                  constant:-centerConstant];
    
    
   
    // [conCenter setPriority:UILayoutPriorityDefaultHigh];
    [self setTitleLabelMultiLine];
    [self addConstraint:conCenter];
    
    NSLayoutConstraint* conTitleY = [NSLayoutConstraint constraintWithItem:self.lblTitle attribute:NSLayoutAttributeCenterY
                                                                 relatedBy:NSLayoutRelationEqual
                                                                    toItem:self
                                                                 attribute:NSLayoutAttributeCenterY
                                                                multiplier:1.0
                                                                  constant:0];
    
    [self addConstraint:conTitleY];

}

#pragma mark - Methods for Activity
-(void)showActivity:(BOOL)animated
{
    if (animated) {
        self.activityView.hidden=NO;
        [self.activityView startAnimating];
    }
    else {
        self.activityView.hidden=YES;
        [self.activityView stopAnimating];
    }
}

-(void)applyLineBreakMode:(NSLineBreakMode)lineBreakMode
{
     self.lblTitle.lineBreakMode = lineBreakMode;
}

-(void)setBackButtonAsLeftButton
{
    RNLoadingButton *btnBack=[RNLoadingButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
    btnBack.frame=BACK_BUTTON_FRAME;
    self.btnLeft=btnBack;
}

-(void)setWhiteBackButtonAsLeftButton
{
    RNLoadingButton *btnBack=[RNLoadingButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    self.btnLeft=btnBack;
}

-(void)setNextButtonRightButton
{
    RNLoadingButton *btnBack=[RNLoadingButton buttonWithType:UIButtonTypeCustom];
    [btnBack setImage:[UIImage imageNamed:@"red_next"] forState:UIControlStateNormal];
    self.btnRight=btnBack;
}

-(void)setRightButtonWithTitle:(NSString *) title font:(UIFont *) font {
    RNLoadingButton *button =[RNLoadingButton buttonWithType:UIButtonTypeCustom];
    
    [button setBackgroundColor:[UIColor clearColor]];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    if(font)
    {
        button.titleLabel.font=font;
    }

    self.btnRight=button;
}

-(void)setRightButtonWithImageName:(NSString*)imageName withTitle:(NSString*)title font:(UIFont*)font
{
    RNLoadingButton *btn =[self getButtonWithImageName:imageName title:title font:font];
    self.btnRight=btn;
}

-(void)setLeftButtonWithImageName:(NSString*)imageName withTitle:(NSString*)title font:(UIFont*)font
{
    RNLoadingButton *btn =[self getButtonWithImageName:imageName title:title font:font];
    self.btnLeft=btn;
}

-(RNLoadingButton*)getButtonWithImageName:(NSString*)imageName title:(NSString*)title font:(UIFont*)font
{
    RNLoadingButton *button =[RNLoadingButton buttonWithType:UIButtonTypeCustom];
    if(imageName)
    {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:imageName] forState:
         UIControlStateNormal];
    }
    if(title)
    {
        [button setTitle:title forState:UIControlStateNormal];
        [button setTitle:title forState:UIControlStateHighlighted];
         [button setTitleColor:UICOLOR_NAV_TITLE forState:UIControlStateNormal];

        
        if(font)
        {
            button.titleLabel.font=font;
        }
    }

    return button;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
