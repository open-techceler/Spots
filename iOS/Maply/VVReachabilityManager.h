//
//  VVReachabilityManager.h
//  VivinoV2
//
//  Created by Admin on 30/04/14.
//
//

#import <Foundation/Foundation.h>

@class Reachability;

@interface VVReachabilityManager : NSObject

@property (strong, nonatomic) Reachability *reachability;

+ (VVReachabilityManager *)sharedManager;

+ (BOOL)isReachable;
+ (BOOL)isUnreachable;
+ (BOOL)isReachableViaWWAN;
+ (BOOL)isReachableViaWiFi;

@end
