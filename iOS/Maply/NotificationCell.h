//
//  NotificationCell.h
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Notification;

typedef NS_ENUM(NSInteger, NotificationCellType)
{
    NotificationCellMaplyNews,
    NotificationCellMessage,
    NotificationCellFriendRequest,
    NotificationCellSound,
    NotificationCellLiveRequest,
    NotificationCellLiveUpdate,
    NotificationCellFriendNearby,
};

@interface NotificationCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *lblTitle;
@property(nonatomic,weak) IBOutlet UISwitch *vwSwitch;
@property(nonatomic) NotificationCellType cellType;

-(void)hideLastCellSperatorIfNeeded:(NSInteger)row_count index:(NSInteger)index;
-(void)configureCellWithObj:(Notification *)obj indexPath:(NSIndexPath *)indexPath;
@end
