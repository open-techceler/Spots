//
//  JourneyCell.h
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"


@interface JourneyCell : UITableViewCell
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgUser,*imgBanner;
@property(nonatomic,weak)IBOutlet UILabel *lblTitle,*lblMoments,*lblName;
@property(nonatomic,weak)IBOutlet UIImageView *imgArrow;
@property(nonatomic,weak)IBOutlet UIButton *btnMyJourney;
@property (weak, nonatomic) IBOutlet BaseImageViewWithData *imgPreviewLoader;

- (void)showImagePreviewLoader;
- (void)hideImagePreviewLoader;

@end
