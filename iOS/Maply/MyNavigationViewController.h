//
//  MyNavigationViewController.h
//  VivinoV2
//
//  Created by Admin on 24/07/13.
//
//

#import <UIKit/UIKit.h>
#import "NavigationView.h"
@class NoInternetView;
@interface MyNavigationViewController : UIViewController

@property(nonatomic,strong) NavigationView *Nav;
@property(nonatomic,strong) NoInternetView *vwOffline;
@property BOOL isControllerPopout;
-(void)addAnExplicitBackButton;
-(void)removeExplicitBackButton;
-(void)hideExplicitBackButton;
-(void)showExplicitBackButton;

/**
 *  Returns CGRect that is centered in the navigation bar
 *
 *  @return CGRect
 */
- (CGRect)navigationBarCenteredRect;
#pragma marks --Offline View functions
-(void)initOfflineView;
-(void)initOfflineViewWithFrame:(CGRect)frame;
-(void)showOfflineView:(BOOL)show error:(NSError*)error;

#pragma marks --Activity View functions
-(void)initLoaderActivityWithFrame:(CGRect)frame;
-(void)initLoaderActivity;
-(void)startLoadingActivity;
-(void)stopLoadingActivity;


#pragma mark- Empty Data Label
-(void)initEmptyDataLabel;
-(void)showEmptyDataLabelWithMsg:(NSString *)message;
-(void)initEmptyDataLabelWithFrame:(CGRect)frame;
- (void) showEmptyDataImageViewWithImage:(UIImage *) image;

-(void)showLocationView;
-(void)hideLocationView;
@end
