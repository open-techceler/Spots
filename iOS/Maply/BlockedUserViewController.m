//
//  BlockedUserViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "BlockedUserViewController.h"
#import "BlockedCell.h"
#import "UserAPI.h"
#import "Friend.h"
#import "FriendsListWrapper.h"

@interface BlockedUserViewController ()
@property(nonatomic,weak)IBOutlet UITableView *tableView;

@property(nonatomic,strong) NSMutableArray *arrayList,*arrayActions;
@property(nonatomic)BOOL server_request_inprogress;
@property(nonatomic)NSInteger next_offset;
@property (nonatomic,strong)UIActivityIndicatorView *activityLoadMore;

@end

@implementation BlockedUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setFontAndColor];
    [self setUpNavigationBar];
    [self initEmptyDataLabel];
    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    [self initLoaderActivity];
    
    self.arrayList=[NSMutableArray new];
    self.arrayActions=[NSMutableArray new];
    
    self.tableView.hidden=YES;
    [self startLoadingActivity];
    [self getBlockedFriendsList];
    
}

-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"blocked_users", nil);
    [self.Nav setBackButtonAsLeftButton];
    [self.Nav setNavigationLayout:NO];
    
}
-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG;
}

-(void)viewDidDisappear:(BOOL)animated
{
    
}

-(void)updateUserActionsOnServer{
    if(self.arrayActions.count)
    {
        [[UserAPI new]updateUserAction:self.arrayActions success:^(FriendsListWrapper *objWrapper) {
            
        } failure:^(NSError *error) {
            
        }];
    }
}


-(void)getBlockedFriendsList
{
    self.server_request_inprogress=YES;
    
    [[UserAPI new]getBlockedFriendsList:self.next_offset success:^(FriendsListWrapper *objWrapper) {
        
        if(self.next_offset==0)
            [self.arrayList removeAllObjects];
        [self.arrayList addObjectsFromArray:objWrapper.list];
        
        self.tableView.hidden=NO;
        [self.tableView reloadData];
        
        self.next_offset=objWrapper.next_offset;
        
        self.server_request_inprogress=NO;
        [self stopLoadingActivity];
        [self setActivityInTableFooter:NO];
        
        if(self.arrayList.count)
        {
             self.tableView.hidden=NO;
        }
        else
        {
            self.tableView.hidden=YES;
            [self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_record_found", nil)];
        }

        
    } failure:^(NSError *error) {
        self.server_request_inprogress=NO;
        [self stopLoadingActivity];
        [self setActivityInTableFooter:NO];
        [self showOfflineView:YES error:error];
        
    }];
}

-(IBAction)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        
        self.next_offset=0;
        self.tableView.hidden=YES;
        [self startLoadingActivity];
        [self getBlockedFriendsList];
        
    }
}

#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    static NSString* MyIdentifier = @"BlockedCell";
    BlockedCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"BlockedCell" owner:self options:nil] objectAtIndex:0];
        [cell.btnBlock  addTarget:self action:@selector(clickBlock:) forControlEvents:UIControlEventTouchUpInside];

    }
    
    Friend *obj=[self.arrayList objectAtIndex:indexPath.row];
    cell.btnBlock.tag=indexPath.row;
    
    
    if([obj.status_type isEqualToString:kStatusTypeBlock])
        [cell.btnBlock setTitle:MPLocalizedString(@"unblock", nil) forState:UIControlStateNormal];
    else
        [cell.btnBlock setTitle:MPLocalizedString(@"block", nil) forState:UIControlStateNormal];
    
    cell.lblFullName.text=obj.name;
    cell.lblUserName.text=obj.username;
    
    [cell.imgUser getImageWithURL:obj.image comefrom:ImageViewComfromUser];

    
    return cell;
    
}

-(void)clickBlock:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    UIButton *btn=(UIButton *)sender;
    Friend *obj=[self.arrayList objectAtIndex:btn.tag];
    
    if([obj.status_type isEqualToString:kStatusTypeBlock])
        obj.status_type=kStatusTypeNone;
    else
        obj.status_type=kStatusTypeBlock;
    
    NSArray *resultArray = [self.arrayActions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"friend_id = %ld",obj.friend_id]];
    if(resultArray.count)
    {
        Friend *objAdded=[resultArray firstObject];
        objAdded.status_type=obj.status_type;
    }
    else
    {
        Friend *objSelected=[Friend new];
        objSelected.friend_id=obj.friend_id;
        objSelected.status_type=obj.status_type;
        [self.arrayActions addObject:objSelected];
        
    }
    [self.tableView reloadData];
    [self updateUserActionsOnServer];
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

#pragma mark -- Load More methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    if (self.server_request_inprogress == NO && self.next_offset!=-1) {
        CGPoint offset = self.tableView.contentOffset;
        CGRect bounds = self.tableView.bounds;
        CGSize size = self.tableView.contentSize;
        
        UIEdgeInsets inset = self.tableView.contentInset;
        
        float y = offset.y + bounds.size.height - inset.bottom;
        float h = size.height;
        
        CGFloat reload_distance = 50;
        
        if (y + reload_distance > h ) {
            [self setActivityInTableFooter:YES];
            self.server_request_inprogress = YES;
            
            double delayInSeconds = 0.5f;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self getBlockedFriendsList];
            });
            
            return;
        }
    }
}

-(void)addLoadMoreActivityasFooter
{
    self.activityLoadMore =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityLoadMore.hidden=YES;
    self.activityLoadMore.frame = CGRectMake(0, 0, SCREEN_SIZE.width, 44);
    self.tableView.tableFooterView=self.activityLoadMore;
}

-(void)setActivityInTableFooter:(BOOL)show
{
    if(show)
    {
        self.activityLoadMore.hidden=NO;
        [self.activityLoadMore startAnimating];
        self.tableView.tableFooterView=self.activityLoadMore;
    }
    else
    {
        self.activityLoadMore.hidden=YES;
        [self.activityLoadMore stopAnimating];
        self.tableView.tableFooterView=nil;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
