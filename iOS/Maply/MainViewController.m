//
//  MainViewController.m
//  Maply
//
//  Created by admin on 5/3/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MainViewController.h"
#import "MessagesMainViewController.h"
#import "HomeViewViewController.h"
#import "JourneysViewController.h"
#import "FriendsMainViewController.h"
#import <Masonry/Masonry.h>
#import "AddFriendsViewController.h"

@interface MainViewController ()<UIPageViewControllerDelegate,UIPageViewControllerDataSource>
@property(nonatomic,strong)UIPageViewController *pageViewController;
@property (nonatomic, strong) NSMutableArray *viewControllerArray;
@property (nonatomic) BOOL isPageScrollingFlag; //%%% prevents scrolling / segment tap crash

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initPageViewController];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)setSwipeEnabled:(BOOL)swipeEnabled
{
    _swipeEnabled=swipeEnabled;
    for (UIScrollView *view in self.pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.scrollEnabled = _swipeEnabled;
            break;
        }
    }
}

-(void)initPageViewController

{
     _pageViewController=[[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        _pageViewController.delegate=self;
        _pageViewController.dataSource=self;
        _pageViewController.view.backgroundColor=UICOLOR_APP_BG;
    
    MessagesMainViewController  *cnt = [[MessagesMainViewController alloc]initWithNibName:@"MessagesMainViewController" bundle:nil];
    UINavigationController *leftnav = [[UINavigationController alloc]initWithRootViewController:cnt];
    
    HomeViewViewController *cntCenter =[[HomeViewViewController alloc] initWithNibName:@"HomeViewViewController" bundle:nil];
    UINavigationController *navCenter = [[UINavigationController alloc]initWithRootViewController:cntCenter];
    navCenter.navigationBarHidden=YES;

    JourneysViewController  *cntJourney = [[JourneysViewController alloc]initWithNibName:@"JourneysViewController" bundle:nil];
    UINavigationController *rightnav  = [[UINavigationController alloc]initWithRootViewController:cntJourney];
    
    FriendsMainViewController *cntFriend=[[FriendsMainViewController alloc]initWithNibName:@"FriendsMainViewController" bundle:nil];
     UINavigationController *frndtnav  = [[UINavigationController alloc]initWithRootViewController:cntFriend];
    
    /*AddFriendsViewController *cntAddfrnd=[[AddFriendsViewController alloc]initWithNibName:@"AddFriendsViewController" bundle:nil];
     UINavigationController *addFrndtnav  = [[UINavigationController alloc]initWithRootViewController:cntAddfrnd];*/

   
    self.viewControllerArray=[NSMutableArray new];
    [self.viewControllerArray addObjectsFromArray:@[leftnav,navCenter,rightnav,frndtnav]];
    
    self.currentPageIndex=kHomeViewViewController;
    NSArray *viewControllers = @[[_viewControllerArray objectAtIndex:self.currentPageIndex]];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            
    // Change the size of page view controller
    _pageViewController.view.frame=CGRectMake(0,0 , SCREEN_SIZE.width, SCREEN_SIZE.height);
        
    [self.view addSubview:_pageViewController.view];
    [self.view sendSubviewToBack:_pageViewController.view];
        
    [_pageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(@0);
            make.right.equalTo(@0);
            make.top.equalTo(@0);
            make.bottom.equalTo(@0);
    }];
            
//    for (UIView *view in self.pageViewController.view.subviews) {
//                if ([view isKindOfClass:[UIScrollView class]]) {
//                    ((UIScrollView *)view).delegate = self;
//                    break;
//            }
//    }
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger index = [_viewControllerArray indexOfObject:viewController];
    
    if ((index == NSNotFound) || (index == 0)) {
        return nil;
    }
    
    index--;
    return [_viewControllerArray objectAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger index = [_viewControllerArray indexOfObject:viewController];
    
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    
    if (index == [_viewControllerArray count]) {
        return nil;
    }
    return [_viewControllerArray objectAtIndex:index];
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (completed) {
         NSInteger tempCurrentIndex = [_viewControllerArray indexOfObject:[pageViewController.viewControllers lastObject]];
        
        //if moving forward
        if(tempCurrentIndex>self.currentPageIndex)
        {
            if(tempCurrentIndex==kFriendsMainViewController)
            {
                /*UINavigationController *nav=[pageViewController.viewControllers lastObject];
                FriendsMainViewController *cnt=[nav.viewControllers firstObject];
                [cnt refreshFriendsList];*/
//                [self reloadFriendListIfNeeded];
            }
          /*  else if (tempCurrentIndex==kAddFriendsViewController)
            {
                UINavigationController *nav=[pageViewController.viewControllers lastObject];
                AddFriendsViewController *cnt=[nav.viewControllers firstObject];
                [cnt refreshFriendsList];
            }*/
        }
        self.currentPageIndex=tempCurrentIndex;
    }
}

- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers
{
    
}
-(void)tapSegmentButtonAction:(UIButton *)button {
    
    if (!self.isPageScrollingFlag) {
        
        NSInteger tempIndex = self.currentPageIndex;
        
        __weak typeof(self) weakSelf = self;
        
        //%%% check to see if you're going left -> right or right -> left
        if (button.tag > tempIndex) {
            
            //%%% scroll through all the objects between the two points
           // for (int i = (int)tempIndex+1; i<=button.tag; i++) {
                [_pageViewController setViewControllers:@[[_viewControllerArray objectAtIndex:button.tag]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL complete){
                    
                    //%%% if the action finishes scrolling (i.e. the user doesn't stop it in the middle),
                    //then it updates the page that it's currently on
                    if (complete) {
                        [weakSelf updateCurrentPageIndex: (int)button.tag];
                    }
                }];
           // }
        }
        
        //%%% this is the same thing but for going right -> left
        else if (button.tag < tempIndex) {
            for (int i = (int)tempIndex-1; i >= button.tag; i--) {
                [_pageViewController setViewControllers:@[[_viewControllerArray objectAtIndex:i]] direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:^(BOOL complete){
                    if (complete) {
                        [weakSelf updateCurrentPageIndex:i];
                    }
                }];
            }
        }
    }
}

//%%% makes sure the nav bar is always aware of what page you're on
//in reference to the array of view controllers you gave
-(void)updateCurrentPageIndex:(int)newIndex {
    self.currentPageIndex = newIndex;
}


-(void)setCurrentIndex:(NSUInteger)currentIndex
{
    _currentIndex=currentIndex;
    UIButton *btn=[UIButton new];
    btn.tag=_currentIndex;
    [self tapSegmentButtonAction:btn];
}

-(void)popToMapView
{
    self.swipeEnabled=YES;
    if(self.currentPageIndex==kMessagesMainViewController)
    {
        UINavigationController *navMessage=[_viewControllerArray objectAtIndex:kMessagesMainViewController];
        [navMessage popToRootViewControllerAnimated:NO];
    }
    else  if(self.currentPageIndex==kFriendsMainViewController)
    {
            UINavigationController *navfriend=[_viewControllerArray objectAtIndex:kFriendsMainViewController];
            [navfriend popToRootViewControllerAnimated:NO];
    }
    
    self.currentPageIndex=kHomeViewViewController;
    NSArray *viewControllers = @[[_viewControllerArray objectAtIndex:self.currentPageIndex]];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
}

-(void)reloadFriendListIfNeeded
{
    UINavigationController *nav=[_pageViewController.viewControllers lastObject];
    FriendsMainViewController *cnt=[nav.viewControllers firstObject];
    [cnt refreshFriendsList];
}
-(HomeViewViewController *)HomeViewInstance
{
    UINavigationController *navHome=[_viewControllerArray objectAtIndex:kHomeViewViewController];
    HomeViewViewController *home=[navHome.viewControllers firstObject];
    return home;
}
#pragma mark - Scroll View Delegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.isPageScrollingFlag = YES;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    self.isPageScrollingFlag = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
