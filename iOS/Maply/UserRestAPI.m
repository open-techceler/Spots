//
//  UserRestAPI.m
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 04/10/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import "UserRestAPI.h"

@implementation UserRestAPI

- (id)init
{
    self = [super init];
    if (self) {
        [self.pathComponents addObject:@"users"];
    }
    return self;
}

- (AFHTTPRequestOperation*)GETAction:(APIRestAction)action
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    
    return [super GETAction:action success:success failure:failure];
}

- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)action
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    
    return [super POSTAction:action success:success failure:failure];
}

- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)action
                           endDynamic:(NSString*)path
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    [self.pathComponents addObject:path];
    
    return [super POSTAction:action success:success failure:failure];
}

- (AFHTTPRequestOperation*)PUTAction:(APIRestAction)action
                          endDynamic:(NSString*)path
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self.pathComponents addObject:path];
    [self setupPathComponents:action];
    return [super PUTsuccess:success failure:failure];
}


- (AFHTTPRequestOperation*)GETAction:(APIRestAction)action
                          endDynamic:(NSString*)path
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    [self.pathComponents addObject:path];
    return [super GETAction:action success:success failure:failure];
}

- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)task
            constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> formData))block
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:task];
    return [super POSTconstructingBodyWithBlock:block success:success failure:failure];
}


- (AFHTTPRequestOperation*)PUTAction:(APIRestAction)action
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    return [super PUTsuccess:success failure:failure];
}

- (AFHTTPRequestOperation*)DELETEAction:(APIRestAction)action
                             endDynamic:(NSString*)person_id
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure
{
    [self setupPathComponents:action];
    [self.pathComponents addObject:person_id];
    return [super DELETEsuccess:success failure:failure];
}

- (void)setupPathComponents:(APIRestAction)action
{
    NSArray *subPath;
    if(action==APIRestUserLogin)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        subPath=[NSArray arrayWithObject:@"login"];
    }
    else if(action==APIRestUserNotification)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        subPath=[NSArray arrayWithObject:@"notification"];
    }
    else if(action==APIRestUserUpdateUsername)
    {
        self.postBodyType = APIPostBodyTypeJSON;
    }
    else if(action==APIRestUserCheckUsername)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        subPath=[NSArray arrayWithObject:@"check"];
    }
    else if(action==APIRestUserUploadFacebookFriends)
    {
        self.postBodyType = APIPostBodyTypeRawJSON;
        subPath=[NSArray arrayWithObject:@"facebook"];
    }
    else if(action==APIRestUserUploadUserImage)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        subPath=[NSArray arrayWithObject:@"image"];
    }
    else if(action==APIRestUserUpdateUserProfile)
    {
        self.postBodyType = APIPostBodyTypeJSON;
    }
    
    else if(action==APIRestUserSearchFriends)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        subPath=[NSArray arrayWithObject:@"search"];
    }
    else if(action==APIRestUserAction)
    {
        self.postBodyType = APIPostBodyTypeRawJSON;
        subPath=[NSArray arrayWithObject:@"action"];
    }
    else if(action==APIRestUserBlock)
    {
        self.postBodyType = APIPostBodyTypeJSON;
       // subPath=[NSArray arrayWithObject:@"block"];
    }
    else if(action==APIRestUserFriends)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        // subPath=[NSArray arrayWithObject:@"friends"];
    }
    else if(action==APIRestUserRequests)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        // subPath=[NSArray arrayWithObject:@"requests"];
    }
    else if(action==APIRestUserNearby)
    {
        self.postBodyType = APIPostBodyTypeJSON;
         subPath=[NSArray arrayWithObject:@"nearby"];
    }
    else if(action==APIRestUserChat)
    {
        self.postBodyType = APIPostBodyTypeJSON;
    }
    else if(action==APIRestUserPushToken)
    {
        self.postBodyType = APIPostBodyTypeJSON;
          subPath=[NSArray arrayWithObject:@"push-token"];
    }
    else if(action==APIRestUserLocation)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        subPath=[NSArray arrayWithObject:@"location"];
    }
    else if(action==APIRestUserDirectMessages)
    {
        self.postBodyType = APIPostBodyTypeJSON;
    }
    else if(action==APIRestUserSentMessages)
    {
        self.postBodyType = APIPostBodyTypeJSON;
    }
    else if(action==APIRestUserNearbyEvent)
    {
        self.postBodyType = APIPostBodyTypeJSON;
        subPath=[NSArray arrayWithObject:@"nearby-event"];
    }
    
    if(subPath.count)
        [self.pathComponents addObjectsFromArray:subPath];
}

- (void)setupPropertiesForTask:(APIRestAction)action
{
    NSArray *subPath=nil;
    if(subPath.count)
        [self.pathComponents addObjectsFromArray:subPath];
}

@end
