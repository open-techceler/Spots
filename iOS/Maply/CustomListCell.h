//
//  CustomListCell.h
//  VivinoV2
//
//  Created by Admin on 09/10/12.
//  Copyright (c) 2012 Vivino. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomListCell : UITableViewCell


@property (nonatomic,strong)IBOutlet UILabel *lblTitle;
@property (nonatomic,strong)IBOutlet UIImageView *accessoryImageView;
@property (nonatomic,strong) IBOutlet UIView *vwSeperator;

@end
