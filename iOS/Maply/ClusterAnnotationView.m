//
//  ClusterAnnotationView.m
//  CCHMapClusterController Example iOS
//
//  Created by Hoefele, Claus(choefele) on 09.01.14.
//  Copyright (c) 2014 Claus Höfele. All rights reserved.
//

// Based on https://github.com/thoughtbot/TBAnnotationClustering/blob/master/TBAnnotationClustering/TBClusterAnnotationView.m by Theodore Calmes

#import "ClusterAnnotationView.h"
#import "BaseImageViewWithData.h"
#import "FBAnnotationCluster.h"

@interface ClusterAnnotationView ()

@property (nonatomic) UILabel *countLabel,*lblBadge;
@property (nonatomic) BaseImageViewWithData *imgUser;
@property (nonatomic) CALayer *colorHaloLayer;
@property (nonatomic) BOOL bShowHaloAnimation;
@property (nonatomic, strong) CAAnimationGroup *pulseAnimationGroup;

@property (nonatomic, readwrite) NSTimeInterval pulseAnimationDuration; // default is 1s
@property (nonatomic, readwrite) NSTimeInterval outerPulseAnimationDuration; // default is 3s
@property (nonatomic, readwrite) NSTimeInterval delayBetweenPulseCycles; // default is 1s

@end

@implementation ClusterAnnotationView

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.bShowHaloAnimation = NO;
        self.backgroundColor = [UIColor clearColor];
        self.pulseAnimationDuration = 1.5;
        self.outerPulseAnimationDuration = 3;
        self.delayBetweenPulseCycles = 0;

        [self setUpLabel];
        [self setCount:1];
        [self setUpcolorHaloLayer];
        [self setUpImage];
        [self setUpBadgeLabel];
    }
    return self;
}
-(void)setUpImage
{
    _imgUser=[[BaseImageViewWithData alloc]initWithFrame:CGRectMake(-4,-4, 44, 44)];
    _imgUser.backgroundColor=UICOLOR_APP_BG;
    _imgUser.layer.borderColor=[UIColor whiteColor].CGColor;
    _imgUser.layer.borderWidth=2.0;
    _imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    _imgUser.clipsToBounds = YES;
    
    [self addSubview:_imgUser];
}

-(void)setUpBadgeLabel
{
    
    _lblBadge = [[UILabel alloc] initWithFrame:CGRectMake(20,-6, 16, 16)];
    _lblBadge.textAlignment = NSTextAlignmentCenter;
    _lblBadge.backgroundColor = PICKER_COLOR_PINK;
    _lblBadge.textColor = [UIColor whiteColor];
    _lblBadge.textAlignment = NSTextAlignmentCenter;
    _lblBadge.adjustsFontSizeToFitWidth = YES;
    _lblBadge.minimumScaleFactor = 2;
    _lblBadge.numberOfLines = 1;
    _lblBadge.font = FONT_AVENIR_PRO_DEMI_H08;
    _lblBadge.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    _lblBadge.layer.cornerRadius=_lblBadge.frame.size.width / 2;
     _lblBadge.clipsToBounds = YES;

    [self addSubview:_lblBadge];
    _lblBadge.hidden=YES;
  
}

- (CAAnimationGroup*)pulseAnimationGroup {
    if(!_pulseAnimationGroup) {
        CAMediaTimingFunction *defaultCurve = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
        
        _pulseAnimationGroup = [CAAnimationGroup animation];
        _pulseAnimationGroup.duration = self.outerPulseAnimationDuration + self.delayBetweenPulseCycles;
        _pulseAnimationGroup.repeatCount = INFINITY;
        _pulseAnimationGroup.removedOnCompletion = NO;
        _pulseAnimationGroup.timingFunction = defaultCurve;
        
        NSMutableArray *animations = [NSMutableArray new];
        
        CABasicAnimation *pulseAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale.xy"];
        pulseAnimation.fromValue = @0.0;
        pulseAnimation.toValue = @1.0;
        pulseAnimation.duration = self.outerPulseAnimationDuration;
        [animations addObject:pulseAnimation];
        
        CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
        animation.duration = self.outerPulseAnimationDuration;
        animation.values = @[@0.45, @0.45, @0];
        animation.keyTimes = @[@0, @0.2, @1];
        animation.removedOnCompletion = NO;
        [animations addObject:animation];
        
        _pulseAnimationGroup.animations = animations;
    }
    return _pulseAnimationGroup;
}

- (void)setUpcolorHaloLayer {
    _colorHaloLayer = [CALayer layer];
    
    CGFloat fBoundSizeWidth = 44.f;
    CGFloat width = fBoundSizeWidth*3;
    _colorHaloLayer.bounds = CGRectMake(0, 0, width, width);
    _colorHaloLayer.position = CGPointMake(18.f, 18.f);
    _colorHaloLayer.contentsScale = [UIScreen mainScreen].scale;
    _colorHaloLayer.backgroundColor = [UIColor colorWithRed:236.f / 255.f green:30.f / 255.f blue:46.f / 255.f alpha:1.f].CGColor;
    _colorHaloLayer.cornerRadius = width/2;
    _colorHaloLayer.opacity = 0;
    
    [self.layer addSublayer:self.colorHaloLayer];

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void) {
        if(self.delayBetweenPulseCycles != INFINITY) {
            CAAnimationGroup *animationGroup = self.pulseAnimationGroup;
            
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [self->_colorHaloLayer addAnimation:animationGroup forKey:@"pulse"];
            });
        }
    });
}

- (void)setUpLabel
{
    _countLabel = [[UILabel alloc] initWithFrame:self.bounds];
    _countLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.backgroundColor = [UIColor clearColor];
    _countLabel.textColor = [UIColor whiteColor];
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.adjustsFontSizeToFitWidth = YES;
    _countLabel.minimumScaleFactor = 2;
    _countLabel.numberOfLines = 1;
    _countLabel.font = FONT_AVENIR_PRO_DEMI_H0;
    _countLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    
    [self addSubview:_countLabel];
}

- (void)setCount:(NSUInteger)count
{
    _count = count;
    
    self.countLabel.text = [@(count) stringValue];
    [self setNeedsLayout];
}

-(void)setImgUrl:(NSString *)imgUrl
{
    _imgUrl=imgUrl;
     [self.imgUser getImageWithURL:imgUrl comefrom:ImageViewComfromUser];
      [self setNeedsLayout];
}

-(void)setBadge_count:(NSUInteger)badge_count
{
    _badge_count=badge_count;
    if(_badge_count)
    {
        _lblBadge.text=[NSString stringWithFormat:@"%ld",(unsigned long)badge_count];
        _lblBadge.hidden=NO;
        [self bringSubviewToFront:_lblBadge];
    }
    else
        _lblBadge.hidden=YES;
}

- (void)layoutSubviews
{
    
     if ([self.annotation isKindOfClass:FBAnnotationCluster.class]) {
         
        self.image = [UIImage imageNamed:@"mappind.png"];
         self.countLabel.frame = self.bounds;
         self.imgUser.hidden=YES;
         self.countLabel.hidden=NO;
         self.colorHaloLayer.hidden = YES;
    } else {
        self.image = [UIImage imageNamed:@"fill.png"];
        if([self.type isEqualToString:@"EVENT"] || [self.type isEqualToString:@"JOURNEY"])
        {
            self.imgUser.hidden=YES;
            self.countLabel.hidden=YES;
            self.colorHaloLayer.hidden = YES;
        }
        else
        {
            self.imgUser.hidden=NO;
            self.countLabel.hidden=YES;
            self.colorHaloLayer.hidden = !self.bShowHaloAnimation;
        }
       
    }
}

- (void) showHaloAnimation:(BOOL) bShow {
    self.bShowHaloAnimation = bShow;
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

@end
