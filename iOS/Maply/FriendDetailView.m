//
//  FriendDetailView.m
//  Maply
//
//  Created by Admin on 28/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "FriendDetailView.h"
#import "Friend.h"

@implementation FriendDetailView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void) layoutSubviews {
    [super layoutSubviews];
    
    self.viewBattery.layer.cornerRadius = CGRectGetHeight(self.viewBattery.frame) / 2.f;
    self.viewBattery.layer.borderColor = [UIColor colorWithRed:148.f / 255.f green:148.f / 255.f blue:148.f / 255.f alpha:1.f].CGColor;
    self.viewBattery.layer.borderWidth = 1.f;
    
    self.btnMsgHey.hidden = true;
    self.btnMsgMeet.hidden = true;
    
    if (!self.friendObj) return;
    
    self.btnMsgHey.titleLabel.font=FONT_AVENIR_ROMAN_H1;
    self.btnMsgMeet.titleLabel.font=FONT_AVENIR_ROMAN_H1;
    
    self.btnMsgHey.layer.cornerRadius=4.0;
    self.btnMsgMeet.layer.cornerRadius=4.0;
    self.btnMsgHey.contentEdgeInsets=UIEdgeInsetsMake(5, 9, 5, 9);
    self.btnMsgMeet.contentEdgeInsets=UIEdgeInsetsMake(5, 9,5, 9);

    if(self.friendObj.arrNotific.count)
    {
        self.btnMsgHey.hidden=NO;
        
        NSString *msgHey=[self.friendObj.arrNotific firstObject];
        [self.btnMsgHey setTitle:msgHey.capitalizedString forState:UIControlStateNormal];
        if(self.friendObj.arrNotific.count>1)
        {
            self.btnMsgMeet.hidden=NO;
            NSString *msgMeet=[self.friendObj.arrNotific objectAtIndex:1] ;
            [self.btnMsgMeet setTitle:msgMeet.capitalizedString forState:UIControlStateNormal];
        }
        else
        {
            self.btnMsgMeet.hidden=YES;
            [self.btnMsgMeet setTitle:nil forState:UIControlStateNormal];
        }
        self.friendObj.arrNotific=nil;
    }
    else
    {
        self.btnMsgHey.hidden=YES;
        self.btnMsgMeet.hidden=YES;
        [self.btnMsgHey setTitle:nil forState:UIControlStateNormal];
        [self.btnMsgMeet setTitle:nil forState:UIControlStateNormal];
    }

    for (int nIdx = 0; nIdx < 3; nIdx++) {
        UIView* viewCanvas = (UIView *)[self viewWithTag:10 + nIdx];
        UILabel* lblTitle = (UILabel *)[self viewWithTag:20 + nIdx];
        
        if (CGRectGetWidth([UIScreen mainScreen].bounds) == 320.f) {
            lblTitle.font = [UIFont fontWithName:@"OpenSans" size:12.f];
            
            if (nIdx == 0) {
                self.constraintHeyWidth.constant = 55.f;
            } else if (nIdx == 1) {
                self.constraintMessageWidth.constant = 55.f;
            } else if (nIdx == 2) {
                self.constraintMeetUpWidth.constant = 55.f;
            }
        }

        [viewCanvas setNeedsLayout];
        [viewCanvas layoutIfNeeded];
        
        self.btnHey.layer.cornerRadius = CGRectGetHeight(self.btnHey.frame) / 2.f;
        self.btnHey.layer.borderColor = [UIColor whiteColor].CGColor;
        self.btnHey.layer.borderWidth = 2.f;
        
        self.btnMessage.layer.cornerRadius = CGRectGetHeight(self.btnMessage.frame) / 2.f;
        self.btnMessage.layer.borderColor = [UIColor whiteColor].CGColor;
        self.btnMessage.layer.borderWidth = 2.f;

        self.btnMeetUp.layer.cornerRadius = CGRectGetHeight(self.btnMeetUp.frame) / 2.f;
        self.btnMeetUp.layer.borderColor = [UIColor whiteColor].CGColor;
        self.btnMeetUp.layer.borderWidth = 2.f;
    }
}

- (void) loadUserInfo:(Friend *) obj {
    self.friendObj = obj;
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
    
    self.lblName.text= obj.name.uppercaseString;
    self.lblLocation.text = obj.full_address;

    self.imgBatterCharge.hidden = true;
    self.imgBatteryNoCharge.hidden = true;
    
    float fBatterStatus = 0.2f;
    self.lblBatteryStatus.text = [NSString stringWithFormat:@"%ld%%", (long)(fBatterStatus * 100)];
    self.constraintBatteryCapacity.constant = 13.f * fBatterStatus;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (IBAction)actionHey:(id)sender {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(sendHey:)])
        [self.delegate sendHey:self];
}

- (IBAction)actionMessage:(id)sender {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(sendMessage:)])
        [self.delegate sendMessage:self];
}

- (IBAction)actionMeetUp:(id)sender {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(sendMeetUp:)])
        [self.delegate sendMeetUp:self];
}

@end
