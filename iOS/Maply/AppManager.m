//
//  AppManager.m
//  SeatAParty
//
//  Created by anand mahajan on 10/03/15.
//  Copyright (c) 2015 anand mahajan. All rights reserved.
//
#import "AppManager.h"
#import "Constants.h"
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonDigest.h>
#import "Reachability.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "APNRegistrationHelpers.h"
static NSString *kSecretKey =@"C@bW@!@#$^&";

@implementation AppManager
@synthesize activity,currentLatLong,currentLocation,loggedinUserId,loggedinUserImage,loggedinUserName,loggedinUserEmail,loggedinUserFirstName,loggedinUserLastName;
@synthesize dictEventDetail,lsParseUsers,userHometown,selectedPanel;

static AppManager *sharedInstance = nil;

+ (AppManager *) sharedData
{
     static AppManager *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
 dispatch_once(&oncePredicate, ^{
     if (sharedInstance == nil) {
         // create shared data
         sharedInstance = [[self alloc]init];
     }
 });

    return sharedInstance;
}

#pragma mark Alert view
-(void)showHintView:(NSString *)text
{
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:MPLocalizedString(@"app_name", nil) message:text delegate:self cancelButtonTitle:MPLocalizedString(@"btn_ok", nil) otherButtonTitles:nil];
    [alert show];
}


-(UILabel *)createLableOnNavigation:(NSString *)lblTitle
{
    UILabel *navLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,20, 30, 44)];
    navLabel.backgroundColor = [UIColor clearColor];
    navLabel.text = lblTitle;
    navLabel.textColor = [UIColor whiteColor];
    navLabel.font = [UIFont fontWithName:@"AvenirNextLTPro-Demi" size:16.0];
   // navLabel.textAlignment = NSTextAlig;
    //[navLabel sizeToFit];

    //self.navigationItem.titleView = navLabel;
    return navLabel;
}
NSString* md5(NSString* concat)
{
    const char* concat_str = [concat UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(concat_str, (CC_LONG)strlen(concat_str), result);
    NSMutableString* hash = [NSMutableString string];
    for (NSInteger i = 0; i < 16; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}
#pragma mark - Check Network Availability



-(BOOL)isNetwokAvailable
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    
    if(networkStatus == NotReachable)
    {
        [[AppManager sharedData] showHintView:MPLocalizedString(@"error_message_no_network", nil)];
        return  NO;

    }
    else
        return YES;
    
    return NO;
}

+ (NSString *) md5:( NSString *)concat
{
    const char *concat_str = [concat UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(concat_str, (int)strlen(concat_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++)
        [hash appendFormat:@"%02X", result[i]];
    return [hash lowercaseString];
}

+(void)setTintColor:(RNLoadingButton *)btn
{
    [btn setActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite forState:UIControlStateNormal];
}


#pragma custom method  for email format

-(BOOL)isValidEmail: (NSString *)txt
{
    //Email Validation
    NSString *regex1 = @"\\A[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\\.)+[a-z]{2,4}\\z";
    NSString *regex2 = @"^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*";
    NSPredicate *test1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    NSPredicate *test2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex2];
    return [test1 evaluateWithObject:txt] && [test2 evaluateWithObject:txt];
    
}

+(NSString*)getLocationStrFromPlaceMarker:(CLPlacemark*)placemark
{
    NSMutableString *str = [NSMutableString new];
//    placemark.thoroughfare, placemark.locality,
//    placemark.administrativeArea,
//    placemark.country
    if(![[AppManager sharedData] isEmpty:placemark.thoroughfare])
    {
        [str appendString:placemark.thoroughfare];
    }
    
   if(![[AppManager sharedData] isEmpty:placemark.subThoroughfare])
    {
        if(str.length>0)
            [str appendString:@", "];
        
        [str appendString:placemark.subThoroughfare];
        
    }
    if(![[AppManager sharedData] isEmpty:placemark.subLocality])
    {
        if(str.length>0)
            [str appendString:@", "];
        
        [str appendString:placemark.subLocality];
        
    }

    if(![[AppManager sharedData] isEmpty:placemark.locality])
    {
        if(str.length>0)
            [str appendString:@", "];
        
        [str appendString:placemark.locality];

    }
    
    if(![[AppManager sharedData] isEmpty:placemark.administrativeArea])
    {
        if(str.length>0)
            [str appendString:@", "];

        [str appendString:placemark.administrativeArea];
    }
    
    if(![[AppManager sharedData] isEmpty:placemark.country])
    {
        if(str.length>0)
            [str appendString:@", "];

        [str appendString:placemark.country];
    }
    return [str copy];
}

+(NSString*)getLocationName:(CLPlacemark*)placemark
{
    NSMutableString *str = [NSMutableString new];
   
    if(![[AppManager sharedData] isEmpty:placemark.subLocality])
    {
        if(str.length>0)
            [str appendString:@", "];
        
        [str appendString:placemark.subLocality];
        
    }
    
    if(![[AppManager sharedData] isEmpty:placemark.locality])
    {
        if(str.length>0)
            [str appendString:@", "];
        
        [str appendString:placemark.locality];
        
    }
   
    return [str copy];
}

#pragma GetServiceType form ServiceName

+(ServiceLoginType)getServiceTypeWithServiceName:(NSString*)str
{
    str=[str lowercaseString];
    if([str isEqualToString:@"mega"])
    {
        return ServiceLoginTypeMEGACAB;
    }
    if([str isEqualToString:@"ola"])
    {
        return ServiceLoginTypeOLA;
    }
    if([str isEqualToString:@"meru"])
    {
        return ServiceLoginTypeMERU;
    }
    if([str isEqualToString:@"tabcab"])
    {
        return ServiceLoginTypeTABCAB;
    }
    else
        return ServiceLoginTypeEASYCAB;
}

-(BOOL)isEmpty:(NSString *)str
{
    if(str.length==0)
        return YES;
    return NO;
}


-(NSString *)getStringFromDate :(NSDate *)date withFormat:(NSString *)frmt
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:frmt];
    NSString *stringFromDate = [dateFormatter stringFromDate:date];
    return stringFromDate;
}

-(NSDate *)getDateFromString:(NSString *)strdate withFormat:(NSString *)frmt
{
    NSDate *dt;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:frmt];
    dt = [dateFormatter dateFromString:strdate];
    return dt;
    
}

-(NSString *)getDateInStringFromString:(NSString *)str withCurrentFormat:(NSString *)currentFrmt withExpectedFrmt:(NSString *)expctedFrmt
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:currentFrmt];
    NSDate *date = [dateFormatter dateFromString:str];
    [dateFormatter setDateFormat:expctedFrmt];
    NSString  *steDate = [dateFormatter stringFromDate:date];
    return steDate;
}

-(NSDate *)getDateFromString:(NSString *)str withCurrentFormat:(NSString *)currentFrmt withExpectedFrmt:(NSString *)expctedFrmt
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:currentFrmt];
    NSDate *date = [dateFormatter dateFromString:str];
    [dateFormatter setDateFormat:expctedFrmt];
    NSString  *steDate = [dateFormatter stringFromDate:date];
    NSDate *returnDate=[dateFormatter dateFromString:steDate];
    return returnDate;
}


#pragma mark fetch current lat and long
//-(void)fetchCurrentLatLong
//{
//    [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
//        if (!error) {
//           // [SVProgressHUD showInfoWithStatus:@"Fetching current location..."];
//            [self reverseGeocodeLocationForLattitude:geoPoint.latitude ForLongitude:geoPoint.longitude];
//        }
//        else
//        {
//            [PFUIAlertView showAlertViewWithTitle:APP_TITLE
//                                          message:@"Oops! Unable to fetch your current location... Go to Settings > Locationist and ALLOW LOCATION ACCESS."
//                                cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok")];
//            
//            [SVProgressHUD dismiss];
//        }
//    }];
//    
//}
#pragma mark fetch location from current lat and long
//-(void) queryGooglePlaces: (NSString *) googleType  ForLattitude:(double) lattitude ForLongitude:(double) longitude
//{
//    // Build the url string to send to Google. NOTE: The kGOOGLE_API_KEY is a constant that should contain your own API key that you obtain from Google. See this link for more info:
//    // https://developers.google.com/maps/documentation/places/#Authentication
//    NSString *url = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?location=%f,%f&radius=%@&types=%@&sensor=true&key=%@", lattitude, longitude, @"10", @"", KGOOGLE_PLACES_API_KEY];
//    
//    //Formulate the string as a URL object.
//    NSURL *googleRequestURL=[NSURL URLWithString:url];
//    
//    // Retrieve the results of the URL.
//    
//    dispatch_async(dispatch_get_main_queue(), ^(void){
//        NSData* data = [NSData dataWithContentsOfURL: googleRequestURL];
//        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
//
//    });
//   }

//-(void)fetchedData:(NSData *)responseData {
//    //parse out the json data
//    NSError* error;
//    NSDictionary* json = [NSJSONSerialization
//                          JSONObjectWithData:responseData
//                          
//                          options:kNilOptions
//                          error:&error];
//    
//    //The results from Google will be an array obtained from the NSDictionary object with the key "results".
//    NSArray* places = [json objectForKey:@"results"];
//    
//    //Write out the data to the console.
//    NSLog(@"Google Data: %@", places);
//}
//- (void) reverseGeocodeLocationForLattitude:(CLLocation *)location
//{
//    double lattitude=location.coordinate.latitude;
//    double longitude=location.coordinate.longitude;
//    
//    if (! geocoder)
//        geocoder = [[CLGeocoder alloc] init];
//    
//   // CLLocation *location = [[CLLocation alloc] initWithLatitude:lattitude longitude:longitude];
//    
//    [geocoder reverseGeocodeLocation:location completionHandler: ^(NSArray* placemarks, NSError* error)
//          {
//         NSString *strLocation=nil;
//
//         if ([placemarks count] > 0)
//         {
//             if (! error)
//             {
//                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                 NSLog(@";;;;;%@",placemark.name);
//                 NSLog(@";;;;;%@",placemark.addressDictionary);
//                 NSLog(@"location;;;;;%f  %f",placemark.location.coordinate.latitude,placemark.location.coordinate.longitude);
//                 NSLog(@"location:%@",[placemark.addressDictionary valueForKey:@"City"]);
//                 [SVProgressHUD dismiss];
//                   strLocation=[placemark.addressDictionary valueForKey:@"City"];
//                 
//                 
//                 
//                 [[NSUserDefaults standardUserDefaults] setValue:strLocation forKey:@"USER_CURRENT_LOCATION"];
//                 [[NSUserDefaults standardUserDefaults]synchronize];
//                 
//                /* NSDictionary* dict = [NSDictionary dictionaryWithObject:strLocation forKey:@"locationName"];
//                 [[NSNotificationCenter defaultCenter] postNotificationName:@"LOCATION" object:nil userInfo:dict];*/
//
//             }
//             else
//             {
//                 [[AppManager sharedData] showHintView:@"Oops! Unable to fetch your current location... Go to Settings > Locationist and ALLOW LOCATION ACCESS."];
//                [SVProgressHUD dismiss];
//             }
//         }
//              }];
//    
//
//}
#pragma GetAttributedString

-(NSMutableAttributedString *)getAttributedString: (NSString *)str
{
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:str];
    [attributeStr addAttribute:NSUnderlineStyleAttributeName
                         value:[NSNumber numberWithInt:1]
                         range:NSMakeRange(0, [attributeStr length])];
    
    [attributeStr addAttribute:NSUnderlineColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, attributeStr.length)];
    return attributeStr;
}

#pragma marks --Files related functions

+ (BOOL)addSkipBackupAttributeToItemAtPath:(NSString*)path
{
    NSURL *URL = [NSURL fileURLWithPath:path];
    return [self addSkipBackupAttributeToItemAtFileURL:URL];
}

// From https://developer.apple.com/library/ios/qa/qa1719/_index.html
+(BOOL)addSkipBackupAttributeToItemAtFileURL:(NSURL*)URL
{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        BOOL success = [URL setResourceValue:[NSNumber numberWithBool:YES]
                                      forKey:NSURLIsExcludedFromBackupKey error:&error];
        if(!success){
           MPLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
        }
        else
        {
            MPLog(@"NSURLIsExcludedFromBackupKey flag successfully set %@",[URL lastPathComponent]);
        }
        return success;
    }
    MPLog(@"Error excluding %@ from backup. File does not exist", [URL lastPathComponent]);
    return NO;
}

+(NSString *)getCacheDirectoryPath
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
}

+(void)createApplicationSupportDirectory
{
    // store in /Library/Application Support/BUNDLE_IDENTIFIER/Reference
    // make sure Application Support folder exists
    NSError *error=nil;
    [[NSFileManager defaultManager] URLForDirectory:NSApplicationSupportDirectory
                                           inDomain:NSUserDomainMask
                                  appropriateForURL:nil
                                             create:YES
                                              error:&error];
    if (error) {
        MPLog(@"KCDM: Could not create application support directory. %@", error);
        return;
    }
    
}

#pragma Trimming String
+(NSString *)trimString: (NSString *)str
{
    return [[str description] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|[7-9])[0-9]{9,10}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    MPLog(@"Here= %d",[phoneTest evaluateWithObject:phoneNumber]);
    return [phoneTest evaluateWithObject:phoneNumber];
}


@end
