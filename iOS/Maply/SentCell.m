//
//  SentCell.m
//  Maply
//
//  Created by admin on 3/8/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SentCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Masonry/Masonry.h>
#import "UIImage+GIF.h"

@implementation SentCell

- (void)awakeFromNib {
    self.lblName.font=FONT_MESSAGE_TITLE;
    self.lblDelivered.font=FONT_AVENIR_PRO_REGULAR_H0;
    
    self.lblName.textColor=UICOLOR_DARK_TEXT;
    self.lblDelivered.textColor=UICOLOR_LIGHT_TEXT;
    
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    self.imgUser.clipsToBounds = YES;
    
    self.imgPhoto.layer.cornerRadius = 3.0;
    self.imgPhoto.layer.borderWidth = 1.5;
    self.imgPhoto.layer.borderColor = [UIColor colorWithHexValue:0xfe4d5d].CGColor;

    
    //@https://trello.com/c/fm3Sqa9B/220-set-a-reply-funktion-for-when-double-tapping-on-a-person-i-moments-just-like-in-snapchat
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTaped:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.contentView addGestureRecognizer:doubleTap];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapped:)];
    singleTap.numberOfTapsRequired = 1;
    [self.contentView addGestureRecognizer:singleTap];
    
    [singleTap requireGestureRecognizerToFail:doubleTap];
    
    //https://trello.com/c/vMFT7GYD/225-remove-the-loading-screen-for-when-opening-a-moment-journey-and-for-when-sending-a-moment-journey
    self.viewPreviewLoader.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)doubleTaped:(UITapGestureRecognizer*)doubleTap {
    if (_delegate) {
        [_delegate sentCell:self doubleTap:doubleTap];
    }
}

-(void)singleTapped:(UITapGestureRecognizer*)singleTap {
    if (_delegate) {
        [_delegate sentCell:self singleTap:singleTap];
    }
}

-(void)startActivity
{
    self.viewPreviewLoader.hidden = NO;
    UIImageView *imgLoader=[[UIImageView alloc]init];
    imgLoader.tag = 10;
    [self.viewPreviewLoader addSubview:imgLoader];
    imgLoader.hidden = YES;
    
    [imgLoader mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(@0);
        make.top.equalTo(@0);
        make.width.equalTo(@30);
        make.height.equalTo(@30);
    }];
    
    imgLoader.image= [UIImage sd_animatedGIFNamed:@"ripple"];
    [self.viewPreviewLoader bringSubviewToFront:imgLoader];
    imgLoader.hidden = NO;
}

-(void)stopActivity
{
    self.viewPreviewLoader.hidden = YES;
    UIImageView* imgLoader = [self.viewPreviewLoader viewWithTag:10];
    [imgLoader removeFromSuperview];
}

@end
