//
//  SentCell.h
//  Maply
//
//  Created by admin on 3/8/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@class SentCell;
@protocol SentCellDelegate <NSObject>
-(void)sentCell:(SentCell*)cell doubleTap:(UITapGestureRecognizer *)doubleTap;
-(void)sentCell:(SentCell*)cell singleTap:(UITapGestureRecognizer*)singleTap;
@end

@interface SentCell : UITableViewCell
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgUser;
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgPhoto;
@property(nonatomic,weak)IBOutlet UILabel *lblName,*lblDelivered;
@property(nonatomic,weak)IBOutlet UIButton *btnStatus;
@property (weak, nonatomic) IBOutlet UIView *viewPreviewLoader;
@property(nonatomic,weak) id<SentCellDelegate> delegate;

-(void)startActivity;
-(void)stopActivity;

@end