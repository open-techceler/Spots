//
//  RequestViewController.m
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "RequestViewController.h"
#import "RequestCell.h"
#import "UserAPI.h"
#import "FriendsListWrapper.h"
#import "Friend.h"
#import "EGORefreshTableHeaderView.h"


@interface RequestViewController ()<EGORefreshTableHeaderDelegate>
@property(nonatomic,strong)FriendsListWrapper *objWrapper;
@property (nonatomic) NSInteger  selectedRow;
@property(nonatomic,strong) EGORefreshTableHeaderView *refreshHeaderView;
@property BOOL reloading;

@end

@implementation RequestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    
    [self initLoaderActivityWithFrame: CGRectMake((SCREEN_SIZE.width/2.0)-40,(SCREEN_SIZE.height/2.0)-(40+kNavBarHeight+kScopeBarHeight+kStatusBarHeight), 80, 80)];

    [self initEmptyDataLabelWithFrame: CGRectMake(40,NAVIGATION_HEIGHT + 30.f,SCREEN_SIZE.width-80.f,SCREEN_SIZE.height-2*(NAVIGATION_HEIGHT+120.f))];
//    [self initEmptyDataLabelWithFrame:CGRectMake(0,(SCREEN_SIZE.height/2.0)-(NAVIGATION_HEIGHT+kScopeBarHeight),SCREEN_SIZE.width,30)];
    
    self.arrayActions=[NSMutableArray new];
    [self setFontAndColor];
    
    [self startLoadingActivity];
    self.tableView.hidden=YES;
    
    if (_refreshHeaderView == nil) {
        
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - 60, SCREEN_SIZE.width, 60)];
        view.delegate = self;
        [self.tableView addSubview:view];
        _refreshHeaderView = view;
    }
   // [self getFriendsRequest];
}



-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG;
    
}

#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    _reloading = YES;
    [self refreshTable];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    return _reloading; // should return if data source model is reloading
    
}

- (void)endRefreshing{
    //  model should call this when its done loading
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

-(void)refreshTable
{
    if(UIAppDelegate.isInternetAvailable)
    {
        //reload in background
        [self getFriendsRequest];
    }
    else
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
    }
}

-(void)reloadFriendList
{
    if(self.objWrapper)
    {
        //reload in background
         [self getFriendsRequest];
    }
    else
    {
        self.tableView.hidden=YES;
        [self startLoadingActivity];
        [self getFriendsRequest];
    }
}
-(void)getFriendsRequest
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

    [[UserAPI new]getFriendsRequest:0 success:^(FriendsListWrapper *objWrapper) {
        self.objWrapper=objWrapper;
        [self.tableView reloadData];
        
        self.tableView.hidden=NO;
        [self stopLoadingActivity];
        
        if(self.objWrapper.live_request.count==0 &&self.objWrapper.friend_reqeust.count==0)
        {
          //  self.tableView.hidden=YES;
//            [self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_request_found", nil)];
            [self showEmptyDataImageViewWithImage:[UIImage imageNamed:@"icon_no_new_request"]];
        }
        [self endRefreshing];
        
        [self.delegate setBadgeForTab:1 value:self.objWrapper.live_request.count+self.objWrapper.friend_reqeust.count];
        
    } failure:^(NSError *error) {
        self.tableView.hidden=NO;
        [self stopLoadingActivity];
        [self showOfflineView:YES error:error];
    }];
    });
}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        [self reloadFriendList];
        
    }
}


#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0 && self.objWrapper.live_request.count)
        return 32;
    else if(section==1 && self.objWrapper.friend_reqeust.count)
        return 32;

        
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vw =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 30)];
    vw.backgroundColor=[UIColor whiteColor];
    
    UILabel *lbl =[[UILabel alloc] initWithFrame:CGRectMake(16, 10, SCREEN_SIZE.width-16, 20)];
    lbl.textColor=UICOLOR_LIGHT_TEXT;
    lbl.font=FONT_HEADER_TITLE;//FONT_AVENIR_PRO_DEMI_H1;
    lbl.textAlignment=NSTextAlignmentLeft;
    lbl.text=(section==0?MPLocalizedString(@"live_requests", nil):MPLocalizedString(@"friends_requests", nil));
    [vw addSubview:lbl];
    
    return vw;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   if(section==0)
       return self.objWrapper.live_request.count;
    else
        return self.objWrapper.friend_reqeust.count;
   
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* MyIdentifier = @"RequestCell";
    RequestCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"RequestCell" owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;

    }
    NSArray *arr=nil;
    if(indexPath.section==0)
    {
        arr= self.objWrapper.live_request;
        
        cell.btnAdd.tag=indexPath.row;
        cell.btnBlock.tag=indexPath.row;
        
        [cell.btnAdd setTitle:MPLocalizedString(@"accept", nil) forState:UIControlStateNormal];
        [cell.btnBlock setTitle:MPLocalizedString(@"decline", nil) forState:UIControlStateNormal];
        
        [cell.btnAdd addTarget:self action:@selector(clickAccept:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnBlock addTarget:self action:@selector(clickDecline:) forControlEvents:UIControlEventTouchUpInside];
        
      

    }
    else
    {
        arr=  self.objWrapper.friend_reqeust;
        [cell.btnAdd setTitle:MPLocalizedString(@"add", nil) forState:UIControlStateNormal];
        [cell.btnBlock setTitle:MPLocalizedString(@"block", nil) forState:UIControlStateNormal];
        
        [cell.btnAdd addTarget:self action:@selector(clickAdd:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnBlock addTarget:self action:@selector(clickBlock:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.btnAdd.tag=indexPath.row;
        cell.btnBlock.tag=indexPath.row;
    }
    
    Friend *obj= arr[indexPath.row];
    
    cell.lblName.text=obj.username.capitalizedString;
    [cell.imgUser getImageWithURL:obj.image comefrom:ImageViewComfromUser];
    
    if(indexPath.section==0)
    {
        cell.lblHours.font = [UIFont systemFontOfSize:10];
        if([obj.live_type isEqualToString:@"H24"]) {
            cell.lblHours.text = @"24h";
        } else if([obj.live_type isEqualToString:@"H72"]) {
            cell.lblHours.text = @"72h";
        } else {
            cell.lblHours.font = [UIFont systemFontOfSize:18];
            cell.lblHours.text = @"∞";
        }
        
        cell.lblHours.hidden=NO;
    }
    else
    {
        cell.lblHours.hidden=YES;

    }
    
    return cell;
    
}

-(void)deleteRowAtIndexpath:(NSIndexPath *)indexPath fromArray:(NSMutableArray *)array
{
    [self.tableView beginUpdates];
    [array removeObjectAtIndex:indexPath.row];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableView endUpdates];
    [self.tableView reloadData];
}

-(void)clickAccept:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    UIButton *btn=(UIButton *)sender;
    Friend *obj=[self.objWrapper.live_request objectAtIndex:btn.tag];
    
    Friend *objSelected=[Friend new];
    objSelected.friend_id=obj.friend_id;
    objSelected.status_type=kStatusTypeLive;
    [self.arrayActions addObject:objSelected];
    [self updateRequestOnTabChange];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    [self deleteRowAtIndexpath:indexPath fromArray:self.objWrapper.live_request];
}

-(void)clickDecline:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    UIButton *btn=(UIButton *)sender;
    Friend *obj=[self.objWrapper.live_request objectAtIndex:btn.tag];
    
    Friend *objSelected=[Friend new];
    objSelected.friend_id=obj.friend_id;
    objSelected.status_type=kStatusTypeDecline;
    [self.arrayActions addObject:objSelected];
    [self updateRequestOnTabChange];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    [self deleteRowAtIndexpath:indexPath fromArray:self.objWrapper.live_request];
}


-(void)clickAdd:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    UIButton *btn=(UIButton *)sender;
    Friend *obj=[self.objWrapper.friend_reqeust objectAtIndex:btn.tag];
    
    Friend *objSelected=[Friend new];
    objSelected.friend_id=obj.friend_id;
    objSelected.status_type=kStatusTypeRequest;
    [self.arrayActions addObject:objSelected];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:1];
    [self deleteRowAtIndexpath:indexPath fromArray:self.objWrapper.friend_reqeust];
    [self updateRequestOnTabChange];
    
}

-(void)clickBlock:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    UIButton *btn=(UIButton *)sender;
    _selectedRow=btn.tag;
    Friend *obj=[self.objWrapper.friend_reqeust objectAtIndex:btn.tag];
    NSString *msg=[NSString stringWithFormat:MPLocalizedString(@"are_you_sure_you_block", nil),obj.name.capitalizedString];
    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:MPLocalizedString(@"app_name", nil) message:msg delegate:self cancelButtonTitle:MPLocalizedString(@"cancel", nil) otherButtonTitles:MPLocalizedString(@"yes", nil) ,nil];
    alert.tag=BLOCK_ALERT_TAG;
    [alert show];
    
    /*UIButton *btn=(UIButton *)sender;
    Friend *obj=[self.objWrapper.friend_reqeust objectAtIndex:btn.tag];
   
    Friend *objSelected=[Friend new];
    objSelected.friend_id=obj.friend_id;
    objSelected.status_type=kStatusTypeBlock;
    [self.arrayActions addObject:objSelected];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:1];
    [self deleteRowAtIndexpath:indexPath fromArray:self.objWrapper.friend_reqeust];*/
    
}


-(void)updateRequestOnTabChange{
    if( self.arrayActions.count)
    {
        [[UserAPI new]updateUserAction:self.arrayActions success:^(FriendsListWrapper *objWrapper) {
            [self.arrayActions removeAllObjects];
            [self reloadFriendList];
        } failure:^(NSError *error) {
            
        }];
    }
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        if(alertView.tag==BLOCK_ALERT_TAG)
        {
            Friend *obj=[self.objWrapper.friend_reqeust objectAtIndex:_selectedRow];

            Friend *objSelected=[Friend new];
            objSelected.friend_id=obj.friend_id;
            objSelected.status_type=kStatusTypeBlock;
            [self.arrayActions addObject:objSelected];
        
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:_selectedRow inSection:1];
            [self deleteRowAtIndexpath:indexPath fromArray:self.objWrapper.friend_reqeust];
            [self updateRequestOnTabChange];
        }
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}


@end
