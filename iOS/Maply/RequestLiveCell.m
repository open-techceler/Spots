//
//  RequestLiveCell.m
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "RequestLiveCell.h"

@implementation RequestLiveCell

- (void)awakeFromNib {
    // Initialization code
    self.lblName.font=FONT_MESSAGE_TITLE;//FONT_AVENIR_PRO_DEMI_H0;
    self.lblWaiting.font=FONT_AVENIR_PRO_DEMI_H0;
    
    self.btnEdit.titleLabel.font=FONT_AVENIR_PRO_REGULAR_H1;
    self.btnBlock.titleLabel.font=FONT_AVENIR_PRO_REGULAR_H1;
    self.btnDelete.titleLabel.font=FONT_AVENIR_PRO_REGULAR_H1;

    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    self.imgUser.clipsToBounds = YES;
    
    self.lblName.textColor=UICOLOR_DARK_TEXT;

    [self.btnRequestLive setTitleColor:UICOLOR_NAV_TITLE forState:UIControlStateNormal];
    self.btnRequestLive.titleLabel.font=FONT_AVENIR_MEDIUM_H00;
}

-(void)setUpDisableLiveButton
{
    [self.btnRequestLive setAttributedTitle:[self getAttributedString:MPLocalizedString(@"disable_live", nil)] forState:UIControlStateNormal];
}

-(void)setUpRequstLiveButton
{
    [self.btnRequestLive setAttributedTitle:[self getAttributedString:MPLocalizedString(@"request_live", nil)] forState:UIControlStateNormal];
}


-(NSMutableAttributedString *)getAttributedString:(NSString *)full_str
{
    
    NSMutableAttributedString* att_string = [[NSMutableAttributedString alloc] initWithString:full_str];
    NSRange atRange = [full_str rangeOfString:MPLocalizedString(@"live", nil).uppercaseString];
    
    if (atRange.location != NSNotFound) {
        [att_string addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:atRange];
         [att_string addAttribute:NSFontAttributeName value:FONT_AVENIR_BLACK_H00 range:atRange];
    }
    return att_string;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
