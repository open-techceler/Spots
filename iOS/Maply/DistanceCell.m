//
//  DistanceCell.m
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "DistanceCell.h"
#import "Notification.h"

@implementation DistanceCell

- (void)awakeFromNib {
    // Initialization code
    _lblTitle.preferredMaxLayoutWidth =SCREEN_SIZE.width - 146;
    _lblTitle.font =FONT_NOTIFICATION_TITLE;
    _lblValue.font =FONT_NOTIFICATION_TITLE;
   
    _lblTitle.textColor= UICOLOR_NOTIFICATION_TITLE;
    _lblValue.textColor= UICOLOR_NOTIFICATION_TITLE;

    
    _lblTitle.text =@"";
    _lblValue.text=@"";
    self.slider.minimumValue=0;
    self.slider.maximumValue=10;
}

-(void)configureCellWithObj:(Notification *)obj
{
     self.lblTitle.text=MPLocalizedString(@"maximum_distance", nil);
    self.objNotification=obj;
    self.slider.value=obj.maximum_distance;
    _lblValue.text=[NSString stringWithFormat:@"%.fkm",self.slider.value];
   

}

-(IBAction)clickSlider:(id)sender
{
    self.objNotification.maximum_distance=self.slider.value;
     _lblValue.text=[NSString stringWithFormat:@"%.fkm",self.slider.value];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
