 //
//  LocationClass.m
//  Pradip
//

#import "LocationClass.h"
#import "UserAPI.h"
#import "NotificationDefinitions.h"
#import "LocationShareModel.h"
#import "LocationTrackEngine.h"
#import "User.h"

@interface LocationClass ()
@property (nonatomic, strong) NSTimer* timer;
@property (strong,nonatomic) LocationShareModel * shareModel;

@end

#define FOREGROUND_SECONDS 120
#define BACKGROUND_SECONDS 400

@implementation LocationClass

@synthesize currentLocation = _currentLocation;
@synthesize delegate = _delegate;


+(LocationClass*)sharedLoctionManager
{
 
    static LocationClass* _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[LocationClass alloc] init];
    });
    
    return _sharedInstance;
}


// setup the data collection
- (id)init{
	
	if (self = [super init]) {
        _track_interval_in_seconds = FOREGROUND_SECONDS;
        
        self.shareModel = [LocationShareModel sharedModel];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationEnterBackground) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceviceLocationStatusError) name:kLocationErrorNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUIApplicationBecomeActive) name:kUIApplicationDidBecomeActive object:nil];
    }
    return self;
}

-(void)applicationEnterBackground{
    
    if([VVBaseUserDefaults userId]!=0)
    {
        //Use the BackgroundTaskManager to manage all the background Task
        self.shareModel.bgTask = [BackgroundTaskManager sharedBackgroundTaskManager];
        [self.shareModel.bgTask beginNewBackgroundTask];
        
        //@added by steven
        if ([_timer isValid]) {
            [self stopUpdateLocationTimer];
            _track_interval_in_seconds = BACKGROUND_SECONDS;
            [self startFetchingLocation];
        }
        //@added by steven
    }
}

-(void)didUIApplicationBecomeActive {
    if ([_timer isValid]) {
        [self stopUpdateLocationTimer];
        _track_interval_in_seconds = FOREGROUND_SECONDS;
        [self startFetchingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch([CLLocationManager authorizationStatus])
    {
        case kCLAuthorizationStatusAuthorized:
            NSLog(@"Location services authorised by user");
            break;
            
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            NSLog(@"Location services authorised by user");
            break;

        case kCLAuthorizationStatusDenied:
            NSLog(@"Location services denied by user");
            break;
            
        case kCLAuthorizationStatusRestricted:
            NSLog(@"Parental controls restrict location services");
            break;
            
        case kCLAuthorizationStatusNotDetermined:
            NSLog(@"Unable to determine, possibly not available");
            break;
    }
}

#pragma mark - Helper Methods

-(void)startFetchingLocation
{
    
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied )
    {
        if(_delegate && [_delegate respondsToSelector:@selector(failedToFetchLocation:)])
            [_delegate failedToFetchLocation:NSLocalizedString(@"Oops! Unable to fetch your current location... Go to Settings > Locationist and ALLOW LOCATION ACCESS.", nil)];
        return;
    }
    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
    {
        
        if(_delegate && [_delegate respondsToSelector:@selector(failedToFetchLocation:)])
            [_delegate failedToFetchLocation:NSLocalizedString(@"Location Services are disabled", nil)];
        return;
    
    }
    
    [self startTimer];
}

-(void)didReceviceLocationStatusError {
    // An error occurred, more info is available by looking at the specific status returned. The subscription has been kept alive.
    if(_delegate && [_delegate respondsToSelector:@selector(failedToFetchLocation:)])
        [_delegate failedToFetchLocation:@"An error occurred while using the system location services."];
}

- (void)startTimer
{
    [self timerFired:_timer];
    if (!_timer) {
        // update the location after 5 min //300 sec
        _timer = [NSTimer scheduledTimerWithTimeInterval:_track_interval_in_seconds
                                                  target:self
                                                selector:@selector(timerFired:)
                                                userInfo:nil
                                                 repeats:YES];
    }
}

- (void)timerFired:(NSTimer*)timer
{
    //update current location to the server
    [self updateLocationToServer];
}

-(void)updateLocationToServer
{
    
    if ([LocationTrackEngine sharedInstance].currentLocation == nil)
    {
        [[INTULocationManager sharedInstance] requestLocationWithDesiredAccuracy:INTULocationAccuracyHouse timeout:3 block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
            if (status != INTULocationStatusError) {
                self.currentLocation = currentLocation;
                
                if(_delegate && [_delegate respondsToSelector:@selector(locationFound:)])
                    [self.delegate locationFound:self.currentLocation];
                
                if([VVBaseUserDefaults userId].integerValue!=0)
                {
                    [self getAddressAndUpdateLocation:self.currentLocation];
                }
                else
                {
                    [self stopTimer];
                }
                
            }
            else {
                [self didReceviceLocationStatusError];
            }
        }];
    }
    else {
        self.currentLocation = [LocationTrackEngine sharedInstance].currentLocation;
        
        if(_delegate && [_delegate respondsToSelector:@selector(locationFound:)])
            [self.delegate locationFound:self.currentLocation];
        
        if([VVBaseUserDefaults userId].integerValue!=0)
        {
            [self getAddressAndUpdateLocation:self.currentLocation];
        }
        else
        {
            [self stopTimer];
        }
    }
}

-(void)getAddressAndUpdateLocation:(CLLocation*)location
{
    MPLog(@"self.currentLocation %@",location);
    if(location.coordinate.latitude>0&&location.coordinate.longitude>0)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error){
                 MPLog(@"Geocode failed with error: %@", error);
                // return;
             }
             
             CLPlacemark *placemark = [placemarks firstObject];
             if(placemark)
             {
                 [[UserAPI new]updateUserLocation:placemark success:^(MessageListWrapper *objWrapper) {
                     [self sendRefreshLiveFriendsNotification];
                     MPLog(@"updateUserLocation>>Success");
                 } failure:^(NSError *error) {
                     MPLog(@"updateUserLocation>>Failed");
                     [self sendRefreshLiveFriendsNotification];
                 }];
               
             }
             else
                 [self sendRefreshLiveFriendsNotification];
         }];
        });
    }
}

-(void)sendRefreshLiveFriendsNotification
{
      [[NSNotificationCenter defaultCenter]postNotificationName:kReloadLiveFriendsListNotification object:nil];
}

- (void)setTimerToUpdateLocation
{
    [self startTimer];
}

-(void)stopUpdateLocationTimer
{
    [self stopTimer];
}

- (void)stopTimer
{
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
}

- (void)dealloc
{
    [[LocationTrackEngine sharedInstance] stopLocationTrackEngine];
    
    if ([_timer isValid]) {
        [_timer invalidate];
        _timer = nil;
    }
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];

}
@end
