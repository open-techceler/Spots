//
//  PreviewViewController.h
//  Maply
//
//  Created by admin on 3/22/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MyNavigationViewController.h"
@protocol  PreviewDelegate <NSObject>
-(void)clickDone:(UIImage *)img;
@end

@interface PreviewViewController : MyNavigationViewController
@property (nonatomic,strong)UIImage *image;
@property(nonatomic,weak) id<PreviewDelegate> previewDelegate;

@end
