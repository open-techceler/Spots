//
//  MessageListWrapper.m
//  Maply
//
//  Created by admin on 2/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MessageListWrapper.h"
#import "Message.h"

@class Message;

@implementation MessageListWrapper

+ (NSValueTransformer*)listJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Message class]];
}
@end
