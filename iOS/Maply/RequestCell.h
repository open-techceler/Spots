//
//  RequestCell.h
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@interface RequestCell : UITableViewCell
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgUser;
@property(nonatomic,weak)IBOutlet UILabel *lblName,*lblHours;
@property(nonatomic,weak)IBOutlet UIButton *btnAdd,*btnBlock;
@end
