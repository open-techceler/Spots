//
//  TestFrameGenerator.h
//  makeMovie
//
//  Created by Rob Mayoff on 4/25/15.
//  Donated to the public domain.
//

@import Foundation;
#import "DqdFrameGenerator.h"

@interface TestFrameGenerator : NSObject<DqdFrameGenerator>

@property (nonatomic, readonly) NSInteger framesEmittedCount;
@property (nonatomic, assign) NSInteger totalFramesCount;
@property (nonatomic, strong) dispatch_block_t frameGeneratedCallback;
@property (nonatomic,strong)NSArray *arrayImages;
@property (nonatomic,strong)UIImage *baseImage;

@end
