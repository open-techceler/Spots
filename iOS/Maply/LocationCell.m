//
//  LocationCell.m
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "LocationCell.h"

@implementation LocationCell

- (void)awakeFromNib {
    // Initialization code
    self.lblTitle.font=FONT_MESSAGE_TITLE;
    self.lblAddress.font=FONT_AVENIR_ROMAN_H09;//FONT_AVENIR_PRO_REGULAR_H00;
    self.lblKm.font=FONT_AVENIR_ROMAN_H09;//FONT_AVENIR_PRO_REGULAR_H00;
    
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    self.imgUser.clipsToBounds = YES;
    
    self.lblTitle.textColor=UICOLOR_LOCATION_TITLE;
    UIColor *subtitle_color=[UIColor colorWithRed:119/255.0f green:119/255.0f blue:119/255.0f alpha:1.0f]
    ;
    self.lblAddress.textColor=subtitle_color;
    self.lblKm.textColor=subtitle_color;
;
    
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
