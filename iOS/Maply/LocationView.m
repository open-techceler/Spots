//
//  LocationView.m
//  Maply
//
//  Created by admin on 5/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "LocationView.h"
#import <Masonry/Masonry.h>

@implementation LocationView


- (id)init
{
    self = [super init];
    if (self) {
            [self setUp];
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self setUp];
}

-(void)setUp
{
    _tempXib= [[[NSBundle mainBundle] loadNibNamed:@"LocationView" owner:self options:nil] objectAtIndex:0];
    _tempXib.backgroundColor=[[UIColor blackColor]colorWithAlphaComponent:0.5];
    [self addSubview:_tempXib];
    _tempXib.frame = self.bounds;
    
   // _lblWhere.textColor=UICOLOR_DARK_TEXT;
    _lblInOrder.textColor=UICOLOR_DARK_TEXT;
    _lblGoto.textColor=UICOLOR_DARK_TEXT;
    
    _lblWhere.font=FONT_AVENIR_BOOK_H32;
    _lblInOrder.font=FONT_AVENIR_BOOK_H17;
    _lblGoto.font=FONT_AVENIR_BOOK_H17;
    
    _lblWherePop.textColor=UICOLOR_DARK_TEXT;
    _lblInOrderPop.textColor=UICOLOR_LIGHT_TEXT;
    
    _lblWherePop.font=FONT_AVENIR_MEDIUM_H4;
    _lblInOrderPop.font=FONT_AVENIR_BOOK_H12;
    
    _lblWhere.text=_lblWherePop.text=MPLocalizedString(@"where_are_you", nil);
    _lblInOrder.text=_lblInOrderPop.text=MPLocalizedString(@"in_order_to_send", nil);
    _lblGoto.text=MPLocalizedString(@"go_to_settings", nil);
    
    _vwPop.layer.cornerRadius=10;

}

-(IBAction)clickSetting:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
}
@end
