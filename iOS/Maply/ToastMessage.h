//
//  ToastMessage.h
//  Beetot
//
//  Created by anand mahajan on 12/11/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TWMessageBarManager.h"
@interface ToastMessage : NSObject
+(void)showMessageWithTitle:(NSString*)title message:(NSString*)message type:(TWMessageBarMessageType)type;
+(void)showSuccessMessage:(NSString*)message;
+(void)showErrorMessageOppsTitleWithMessage:(NSString*)message;
+(void)showErrorMessageAppTitleWithMessage:(NSString*)message;
+(void)showInfoMessage:(NSString*)message;
@end
