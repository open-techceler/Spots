//
//  LiveFeedCustomView.h
//  Maply
//
//  Created by Admin on 28/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Friend;

@protocol LiveFeedCustomViewDelegate;

@interface LiveFeedCustomView : UIView
{
    NSArray* arraySelectedFriends;
}

@property (nonatomic, weak) id<LiveFeedCustomViewDelegate> delegate;

- (void) loadUsers:(NSArray *) arrayFriends;

@end

@protocol LiveFeedCustomViewDelegate <NSObject>

- (void) tappedLiveFriend:(Friend *) obj;

@end
