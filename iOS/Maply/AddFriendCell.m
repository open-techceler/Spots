//
//  AddFriendCell.m
//  Maply
//
//  Created by Admin on 01/10/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "AddFriendCell.h"
#import "BaseImageViewWithData.h"
#import "Friend.h"

@implementation AddFriendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    for (int nIdx = 0; nIdx < 3; nIdx++) {
        UIView* viewCanvas = (UIView *)[self viewWithTag:10 + nIdx];
        viewCanvas.hidden = YES;
        
        BaseImageViewWithData* imgUser = (BaseImageViewWithData *)[self viewWithTag:20 + nIdx];
        imgUser.layer.cornerRadius=imgUser.frame.size.width / 2;
        imgUser.clipsToBounds = YES;

        UIButton* btnAction = (UIButton *)[self viewWithTag:40 + nIdx];
        [btnAction addTarget:self action:@selector(actionAddOrRemove:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) loadUsers:(NSArray *) arrayUsers {
    self.arrayFriends = arrayUsers;
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    if (self.arrayFriends == nil) return;
    if (self.arrayFriends.count == 0) return;
    
    [self updateConstraintsIfNeeded];

    for (int nIdx = 0; nIdx < self.arrayFriends.count; nIdx++) {
        Friend* obj = self.arrayFriends[nIdx];
        
        UIView* viewCanvas = (UIView *)[self viewWithTag:10 + nIdx];
        viewCanvas.hidden = NO;
        
        [viewCanvas setNeedsLayout];
        [viewCanvas layoutIfNeeded];
        
        BaseImageViewWithData* imgUser = (BaseImageViewWithData *)[self viewWithTag:20 + nIdx];
        imgUser.layer.cornerRadius=imgUser.frame.size.width / 2;
        
        imgUser.clipsToBounds = YES;

        UILabel* lblName = (UILabel *)[self viewWithTag:30 + nIdx];
        UIButton* btnAction = (UIButton *)[self viewWithTag:40 + nIdx];
        
        if([obj.status_type isEqualToString:kStatusTypeRequest] || [obj.status_type isEqualToString:kStatusTypeFriend])
            [btnAction setTitle:MPLocalizedString(@"remove", nil) forState:UIControlStateNormal];
        else
            [btnAction setTitle:MPLocalizedString(@"add", nil) forState:UIControlStateNormal];
        
        lblName.text=obj.name.capitalizedString;
        
        [imgUser getImageWithURL:obj.image comefrom:ImageViewComfromUser];
    }
    
}

- (void) actionAddOrRemove:(UIButton *) sender {
    NSInteger nIdx = sender.tag - 40;
    
    if (self.delegate!= nil && [self.delegate respondsToSelector:@selector(tappedAddButton:friend:)])
        [self.delegate tappedAddButton:self friend:self.arrayFriends[nIdx]];
}

@end
