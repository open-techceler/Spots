//
//  FriendsManager.m
//  Maply
//
//  Created by admin on 5/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "FriendsManager.h"
#import "UserAPI.h"
#import "FriendsListWrapper.h"
@interface FriendsManager()

@property (nonatomic,assign)BOOL isApiRunning;
@property (nonatomic, strong) FriendsListWrapper * objWrapper;
@end
@implementation FriendsManager
+ (FriendsManager*)sharedInstance
{
    static FriendsManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {
    }
    
    return self;
}

-(void)fetchFriendList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self getAllFriends:^(FriendsListWrapper *objWrapper) {
        } failure:^(NSError *error) {
        }];
    });
}

-(void)getAllFriends:(void (^)(FriendsListWrapper* objWrapper))success
                    failure:(void (^)(NSError* error))failure
{
    [[UserAPI new]getSendToFriendSuccess:^(FriendsListWrapper *objWrapper) {
        _objWrapper=objWrapper;
    } failure:^(NSError *error) {
    }];

}

-(void)getSendToFriendsList:(void (^)(FriendsListWrapper* objWrapper))success
                           failure:(void (^)(NSError* error))failure
{
    if(_objWrapper)
    {
        success(_objWrapper);
    }
    else
    {
        [self getAllFriends:^(FriendsListWrapper *objWrapper) {
            success(objWrapper);
        } failure:^(NSError *error) {
            failure (error);
        }];
    }
}

@end
