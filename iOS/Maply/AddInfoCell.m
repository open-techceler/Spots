//
//  AddInfoCell.m
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "AddInfoCell.h"

@implementation AddInfoCell

- (void)awakeFromNib {
    // Initialization code
    _lblTitle.preferredMaxLayoutWidth =SCREEN_SIZE.width - 146;
    
    self.txtField.font =FONT_AVENIR_ROMAN_H4;//FONT_AVENIR_ROMAN_H2;
    _lblTitle.font =FONT_SETTING_TITLE;
    
    self.lblTitle.textColor=UICOLOR_NOTIFICATION_TITLE;//UICOLOR_DARK_TEXT;
    self.txtField.textColor=UICOLOR_LIGHT_TEXT;
    
    _lblTitle.text =@"";
}

-(void)setTitle:(NSString*)title value:(NSString*)value
{
    _lblTitle.text=title;
    self.txtField.text=value;
}

@end
