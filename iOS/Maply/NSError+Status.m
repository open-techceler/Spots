//
//  NSError+Status.m
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 11/9/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import "NSError+Status.h"
#import "AFSphinxAPIClient.h"
@implementation NSError (Status)
-(NSInteger)statusCode
{
    return [[[self userInfo] objectForKey:AFNetworkingOperationFailingURLResponseErrorKey] statusCode];

}
-(BOOL)isSuccess
{
    NSInteger code=[self statusCode];
    if(code==200)
        return YES;

    return NO;
}
@end
