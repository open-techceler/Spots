//
//  MyJourneySubCell.h
//  Maply
//
//  Created by admin on 6/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@interface MyJourneySubCell : UITableViewCell
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgPhoto;
@property(nonatomic,weak)IBOutlet UILabel *lblName,*lblAddress;
@property(nonatomic,weak)IBOutlet UILabel *lblCount;
@end
