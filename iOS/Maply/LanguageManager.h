//
//  LanguageManager.h
//  VivinoV2
//
//  Created by Admin on 17/04/14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LanguageManager : NSObject

+ (NSString*)get:(NSString*)key alter:(NSString*)alternate;
+ (NSInteger)getPerferedLanguageInInteger;
+ (NSString*)getLanguageNameWithCode:(NSInteger)code;
+ (NSString*)nativeLanguageNameForLocale:(NSString*)isoIdentifier;
+ (NSString*)getPerferedLocaleCode;
+ (NSLocale*)getPerferedLocale;
+ (NSString*)getPerferedLanguage;
+ (NSString*)getPerferedLanguageForServer;
+ (NSString*)getPluralizedString:(NSString*)key
                      withNumber:(CGFloat)n
                           alter:(NSString*)alternate;

+ (NSString*)getLocalizeCountryNameWithCode:(NSString*)countryCode;
+ (NSString*)getLocalizeCurrencyNameWithCode:(NSString*)currencyCode;
+ (UIImage*)getLocalizedImage:(NSString*)imageName;

+ (NSLocale*)getCurrentLocale;

+ (void)setUserPerferedLanguage:(NSString*)language
                     withLocale:(NSString*)locale;
+ (void)setLanguageWithIntType:(NSInteger)language_value;
+ (void)setLanguageAsEnglish;
+ (void)setLanguageAsGerman;

+ (BOOL)isEnglishLanguage;
@end
