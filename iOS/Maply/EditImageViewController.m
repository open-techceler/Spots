//
//  EditImageViewController.m
//  Maply
//
//  Created by admin on 2/9/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "EditImageViewController.h"
#import "ACEDrawingView.h"
#import <QuartzCore/QuartzCore.h>
#import "SelectLocationViewController.h"
#import "SendToViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Maply-Swift.h"
#import "UserAPI.h"
#import "VVButton.h"
#import "LocationClass.h"
#import "ImageFiltersViewController.h"
#import <Masonry/Masonry.h>
#import "NSGIF.h"
#import "UIImage+animatedGIF.h"
#import "SCTouchDetector.h"
#import "FriendsManager.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "BaseImageViewWithData.h"
#import "TestFrameGenerator.h"
#import "DqdGeneratedVideo.h"
#import "MediaPlayer/MediaPlayer.h"
#import "BufferReader.h"


@import AVFoundation;

@interface EditImageViewController ()<ACEDrawingViewDelegate,UITextFieldDelegate,UIGestureRecognizerDelegate,SelectLocationDelegate,UIPageViewControllerDataSource,UIPageViewControllerDelegate,SCPlayerDelegate,BufferReaderDelegate>
{
    CGSize _originalImageSize;
}
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imageView;
@property (nonatomic, strong) IBOutlet ACEDrawingView *drawingView;
@property (nonatomic, weak) IBOutlet UIView *vwColor;
@property (nonatomic, weak) IBOutlet UIButton *btnUndo,*btnClose;
@property (nonatomic, strong)  UIImage *imageSnap;
@property (nonatomic, weak)IBOutlet  UIView *vwLocation;

@property (nonatomic, strong)  FSVenue *objVenue;
@property (nonatomic, weak) IBOutlet UILabel *lblLocationName,*lblLocationAddress;

@property(nonatomic,strong)IBOutlet TextDrawer *drawTextView;
@property(nonatomic)BOOL isDrawingTextEditing;

@property(nonatomic)NSInteger uploadedMsgId;
@property (nonatomic, strong) IBOutlet VVButton *btnSelectedColor;

@property(nonatomic,strong)UIPageViewController *pageViewController;
@property NSInteger currentPageIndex;

@property (strong, nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) AVPlayerLayer *avPlayerLayer;

//@property (strong, nonatomic) SCAssetExportSession *exportSession;
@property (strong, nonatomic) SCPlayer *player;
@property (strong, nonatomic) IBOutlet SCSwipeableFilterView *filterSwitcherView;
@property (strong, nonatomic)ALAssetsLibrary *library;

@property (nonatomic, strong) IBOutlet UIView *vwSongInfo;
@property (nonatomic, weak) IBOutlet UILabel *lblSongTitle,*lblAlbumName,*lblArtist;
@property (nonatomic, weak) IBOutlet BaseImageViewWithData *imgAlbum;
@property (nonatomic, strong)MPMusicPlayerController *myPlayer;
@property (nonatomic, strong)BufferReader *bufferReader;

@property (nonatomic, weak) IBOutlet BaseImageViewWithData *imgViewReceiverProfileImage;

@end

@implementation EditImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    //@set profile picture when coming from Reply or clicking camera with the userID
    [_imgViewReceiverProfileImage.layer setCornerRadius:_imgViewReceiverProfileImage.frame.size.width / 2];
    [_imgViewReceiverProfileImage setClipsToBounds:YES];
    if (self.comefrom == CameraMainComeFromReplyUser) {
        [_imgViewReceiverProfileImage loadImageWithURL:_reply_userImgUrl success:^{
            [_imgViewReceiverProfileImage setContentMode:UIViewContentModeScaleAspectFit];
        } failure:nil];
        [_imgViewReceiverProfileImage.layer setBorderColor:[UIColor whiteColor].CGColor];
        [_imgViewReceiverProfileImage.layer setBorderWidth:2.0];
        
        self.imgViewReceiverProfileImage.hidden = NO;
    }
    else {
        self.imgViewReceiverProfileImage.hidden = YES;
    }
    
    self.view.backgroundColor=[UIColor blackColor];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didEndTextEditing) name:@"didEndTextEditing" object:nil];
    self.isDrawingTextEditing = NO;
    
    [self initLoaderActivityWithFrame: CGRectMake((SCREEN_SIZE.width/2.0)-10,(SCREEN_SIZE.height/2.0)-40, 20, 20)];
    
    [self setFontAndColor];
    self.vwColor.hidden=YES;
    self.btnUndo.hidden=YES;
    
    self.library=[ALAssetsLibrary new];
   
    if(self.camera_mode==CameraModeVideo)//show first frame until player gets ready
    {
       SCRecordSessionSegment *segment = _recordSession.segments.firstObject;
       self.imageView.image = segment.thumbnail;
    }
    else
        self.imageView.image=self.originalImage;
    
    self.filterSwitcherView.backgroundColor=[UIColor clearColor];

    if(self.camera_mode==CameraModeGIF)
    {
        [[NSNotificationCenter defaultCenter]addObserver:self
                                                selector:@selector(applicationDidBecomeActive:)
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
        
        //https://stackoverflow.com/questions/29723024/creating-a-large-gif-with-cgimagedestinationfinalize-running-out-of-memory
        [self startLoadingActivity];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            _bufferReader = [[BufferReader alloc] initWithDelegate:self];
            [_bufferReader startReadingAsset:_recordSession.assetRepresentingSegments error:nil];
            self.arrayImages = [NSMutableArray new];
        });
    }
    else
        [self loadData];
    
}

-(void)generateGifFromFrames
{
    NSMutableArray *array=[NSMutableArray new];
    [array addObjectsFromArray:self.arrayImages];
    NSArray* reversedArray = [[self.arrayImages reverseObjectEnumerator] allObjects];
    [array addObjectsFromArray:reversedArray];
    [self.arrayImages removeAllObjects];
    
    TestFrameGenerator *generator = [[TestFrameGenerator alloc] init];
    generator.baseImage=[array firstObject];
    generator.arrayImages=array;
    generator.totalFramesCount=array.count;
    generator.frameGeneratedCallback = ^{
    };
    
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil];
    NSURL *url = [documentsDirectoryURL URLByAppendingPathComponent:@"video.m4v"];
    
    DqdGeneratedVideo *video = [[DqdGeneratedVideo alloc] initWithOutputURL:url frameGenerator:generator];
    [video whenVideoIsFullyWritten:^{
        generator.arrayImages=nil;
        generator.baseImage=nil;
        _recordSession = [SCRecordSession recordSession];
        _recordSession.fileType = AVFileTypeQuickTimeMovie;
        [_recordSession addSegment:[SCRecordSessionSegment segmentWithURL:url info:nil]];
        [self loadData];
        
        MPLog(@"path = %@", url.path);
    } ifError:^(NSError *error) {
        MPLog(@"error = %@", error);
        [ToastMessage showSuccessMessage:error.description];
    }];

}

#pragma mark - BufferReaderDelegate
- (void)bufferReader:(BufferReader *)reader didFinishReadingAsset:(AVAsset *)asset
{
      //  MPLog(@"didFinishReadingAsset");
        [self generateGifFromFrames];
        self.bufferReader=nil;
}

- (void)bufferReader:(BufferReader *)reader didGetNextVideoSample:(CMSampleBufferRef)bufferRef
{
   // MPLog(@"didGetNextVideoSample");
    if(self.arrayImages.count<kGifImageCount && _bufferReader.frameCount%2 !=0)
    {
        //http://stackoverflow.com/questions/3305862/uiimage-created-from-cmsamplebufferref-not-displayed-in-uiimageview
        CGImageRef cgImage = [self imageFromSampleBuffer:bufferRef];
        UIImage *img = [UIImage imageWithCGImage:cgImage];
        NSData *imageData = UIImageJPEGRepresentation(img,0.7);
        [self.arrayImages addObject:[UIImage imageWithData:imageData]];
        CGImageRelease(cgImage);
    }

}
- (void)bufferReader:(BufferReader *)reader didGetErrorRedingSample:(NSError *)error
{
   // MPLog(@"didGetErrorRedingSample");
}

- (CGImageRef) imageFromSampleBuffer:(CMSampleBufferRef) sampleBuffer // Create a CGImageRef from sample buffer data
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer,0);        // Lock the image buffer
    
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddressOfPlane(imageBuffer, 0);   // Get information of the image
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGContextRef newContext = CGBitmapContextCreate(baseAddress, width, height, 8, bytesPerRow, colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef newImage = CGBitmapContextCreateImage(newContext);
    CGContextRelease(newContext);
    
    CGColorSpaceRelease(colorSpace);
    CVPixelBufferUnlockBaseAddress(imageBuffer,0);
    /* CVBufferRelease(imageBuffer); */  // do not call this!
    
    return newImage;
}

-(void)setUpMusicTrackNotification
{
    if(self.myPlayer==nil)
    {
    self.vwSongInfo.frame=CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
   
    self.lblAlbumName.textColor=[UIColor whiteColor];
    self.lblSongTitle.textColor=[UIColor whiteColor];
    self.lblArtist.textColor=[UIColor whiteColor];
    
    self.lblSongTitle.font=FONT_AVENIR_PRO_DEMI_H6;
    self.lblAlbumName.font=FONT_AVENIR_PRO_DEMI_H4;
    self.lblArtist.font=FONT_AVENIR_PRO_DEMI_H4;
    // creating simple audio player
  
    self.myPlayer = [MPMusicPlayerController systemMusicPlayer];
    
    // assing a playback queue containing all media items on the device
    [self.myPlayer setQueueWithQuery:[MPMediaQuery songsQuery]];
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver:self
                                selector:@selector(getTrackDescription:)
                                    name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                                  object:self.myPlayer];
    
    [notificationCenter addObserver:self
                                selector:@selector(handle_PlayBackNotification:)
                                    name:MPMusicPlayerControllerPlaybackStateDidChangeNotification
                                  object:self.myPlayer];
    
    
    [self.myPlayer beginGeneratingPlaybackNotifications];
     
    if(self.myPlayer.playbackState==MPMoviePlaybackStatePlaying)
         [self getTrackDescription:nil];
    }
}

-(void)removeMusicTrackNotification
{
    
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                           name:MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                                         object:self.myPlayer];
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                           name:MPMusicPlayerControllerPlaybackStateDidChangeNotification
                                         object:self.myPlayer];
        
        [self.myPlayer endGeneratingPlaybackNotifications];
        self.myPlayer=nil;
    
}

- (void)handle_PlayBackNotification:(id)notification{
     MPLog(@"self.myPlayer.playbackState>>%ld",(long)self.myPlayer.playbackState);
    if(self.myPlayer.playbackState == MPMusicPlaybackStatePlaying){
        [self getTrackDescription:notification];
    } else {
    
    }
    
}
- (void)getTrackDescription:(id)notification {
    MPLog(@"self.myPlayer.playbackState>>%ld",(long)self.myPlayer.playbackState);
    if(self.myPlayer.playbackState==MPMusicPlaybackStatePlaying)
    {
    dispatch_async(dispatch_get_main_queue(), ^(void){

    // getting whats currently playing
    MPMediaItem * nowPlayingItem = self.myPlayer.nowPlayingItem;
    
    // song title currently playing
    NSString * title = [nowPlayingItem valueForProperty:MPMediaItemPropertyTitle];
    
    // if title is not fund Unknown will be displayed
    if (title == (id)[NSNull null] || title.length == 0) {
        title = @"Unknown";
    }
    
    // artist currently playing
    NSString * artist = [nowPlayingItem valueForProperty:MPMediaItemPropertyArtist];
    
    // if artist is not fund Unknown will be displayed
    if (artist == (id)[NSNull null] || artist.length == 0) {
        artist = @"Unknown";
    }
    
    NSString * album = [nowPlayingItem valueForProperty:MPMediaItemPropertyAlbumTitle];
    
    // if artist is not fund Unknown will be displayed
    if (album == (id)[NSNull null] || album.length == 0) {
        album = @"Unknown";
    }
        
    self.vwSongInfo.frame=CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
//    self.vwSongInfo.frame=CGRectMake(0, 0, _originalImageSize.width, _originalImageSize.height);

    self.lblSongTitle.text=title;
    self.lblArtist.text=artist;
    self.lblAlbumName.text=album;
    [self.vwSongInfo layoutIfNeeded];
        
    UIImage *artworkUIImage = [nowPlayingItem.artwork imageWithSize:CGSizeMake(90, 90)];
    self.imgAlbum.image=artworkUIImage;
    
    UIImage* image1 =[self imageWithView:self.vwSongInfo];
    CIImage* ciimage = [[CIImage alloc] initWithImage:image1];
    
    NSInteger selectedFilterIndex=[self.filterSwitcherView.filters indexOfObject:self.filterSwitcherView.selectedFilter];
    self.filterSwitcherView.filters=nil;
    
    self.filterSwitcherView.filters = @[
                                        [SCFilter emptyFilter],
                                        [SCFilter filterWithCIFilterName:@"CIPhotoEffectFade"],
                                        [SCFilter filterWithCIFilterName:@"CIPhotoEffectInstant"],
                                        [SCFilter filterWithCIFilterName:@"CIPhotoEffectNoir"],
                                        [SCFilter filterWithCIImage:ciimage]
                                        ];
    
        _player.SCImageView=nil;
         [self.filterSwitcherView setImageByUIImage:nil];
       _player.SCImageView = self.filterSwitcherView;
        [self.filterSwitcherView setImageByUIImage:self.originalImage];
        [self.filterSwitcherView scrollToFilter:[self.filterSwitcherView.filters objectAtIndex:selectedFilterIndex] animated:NO];
        
        [self.filterSwitcherView layoutIfNeeded];
    });
    }
}

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    return [documentsPath stringByAppendingPathComponent:name];
}
-(UIImage*)getImagedFromVideo
{
    //http://stackoverflow.com/questions/17436552/get-all-frames-of-video-ios-6
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
//    NSString *documentsPath = [paths objectAtIndex:0];
    
    AVAsset *asset = _recordSession.assetRepresentingSegments;
    // AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:_recordSession.outputUrl options:nil];
   
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.requestedTimeToleranceAfter =  kCMTimeZero;
    generator.requestedTimeToleranceBefore =  kCMTimeZero;
    UIImage* generatedImage;
    for (Float64 i = 0; i < CMTimeGetSeconds(asset.duration) *  10 ; i++){
        @autoreleasepool {
            CMTime time = CMTimeMake(i, 10);
            NSError *err;
            CMTime actualTime;
            CGImageRef image = [generator copyCGImageAtTime:time actualTime:&actualTime error:&err];
            generatedImage = [[UIImage alloc] initWithCGImage:image];
//            NSData *pngData = UIImagePNGRepresentation(generatedImage);
//            NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"gif_image_%f.png",i]]; //Add the file name
//            
//            //  MPLog(@"filePath >>%@",filePath);
//            [pngData writeToFile:filePath atomically:NO]; //Wr
//            [_arrayImages addObject:filePath];
            
            CGImageRelease(image);
        }
    }
    return generatedImage;
}

- (NSURL *)documentDirectoryURL {
    return [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil];
}


-(void)loadData
{
   // double delayInSeconds = 0.0f;
   // dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
   // dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
    if(self.camera_mode==CameraModeVideo || self.camera_mode==CameraModeGIF)
    {
        //if(self.camera_mode==CameraModeVideo)//for gif its already started
           //[self startLoadingActivity];
           
        _player = [SCPlayer player];
        _player.delegate = self;
        [_player beginSendingPlayMessages];
        
        [self addFilters];
        
        _player.loopEnabled = YES;
       // [_player setItemByAsset:_recordSession.assetRepresentingSegments];
        if(!_player.isPlaying)
        {
           // MPLog(@"Playing ---1");
            [_player setItemByAsset:_recordSession.assetRepresentingSegments];
            [_player play];
        }
        
        /*self.avPlayer = [AVPlayer playerWithURL:self.videoUrl];
         self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
         
         self.avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
         self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
         
         [[NSNotificationCenter defaultCenter] addObserver:self
         selector:@selector(playerItemDidReachEnd:)
         name:AVPlayerItemDidPlayToEndTimeNotification
         object:[self.avPlayer currentItem]];
         
         self.avPlayerLayer.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
         [self.imageView.layer addSublayer:self.avPlayerLayer];
         [self.avPlayer play];*/
        
    }
    else
    {
        self.filterSwitcherView.hidden=NO;
        [self addFilters];
      
    }
    
    _originalImageSize = self.imageView.image.size;
    self.drawingView.delegate = self;
    self.drawingView.drawTool = ACEDrawingToolTypePen;
    // self.drawingView.drawMode=ACEDrawingModeScale;
    self.drawingView.lineWidth = 5.0;
    
    //selt default color
    self.drawingView.lineColor=PICKER_COLOR_PINK;
    
    
    //[self.drawingView loadImage:self.image];
    
    //    self.drawingView.lineColor = [UIColor blackColor];
    //
    //    self.drawingView.lineAlpha = 1;
    //   // [self.drawingView setBackgroundImage:self.image];
    //
    
    
    _drawTextView.font =FONT_AVENIR_LIGHT_H20;// FONT_AVENIR_PRO_DEMI_H7;
    _drawTextView.textColor = [UIColor whiteColor];
    _drawTextView.textBackgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.5];
    
    [self.filterSwitcherView setImageByUIImage:self.originalImage];
    [self.filterSwitcherView bringSubviewToFront:_drawingView];
    [self.filterSwitcherView bringSubviewToFront:_drawTextView];
    [self getAddressFromLatLon];
    
    UITapGestureRecognizer *swipeRecognizer =
    [[UITapGestureRecognizer alloc]
     initWithTarget:self
     action:@selector(swipeDetected:)];
    swipeRecognizer.numberOfTapsRequired=1;
    [self.filterSwitcherView addGestureRecognizer:swipeRecognizer];
   // _drawTextView.userInteractionEnabled=NO;
    _drawingView.userInteractionEnabled=NO;
     self.filterSwitcherView.userInteractionEnabled=YES;

    //preload friends list
    [self preLoadFriendsList];
// });
//  [self initPageViewController];

}

-(void)addFilters
{
    self.filterSwitcherView.contentMode = UIViewContentModeScaleAspectFill;
    
    if ([[NSProcessInfo processInfo] activeProcessorCount] > 1) {
        
        SCFilter *emptyFilter = [SCFilter emptyFilter];
        emptyFilter.name = @"#nofilter";
     
        self.filterSwitcherView.filters = @[
                                            emptyFilter,
                                            [SCFilter filterWithCIFilterName:@"CIPhotoEffectFade"],
                                            [SCFilter filterWithCIFilterName:@"CIPhotoEffectInstant"],
                                            [SCFilter filterWithCIFilterName:@"CIPhotoEffectNoir"],
//                                            [SCFilter filterWithCIFilterName:@"CIPhotoEffectTonal"],
//                                            [SCFilter filterWithCIFilterName:@"CIPhotoEffectFade"],
                                            // Adding a filter created using CoreImageShop
                                            //                                                    [SCFilter filterWithContentsOfURL:[[NSBundle mainBundle] URLForResource:@"a_filter" withExtension:@"cisf"]],
                                            // [self createAnimatedFilter]
                                            ];
        
      
        _player.SCImageView = self.filterSwitcherView;
        // [self.filterSwitcherView addObserver:self forKeyPath:@"selectedFilter" options:NSKeyValueObservingOptionNew context:nil];
    } else {
        SCVideoPlayerView *playerView = [[SCVideoPlayerView alloc] initWithPlayer:_player];
        playerView.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        playerView.frame = self.filterSwitcherView.frame;
        playerView.autoresizingMask = self.filterSwitcherView.autoresizingMask;
        [self.filterSwitcherView.superview insertSubview:playerView aboveSubview:self.filterSwitcherView];
        [self.filterSwitcherView removeFromSuperview];
    }

  /*  self.filterSwitcherView.filters = @[
                                        emptyFilter,
                                        [SCFilter filterWithCIFilterName:@"CIPhotoEffectNoir"],
                                        [SCFilter filterWithCIFilterName:@"CIPhotoEffectChrome"],
                                        [SCFilter filterWithCIFilterName:@"CIPhotoEffectInstant"],
                                        [SCFilter filterWithCIFilterName:@"CIPhotoEffectTonal"],
                                        [SCFilter filterWithCIFilterName:@"CIPhotoEffectFade"],
                                      
                                        ];*/

   
}

//http://stackoverflow.com/questions/29723024/creating-a-large-gif-with-cgimagedestinationfinalize-running-out-of-memory
-(NSURL *)makeAnimatedGif{
    NSMutableArray *array=[NSMutableArray new];
    [array addObjectsFromArray:self.arrayImages];
    NSArray* reversedArray = [[self.arrayImages reverseObjectEnumerator] allObjects];
    [array addObjectsFromArray:reversedArray];
   // [self.arrayImages removeAllObjects];
    
    NSUInteger kFrameCount = array.count;
    
    NSDictionary *fileProperties = @{
                                     (__bridge id)kCGImagePropertyGIFDictionary: @{
                                             (__bridge id)kCGImagePropertyGIFLoopCount: @0,
                                             (__bridge id) kCGImagePropertyGIFHasGlobalColorMap:@0,// 0 means loop forever,
                                             (__bridge NSString *)kCGImageDestinationLossyCompressionQuality: @(0.6)

                                             }
                                     };
    
    CGFloat time_per_frame=0.5/(self.arrayImages.count/2);
    NSDictionary *frameProperties = @{
                                      (__bridge id)kCGImagePropertyGIFDictionary: @{
                                              (__bridge id)kCGImagePropertyGIFDelayTime: [NSNumber numberWithFloat:time_per_frame], // a float (not double!) in seconds, rounded to centiseconds in the GIF data
                                              }
                                      };
    
    NSDictionary *frameProperties1 = @{
                                       (__bridge id)kCGImagePropertyGIFDictionary: @{
                                               (__bridge id)kCGImagePropertyGIFDelayTime:[NSNumber numberWithFloat:time_per_frame+0.1], // a float (not double!) in seconds, rounded to centiseconds in the GIF data
                                               }
                                       };
    
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil];
    NSURL *fileURL = [documentsDirectoryURL URLByAppendingPathComponent:@"animated.gif"];
    
    CGImageDestinationRef destination = CGImageDestinationCreateWithURL((__bridge CFURLRef)fileURL, kUTTypeGIF, kFrameCount, NULL);
    CGImageDestinationSetProperties(destination, (__bridge CFDictionaryRef)fileProperties);
    for (NSUInteger i = 0; i < kFrameCount; i++) {
        @autoreleasepool {
            NSData *pngData = [NSData dataWithContentsOfFile:array[i]];
            UIImage *image = [UIImage imageWithData:pngData];
          //  UIImage *image = array[i] ;
            if(i>=self.arrayImages.count)
                   CGImageDestinationAddImage(destination, image.CGImage, (__bridge CFDictionaryRef)frameProperties1);
            else
                CGImageDestinationAddImage(destination, image.CGImage, (__bridge CFDictionaryRef)frameProperties);
        }
    }
    
    if (!CGImageDestinationFinalize(destination)) {
       MPLog(@"failed to finalize image destination");
    }
    CFRelease(destination);
    MPLog(@"url=%@", fileURL);
  
    UIImage* mygif = [UIImage animatedImageWithAnimatedGIFURL:fileURL];
    self.imageView.image=mygif;
    [self.arrayImages removeAllObjects];
    [self stopLoadingActivity];
    self.originalImage=mygif;
    [self loadData];
    return fileURL;
}
-(void)preLoadFriendsList
{
    if(self.comefrom==CameraMainComeFromHome)
    {
          double delayInSeconds = 1.0f;
         dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
         dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
             [[FriendsManager sharedInstance]fetchFriendList];
         });
    }
}

-(void)didEndTextEditing
{
    //_drawTextView.userInteractionEnabled=NO;

}
-(void)swipeDetected:(UITapGestureRecognizer *)gestureRecognizer
{
    if(_drawingView.userInteractionEnabled==NO)
    {
        if (_isDrawingTextEditing) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"DISMISS_KEYBOARD" object:nil];
            _isDrawingTextEditing = NO;
        }
        else {
            _drawTextView.userInteractionEnabled=YES;
            [self clickText:nil];
            _isDrawingTextEditing = YES;
        }
    }
  
}

- (SCFilter *)createAnimatedFilter {
    SCFilter *animatedFilter = [SCFilter emptyFilter];
    animatedFilter.name = @"Animated Filter";
    
    SCFilter *gaussian = [SCFilter filterWithCIFilterName:@"CIGaussianBlur"];
    SCFilter *blackAndWhite = [SCFilter filterWithCIFilterName:@"CIColorControls"];
    
    [animatedFilter addSubFilter:gaussian];
    [animatedFilter addSubFilter:blackAndWhite];
    
    double duration = 0.5;
    double currentTime = 0;
    BOOL isAscending = YES;
    
    Float64 assetDuration = CMTimeGetSeconds(_recordSession.assetRepresentingSegments.duration);
    
    while (currentTime < assetDuration) {
        if (isAscending) {
            [blackAndWhite addAnimationForParameterKey:kCIInputSaturationKey startValue:@1 endValue:@0 startTime:currentTime duration:duration];
            [gaussian addAnimationForParameterKey:kCIInputRadiusKey startValue:@0 endValue:@10 startTime:currentTime duration:duration];
        } else {
            [blackAndWhite addAnimationForParameterKey:kCIInputSaturationKey startValue:@0 endValue:@1 startTime:currentTime duration:duration];
            [gaussian addAnimationForParameterKey:kCIInputRadiusKey startValue:@10 endValue:@0 startTime:currentTime duration:duration];
        }
        
        currentTime += duration;
        isAscending = !isAscending;
    }
    
    return animatedFilter;
}

/*- (void)player:(SCPlayer *__nonnull)player didPlay:(CMTime)currentTime loopsCount:(NSInteger)loopsCount
{
     //MPLog(@"^^^^^didPlay");
}
- (void)player:(SCPlayer *__nonnull)player didChangeItem:(AVPlayerItem *__nullable)item
{
   // MPLog(@"^^^^^didChangeItem");
}

- (void)player:(SCPlayer *__nonnull)player didReachEndForItem:(AVPlayerItem *__nonnull)item
{
   // MPLog(@"^^^^^didReachEndForItem");
}*/

- (void)player:(SCPlayer *__nonnull)player didSetupSCImageView:(SCImageView *__nonnull)SCImageView
{
    [self stopLoadingActivity];
    MPLog(@"^^^^^didSetupSCImageView");
}

- (void)player:(SCPlayer *__nonnull)player itemReadyToPlay:(AVPlayerItem *__nonnull)item
{
   // MPLog(@"^^^^^itemReadyToPlay");
}

- (void)playerItemDidReachEnd:(NSNotification *)notification {
   // AVPlayerItem *p = [notification object];
    //[p seekToTime:kCMTimeZero];
}

-(void)startPlayingVideo
{
    if(!_player.isPlaying && _player !=nil)
    {
       //  MPLog(@"Playing---2");
       // [_player setItemByAsset:_recordSession.assetRepresentingSegments];
        [_player play];
    }
}

- (void)applicationDidBecomeActive:(id)sender {
    //the GIF stops replaying when you go out of the app and then go back in
    [self startPlayingVideo];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(self.navigationController.navigationBarHidden==NO)
        [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
    if(self.camera_mode==CameraModeVideo|| self.camera_mode==CameraModeGIF)
    {
        [self startPlayingVideo];
    }
    
    [self setUpMusicTrackNotification];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [_player pause];
    [self removeMusicTrackNotification];
}

-(void)initPageViewController

{
  //  if(self.arrayList.count)
    {
        if(_pageViewController.view)
        {
           
            ImageFiltersViewController *startingViewController = [self viewControllerAtIndex:nil index:0];;
            if(startingViewController)
            {
                NSArray *viewControllers = @[startingViewController];
                [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            }
            
        }
        else
        {
            _pageViewController=[[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
            _pageViewController.delegate=self;
            _pageViewController.dataSource=self;
            _pageViewController.view.backgroundColor=[UIColor clearColor];
            
            ImageFiltersViewController *startingViewController = [self viewControllerAtIndex:nil index:0];
            NSArray *viewControllers = @[startingViewController];
            [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            
            // Change the size of page view controller
            _pageViewController.view.frame=CGRectMake(0,0 , SCREEN_SIZE.width, SCREEN_SIZE.height);
            
            [self.view addSubview:_pageViewController.view];
           // [self.view sendSubviewToBack:_pageViewController.view];
            
            [_pageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@0);
                make.right.equalTo(@0);
                make.top.equalTo(@0);
                make.bottom.equalTo(@0);
            }];
        }
    }
}
#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((ImageFiltersViewController*) viewController).pageIndex;
    _currentPageIndex=index;
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
   return [self viewControllerAtIndex:viewController index:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((ImageFiltersViewController*) viewController).pageIndex;
    _currentPageIndex=index;
    index++;
 
    return [self viewControllerAtIndex:viewController index:index];
}

- (ImageFiltersViewController *)viewControllerAtIndex:(UIViewController *)viewController index:(NSInteger)index
{
    ImageFiltersViewController *contentViewController = [[ImageFiltersViewController alloc]initWithNibName:@"ImageFiltersViewController" bundle:nil];
    contentViewController.view.frame=CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    contentViewController.view.alpha=0.4;
    if(viewController.view.backgroundColor==nil)
        contentViewController.view.backgroundColor=[UIColor clearColor];
    else  if(viewController.view.backgroundColor==[UIColor clearColor])
            contentViewController.view.backgroundColor=[UIColor redColor];
    else  if(viewController.view.backgroundColor==[UIColor redColor])
        contentViewController.view.backgroundColor=[UIColor lightGrayColor];
    else  if(viewController.view.backgroundColor==[UIColor lightGrayColor])
        contentViewController.view.backgroundColor=[UIColor clearColor];
  
    contentViewController.pageIndex = index;
    return contentViewController;
}


-(void)getAddressFromLatLon
{
    [Utility getAddressFromLatLngSuccess:^(CLPlacemark *placemark) {
        
        NSString *address= [Utility getCityCountryFromPlacemark:placemark];
        
        FSVenue *obj=[FSVenue new];
        obj.venueId=[NSString stringWithFormat:@"%@_%@",address,[VVBaseUserDefaults userId]];
        if([Utility isEmpty:placemark.name])
        {
            obj.name=address;
        }
        else
        {
            obj.name=placemark.name;
            obj.location=[FSLocation new];
            obj.location.address=address;
        }
        if(obj.location==nil)
            obj.location=[FSLocation new];
        
        obj.location.coordinate=[LocationClass sharedLoctionManager].currentLocation.coordinate;
        
        [self didSelectLocation:obj];
    }];
}

-(CLLocationCoordinate2D)translateCoordMetersLat:(double)metersLat MetersLong:(double)metersLong{
    
    CLLocationCoordinate2D coord=[LocationClass sharedLoctionManager].currentLocation.coordinate;
    CLLocationCoordinate2D tempCoord;
    
    MKCoordinateRegion tempRegion = MKCoordinateRegionMakeWithDistance(coord, metersLat, metersLong);
    MKCoordinateSpan tempSpan = tempRegion.span;
    
    tempCoord.latitude = coord.latitude + tempSpan.latitudeDelta;
    tempCoord.longitude = coord.longitude + tempSpan.longitudeDelta;
    
    MPLog(@"current>>>%f,  %f",coord.latitude,coord.longitude);
    MPLog(@"new>>>%f,  %f",tempCoord.latitude,tempCoord.longitude);

    return tempCoord;
    
}

#pragma mark - IBAction Methods
-(void)setFontAndColor
{
    self.lblLocationName.font=FONT_AVENIR_MEDIUM_H2;//FONT_AVENIR_PRO_DEMI_H2;
    self.lblLocationAddress.font=FONT_AVENIR_LIGHT_H0;//FONT_AVENIR_PRO_DEMI_H0;
    self.lblLocationName.text=MPLocalizedString(@"add_location", nil);
    self.lblLocationAddress.text=nil;
    
    if(self.camera_mode==CameraModeVideo)
    {
        [self.btnClose setImage:[UIImage imageNamed:@"back_white"] forState:UIControlStateNormal];
    }
    
}


-(void)replyToFriend:(UIImage *)final_image
{
    MPLog(@"replyToFriend>>%ld",(long)self.reply_userId);
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[UserAPI new]replyToFriendWithImage:final_image withFriendId:self.reply_userId withVenue:nil withText:nil success:nil failure:^(NSError *error) {
            [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
            
        }];
    });
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)uploadSnapToServer
{
    
    if(self.camera_mode==CameraModeVideo||self.camera_mode==CameraModeGIF)
    {
        self.drawingView.backgroundColor=[UIColor clearColor];
//        UIImage * color_image=[self imageWithView:self.drawingView];
        UIImage *final_image = [self getImagedFromVideo];
        
        SendToViewController *cnt=[[SendToViewController alloc]initWithNibName:@"SendToViewController" bundle:nil];
        cnt.finalSnap=final_image;
        
        //https://trello.com/c/CIo9GGnu/228-there-is-no-preview-of-video-gif-in-my-journey-only-white-screen
        cnt.bannerImg=final_image;
        
        cnt.objVenue=self.objVenue;
        cnt.recordSession=_recordSession;
        cnt.selectedFilter=self.filterSwitcherView.selectedFilter;
        cnt.camera_mode=self.camera_mode;
        cnt.gifUrl=self.gifUrl;
        cnt.imageText=_drawTextView.text;
        [self.navigationController pushViewController:cnt animated:NO];
        
       /* [_recordSession mergeSegmentsUsingPreset:AVAssetExportPresetLowQuality completionHandler:^(NSURL *url, NSError *error) {
            if (error == nil) {
                self.videoUrl=url;
                NSLog(@"url>>%@",self.videoUrl);
                // Easily save to camera roll
                UIImage * color_image=[self imageWithView:self.drawingView];
                UIImage *final_image = [self.drawTextView renderTextOnImage:color_image];
                UIImage *thumbnail=[Utility thumbnailImageFromURL:self.videoUrl];
                
                SendToViewController *cnt=[[SendToViewController alloc]initWithNibName:@"SendToViewController" bundle:nil];
                cnt.bannerImg=thumbnail;
                cnt.finalSnap=final_image;
                cnt.objVenue=self.objVenue;
                cnt.videoUrl=self.videoUrl;
                cnt.recordSession=_recordSession;
                [self.navigationController pushViewController:cnt animated:YES];

            } else {
                NSLog(@"Bad things happened: %@", error);
            }
        }];*/
        
    }
    else
    {
        UIImage * color_image=[self buildImage:[self imageWithView:self.filterSwitcherView]];
       // UIImage * color_image=[self buildImage:[self imageWithView:self.drawingView]];
       // UIImage *final_image = [self.drawTextView renderTextOnImage:color_image];
        
        UIImage *final_image=color_image;
       /* UIImageView *imgTemp=[[UIImageView alloc]initWithFrame:CGRectMake(0, 50, 250,400)];
        imgTemp.contentMode=UIViewContentModeScaleToFill;
        imgTemp.image=final_image;
        [self.view addSubview:imgTemp];
        [self.view bringSubviewToFront:imgTemp];
        return;*/
        if(self.comefrom==CameraMainComeFromReplyUser)
        {
            [self replyToFriend:final_image];
        }
        else  if(self.comefrom==CameraMainComeFromHome)
        {
            SendToViewController *cnt=[[SendToViewController alloc]initWithNibName:@"SendToViewController" bundle:nil];
            cnt.bannerImg=self.originalImage;
            cnt.finalSnap=final_image;
            cnt.objVenue=self.objVenue;
            cnt.camera_mode=self.camera_mode;
            cnt.gifUrl=self.gifUrl;
            cnt.imageText=_drawTextView.text;
            [self.navigationController pushViewController:cnt animated:NO];
        }
    }

}

-(void)uploadSnapToServerWithImage:(UIImage *)finalSnap
{
    if(self.uploadedMsgId==0)
    {
        [self showLoader];
        [[UserAPI new]uploadSnapToServer:finalSnap
                             withBannerImag:self.originalImage
                                withText:_drawTextView.text
                            withVideoUrl:self.videoUrl
                             isMyJourney:NO
                               withVenue:self.objVenue
                              cameraMode:self.camera_mode
                                 success:^(NSInteger uploaded_id) {
            self.uploadedMsgId=uploaded_id;
            [self sendMessageToFriends];
            
        } failure:^(NSError *error) {
            [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
            [self stopLoader];
        }];
    }
    else
        [self sendMessageToFriends];
}

-(void)sendMessageToFriends
{
    if(self.reply_userId)
    {
        [self showLoader];
        NSMutableArray *arr=[[NSMutableArray alloc]initWithObjects:[NSString stringWithFormat:@"%ld",(long)self.reply_userId], nil];
        [[UserAPI new]sendMessageToFriends:arr withMsgId:self.uploadedMsgId success:^(BOOL status) {
            [self dismissViewControllerAnimated:NO completion:nil];
        } failure:^(NSError *error) {
            [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
            [self stopLoader];
        }];
    }
    else
        [self dismissViewControllerAnimated:NO completion:nil];
    
}

-(void)showLoader
{
    [self startLoadingActivity];
}

-(void)stopLoader
{
    [self stopLoadingActivity];
}
-(IBAction)clickSelectLocation:(id)sender
{
    SelectLocationViewController *cnt=[[SelectLocationViewController alloc]initWithNibName:@"SelectLocationViewController" bundle:nil];
    cnt.delegate=self;
    cnt.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:cnt animated:YES completion:nil];
}

-(IBAction)clickNext:(id)sender
{
 
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    else if(self.objVenue==nil)
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"you_have_to_choose_location", nil)];
    else
    {
        [self uploadSnapToServer];
    }
}
-(IBAction)clickClose:(id)sender
{
    MPLog(@"clickClose");
    [self.navigationController popViewControllerAnimated:NO];
   
}

-(UIImage *)imageWithView:(UIView *)view
{
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

- (UIImage*)buildImage:(UIImage *)secondImage
{
    //http://stackoverflow.com/questions/18273271/merge-two-image-on-to-one-image-programmatically-in-iphone
    UIImage *firstImage = [_filterSwitcherView renderedUIImageInRect:CGRectMake(0, 0, SCREEN_SIZE.width*2, SCREEN_SIZE.height*2)];
    UIGraphicsBeginImageContextWithOptions(_originalImageSize, NO, firstImage.scale);

    [firstImage drawInRect:CGRectMake(0, 0, _originalImageSize.width, _originalImageSize.height)];
    [secondImage drawInRect:CGRectMake(0, 0, _originalImageSize.width, _originalImageSize.height)];
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
   /* UIGraphicsBeginImageContextWithOptions(_originalImageSize, NO, self.imageView.image.scale);
    [self.imageView.image drawAtPoint:CGPointZero];
    [image drawInRect:CGRectMake(0, 0, _originalImageSize.width, _originalImageSize.height)];
    
    UIImage *tmp = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return tmp;*/
}

-(IBAction)clickDownLoad:(id)sender
{
    if(self.camera_mode==CameraModePhoto)
    {
        UIImage * color_image=[self buildImage:[self imageWithView:self.filterSwitcherView]];
        if(color_image)
        {
            [self saveToMaplyAlbum:color_image];
        }
    }
    else  if(self.camera_mode==CameraModeVideo || self.camera_mode==CameraModeGIF)
    {
        [self saveVideoToMaplyAlbum];
    }

}

-(void)saveToMaplyAlbum:(UIImage *)image
{
        __weak EditImageViewController *weakself = self;
        __weak ALAssetsLibrary *lib = self.library;
    
         [KSToastView ks_showToast:MPLocalizedString(@"saving", nil) duration:1.0f completion:nil];
    
        [self.library addAssetsGroupAlbumWithName:APP_NAME_TITLE resultBlock:^(ALAssetsGroup *group) {
                 
                 ///checks if group previously created
                 if(group == nil){
                     //enumerate albums
                     [lib enumerateGroupsWithTypes:ALAssetsGroupAlbum
                                        usingBlock:^(ALAssetsGroup *g, BOOL *stop)
                      {
                          //if the album is equal to our album
                          if ([[g valueForProperty:ALAssetsGroupPropertyName] isEqualToString:APP_NAME_TITLE]) {
                              
                              if(self.camera_mode==CameraModeVideo|| self.camera_mode==CameraModeGIF)
                              {
                                  [lib writeVideoAtPathToSavedPhotosAlbum:weakself.videoUrl completionBlock:^(NSURL *assetURL, NSError *error) {
                                      
                                      //then get the image asseturl
                                      [lib assetForURL:assetURL
                                           resultBlock:^(ALAsset *asset) {
                                               //put it into our album
                                               [g addAsset:asset];
                                               
                                               [KSToastView ks_showToast:MPLocalizedString(@"saved_to_gallary", nil) duration:1.0f completion:nil];
                                               
                                           } failureBlock:^(NSError *error) {
                                               [ToastMessage showErrorMessageAppTitleWithMessage:error.description];
                                           }];
                                       }];
                              }
                            else
                            {
                              //save image
                              [lib writeImageDataToSavedPhotosAlbum:UIImagePNGRepresentation(image) metadata:nil
                                                    completionBlock:^(NSURL *assetURL, NSError *error) {
                                                        
                                                        //then get the image asseturl
                                                        [lib assetForURL:assetURL
                                                             resultBlock:^(ALAsset *asset) {
                                                                 //put it into our album
                                                                 [g addAsset:asset];
                                                                 
                                                                   [KSToastView ks_showToast:MPLocalizedString(@"saved_to_gallary", nil) duration:1.0f completion:nil];
                                                                
                                                             } failureBlock:^(NSError *error) {
                                                                 [ToastMessage showErrorMessageAppTitleWithMessage:error.description];
                                                             }];
                                                    }];
                                }
                              
                          }
                      }failureBlock:^(NSError *error){
                          NSLog(@"error.description>>%@",error.description);
                         [ToastMessage showErrorMessageAppTitleWithMessage:error.description];
                      }];
                     
                 }else{
                     if(self.camera_mode==CameraModeVideo|| self.camera_mode==CameraModeGIF)
                     {
                         [lib writeVideoAtPathToSavedPhotosAlbum:weakself.videoUrl completionBlock:^(NSURL *assetURL, NSError *error) {
                             
                             //then get the image asseturl
                             [lib assetForURL:assetURL
                                  resultBlock:^(ALAsset *asset) {
                                      //put it into our album
                                      [group addAsset:asset];
                                      
                                      [KSToastView ks_showToast:MPLocalizedString(@"saved_to_gallary", nil) duration:1.0f completion:nil];
                                      
                                  } failureBlock:^(NSError *error) {
                                       [ToastMessage showErrorMessageAppTitleWithMessage:error.description];
                                  }];
                         }];
                     }
                     else
                     {
                     // save image directly to library
                     [lib writeImageDataToSavedPhotosAlbum:UIImagePNGRepresentation(image)
                                        metadata:nil
                                           completionBlock:^(NSURL *assetURL, NSError *error) {
                                               
                                               [lib assetForURL:assetURL
                                                    resultBlock:^(ALAsset *asset) {
                                                        
                                                        [group addAsset:asset];
                                                       [KSToastView ks_showToast:MPLocalizedString(@"saved_to_gallary", nil) duration:1.0f completion:nil];
                                                        
                                                    } failureBlock:^(NSError *error) {
                                                         [ToastMessage showErrorMessageAppTitleWithMessage:error.description];
                                                    }];
                                           }];
                     }
                 }
                 
             } failureBlock:^(NSError *error) {
                  [ToastMessage showErrorMessageAppTitleWithMessage:error.description];
             }];
}

-(void)saveVideoToMaplyAlbum
{  AVAsset *asset =nil;
    if(self.camera_mode == CameraModeVideo)
    {
       asset= self.recordSession.assetRepresentingSegments;
    }
    else if(self.camera_mode == CameraModeGIF)
    {
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:YES error:nil];
        NSURL *url = [documentsDirectoryURL URLByAppendingPathComponent:@"video.m4v"];
        SCRecordSession *tmpSession=[SCRecordSession new];
        [tmpSession addSegment:[SCRecordSessionSegment segmentWithURL:url info:nil]];
        [tmpSession addSegment:[SCRecordSessionSegment segmentWithURL:url info:nil]];
        [tmpSession addSegment:[SCRecordSessionSegment segmentWithURL:url info:nil]];
        [tmpSession addSegment:[SCRecordSessionSegment segmentWithURL:url info:nil]];
        asset=tmpSession.assetRepresentingSegments;
    }

    UIImage * color_image=[self imageWithView:self.drawingView];
    UIImage *final_image = [self.drawTextView renderTextOnImage:color_image];
    SCAssetExportSession *assetExportSession = [[SCAssetExportSession alloc] initWithAsset:asset];
    assetExportSession.outputUrl = _recordSession.outputUrl;
    assetExportSession.outputFileType = AVFileTypeMPEG4;
    assetExportSession.videoConfiguration.filter = self.filterSwitcherView.selectedFilter;
    assetExportSession.videoConfiguration.preset = SCPresetHighestQuality;
    assetExportSession.audioConfiguration.preset = SCPresetLowQuality;
    
    //render text & drawing on video
    assetExportSession.videoConfiguration.watermarkImage=final_image;
    assetExportSession.videoConfiguration.watermarkFrame=[UIScreen mainScreen].bounds;
    
    [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    [assetExportSession exportAsynchronouslyWithCompletionHandler: ^{
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];

        if (assetExportSession.error == nil) {
            self.videoUrl=_recordSession.outputUrl;
            NSError *error = assetExportSession.error;
            if (assetExportSession.cancelled) {
               MPLog(@"Export was cancelled");
            } else if (error == nil) {
                //[[UIApplication sharedApplication] beginIgnoringInteractionEvents];
                // [[UIApplication sharedApplication] endIgnoringInteractionEvents];
                [self saveToMaplyAlbum:nil];
                
            } else {
                if (!assetExportSession.cancelled) {
                    [[[UIAlertView alloc] initWithTitle:@"Failed to save" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                }
            }

        } else {
            MPLog(@"Export was cancelled>>%@",assetExportSession.error);
             [[[UIAlertView alloc] initWithTitle:@"Failed to save" message:assetExportSession.error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            // Something bad happened
        }
    }];
}

-(IBAction)clickText:(id)sender
{
    [self.filterSwitcherView bringSubviewToFront:_drawTextView];
    
    //self.drawingView.drawTool = ACEDrawingToolTypeText;
   // [self.view bringSubviewToFront:self.drawTextView];
    self.drawTextView.userInteractionEnabled=YES;
    self.btnUndo.hidden=YES;
    self.vwColor.hidden=YES;
     self.btnSelectedColor.tag=0;
    [self.drawTextView startEditing];
//    UIImage * color_image=[self buildImage:[self imageWithView:self.drawingView]];
//    self.imgTmp.image = [self.drawTextView renderTextOnImage:color_image];
  //  self.filterSwitcherView.userInteractionEnabled=NO;
    self.drawingView.userInteractionEnabled=NO;
    

}

-(IBAction)clickColor:(id)sender
{
  //  [self.view bringSubviewToFront:self.drawingView];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"DISMISS_KEYBOARD" object:nil];
    self.drawingView.hidden=NO;
    self.drawTextView.userInteractionEnabled=NO;
    self.drawingView.userInteractionEnabled=YES;

    if(self.btnSelectedColor.tag==0)
    {
        self.btnSelectedColor.tag=1;
    }
    else
    {
        self.vwColor.hidden=NO;
        
        [self.view bringSubviewToFront:self.vwColor];
        self.drawTextView.userInteractionEnabled=NO;
        
    }
    
    self.btnUndo.hidden = ![self.drawingView canUndo];

   // [self updateButtonStatus];
    


}

-(IBAction)clickSelectColor:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    switch (btn.tag) {
        case 1:
            self.drawingView.lineColor=PICKER_COLOR_PINK;
            break;
        case 2:
            self.drawingView.lineColor=PICKER_COLOR_BLACK;
            break;
        case 3:
            self.drawingView.lineColor=PICKER_COLOR_WHITE;
            break;
        case 4:
            self.drawingView.lineColor=PICKER_COLOR_BLUE;
            break;
        case 5:
            self.drawingView.lineColor=PICKER_COLOR_GREEN;
            break;
        case 6:
            self.drawingView.lineColor=PICKER_COLOR_YELLOW;
            break;
            
        default:
            break;
    }
    
    self.vwColor.hidden=YES;
     self.drawingView.userInteractionEnabled=YES;
    self.btnSelectedColor.color=self.drawingView.lineColor;
}



-(IBAction)clickUndo:(id)sender
{
    [self.drawingView undoLatestStep];
    [self updateButtonStatus];

}
- (void)updateButtonStatus
{
    self.btnUndo.hidden = ![self.drawingView canUndo];

    //if(self.btnUndo.hidden)
        //self.drawingView.userInteractionEnabled=NO;
    
    if(self.btnUndo.hidden==NO)
        self.vwColor.hidden=YES;

}

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string{
    
    NSString *text = textField.text;
    text = [text stringByReplacingCharactersInRange:range withString:string];
    CGSize textSize = [text sizeWithAttributes: @{NSFontAttributeName:textField.font}];
    return (textSize.width + 10 < textField.bounds.size.width) ? true : false;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return true;
}

-(void) hideExactLocation:(FSVenue *)objVenue
{
    self.objVenue=objVenue;
    self.lblLocationName.text=objVenue.name;
    self.lblLocationAddress.text=objVenue.location.address;
    self.vwLocation.hidden=NO;
    
    self.objVenue.location=[FSLocation new];
    self.objVenue.location.coordinate=[self translateCoordMetersLat:250 MetersLong:250];
}

-(void) didSelectLocation:(FSVenue *)objVenue
{
    self.objVenue=objVenue;
    self.lblLocationName.text=objVenue.name;
    self.lblLocationAddress.text=objVenue.location.address;
    self.vwLocation.hidden=NO;
}

#pragma mark - ACEDrawing View Delegate

- (void)drawingView:(ACEDrawingView *)view didEndDrawUsingTool:(id<ACEDrawingTool>)tool
{
  [self updateButtonStatus];
  //  self.drawingView.lineWidth = 3.0;
   // self.drawingView.lineAlpha = 1;
}
- (void)drawingView:(ACEDrawingView *)view willBeginDrawUsingTool:(id<ACEDrawingTool>)tool
{
    
}

-(UIImage *)thumbnailImageFromURL:(NSURL *)videoURL {
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime requestedTime = CMTimeMake(1, 60);     // To create thumbnail image
    CGImageRef imgRef = [generator copyCGImageAtTime:requestedTime actualTime:NULL error:&err];
    NSLog(@"err = %@, imageRef = %@", err, imgRef);
    
    UIImage *thumbnailImage = [[UIImage alloc] initWithCGImage:imgRef];
    CGImageRelease(imgRef);    // MUST release explicitly to avoid memory leak
    
    return thumbnailImage;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    MPLog(@"EditImageViewController -- Dealloc");
}

@end
