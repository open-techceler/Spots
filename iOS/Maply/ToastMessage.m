//
//  ToastMessage.m
//  Beetot
//
//  Created by anand mahajan on 12/11/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import "ToastMessage.h"
static CGFloat duration =2.0f;
@implementation ToastMessage

+(void)showMessageWithTitle:(NSString*)title message:(NSString*)message type:(TWMessageBarMessageType)type
{
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:title
                                                   description:message
                                                          type:type duration:duration];
    
}

+(void)showInfoMessage:(NSString*)message
{
    [ToastMessage showMessageWithTitle:APP_NAME_TITLE message:message type:TWMessageBarMessageTypeInfo];
}

+(void)showSuccessMessage:(NSString*)message
{
    [ToastMessage showMessageWithTitle:APP_NAME_TITLE message:message type:TWMessageBarMessageTypeSuccess];
}

+(void)showErrorMessageOppsTitleWithMessage:(NSString*)message
{
    [ToastMessage showMessageWithTitle:MPLocalizedString(@"oops", nil) message:message type:TWMessageBarMessageTypeError];
}
+(void)showErrorMessageAppTitleWithMessage:(NSString*)message
{
    [ToastMessage showMessageWithTitle:APP_NAME_TITLE message:message  type:TWMessageBarMessageTypeError];

}
@end
