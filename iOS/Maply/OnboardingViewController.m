//
//  OnboardingViewController.m
//  Maply
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "OnboardingViewController.h"
#import "OnboardingView.h"
#import "LocationEnableViewController.h"
#import "NotificationEnableViewController.h"

@interface OnboardingViewController () <UIScrollViewDelegate, NotificationEnableViewControllerDelegateForSkip, LocationEnableViewControllerDelegateForSkip>
{
    bool bLoadedView;
    
    NSArray* arrayImages;
    NSArray* arrayTitles;
    NSArray* arrayDescriptions;
    
}

@property (nonatomic, strong) NSMutableArray* m_arrayViews;

@end

@implementation OnboardingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    bLoadedView = false;
    
    self.m_arrayViews = [[NSMutableArray alloc] init];
    
    arrayImages = @[@"guide1", @"guide2", @"guide3"];
    arrayTitles = @[@"LIVE Feed", @"Journeys", @"Moments"];
    arrayDescriptions = @[@"Med LIVE Feed kan du anmode dine venner \r\n om at skabe en direkte forbindelse, hvorefter I \r\n kan folge hinandens lokation live.",
                          @"Med Journeys kan du dele fede ojeblikke med \r\n hele din venneliste i 24 timer i form af bade \r\n billeder, videoer og GIF's.",
                          @"Del dine moments med udvalgte venner. \r\n Send et selfie, en video eller en GIF som kan ses i \r\n 60 sekunder."];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];

    if (bLoadedView)
        return;
    
    bLoadedView = true;
    
    self.scrollView.delegate = self;
    [self.scrollView setPagingEnabled:YES];
    [self.scrollView setScrollEnabled:YES];
    self.scrollView.showsVerticalScrollIndicator = FALSE;
    self.scrollView.showsHorizontalScrollIndicator = FALSE;
    
    self.pageControl.numberOfPages = 5;
    self.pageControl.currentPage = 0;

    [self makeScrollView];
}

- (void) makeScrollView {
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.viewContent.backgroundColor = [UIColor clearColor];
    
    float fScrollViewHeight = CGRectGetHeight(self.scrollView.bounds);
    float fScrollViewWidth = CGRectGetWidth(self.scrollView.bounds);
    
    NSDictionary *metrics = @{@"width":[NSNumber numberWithFloat:fScrollViewWidth], @"height":[NSNumber numberWithFloat:fScrollViewHeight], @"padding":[NSNumber numberWithFloat:0.f]};
    NSMutableDictionary *subviews = [[NSMutableDictionary alloc] init];
    NSString* strConstraints = @"";
    NSString* strSubViewName = @"";
    
    int nTotalAmounts = (int)arrayImages.count + 2;
    
    self.constraintContentViewWidth.constant = nTotalAmounts * fScrollViewWidth;
    [self.viewContent layoutIfNeeded];
    
    if (nTotalAmounts > 0)
    {
        for (int nIdx = 0; nIdx < nTotalAmounts; nIdx++)
        {
            UIView* curView = nil;
            
            strSubViewName = [NSString stringWithFormat:@"subview%d", nIdx + 1];

            UIView* genericSubView = nil;
            if (nIdx < 3) {
                OnboardingView* subView = (OnboardingView *)[[[NSBundle mainBundle] loadNibNamed:@"OnboardingView" owner:self options:nil] objectAtIndex:0];
                subView.frame = CGRectMake(0, 0, fScrollViewWidth, fScrollViewHeight);
                
                subView.imgGuide.image = [UIImage imageNamed:arrayImages[nIdx]];
                subView.lblTitle.text = arrayTitles[nIdx];
                subView.lblDescription.text = arrayDescriptions[nIdx];
                
                subView.translatesAutoresizingMaskIntoConstraints = NO;
                [self.viewContent addSubview:subView];

                genericSubView = subView;
            } else if (nIdx == 3) { //push notification enable
                NotificationEnableViewController* viewCon = [[NotificationEnableViewController alloc] initWithNibName:@"NotificationEnableViewController" bundle:nil];
                viewCon.bShowSkipButton = true;
                viewCon.delegateForSkip = self;
                
                [self addChildViewController:viewCon];
                viewCon.view.translatesAutoresizingMaskIntoConstraints = NO;
                [self.viewContent addSubview:viewCon.view];
                [viewCon didMoveToParentViewController:self];
                
                genericSubView = viewCon.view;
            } else if (nIdx == 4) { // location service enable
                LocationEnableViewController* viewCon = [[LocationEnableViewController alloc] initWithNibName:@"LocationEnableViewController" bundle:nil];
                viewCon.bShowSkipButton = true;
                viewCon.delegateForSkip = self;
                
                [self addChildViewController:viewCon];
                viewCon.view.translatesAutoresizingMaskIntoConstraints = NO;
                [self.viewContent addSubview:viewCon.view];
                [viewCon didMoveToParentViewController:self];
                
                genericSubView = viewCon.view;
            }
            
            curView = genericSubView;
            
            if (nIdx == 0) {
                strConstraints = [strConstraints stringByAppendingString:[NSString stringWithFormat:@"H:|[%@(width)]", strSubViewName]];
            } else if (nIdx < nTotalAmounts - 1) {
                strConstraints = [strConstraints stringByAppendingString:[NSString stringWithFormat:@"-padding-[%@(width)]", strSubViewName]];
            } else if (nIdx == nTotalAmounts - 1)
            {
                strConstraints = [strConstraints stringByAppendingString:[NSString stringWithFormat:@"-padding-[%@(width)]|", strSubViewName]];
            }
            
            [subviews setValue:genericSubView forKey:strSubViewName];
            
            [self.m_arrayViews addObject:genericSubView];
        }
        
        [self.viewContent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:strConstraints options:0 metrics:metrics views:subviews]];
        
        for (NSString* strViewName in subviews.allKeys)
        {
            [self.viewContent addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[%@(height)]|", strViewName] options:0 metrics:metrics views:subviews]];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - scrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    self.pageControl.currentPage = page;
    
    if (page >= 3) {
        self.pageControl.hidden = TRUE;
        self.scrollView.scrollEnabled = NO;
    }
}

- (void) tappedLocationSkipButton {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) tappedNotificationSkipButton {
    float fScrollViewHeight = CGRectGetHeight(self.scrollView.bounds);
    float fScrollViewWidth = CGRectGetWidth(self.scrollView.bounds);

    [self.scrollView scrollRectToVisible:CGRectMake((self.pageControl.currentPage + 1) * fScrollViewWidth, 0, fScrollViewWidth, fScrollViewHeight) animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
