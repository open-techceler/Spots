//
//  AppDelegate.h
//  Maply
//
//  Created by admin on 2/9/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) MainViewController *mainController;
@property (nonatomic, strong) NSDateFormatter *global_formatter;
- (BOOL)isInternetAvailable;
-(void)initMainAppFlow;
- (void)applyGlobalInterfaceAppearance;
- (void)applyNativeInterfaceApperance;

- (BOOL) checkPushNotificationSetting;
- (void)registerUserNotificationSetting;

- (int) checkLocationServicePermission;

- (UIViewController *) getTopViewController;

@end

