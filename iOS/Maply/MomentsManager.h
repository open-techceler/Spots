//
//  MomentsManager.h
//  Maply
//
//  Created by admin on 6/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MomentsManager : NSObject
+ (MomentsManager*)sharedInstance;
-(void)deleteMoments:(NSMutableArray *)arrIds;
-(void)markMomentAsRead:(NSInteger)messageId;
@end
