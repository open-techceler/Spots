//
//  JourneyDetailsViewController.m
//  Maply
//
//  Created by admin on 2/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "JourneyDetailsViewController.h"
#import "GetDirectionViewController.h"
#import "CameraMainViewController.h"
#import "ZFModalTransitionAnimator.h"
#import <Masonry/Masonry.h>
#import "SubbPromotionViewController.h"
#import "UserAPI.h"
#import "Journey.h"
#import "Message.h"
#import "MessageListWrapper.h"
#import "User.h"
#import "LocationClass.h"
#import "MiniToLargeViewAnimator.h"
#import "MiniToLargeViewInteractive.h"
#import "MomentsManager.h"
#import "UIImage+GIF.h"
#import "NotificationDefinitions.h"

@interface JourneyDetailsViewController ()<UIPageViewControllerDataSource,UIPageViewControllerDelegate,UIViewControllerTransitioningDelegate,UIScrollViewDelegate,UIActionSheetDelegate>

@property(nonatomic,strong)UIPageViewController *pageViewController;
@property NSInteger currentPageIndex;

@property (nonatomic, strong)  UIImage *imageSnap;
@property (nonatomic, weak)IBOutlet  UIView *vwLocation;

@property (nonatomic, weak) IBOutlet UILabel *lblLocationName,*lblLocationAddress,*lblKm;
@property (nonatomic, weak)IBOutlet  UIImageView *imgLocation;


@property (nonatomic, strong) ZFModalTransitionAnimator *animator;

@property(nonatomic)NSInteger next_offset;
@property(nonatomic,strong) NSMutableArray *arrayList;
@property (nonatomic, weak) IBOutlet RNLoadingButton *btnReply,*btnClose;
@property(nonatomic)BOOL server_request_inprogress;
@property (nonatomic, weak)IBOutlet  UIImageView *iconLocation;
@property (nonatomic) MiniToLargeViewInteractive *dismissInteractor;
@property (nonatomic) BOOL disableInteractivePlayerTransitioning;
@property (nonatomic) BOOL isMyJorney;

@end

@implementation JourneyDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setFontAndColor];
    [self initLoaderActivityWithFrame: CGRectMake((SCREEN_SIZE.width/2.0)-10,(SCREEN_SIZE.height/2.0)-40, 20, 20)];
    [self initEmptyDataLabel];
    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    [self.view sendSubviewToBack:self.vwOffline];

   // [self initPageViewController];
   // self.pageViewController.view.hidden=YES;
    
    self.arrayList=[NSMutableArray new];
    [self startLoadingActivity];
    
    if(self.objJourney==nil)//@means come from MyJourneyViewController
    {
        self.isMyJorney=YES;
        [self.btnReply setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateNormal];
        [self.btnReply setImage:[UIImage imageNamed:@"delete"] forState:UIControlStateHighlighted];
        [self.btnReply setImage:[UIImage sd_animatedGIFNamed:@"ripple"] forState:UIControlStateSelected];
        [self.btnReply  setActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite forState:UIControlStateNormal];
    }
    else if(self.comefrom==JourneyDetailsComeFromDirect)
    {
        [self.btnClose setImage:[UIImage sd_animatedGIFNamed:@"ripple"] forState:UIControlStateSelected];
    }
    else if(self.comefrom==JourneyDetailsComeFromSent)
    {
        //https://trello.com/c/1S4OQYvF
        [self.btnReply setImage:[UIImage imageNamed:@"24w"] forState:UIControlStateNormal];
        [self.btnReply setImage:[UIImage imageNamed:@"24w"] forState:UIControlStateHighlighted];

        self.btnReply.hidden = YES;
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kMessageIsDestructed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveNotification:) name:kSubbPromotionVCLoadingDone object:nil];
    
    [self loadData];
    
}

-(void)didReceiveNotification:(NSNotification*)notification {
    if ([notification.name isEqualToString:kMessageIsDestructed]) {
        NSInteger destructedMsgID = [[notification.userInfo objectForKey:@"message_id"] integerValue];
        NSInteger pageIndex = [[notification.userInfo objectForKey:@"page_index"] integerValue];
        SubbPromotionViewController *currentPage = [self.pageViewController.viewControllers lastObject];
        
        if (currentPage.objMessage.message_id == destructedMsgID) {
            if (pageIndex == self.arrayList.count - 1) {//if destructed sub is last page in the pageViewController, return
                self.disableInteractivePlayerTransitioning = YES;
                [self dismissViewControllerAnimated:NO completion:^{
                    self.disableInteractivePlayerTransitioning = NO;
                }];
            }
            else {
                [self flipToPage:pageIndex + 1];
            }
        }
    }
    else if ([notification.name isEqualToString:kSubbPromotionVCLoadingDone]) {
        [_delegate didDoneLoading:self];
    }
}

-(void) flipToPage:(NSInteger)index {
    
    SubbPromotionViewController *theCurrentViewController = [self.pageViewController.viewControllers objectAtIndex:0];
    
    NSUInteger retreivedIndex = theCurrentViewController.pageIndex;
    
    SubbPromotionViewController *firstViewController = [self viewControllerAtIndex:index];
    
    NSArray *viewControllers = @[firstViewController];
    
    if (retreivedIndex < index){
        
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
        
    } else {
        
        if (retreivedIndex > index ){
            
            [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:NULL];
        } 
    }
}

-(void)show24HSaveButton
{
    [self.btnClose setImage:[UIImage imageNamed:@"24w"] forState:UIControlStateHighlighted];
    [self.btnClose setImage:[UIImage imageNamed:@"24w"] forState:UIControlStateNormal];
}

-(void)showCloseButton
{
    [self.btnClose setImage:[UIImage imageNamed:@"close"] forState:UIControlStateHighlighted];
    [self.btnClose setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //for drag to dismiss view
    if(self.dismissInteractor==nil)
    {
        self.transitioningDelegate = self;
        self.dismissInteractor = [[MiniToLargeViewInteractive alloc] init];
        [self.dismissInteractor attachToViewController:self withView:self.view presentViewController:nil];
        self.disableInteractivePlayerTransitioning = NO;
    }
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.transitioningDelegate = nil;
    self.dismissInteractor=nil;
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self deleteViewedMoments];
    
    //@added by steven
    if ([self.delegate respondsToSelector:@selector(didClosed:)]) {
        [self.delegate didClosed:self];
    }
    //@added by steven
}

-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.lblLocationName.font=FONT_AVENIR_MEDIUM_H2;//FONT_AVENIR_PRO_DEMI_H2;
    self.lblLocationAddress.font=FONT_AVENIR_LIGHT_H0;//FONT_AVENIR_PRO_DEMI_H0;
    self.lblKm.font=FONT_AVENIR_PRO_DEMI_H0;
    
    self.lblLocationName.text=nil;
    self.lblLocationAddress.text=nil;
    
    self.lblKm.text=@"";
    self.imgLocation.hidden=YES;
}

-(void)loadData
{
    if(self.comefrom==JourneyDetailsComeFromDirect ||self.comefrom==JourneyDetailsComeFromSent)
        [self getUserChat];
    else
    {
        if(self.isMyJorney)
        {
            self.next_offset=-1;
            [self.arrayList removeAllObjects];
            [self.arrayList addObjectsFromArray:self.arrMyJourney];
            
            [self initPageViewController];
            [self stopLoadingActivity];
            self.server_request_inprogress=NO;
            
            if(self.arrayList.count==0)
            {
                self.pageViewController.view.hidden=YES;
                self.vwLocation.hidden=YES;
                self.btnReply.hidden=YES;
                [self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_record_found", nil)];
                self.view.backgroundColor=UICOLOR_LIGHT_TEXT;
            }
            else
            {
                self.pageViewController.view.hidden=NO;
                self.vwLocation.hidden=NO;
                self.btnReply.hidden=NO;
            }
        }
        else
        {
            [self getJourneyDetails];
 
        }
    }
}

-(void)getUserChat
{
    self.server_request_inprogress=YES;
    [[UserAPI new]getChatOfFriend:self.objJourney.user.user_id
                       withOffset:self.next_offset
                        withComeFrom:self.comefrom
                          success:^(MessageListWrapper *objWrapper) {
        self.next_offset=objWrapper.next_offset;
        
        [self.arrayList addObjectsFromArray:objWrapper.list];
        
        [self initPageViewController];
        [self stopLoadingActivity];
        self.server_request_inprogress=NO;
        if(self.arrayList.count==0)
        {
            self.pageViewController.view.hidden=YES;
            self.vwLocation.hidden=YES;
            self.btnReply.hidden=YES;
            [self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_record_found", nil)];
            //self.view.backgroundColor=UICOLOR_LIGHT_TEXT;
        }
                              
      
    } failure:^(NSError *error) {
        self.server_request_inprogress=NO;

        [self stopLoadingActivity];
        [self showOfflineView:YES error:error];
    }];
}

//-(void)stopLoadingActivity {
//    [super stopLoadingActivity];
//    [_delegate didDoneLoading:self];
//}

-(void)getJourneyDetails
{
    self.server_request_inprogress=YES;
    [[UserAPI new]getJourneysDetailsWithUserId:self.objJourney.user.user_id withOffset:self.next_offset  success:^(MessageListWrapper *objWrapper) {
        self.next_offset=objWrapper.next_offset;
        
        [self.arrayList addObjectsFromArray:[NSMutableArray arrayWithArray:[[objWrapper.list reverseObjectEnumerator] allObjects]]];
        
        
         [self initPageViewController];
         [self stopLoadingActivity];
         self.server_request_inprogress=NO;

        if(self.arrayList.count==0)
        {
            self.pageViewController.view.hidden=YES;
            self.vwLocation.hidden=YES;
            self.btnReply.hidden=YES;
            [self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_record_found", nil)];
            self.view.backgroundColor=UICOLOR_LIGHT_TEXT;
        }
        else
        {
            self.pageViewController.view.hidden=NO;
            self.vwLocation.hidden=NO;
            self.btnReply.hidden=NO;
        }
     
        
    } failure:^(NSError *error) {
        self.server_request_inprogress=NO;
        
        self.pageViewController.view.hidden=YES;
        self.vwLocation.hidden=YES;
        self.btnReply.hidden=YES;

        [self stopLoadingActivity];
        [self showOfflineView:YES error:error];
    }];
}



-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        self.next_offset=0;
        [self startLoadingActivity];
        [self loadData];
        
    }
}

-(void)initPageViewController

{
    if(self.arrayList.count)
    {
        if(_pageViewController.view)
        {
            if(_currentPageIndex>=self.arrayList.count)
                _currentPageIndex=self.arrayList.count-1;
            
            SubbPromotionViewController *startingViewController = [self viewControllerAtIndex:_currentPageIndex];
            
            if(startingViewController)
            {
                NSArray *viewControllers = @[startingViewController];
                [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
            }
            
            [self checkFlagAndShowSaveButton:startingViewController.objMessage];
            [self setLocationDetails:startingViewController.objMessage];
           
        }
        else
        {
            _pageViewController=[[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
            _pageViewController.delegate=self;
            _pageViewController.dataSource=self;
            _pageViewController.view.backgroundColor=UICOLOR_APP_BG;

            SubbPromotionViewController *startingViewController = [self viewControllerAtIndex:0];
            
            NSArray *viewControllers = @[startingViewController];
            [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
            // Change the size of page view controller
            _pageViewController.view.frame=CGRectMake(0,0 , SCREEN_SIZE.width, SCREEN_SIZE.height);
    
            [self.view addSubview:_pageViewController.view];
            [self.view sendSubviewToBack:_pageViewController.view];
    
            [_pageViewController.view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(@0);
                make.right.equalTo(@0);
                make.top.equalTo(@0);
                make.bottom.equalTo(@0);
            }];
            
            [self checkFlagAndShowSaveButton:startingViewController.objMessage];
            [self setLocationDetails:startingViewController.objMessage];
            
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture)];
            singleTap.numberOfTapsRequired = 1;
            [self.pageViewController.view addGestureRecognizer:singleTap];
            
            for (UIView *view in self.pageViewController.view.subviews) {
                if ([view isKindOfClass:[UIScrollView class]]) {
                    ((UIScrollView *)view).delegate = self;
                    break;
                }
            }
        }
    }
    else if(self.isMyJorney)
    {
         [self clickClose:nil];
    }
}

-(void)checkFlagAndShowSaveButton:(Message *)objMessage
{
    if (self.comefrom == JourneyDetailsComeFromDirect &&objMessage.is_saved==false && objMessage.isDistructed==NO){
        [self showCloseButton];
        //[self show24HSaveButton];
    }
    else if(self.comefrom == JourneyDetailsComeFromSent)
    {
        if(objMessage.is_saved)
            self.btnReply.hidden = NO;
        else
            self.btnReply.hidden = YES;
    }
    else
    {
        [self showCloseButton];
    }
}

-(void)handleTapGesture
{
    MPLog(@"self.currentPageIndex>>%ld >>%ld",(long)self.currentPageIndex,(unsigned long)self.arrayList.count);
    if(self.currentPageIndex==self.arrayList.count-1 && self.next_offset==-1)
    {
        [self clickClose:nil];
    }
    else
    {
        if(_currentPageIndex!=self.arrayList.count)
        {
            _currentPageIndex=_currentPageIndex+1;
            [self initPageViewController];
        }
    }
    
}

-(void)calculateDistanceForMessage
{
    Message *obj=[self.arrayList objectAtIndex:_currentPageIndex];

    NSString *mode=@"driving";

    double current_lat=[LocationClass sharedLoctionManager].currentLocation.coordinate.latitude;
    double current_lng=[LocationClass sharedLoctionManager].currentLocation.coordinate.longitude;


    double dest_lat=obj.latitude;
    double dest_lng=obj.longitude;


    MPLog(@"index>> >>lat : %f  long: %f",dest_lat,dest_lng);

    //https://maps.googleapis.com/maps/api/distancematrix/json?origins=12.9544874,77.6190723&destinations=13.0191445,77.6464534&mode=driving&language=en-IN&key=AIzaSyAMe0dJxJ1lSmM0Ip7pFsGVDzszXWwr8VE
    NSString *url=[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f&mode=%@&language=en-IN&key=AIzaSyAMe0dJxJ1lSmM0Ip7pFsGVDzszXWwr8VE",current_lat,current_lng,dest_lat,dest_lng,mode];

    MPLog(@"url>>%@",url);

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{

        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url]];
        [request setHTTPMethod:@"POST"];
        
        NSError *err;
        NSURLResponse *response;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&err];
        NSError* error;
        if(responseData == nil)
        {
            MPLog(@"data is nil");
            return ;
        }
        NSDictionary* json = [NSJSONSerialization JSONObjectWithData:responseData
                                                             options:kNilOptions
                                                               error:&error];
      // MPLog(@"json>>%@",json);
        if(json)
        {
            NSArray *rows=[json objectForKey:@"rows"];
            NSDictionary *dict=[rows firstObject];
            if(dict)
            {
                NSArray *elements=[dict objectForKey:@"elements"];
                NSDictionary *dict_element=[elements firstObject];
                NSDictionary *dict_distance=[dict_element objectForKey:@"distance"];
                NSString * distance=[dict_distance objectForKey:@"text"];
                MPLog(@"distance>>%@",distance);
                if(![Utility isEmpty:distance])
                {
                    dispatch_async(dispatch_get_main_queue(), ^(void){
                        self.lblKm.text=distance;
                    });
                  
                }
            }
        }
    });
}


#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SubbPromotionViewController*) viewController).pageIndex;
    _currentPageIndex=index;

    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SubbPromotionViewController*) viewController).pageIndex;
    _currentPageIndex=index;
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    
    if (index >= [self.arrayList count] && self.next_offset==-1) {
        return nil;
    }
    
    return [self viewControllerAtIndex:index];
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (completed) {
        if (self.comefrom == JourneyDetailsComeFromDirect || self.comefrom == JourneyDetailsComeFromSent){
            SubbPromotionViewController *cnt=[pageViewController.viewControllers lastObject];
            
            [self setLocationDetails:cnt.objMessage];
            [self checkFlagAndShowSaveButton:cnt.objMessage];
        }

    }
}

- (SubbPromotionViewController *)viewControllerAtIndex:(NSUInteger)index
{
 
    Message *obj=nil;
    if(self.arrayList.count==0)
    {
         return nil;
    }
    else if(self.next_offset!=-1 && index==self.arrayList.count)
    {
        MPLog(@"Load more...");
        if(self.server_request_inprogress==NO)
        {
            self.server_request_inprogress=YES;
            double delayInSeconds = 1.0f;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self loadData];
            });
        }
        
    }
    else if(index < [self.arrayList count])
    {
         obj=[self.arrayList objectAtIndex:index];
    }
    else
        return nil;
    
    SubbPromotionViewController *contentViewController = [[SubbPromotionViewController alloc]initWithNibName:@"SubbPromotionViewController" bundle:nil];
    contentViewController.view.frame=CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    contentViewController.comefrom = self.comefrom;
    contentViewController.pageIndex = index;
    contentViewController.objMessage=obj;
    
    return contentViewController;
}

-(void)deleteViewedMoments
{
    if(self.comefrom == JourneyDetailsComeFromDirect)
    {
        [[MomentsManager sharedInstance]deleteMoments:nil];
    }
    else if(_isMyJorney)
    {
        self.arrMyJourney=self.arrayList;
    }
}

-(void)setLocationDetails:(Message *)obj
{
    if([Utility isEmpty:obj.location_name])
    {
        self.vwLocation.hidden=YES;
    }
    else
    {
        self.vwLocation.hidden=NO;
        self.lblLocationName.text=obj.location_name;
        self.lblLocationAddress.text=obj.location_address;
        self.lblKm.text = @"Caculating...";
        self.imgLocation.hidden=NO;
        [self calculateDistanceForMessage];
    }
}

-(void)saveMomentFor24H
{
    Message *obj=[self.arrayList objectAtIndex:_currentPageIndex];
    if(obj.isDistructed==NO && obj.is_saved==NO)
    {
        self.btnClose.selected=YES;
        [[UserAPI new]saveMomentFor24HWithMsgId:obj.message_id success:^(BOOL status) {
            self.btnClose.selected=NO;
            obj.is_saved=YES;
            [self showCloseButton];
            [self initPageViewController];
        } failure:^(NSError *error) {
            self.btnClose.selected=NO;
            [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
        }];
    }
    else
    {
        [self showCloseButton];
    }

}

-(IBAction)clickClose:(id)sender
{
    MPLog(@"clickClose");
    if(self.btnClose.selected)
        return;
    if ([self.btnClose.currentImage isEqual:[UIImage imageNamed:@"24w"]])
    {
        if(![UIAppDelegate isInternetAvailable])
        {
            [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
            return;
        }
        [self saveMomentFor24H];
    }
    else
    {
        self.disableInteractivePlayerTransitioning = YES;
        [self dismissViewControllerAnimated:NO completion:^{
            self.disableInteractivePlayerTransitioning = NO;
        }];
    }
}

-(IBAction)clickReply:(id)sender
{
    MPLog(@"clickClose");
    if(self.comefrom==JourneyDetailsComeFromSent || self.btnReply.selected) //selected means loading...
    {
        return;
    }
    
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    if(self.isMyJorney)
    {
        if(_currentPageIndex!=self.arrayList.count)
        {
   
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:APP_NAME_TITLE message:MPLocalizedString(@"are_you_sure_you_delete", nil) delegate:self cancelButtonTitle:MPLocalizedString(@"cancel", nil) otherButtonTitles:MPLocalizedString(@"yes", nil) ,nil];
            alert.tag=DELETE_ALERT_TAG;
            [alert show];
    
        }
    }
    else
    {
        CameraMainViewController *cnt=[[CameraMainViewController alloc]initWithNibName:@"CameraMainViewController" bundle:nil];
        cnt.comefrom= CameraMainComeFromReplyUser;
        cnt.reply_userId=self.objJourney.user.user_id;
        cnt.reply_userImgUrl = self.objJourney.user.image;
        UINavigationController *modalVC=[[UINavigationController alloc]initWithRootViewController:cnt];
        modalVC.navigationBarHidden=YES;
        
        /*modalVC.modalPresentationStyle = UIModalPresentationCustom;
         self.animator = [[ZFModalTransitionAnimator alloc] initWithModalViewController:modalVC];
         self.animator.dragable = NO;
         self.animator.bounces = NO;
         self.animator.behindViewAlpha = 0.5f;
         self.animator.behindViewScale = 0.5f;
         
         modalVC.transitioningDelegate = self.animator;*/
        [self presentViewController:modalVC animated:NO completion:nil];
    }
    
}
#pragma mark -- UIAlertView

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==1)
    {
        if(alertView.tag==DELETE_ALERT_TAG)
        {
            self.btnReply.selected=YES;
            Message *obj=[self.arrayList objectAtIndex:_currentPageIndex];
            [[UserAPI new]deleteMyJourneyWithMsgId:obj.message_id success:^(BOOL status) {
                [self.arrayList removeObjectAtIndex:_currentPageIndex];
                [self.delegate didDeleteJourneyAtIndex:_currentPageIndex];
                [self initPageViewController];
                self.btnReply.selected=NO;
                
            } failure:^(NSError *error) {
                self.btnReply.selected=NO;
                [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
                
            }];
        }
    }
}

-(IBAction)clickLocation:(id)sender
{
    Message *obj=[self.arrayList objectAtIndex:_currentPageIndex];
    
    if(obj.latitude==0 && obj.longitude ==0)
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"mapview_not_available", nil)];
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^(void){
        GetDirectionViewController *cnt=[[GetDirectionViewController alloc]initWithNibName:@"GetDirectionViewController" bundle:nil];
        cnt.objJourney=self.objJourney;
        cnt.objMessage=obj;
        cnt.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        cnt.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        [self presentViewController:cnt animated:YES completion:nil];
    });
}

#pragma mark - transitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    MiniToLargeViewAnimator *animator = [[MiniToLargeViewAnimator alloc] init];
    animator.initialY = 0;
    animator.transitionType = ModalAnimatedTransitioningTypeDismiss;
    return animator;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator
{
    if (self.disableInteractivePlayerTransitioning) {
        return nil;
        
    }
    return self.dismissInteractor;
}
#pragma mark - scrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (_currentPageIndex == 0 && scrollView.contentOffset.x < scrollView.bounds.size.width) {
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    } else if (_currentPageIndex == self.arrayList.count-1 && scrollView.contentOffset.x > scrollView.bounds.size.width&&self.next_offset==-1) {
        scrollView.contentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if (_currentPageIndex == 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width) {
        *targetContentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    } else if (_currentPageIndex == self.arrayList.count-1 && scrollView.contentOffset.x >= scrollView.bounds.size.width) {
        *targetContentOffset = CGPointMake(scrollView.bounds.size.width, 0);
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    self.dismissInteractor=nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    MPLog(@"JourneyDetailsViewController --- Dealloc");
}

@end
