//
//  OnboardingView.h
//  Maply
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OnboardingView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imgGuide;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end
