//
//  Utility.m
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 15/09/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import "Utility.h"
#import "FormatterCollection.h"
#import "AFSphinxAPIClient.h"
#import "User.h"
#import "LocationClass.h"
#import "ISO8601DateFormatter.h"
#import "UIImage+Resize.h"
#import "UIImage+FixOrientation.h"

@import AVFoundation;
@implementation Utility

+ (BOOL)isEmpty:(NSString*)val
{
    if ([val isKindOfClass:[NSString class]]) {
        if (val != nil && [val isEqualToString:@"(null)"] == NO && [val isEqualToString:@"<null>"] == NO && [val isEqualToString:@""] == NO && [val isEqual:[NSNull null]] == NO)
            return NO;
        else
            return YES;
    }
    return YES;
}
#pragma marks -- Device related funtions
+(BOOL)isIOS7
{
    return SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0");
}
+(BOOL)isIOS8
{
    return SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0");
}

+(BOOL)isIOS9
{
    return SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"9.0");
}


+(void)logoutFromApp
{
    User *user = [User new];
    [VVBaseUserDefaults saveCustomObject:user key:@"user"];
    
    [VVBaseUserDefaults resetProfilePictureFlag];
    
    //set timer to update location after 5 min
    [[LocationClass sharedLoctionManager]stopUpdateLocationTimer];
    
    [UIAppDelegate initMainAppFlow];
}

+(NSMutableArray *)getSectionTitleArray
{
    NSMutableArray * _lsSections = [[NSMutableArray alloc] init];
    [_lsSections addObject:@"A"];
    [_lsSections addObject:@"B"];
    [_lsSections addObject:@"C"];
    [_lsSections addObject:@"D"];
    [_lsSections addObject:@"E"];
    [_lsSections addObject:@"F"];
    [_lsSections addObject:@"G"];
    [_lsSections addObject:@"H"];
    [_lsSections addObject:@"I"];
    [_lsSections addObject:@"J"];
    [_lsSections addObject:@"K"];
    [_lsSections addObject:@"L"];
    [_lsSections addObject:@"M"];
    [_lsSections addObject:@"N"];
    [_lsSections addObject:@"O"];
    [_lsSections addObject:@"P"];
    [_lsSections addObject:@"Q"];
    [_lsSections addObject:@"R"];
    [_lsSections addObject:@"S"];
    [_lsSections addObject:@"T"];
    [_lsSections addObject:@"U"];
    [_lsSections addObject:@"V"];
    [_lsSections addObject:@"W"];
    [_lsSections addObject:@"Y"];
    [_lsSections addObject:@"X"];
    [_lsSections addObject:@"Z"];
    
    return _lsSections;

}

+(void)getAddressFromLatLngSuccess:(void (^)(CLPlacemark *placemark))success
                   // failure:(void (^)(NSError* error))failure
{
        CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
        [geocoder reverseGeocodeLocation:[LocationClass sharedLoctionManager].currentLocation
                       completionHandler:^(NSArray *placemarks, NSError *error)
         {
             if (error){
                 MPLog(@"Geocode failed with error: %@", error);
                 return;
             }
             CLPlacemark *placemark = [placemarks objectAtIndex:0];
            success(placemark);
         }];
}

+(NSString *)getCityCountryFromPlacemark:(CLPlacemark *) placemark
{
    NSMutableArray *arr=[NSMutableArray new];
    if(![Utility isEmpty:placemark.locality])
        [arr addObject:placemark.locality];
    if(![Utility isEmpty:placemark.country])
        [arr addObject:placemark.country];
    
    return [arr componentsJoinedByString:@", "];
}

+(UIImage *)thumbnailImageFromURL:(NSURL *)videoURL {
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    NSError *err = NULL;
    CMTime requestedTime = CMTimeMake(1, 60);     // To create thumbnail image
    CGImageRef imgRef = [generator copyCGImageAtTime:requestedTime actualTime:NULL error:&err];
    NSLog(@"err = %@, imageRef = %@", err, imgRef);
    
    UIImage *thumbnailImage = [[UIImage alloc] initWithCGImage:imgRef];
    CGImageRelease(imgRef);    // MUST release explicitly to avoid memory leak
    
    return thumbnailImage;
}

+(NSString *)getCountdownText:(NSString*)valid_until
{
    NSDateComponents *components =  [Utility getDateComponetsSinceToday:valid_until withDiffUnit:1];
    // NSLog(@">>>> %@ ",components);
    // If the valid until is =< 48h: “Offer expires in 1 day, [y] hours [x] mins.”
    // If the valid until is > 48h: “Offer expires in [z] days, [y] hours [x] mins.”
    
    if(components.day>0)
    {
        NSString *days=[NSString stringWithFormat:@"%ldd",(long)components.day];
        NSString *hours=[NSString stringWithFormat:@"%ldh",(long)components.hour];
        NSString *mins=[NSString stringWithFormat:@"%ldm",(long)components.minute];
        NSString *str = [NSString stringWithFormat:@"%@ %@ %@",days,hours,mins];

       /* NSString *days = [NSString stringWithFormat:MPPluralizedString(@"%ld days", components.day, nil),components.day];
        NSString *hours = [NSString stringWithFormat:MPPluralizedString(@"%ld hours", components.hour, nil),components.hour];
        NSString *mins = [NSString stringWithFormat:MPPluralizedString(@"%ld mins", components.minute, nil),components.minute];
        
        NSString *str = [NSString stringWithFormat:MPLocalizedString(@"offer_expires_in_xxx_days_xxx_hours_xxx_mins", nil),days,hours,mins];*/
        
        return str;
    }
    
    // If the valid until is =< 24h: “Offer expires in [y] hours and [x] mins.”
    //  If the valid until is < 2h: “Offer expires in 1 hour and [x] mins.”
    
    if(components.hour>0 && components.hour<=24)
    {
        NSString *hours=[NSString stringWithFormat:@"%ldh",(long)components.hour];
         NSString *mins=[NSString stringWithFormat:@"%ldm",(long)components.minute];
         NSString *str = [NSString stringWithFormat:@"%@ %@",hours,mins];
       /* NSString *hours = [NSString stringWithFormat:MPPluralizedString(@"%ld hours", components.hour, nil),components.hour];
        NSString *mins = [NSString stringWithFormat:MPPluralizedString(@"%ld mins", components.minute, nil),components.minute];
        
        NSString *str = [NSString stringWithFormat:MPLocalizedString(@"offer_expires_in_xxx_hours_xxx_mins", nil),hours,mins];*/
        
        return str;
    }
    
    
    //If the valid_until is =< than 1 min: “Offer expires in 1 min.”
    // If the valid until is =< 1 hour: “Offer expires in [x] mins.”
    if(components.minute>0)
    {
        NSString *str=[NSString stringWithFormat:@"%ldm",(long)components.minute];

        /*NSString *mins = [NSString stringWithFormat:MPPluralizedString(@"%ld mins", components.minute, nil),components.minute];
        
        NSString *str = [NSString stringWithFormat:MPLocalizedString(@"offer_expires_in_xxx_mins", nil),mins];*/
       // [self setAttributedText:str];
        
        return str;
    }
    
    return nil;
}


+(NSDateComponents*)getDateComponetsSinceToday:(NSString*)date withDiffUnit:(NSInteger)diffUnit
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSString *strPlainformatDate =  [self parseISOFormattedDate:date];
    NSDate *dtIp = [UIAppDelegate.global_formatter dateFromString:strPlainformatDate];
    NSString  *strToday= [self parseISOFormattedDate:[Utility getUTCFormattedDate:[NSDate date]]];
    NSDate *dtToday = [UIAppDelegate.global_formatter dateFromString:strToday];
    
    // VVLog(@"strPlainformatDate %@  dtToday %@",dtIp,dtToday);
    
    return  [gregorian components: (NSYearCalendarUnit | NSMonthCalendarUnit | NSWeekOfMonthCalendarUnit| NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit)  fromDate:dtToday toDate:dtIp options:0];
}

+ (NSString*)parseISOFormattedDate:(NSString*)str_ISODate
{
    NSDate* date = [[ISO8601DateFormatter new] dateFromString:str_ISODate];
    /*[self.global_formatter_for_winestream setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [self.global_formatter_for_winestream stringFromDate:date];*/
    [UIAppDelegate.global_formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [UIAppDelegate.global_formatter stringFromDate:date];

}

+(NSString *)getUTCFormattedDate:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    
    NSLocale* enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
    [dateFormatter setLocale:enUSPOSIXLocale];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    MPLog(@"getUTCFormattedDate dateString==>%@",dateString);
    return dateString;
}
+ (UIImage *)fixOrientationOfImage:(UIImage *)image {
    
    // No-op if the orientation is already correct
    if (image.imageOrientation == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

+(UIImage *)bannerImageFrom:(UIImage*)image {
    //https://trello.com/c/PXjJg3Lb/121-the-pictures-on-my-journey-seems-streched
    float bannerImg_ratio = 1.6; // width/height
    
    float imgWidth = image.size.width;
    float imgHeight = image.size.height;
    
    float width, height, x, y;
    
    if (imgWidth >= imgHeight) {//landscape
        height = imgHeight;
        width = (imgWidth >= imgHeight * bannerImg_ratio) ? (imgHeight * bannerImg_ratio) : imgWidth;
        x = imgWidth / 2 - width / 2;
        y = 0;
    }
    else {//portrait
        height = imgWidth / bannerImg_ratio;
        width = imgWidth;
        x = 0;
        y = imgHeight / 2 - height / 2;
    }
    
    CGRect rect = CGRectMake(x, y, width, height);
    UIImage *croppedImage = [Utility cropImage:image toRect:rect];
    return croppedImage;
}

+ (UIImage *)cropImage:(UIImage*)image toRect:(CGRect)rect {
    CGFloat (^rad)(CGFloat) = ^CGFloat(CGFloat deg) {
        return deg / 180.0f * (CGFloat) M_PI;
    };
    
    // determine the orientation of the image and apply a transformation to the crop rectangle to shift it to the correct position
    CGAffineTransform rectTransform;
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(90)), 0, -image.size.height);
            break;
        case UIImageOrientationRight:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(-90)), -image.size.width, 0);
            break;
        case UIImageOrientationDown:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(rad(-180)), -image.size.width, -image.size.height);
            break;
        default:
            rectTransform = CGAffineTransformIdentity;
    };
    
    // adjust the transformation scale based on the image scale
    rectTransform = CGAffineTransformScale(rectTransform, image.scale, image.scale);
    
    // apply the transformation to the rect to create a new, shifted rect
    CGRect transformedCropSquare = CGRectApplyAffineTransform(rect, rectTransform);
    // use the rect to crop the image
    CGImageRef imageRef = CGImageCreateWithImageInRect(image.CGImage, transformedCropSquare);
    // create a new UIImage and set the scale and orientation appropriately
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:image.scale orientation:image.imageOrientation];
    // memory cleanup
    CGImageRelease(imageRef);
    
    return result;
}

@end
