//
//  CameraMainViewController.m
//  oscar
//
//  Created by Rishi on 24/12/15.
//  Copyright © 2015 Rishi. All rights reserved.
//

#import "CameraMainViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "EditImageViewController.h"
#import "PreviewViewController.h"
#import "HMSegmentedControl.h"
#import "SCRecorder.h"
//#import "SCTouchDetector.h"
#import "CustomStatusView.h"
//#import "NSGIF.h"
//#import "UIImage+animatedGIF.h"
#import "MiniToLargeViewAnimator.h"
#import "MiniToLargeViewInteractive.h"

#define kVideoPreset AVCaptureSessionPresetHigh
#define kVideoDuration 15


@interface CameraMainViewController ()<UIImagePickerControllerDelegate,
UINavigationControllerDelegate,PreviewDelegate,UITableViewDelegate,SCRecorderDelegate,
    SCRecorderToolsViewDelegate
>
{
    SCRecorder *_recorder;
    SCRecordSession *_recordSession;
}


//Varibale for UI Design
@property (nonatomic, weak) IBOutlet UIButton *btnCapture;
@property (nonatomic, strong) CAShapeLayer *btnCaptureLayer;
@property (nonatomic, strong) NSTimer *gifCapTimer;
@property (nonatomic)   float oneStepLen;
@property (nonatomic)   float currentLen;

@property (nonatomic, weak) IBOutlet UIButton *btnCameraToggle;
@property (nonatomic, weak) IBOutlet UIButton *btnFlash;

@property (nonatomic, weak) IBOutlet UIView *vwButton;
@property (nonatomic, weak) IBOutlet UIButton *btnClose;
@property (nonatomic) BOOL isCameraRunning;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) HMSegmentedControl *segmentedControl4;


@property (nonatomic, weak) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UILabel *timeRecordedLabel;
@property (nonatomic)NSInteger totalSeconds;
@property (strong, nonatomic) SCRecorderToolsView *focusView;
@property (weak, nonatomic) IBOutlet CustomStatusView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *btnUndo,*btnNext;

@property (nonatomic, strong)NSMutableArray *arrayImages;
@property (nonatomic, strong)UIView *whiteScreen;
@property (nonatomic)BOOL didFinishFlashAnimation,didFinishCapturing;

@property (nonatomic,strong) UILongPressGestureRecognizer *lpgr;

@property (strong, nonatomic) CALayer *focusBoxLayer;
@property (strong, nonatomic) CAAnimation *focusBoxAnimation;
@property (assign)NSInteger currentTabIndex;
@end

@implementation CameraMainViewController

#pragma mark - View LifeCycle Methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        self.view.backgroundColor=[UIColor blackColor];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
   // [self.btnCapture addGestureRecognizer:[[SCTouchDetector alloc] initWithTarget:self action:@selector(handleTouchDetected:)]];
    NSLog(@"self.comefrom>>%ld",(long)self.comefrom);
  //  if(self.comefrom==CameraMainComeFromSetting)
//    {
    
    _totalSeconds=kVideoDuration;
    
    _btnUndo.hidden=YES;
    _btnNext.hidden=YES;
    
    _arrayImages=[NSMutableArray new];
    
    _recorder = [SCRecorder recorder];
    _recorder.captureSessionPreset = [SCRecorderTools bestCaptureSessionPresetCompatibleWithAllDevices];
    _recorder.captureSessionPreset = kVideoPreset;
    _recorder.flashMode = SCFlashModeOff;
    _recorder.maxRecordDuration = CMTimeMake(_totalSeconds, 1);
    
    //    _recorder.fastRecordMethodEnabled = YES;
    _recorder.delegate = self;
    _recorder.autoSetVideoOrientation = NO;
    self.previewView.frame=CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    UIView *previewView = self.previewView;
    _recorder.previewView = previewView;
    _recorder.mirrorOnFrontCamera=YES;
    _recorder.initializeSessionLazily = NO;
    //  [_recorder refocus];
    // [self addCustomFocusView];
    
    _recorder.device = AVCaptureDevicePositionBack;
    
    NSError *error;
    if (![_recorder prepare:&error]) {
        NSLog(@"Prepare error: %@", error.localizedDescription);
    }
    
    self.lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGestures:)];
    self.lpgr.minimumPressDuration = 1.0f;
    self.lpgr.allowableMovement = 100.0f;
    [self.btnCapture addGestureRecognizer:self.lpgr];
    [self addSegmentView];

    //Add gesture to double tab on screen when the camera is on to switch between front and back camera.
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickCameraToggle:)];
    doubleTap.numberOfTapsRequired = 2;
    [self.view addGestureRecognizer:doubleTap];
    
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeLeft];
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self  action:@selector(didSwipe:)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeRight];
    
    self.progressView.translatesAutoresizingMaskIntoConstraints=YES;
    CGRect frame =  self.progressView.frame;
    frame.size.width = 0;
    self.progressView.frame = frame;
    
    self.whiteScreen = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width,  SCREEN_SIZE.height)];
    self.whiteScreen.layer.opacity = 1.0f;
    self.whiteScreen.layer.backgroundColor = [[UIColor whiteColor] CGColor];
    [UIAppDelegate.window addSubview:self.whiteScreen];
    self.whiteScreen.hidden=YES;
    
    //to draw cycle progress
    _btnCaptureLayer = [CAShapeLayer layer];
    _btnCaptureLayer.contentsScale = [[UIScreen mainScreen] scale];
    _btnCaptureLayer.strokeColor = [UIColor whiteColor].CGColor;
    _btnCaptureLayer.fillColor = nil;
    _btnCaptureLayer.lineCap = kCALineCapButt;
    _btnCaptureLayer.lineWidth = 4.0;
    _btnCaptureLayer.fillRule = kCAFillRuleNonZero;
    _btnCaptureLayer.frame = _btnCapture.bounds;
    [_btnCapture.layer addSublayer:_btnCaptureLayer];
    
//    }
    
}
- (void)didSwipe:(UISwipeGestureRecognizer*)swipe{
  if( self.segmentedControl4.hidden)
      return;
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        if(_currentTabIndex==2)
            return;
        _currentTabIndex=_currentTabIndex+1;
        if(_currentTabIndex>2)
            _currentTabIndex=2;
        [self.segmentedControl4 setSelectedSegmentIndex:_currentTabIndex animated:YES];
        [self clickTabButtonAtIndex:_currentTabIndex];
        NSLog(@"Swipe Left");
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        
        if(_currentTabIndex==0)
            return;
        _currentTabIndex=_currentTabIndex-1;
        if(_currentTabIndex<0)
            _currentTabIndex=0;
        [self.segmentedControl4 setSelectedSegmentIndex:_currentTabIndex animated:YES];
        [self clickTabButtonAtIndex:_currentTabIndex];
        
        NSLog(@"Swipe Right");
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionUp) {
        NSLog(@"Swipe Up");
    } else if (swipe.direction == UISwipeGestureRecognizerDirectionDown) {
        NSLog(@"Swipe Down");
    }
}
-(void)addCustomFocusView
{
    if(self.focusView==nil)
    {
    self.focusView = [[SCRecorderToolsView alloc] initWithFrame:_previewView.bounds];
    self.focusView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    self.focusView.recorder = _recorder;
    self.focusView.tapToFocusEnabled=YES;
    self.focusView.doubleTapToResetFocusEnabled=NO;
    self.focusView.showsFocusAnimationAutomatically=NO;
    self.focusView.delegate=self;
    [_previewView addSubview:self.focusView];
    
    CALayer *focusBox = [[CALayer alloc] init];
    focusBox.cornerRadius = 30.0f;
    focusBox.bounds = CGRectMake(0.0f, 0.0f, 60, 60);
    focusBox.borderWidth = 2.0f;
    focusBox.borderColor = [[UIColor whiteColor] CGColor];
    focusBox.opacity = 0.0f;
    [self.view.layer addSublayer:focusBox];
    
    CABasicAnimation *focusBoxAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    focusBoxAnimation.duration = 0.75;
    focusBoxAnimation.autoreverses = NO;
    focusBoxAnimation.repeatCount = 0.0;
    focusBoxAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    focusBoxAnimation.toValue = [NSNumber numberWithFloat:0.0];
    
    self.focusBoxLayer=focusBox;
    self.focusBoxAnimation=focusBoxAnimation;
   // self.focusView.outsideFocusTargetImage = [UIImage imageNamed:@"capture_flip"];
   // self.focusView.insideFocusTargetImage = [UIImage imageNamed:@"capture_flip"];
    }
}

- (void)recorderToolsView:(SCRecorderToolsView *__nonnull)recorderToolsView didTapToFocusWithGestureRecognizer:(UIGestureRecognizer *__nonnull)gestureRecognizer
{
    [self showFocusBox:CGPointZero];
}

- (void)showFocusBox:(CGPoint)point
{
    if(self.focusBoxLayer) {
        // clear animations
        [self.focusBoxLayer removeAllAnimations];
        
        // move layer to the touc point
        [CATransaction begin];
        [CATransaction setValue: (id) kCFBooleanTrue forKey: kCATransactionDisableActions];
        
        CGPoint currentFocusPoint = CGPointMake(0.5, 0.5);
        
        if (_recorder.focusSupported) {
            currentFocusPoint = _recorder.focusPointOfInterest;
        } else if (_recorder.exposureSupported) {
            currentFocusPoint = _recorder.exposurePointOfInterest;
        }
        
        CGPoint viewPoint = [_recorder convertPointOfInterestToViewCoordinates:currentFocusPoint];
        viewPoint = [self.view convertPoint:viewPoint fromView:_recorder.previewView];
        self.focusBoxLayer.position = viewPoint;
        
       // self.focusBoxLayer.position = _recorder.focusPointOfInterest;
        [CATransaction commit];
    }
    
    if(self.focusBoxAnimation) {
        // run the animation
        [self.focusBoxLayer addAnimation:self.focusBoxAnimation forKey:@"animateOpacity"];
    }
}

- (void)handleLongPressGestures:(UILongPressGestureRecognizer *)sender
{
    if ([sender isEqual:self.lpgr]) {
        if(self.camera_mode==CameraModeGIF)
        {
//            if (sender.state == UIGestureRecognizerStateBegan) {
//                if([self isVideoFullyRecorded])
//                {
//                    MPLog(@"VIDEO FULLY RECORDED");
//                    //MPLog(@"VIDEO FULLY RECORDED>>%f",CMTimeGetSeconds(_recordSession.duration));
//                    MPLog(@"_recorder.ratioRecorded>>%f",_recorder.ratioRecorded);
//                }
//                else
//                {
//                    [_recorder record];
//                    //set progress ui
//                }
//            }
//            else if (sender.state == UIGestureRecognizerStateEnded ||
//                     sender.state == UIGestureRecognizerStateCancelled||
//                     sender.state == UIGestureRecognizerStateFailed)
//            {
//                [_recorder pause];
//                [self stopAnimateRingTimer];
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [self setProgress:0];
//                });
//            }
            return;
        }
        else {
            if (sender.state == UIGestureRecognizerStateBegan)
            {
                [self.segmentedControl4 setSelectedSegmentIndex:2 animated:NO];
                [self clickTabButtonAtIndex:2];
                
                if([self isVideoFullyRecorded])
                {
                    MPLog(@"VIDEO FULLY RECORDED");
                    //MPLog(@"VIDEO FULLY RECORDED>>%f",CMTimeGetSeconds(_recordSession.duration));
                    MPLog(@"_recorder.ratioRecorded>>%f",_recorder.ratioRecorded);
                }
                else
                {
                    [_recorder record];
                    [_progressView startNewSession];
                }
            }
            else if (sender.state == UIGestureRecognizerStateEnded ||
                     sender.state == UIGestureRecognizerStateCancelled||
                     sender.state == UIGestureRecognizerStateFailed)
            {
                [_recorder pause];
                [_progressView stopSession];
            }
        }
    }
}
-(void)flashScreen {
    _didFinishFlashAnimation=NO;
    self.whiteScreen.hidden=NO;
    [self.view bringSubviewToFront:self.whiteScreen];
    self.whiteScreen.layer.opacity = 0.0f;
    CAKeyframeAnimation *opacityAnimation = [CAKeyframeAnimation animationWithKeyPath:@"opacity"];
    NSArray *animationValues = @[ @0.2f, @0.0f ];
    NSArray *animationTimes = @[ @0.1f, @0.4f ];
    id timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    NSArray *animationTimingFunctions = @[ timingFunction, timingFunction ];
    [opacityAnimation setValues:animationValues];
    [opacityAnimation setKeyTimes:animationTimes];
    [opacityAnimation setTimingFunctions:animationTimingFunctions];
    opacityAnimation.fillMode = kCAFillModeBoth;
    opacityAnimation.removedOnCompletion = YES;
    opacityAnimation.duration = 0.1;
    opacityAnimation.repeatCount=kGifImageCount;
    opacityAnimation.delegate=self;
    [self.whiteScreen.layer addAnimation:opacityAnimation forKey:@"animation"];
}

#pragma mark - CAAnimationDelegate
//- (void)animationDidStart:(CAAnimation *)anim
//{
//    
//}
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    _didFinishFlashAnimation=YES;
    self.whiteScreen.hidden=YES;
   // [self pushEditController];
    
}
-(IBAction)clickNext:(id)sender
{
     if(_recorder.session.segments.count)
     {
         [self saveAndShowSession:_recorder.session];
     }
}

-(IBAction)clickUndo:(id)sender
{
    if(_recorder.session.segments.count)
    {
        [_recorder.session removeSegmentAtIndex:_recorder.session.segments.count-1 deleteFile:YES];
        [_progressView removeLastSession];
        
        if (_recorder.session.segments.count==0) {
            self.segmentedControl4.hidden=NO;
            self.scrollView.hidden=YES;
            
            self.btnClose.hidden=NO;
            self.btnUndo.hidden=YES;
        }
    }
    else
    {
        self.btnClose.hidden=NO;
        self.btnUndo.hidden=YES;
    }

}

#pragma mark - SCRecorderDelegate

- (BOOL)recorderShouldAutomaticallyRefocus:(SCRecorder *__nonnull)recorder
{
    return YES;
}

//@called while recording video
- (void)recorder:(SCRecorder *)recorder didAppendVideoSampleBufferInSession:(SCRecordSession *)recordSessio
{
    if(self.camera_mode==CameraModeVideo)
        [self updateTimeRecordedLabel];
}

/*- (void)handleTouchDetected:(SCTouchDetector*)touchDetector {
    
    if(self.camera_mode==CameraModeGIF)
    {
        if (touchDetector.state == UIGestureRecognizerStateBegan)
        {
            self.btnCapture.userInteractionEnabled=NO;
            [self clickPicturesForGif];
        }
        return;
    }
    
     if (self.camera_mode==CameraModePhoto)
     {
         if (touchDetector.state == UIGestureRecognizerStateEnded)
         {
             [self clickCapture:nil];
         }
         return;
     }
   

   
    if (touchDetector.state == UIGestureRecognizerStateBegan)
    {
        if([self isVideoFullyRecorded])
        {
            MPLog(@"VIDEO FULLY RECORDED");
            //MPLog(@"VIDEO FULLY RECORDED>>%f",CMTimeGetSeconds(_recordSession.duration));
            MPLog(@"_recorder.ratioRecorded>>%f",_recorder.ratioRecorded);

        }
        else
        {
            [_recorder record];
            [_progressView startNewSession];
        }
    }
    else if (touchDetector.state == UIGestureRecognizerStateEnded)
    {
            [_recorder pause];
            [_progressView stopSession];
        
    }
}*/

- (void)recorder:(SCRecorder *)recorder didSkipVideoSampleBufferInSession:(SCRecordSession *)recordSession {
    NSLog(@"Skipped video buffer");
}

- (void)recorder:(SCRecorder *)recorder didReconfigureAudioInput:(NSError *)audioInputError {
    NSLog(@"Reconfigured audio input: %@", audioInputError);
}

- (void)recorder:(SCRecorder *)recorder didReconfigureVideoInput:(NSError *)videoInputError {
    NSLog(@"Reconfigured video input: %@", videoInputError);
}

- (void)recorder:(SCRecorder *)recorder didInitializeAudioInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    if (error == nil) {
        NSLog(@"Initialized audio in record session");
    } else {
        NSLog(@"Failed to initialize audio in record session: %@", error.localizedDescription);
    }
}

- (void)recorder:(SCRecorder *)recorder didInitializeVideoInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    if (error == nil) {
        NSLog(@"Initialized video in record session");
    } else {
        NSLog(@"Failed to initialize video in record session: %@", error.localizedDescription);
    }
}

- (void)recorder:(SCRecorder *)recorder didBeginSegmentInSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"Began record segment: %@", error);
}

//@called by [_recorder pause] in longPress
- (void)recorder:(SCRecorder *)recorder didCompleteSegment:(SCRecordSessionSegment *)segment inSession:(SCRecordSession *)recordSession error:(NSError *)error {
    NSLog(@"Completed record segment at %@: %@ (frameRate: %f)", segment.url, error, segment.frameRate);
    
    if(self.camera_mode==CameraModeVideo && _recorder.session.segments.count)
    {
        self.btnClose.hidden=YES;
        self.btnUndo.hidden=NO;
    }
    
    //[self updateGhostImage];
}

- (void)recorder:(SCRecorder *)recorder didCompleteSession:(SCRecordSession *)recordSession {
    NSLog(@"didCompleteSession:");
    [self saveAndShowSession:recordSession];
    [_recorder pause];
}

-(BOOL)isVideoFullyRecorded
{
    if (_recorder.ratioRecorded>=1) return YES;
    
    return NO;
}
//////////////
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // NSLog(@"self.comefrom>>%ld",(long)self.comefrom);
    if(self.comefrom==CameraMainComeFromSetting)
    {
        self.segmentedControl4.hidden=YES;
        self.scrollView.hidden=YES;
        self.lpgr=nil;
    }

    [[UIApplication sharedApplication] setStatusBarHidden:YES];

    // start the camera
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self resetGifSession];
    [self prepareSession];
    if(self.isCameraRunning==NO)
         [self startCameraWithAnimation];
    
    [self.view bringSubviewToFront:self.scrollView];
    [self.view bringSubviewToFront:self.segmentedControl4];
    [self.view bringSubviewToFront:self.vwButton];
    [self.view bringSubviewToFront:self.btnClose];
    [self.view bringSubviewToFront:self.btnUndo];
    [self.view bringSubviewToFront:self.btnCameraToggle];
    
    [_recorder focusCenter];
}
//- (void)viewDidLayoutSubviews {
//    [super viewDidLayoutSubviews];
//    
//    [_recorder previewViewFrameChanged];
//}

-(void)resetGifSession
{
    [_arrayImages removeAllObjects];
    if(self.camera_mode==CameraModeGIF)
    {
         [_recorder.session removeAllSegments];
       // [self clickUndo:nil];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [_recorder captureSession].automaticallyConfiguresApplicationAudioSession = NO;
    [_recorder startRunning];
    [self addCustomFocusView];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];

    double delayInSeconds = 0.1f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        if(_recorder.flashMode ==SCFlashModeLight)
        {
            [self flashButtonClick:nil];
        }
        [_recorder stopRunning];
        [self.focusView removeFromSuperview];
        self.focusView=nil;
        //[self.camera stop];
    });
    self.isCameraRunning=NO;
}

- (void)updateTimeRecordedLabel {
    CMTime currentTime = kCMTimeZero;
    
    if (_recorder.session != nil) {
        currentTime = _recorder.session.duration;
    }
    
    self.timeRecordedLabel.hidden=YES;
    self.timeRecordedLabel.text = [NSString stringWithFormat:@"%.2f sec", CMTimeGetSeconds(currentTime)];
    
    
    CGFloat seconds=CMTimeGetSeconds(currentTime);
    float width=SCREEN_SIZE.width/_totalSeconds;
    float newWidth= width*seconds;
    if(seconds>=_totalSeconds)
        newWidth=SCREEN_SIZE.width;
    [self.progressView updateSessionValue:newWidth];
    
    if(seconds>0)
    {
        self.segmentedControl4.hidden=YES;
        self.scrollView.hidden=YES;
    }
}


- (void)prepareSession {
    if (_recorder.session == nil) {
        
        SCRecordSession *session = [SCRecordSession recordSession];
        session.fileType = AVFileTypeQuickTimeMovie;
        
        _recorder.session = session;
    }
    
    [self updateTimeRecordedLabel];
   // [self updateGhostImage];
}

- (void)saveAndShowSession:(SCRecordSession *)recordSession {
   // [[SCRecordSessionManager sharedInstance] saveRecordSession:recordSession];
    
    _recordSession = recordSession;
    if(self.camera_mode==CameraModeVideo)
    {
        [_progressView stopSession];
        [self showVideo];
    }
    else if(self.camera_mode==CameraModeGIF)
    {
        _didFinishCapturing=YES;
        [self pushEditController];
    }
}

- (void)showVideo {
    /*NSLog(@"_recordSession.outputUrl>>%@",_recordSession.outputUrl);
    AVAsset *asset = _recordSession.assetRepresentingSegments;

    SCAssetExportSession *assetExportSession = [[SCAssetExportSession alloc] initWithAsset:asset];
    assetExportSession.outputUrl = _recordSession.outputUrl;
    assetExportSession.outputFileType = AVFileTypeMPEG4;
   // assetExportSession.videoConfiguration.filter = [SCFilter filterWithCIFilterName:@"CIPhotoEffectInstant"];
    assetExportSession.videoConfiguration.preset = SCPresetHighestQuality;
    assetExportSession.audioConfiguration.preset = SCPresetMediumQuality;
    [assetExportSession exportAsynchronouslyWithCompletionHandler: ^{
        if (assetExportSession.error == nil) {
            EditImageViewController *cnt=[[EditImageViewController alloc]initWithNibName:@"EditImageViewController" bundle:nil];
            cnt.reply_userId=self.reply_userId;
            cnt.comefrom=self.comefrom;
            cnt.recordSession=_recordSession;
            cnt.videoUrl=assetExportSession.outputUrl;
            [self.navigationController pushViewController:cnt animated:NO];
            // We have our video and/or audio file
        } else {
            NSLog(@"assetExportSession.error>>%@",assetExportSession.error);
            // Something bad happened
        }
    }];*/
    
    if(self.camera_mode==CameraModeVideo)
    {
       if([self.navigationController.topViewController isKindOfClass:[EditImageViewController class]]==NO)
       {
        EditImageViewController *cnt=[[EditImageViewController alloc]initWithNibName:@"EditImageViewController" bundle:nil];
        cnt.reply_userId=self.reply_userId;
        cnt.reply_userImgUrl=self.reply_userImgUrl;
        cnt.comefrom=self.comefrom;
        cnt.recordSession=_recordSession;
        cnt.videoUrl=nil;
        cnt.camera_mode=self.camera_mode;
        [self.navigationController pushViewController:cnt animated:NO];
       }
    }

}

-(void)addSegmentView
{
    CGFloat viewWidth = SCREEN_SIZE.width;
    CGFloat segmentWidth = 220;

    // Tying up the segmented control to a scroll view
    self.segmentedControl4 = [[HMSegmentedControl alloc] initWithFrame:CGRectMake((SCREEN_SIZE.width/2)-(segmentWidth/2), SCREEN_SIZE.height-22, segmentWidth, 22)];
    self.segmentedControl4.sectionTitles = @[MPLocalizedString(@"gif", nil), MPLocalizedString(@"photo", nil), MPLocalizedString(@"video", nil)];
    self.segmentedControl4.selectedSegmentIndex = 1;
    self.segmentedControl4.backgroundColor = [UIColor clearColor];
    self.segmentedControl4.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:FONT_AVENIR_ROMAN_H4};
    self.segmentedControl4.selectedTitleTextAttributes = @{NSForegroundColorAttributeName : [UIColor whiteColor],NSFontAttributeName:FONT_AVENIR_ROMAN_H4};
    self.segmentedControl4.selectionIndicatorColor = [UIColor whiteColor];
    self.segmentedControl4.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.segmentedControl4.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.segmentedControl4.tag = 3;
    self.segmentedControl4.selectionIndicatorHeight=3;
    
    __weak typeof(self) weakSelf = self;
    [self.segmentedControl4 setIndexChangeBlock:^(NSInteger index) {
        [weakSelf.scrollView scrollRectToVisible:CGRectMake(viewWidth * index, 0, SCREEN_SIZE.width, 200) animated:NO];
        [weakSelf clickTabButtonAtIndex:index];
    }];
    
    [self.view addSubview:self.segmentedControl4];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height)];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.contentSize = CGSizeMake(viewWidth * 3, 200);
    self.scrollView.delegate = self;
    [self.scrollView scrollRectToVisible:CGRectMake(viewWidth, 0, viewWidth, 200) animated:NO];
    [self.view addSubview:self.scrollView];
    self.scrollView.hidden=YES;
    _currentTabIndex=1;
}

-(void)initializeCamera
{
   /*CALayer *focusBox = [[CALayer alloc] init];
    focusBox.cornerRadius = 30.0f;
    focusBox.bounds = CGRectMake(0.0f, 0.0f, 60, 60);
    focusBox.borderWidth = 2.0f;
    focusBox.borderColor = [[UIColor whiteColor] CGColor];
    focusBox.opacity = 0.0f;
    focusBox.backgroundColor=[[UIColor whiteColor] CGColor];
    [self.view.layer addSublayer:focusBox];
    
    CAKeyframeAnimation *resize = [CAKeyframeAnimation animationWithKeyPath:@"bounds.size"];
    resize.values = @[[NSValue valueWithCGSize:CGSizeMake(30, 30)],
                      [NSValue valueWithCGSize:CGSizeMake(60, 60)],
                      [NSValue valueWithCGSize:CGSizeMake(70, 70)],
                      [NSValue valueWithCGSize:CGSizeMake(60, 60)]];
    resize.keyTimes = @[@.25, @.5, @.75, @1];
    
    CABasicAnimation *fadeOut = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeOut.fromValue = [NSNumber numberWithFloat:1.0];
    fadeOut.toValue = @0;
    fadeOut.beginTime = .2;
    fadeOut.duration=2;
    
    CAAnimationGroup *both = [CAAnimationGroup animation];
    both.animations = @[resize, fadeOut];*/
}

-(void)startCameraWithAnimation
{
   // [self.camera start];
    self.isCameraRunning=YES;
    self.previewView.alpha=0;
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [UIView animateWithDuration:0.7
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.previewView.alpha=1;
                         }
                         completion:^(BOOL finished) {
                         }];
    });
  /*  [self.camera start];
    self.isCameraRunning=YES;
     self.camera.view.alpha=0;
    
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [UIView animateWithDuration:0.7
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.camera.view.alpha=1;
                         }
                         completion:^(BOOL finished) {
                         }];
    });*/
    
    
}

- (void)snapButtonPressed:(UIButton *)button
{
    if(self.camera_mode==CameraModeGIF)
    {
        self.btnCapture.userInteractionEnabled=NO;
        [self clickPicturesForGif];
    }
    else if(self.camera_mode==CameraModePhoto)
    {
    
        [_recorder capturePhoto:^(NSError *error, UIImage *image) {
            if (image != nil) {
                [self loadCropview:image];
            } else {
                NSLog(@"error.localizedDescription>>%@",error.localizedDescription);
            }
        }];
    }

    
}
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)flashButtonPressed:(UIButton *)button
{
    if(_recorder.device==AVCaptureDevicePositionFront)
    {
        if(self.btnFlash.selected)
        {
            self.btnFlash.selected=NO;
        }
        else
        {
            self.btnFlash.selected=YES;

        }
    }
    else
    {
        if(self.btnFlash.selected)
        {
            _recorder.flashMode = SCFlashModeOff;
            self.btnFlash.selected = NO;
        }
        else
        {
            _recorder.flashMode = SCFlashModeLight;
            self.btnFlash.selected = YES;
            
        }
    }

    /*NSString *flashModeString = nil;
    if (_isVideo==NO) {
        switch (_recorder.flashMode) {
            case SCFlashModeOff:
                _recorder.flashMode = SCFlashModeOn;
                break;
            case SCFlashModeOn:
                _recorder.flashMode = SCFlashModeOff;
                break;
            default:
                break;
        }
    } else {
        switch (_recorder.flashMode) {
            case SCFlashModeOff:
                flashModeString = @"Flash : On";
                _recorder.flashMode = SCFlashModeLight;
                break;
            case SCFlashModeLight:
                flashModeString = @"Flash : Off";
                _recorder.flashMode = SCFlashModeOff;
                break;
            default:
                break;
        }
    }*/

    /*if(self.camera.flash == LLCameraFlashOff) {
        BOOL done = [self.camera updateFlashMode:LLCameraFlashOn];
        if(done) {
            //self.btnFlash.selected = YES;
            //self.btnFlash.tintColor = [UIColor yellowColor];
            
            
            [ToastMessage showSuccessMessage:MPLocalizedString(@"Flash On", nil)];
            self.btnFlash.selected = YES;
            
        }
    }
    else {
        BOOL done = [self.camera updateFlashMode:LLCameraFlashOff];
        if(done) {
            //self.btnFlash.selected = NO;
           // self.btnFlash.tintColor = [UIColor whiteColor];
          
            
            
            [ToastMessage showSuccessMessage:MPLocalizedString(@"Flash Off", nil)];
            self.btnFlash.selected = NO;
        }
    }*/
}


- (void)switchButtonPressed:(UIButton *)button
{
    if(_recorder.isRecording)
        return;
    
    //[self.camera togglePosition];
    
    [_recorder switchCaptureDevices];
    
    //@added by steven
    if (_recorder.device == AVCaptureDevicePositionBack) {
        if(self.btnFlash.selected)
        {
            _recorder.flashMode = SCFlashModeLight;
            self.btnFlash.selected = YES;
        }
        else
        {
            _recorder.flashMode = SCFlashModeOff;
            self.btnFlash.selected = NO;
        }
    }
}

#pragma mark - IBAction Methods
-(IBAction)clickClose:(id)sender
{
    if(_recorder.isRecording)
        return;
    
    MPLog(@"clickClose");
    [_recorder.session removeAllSegments];
    
    [self dismissViewControllerAnimated:NO completion:^{
    }];
}

-(void)clickTabButtonAtIndex:(NSInteger)index
{
    _currentTabIndex=index;
    if(index==0)
    {
         //gif
        self.btnCapture.selected=NO;
        
        _btnUndo.hidden=YES;
        _btnCapture.selected=NO;
        _btnNext.hidden=YES;
        
        self.camera_mode=CameraModeGIF;
        _recorder.captureSessionPreset = kVideoPreset;
        
        self.lpgr.minimumPressDuration = 1.0f;
        
        _totalSeconds=1;
        _recorder.maxRecordDuration = CMTimeMake(_totalSeconds, 1);

    }
    else if(index==1)
    {
        self.btnCapture.selected=NO;
       //photo
         _btnUndo.hidden=YES;
        _btnNext.hidden=YES;
        _recorder.captureSessionPreset = kVideoPreset;
        _btnCapture.selected=NO;
        
        self.camera_mode=CameraModePhoto;
        self.lpgr.minimumPressDuration = 1.0f;

    }
    else if(index==2)
    {
        _totalSeconds=kVideoDuration;

        _recorder.maxRecordDuration = CMTimeMake(_totalSeconds, 1);

        //video
        self.btnCapture.selected=NO;
        self.camera_mode=CameraModeVideo;
        
        _btnUndo.hidden=YES;
        _btnClose.hidden=NO;
        _recorder.captureSessionPreset = kVideoPreset;
        _btnCapture.selected=YES;
        _btnNext.hidden=NO;
        
         self.lpgr.minimumPressDuration = 0.0f;
        [[UILabel new].layer setCornerRadius:3];
    }
}

//progress should be the number between 0 and 1
-(void)setProgress:(CGFloat)progress {
    if (progress > 1.0) progress = 1.0;
    
    CGFloat startAngle = - ((float)M_PI / 2);
    CGFloat endAngle = (progress * 2 * (float)M_PI) + startAngle;
    UIBezierPath *processPath = [UIBezierPath bezierPath];
    processPath.lineCapStyle = kCGLineCapButt;
//    processPath.lineWidth = 4.0;
    
    CGFloat radius = _btnCaptureLayer.bounds.size.width / 2.0;
    CGPoint center = CGPointMake(_btnCaptureLayer.bounds.size.width/2, _btnCaptureLayer.bounds.size.height/2);
    
    [processPath addArcWithCenter:center radius:radius startAngle:startAngle endAngle:endAngle clockwise:YES];
    _btnCaptureLayer.path = processPath.CGPath;
}

//duration must be united as second

-(void)animateRingWithDuration:(float)seconds {
    float oneStepDuration = 0.01;
    _oneStepLen = (float)(1 / (seconds / oneStepDuration));
    _currentLen = 0;
    
    [self stopAnimateRingTimer];
    
    _gifCapTimer = [NSTimer scheduledTimerWithTimeInterval:oneStepDuration target:self selector:@selector(timerFired:) userInfo:nil repeats:YES];
}

-(void)stopAnimateRingTimer {
    if ([_gifCapTimer isValid]) {
        [_gifCapTimer invalidate];
    }
    _gifCapTimer = nil;
}

-(void)timerFired:(NSTimer*)timer {
    _currentLen = _currentLen + _oneStepLen;
    if (_currentLen > 1.1) {
        [self stopAnimateRingTimer];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.7 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self setProgress:0];
        });
    }
    else {
        [self setProgress:_currentLen];
    }
}

-(void)clickPicturesForGif
{
    _didFinishCapturing=NO;
    [self flashScreen];
//    [self animateRingWithDuration:(double)(kGifImageCount / 10.0)];
    [self animateRingWithDuration:_totalSeconds];
    [_recorder record];
    
  /*  for (NSInteger i=0; i<kGifImageCount; i++) {
        
        [_recorder capturePhoto:^(NSError *error, UIImage *image) {
            if (image != nil) {
                
                // MPLog(@"filepath>>>%@",filePath);
                [_arrayImages addObject:image];
                if(i==kGifImageCount-1)
                {
                    _didFinishCapturing=YES;
                    [self pushEditController];
                    return ;
                }
            } else {
                self.btnCapture.userInteractionEnabled=YES;

                MPLog(@"error.localizedDescription>>%@",error.localizedDescription);
            }
        }];
    }*/
    
    /*UIImage *image1=[self fixOrientationOfImage:image];
     NSData *pngData = UIImagePNGRepresentation(image1);
     
     NSString *filePath = [self documentsPathForFileName:[NSString stringWithFormat:@"image_%ld.png",i]]; //Add the file name
     [pngData writeToFile:filePath atomically:NO]; //Wr*/
}

- (NSString *)documentsPathForFileName:(NSString *)name
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    return [documentsPath stringByAppendingPathComponent:name];
}

-(void)pushEditController
{
    _didFinishFlashAnimation=YES;
    _didFinishCapturing=YES;

    if (_didFinishFlashAnimation&&_didFinishCapturing)
    {
        _didFinishCapturing=NO;
        _didFinishFlashAnimation=NO;
        [self loadCropview:nil];
        self.btnCapture.userInteractionEnabled=YES;
    }
}

-(void)flashFrontAndCapture
{
    self.whiteScreen.hidden=NO;
    [self.view bringSubviewToFront:self.whiteScreen];
    self.whiteScreen.layer.opacity = 1.0f;
    CGFloat currentbrightness=[UIScreen mainScreen].brightness;
    [UIScreen mainScreen].brightness=1.0;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self snapButtonPressed:nil];
        
        [UIView animateWithDuration:1.5 animations:^(void) {
            self.whiteScreen.layer.opacity = 0;
        }
         completion:^(BOOL finished){
             [UIScreen mainScreen].brightness=currentbrightness;;
             self.whiteScreen.hidden=YES;
             
         }];
    });
}

-(IBAction)clickCapture:(id)sender
{
    MPLog(@"clickCapture");
    #if TARGET_IPHONE_SIMULATOR
    [self clickGallery:nil];
    return;
    #else
    if(self.camera_mode==CameraModeVideo)
        return;
    
    if(self.btnFlash.selected && _recorder.device==AVCaptureDevicePositionFront)
    {
        [self flashFrontAndCapture];
        return;
    }
    else
        [self snapButtonPressed:sender];
    #endif
}

-(void)clickGallery:(id)sender
{
    MPLog(@"click Gallery");
    
    //https://tickets.vivino.com/issues/8259
    if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusNotDetermined) {
        ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
        [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            if (*stop) {
                //Gallary allow button pressed
                MPLog(@"Gallary  allow button pressed");
                [self openImageGallary];
                return;
            }
            *stop = TRUE;
        } failureBlock:^(NSError *error) {
            
            MPLog(@"Gallary Don't allow button pressed");
            
        }];
    }
    else
    {
        [self openImageGallary];
    }
}

#pragma mark - Methods For Pick Image From Gallery
-(void)openImageGallary
{
    UIImagePickerController *picker = [UIImagePickerController new];
    picker.delegate = self;
    picker.allowsEditing = NO;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
 
        picker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage, nil];
   
    
  ///  [UIAppDelegate applyNativeInterfaceApperance];
    
    [self presentViewController:picker animated:YES completion:^{
       
    }];
}

#pragma mark - Methods For UIImagePickerController Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString* mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    if ( [mediaType isEqualToString:@"public.movie" ])
    {
        MPLog(@"Picked a movie at URL %@",  [info objectForKey:UIImagePickerControllerMediaURL]);
//        NSURL *url =  [info objectForKey:UIImagePickerControllerMediaURL];
        
       // [self didUseVideoWithVideoURL:url];
        [picker dismissViewControllerAnimated:YES completion:NULL];
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }
    else
    {
        UIImage *originalImage = [info valueForKey:UIImagePickerControllerOriginalImage];
        if (originalImage != nil)
        {
            [self performSelector:@selector(loadCropview:) withObject:originalImage afterDelay:0];
            [picker dismissViewControllerAnimated:YES completion:NULL];
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }
    }
    
   /// [UIAppDelegate applyGlobalInterfaceAppearance];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
    ///[UIAppDelegate applyGlobalInterfaceAppearance];
}


#pragma mark - Methods For Image Cropper
-(void)loadCropview:(UIImage*)selectedImage
{
    if(self.comefrom==CameraMainComeFromSetting)
    {
        PreviewViewController *cnt=[[PreviewViewController alloc]initWithNibName:@"PreviewViewController" bundle:nil];
        cnt.image=selectedImage;
        cnt.previewDelegate=self;
        [self.navigationController pushViewController:cnt animated:NO];
     
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^(void){
            if([self.navigationController.topViewController isKindOfClass:[EditImageViewController class]]==NO){
            EditImageViewController *cnt=[[EditImageViewController alloc]initWithNibName:@"EditImageViewController" bundle:nil];
            cnt.reply_userId=self.reply_userId;
            cnt.reply_userImgUrl=self.reply_userImgUrl;
            cnt.comefrom=self.comefrom;
            cnt.originalImage=selectedImage;
            cnt.camera_mode=self.camera_mode;
            cnt.arrayImages=self.arrayImages;
            cnt.recordSession=_recordSession;
            [self.navigationController pushViewController:cnt animated:NO];
            }
        });
      
    }
}

#pragma mark - ************ METHODS FOR CAMERA TOGGLE ************
#pragma mark - Methods For Camera Toggle
-(IBAction)clickCameraToggle:(id)sender
{
    [self switchButtonPressed:sender];
}

- (IBAction)flashButtonClick:(id)sender {
    [self flashButtonPressed:sender];
}

#pragma mark - Preview delegate

-(void)clickDone:(UIImage *)img
{
    [self.camaraDelegate didSelectImage:img];
    [self clickClose:nil];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width;
    NSInteger page = scrollView.contentOffset.x / pageWidth;
    
    [self.segmentedControl4 setSelectedSegmentIndex:page animated:YES];
    [self clickTabButtonAtIndex:page];
}

#pragma mark - Methods For Status Bar Hidden
- (BOOL)prefersStatusBarHidden
{
    return YES;
}


#pragma mark - Memory Management
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    MPLog(@"CameraMainViewController -- Dealloc");
    self.camaraDelegate=nil;
}

@end
