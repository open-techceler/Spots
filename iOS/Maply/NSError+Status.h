//
//  NSError+Status.h
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 11/9/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSError (Status)
-(NSInteger)statusCode;
-(BOOL)isSuccess;
@end
