//
//  MomentsManager.m
//  Maply
//
//  Created by admin on 6/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MomentsManager.h"
#import "UserAPI.h"
@interface MomentsManager ()
@property (nonatomic, strong) NSMutableArray *arrayIds;
@end
@implementation MomentsManager

+ (MomentsManager*)sharedInstance
{
    //VVLog(@"");
    static MomentsManager* _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[MomentsManager alloc] init];
        
    });
    
    return _sharedInstance;
}

#pragma mark - init Method

- (id)init
{
    self = [super init];
    if (self != nil) {
        //VVLog(@"");
        _arrayIds = [[NSMutableArray alloc] init];
       
    }
    return self;
}

-(void)markMomentAsRead:(NSInteger)messageId
{
    NSString *strMessageId =[NSString stringWithFormat:@"%ld",(long)messageId];
    if([self.arrayIds containsObject:strMessageId] == NO)
    {
        [self.arrayIds addObject:strMessageId];
    }
}

-(void)deleteMoments:(NSMutableArray *)arrIds
{
    if(self.arrayIds.count)
    {
    [[UserAPI new]markMomentAsRead:self.arrayIds withMsgId:0 success:^(BOOL status) {
        [self.arrayIds removeAllObjects];
    } failure:^(NSError *error) {
        
    }];
    }
}

@end
