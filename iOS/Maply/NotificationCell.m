//
//  NotificationCell.m
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "NotificationCell.h"
#import "Notification.h"
@interface NotificationCell()
@property(nonatomic,strong)IBOutlet NSLayoutConstraint *constFooterLeading;
@property(nonatomic,strong) Notification *objNotification;

@end
@implementation NotificationCell

- (void)awakeFromNib {
    // Initialization code
    _lblTitle.preferredMaxLayoutWidth =SCREEN_SIZE.width - 146;
    
    _lblTitle.font =FONT_NOTIFICATION_TITLE;
    _lblTitle.textColor=UICOLOR_NOTIFICATION_TITLE;//UICOLOR_DARK_TEXT;
    
    _lblTitle.text =@"";
    
    [self.vwSwitch addTarget:self action:@selector(clickSwitch:) forControlEvents:UIControlEventValueChanged];

}

-(void)configureCellWithObj:(Notification *)obj indexPath:(NSIndexPath *)indexPath
{
    self.objNotification=obj;
    self.lblTitle.text=[self getTitleForIndexPath:indexPath];
    [self setSwichStatus];
}

-(void)setSwichStatus
{
    switch (self.cellType) {
        case NotificationCellMaplyNews:
           self.vwSwitch.on=self.objNotification.maply_news;
            break;
        case NotificationCellMessage:
            self.vwSwitch.on=self.objNotification.message_noti;
            break;
        case NotificationCellFriendRequest:
            self.vwSwitch.on=self.objNotification.friend_request;
            break;
        case NotificationCellSound:
            self.vwSwitch.on=self.objNotification.sound;
            break;
        case NotificationCellLiveRequest:
            self.vwSwitch.on=self.objNotification.live_request;
            break;
        case NotificationCellLiveUpdate:
            self.vwSwitch.on=self.objNotification.live_update;
            break;
        case NotificationCellFriendNearby:
            self.vwSwitch.on=self.objNotification.llive_friends_nearby;
            break;
            
        default:
            break;
    }

}

-(IBAction)clickSwitch:(id)sender
{
    switch (self.cellType) {
        case NotificationCellMaplyNews:
            self.objNotification.maply_news=self.vwSwitch.on;
            break;
        case NotificationCellMessage:
            self.objNotification.message_noti=self.vwSwitch.on;
            break;
        case NotificationCellFriendRequest:
            self.objNotification.friend_request=self.vwSwitch.on;
            break;
        case NotificationCellSound:
            self.objNotification.sound=self.vwSwitch.on;
            break;
        case NotificationCellLiveRequest:
             self.objNotification.live_request=self.vwSwitch.on;
            break;
        case NotificationCellLiveUpdate:
             self.objNotification.live_update=self.vwSwitch.on;
            break;
        case NotificationCellFriendNearby:
             self.objNotification.llive_friends_nearby=self.vwSwitch.on;
            break;

        default:
            break;
    }
}

-(NSString *)getTitleForIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSString *title=nil;
    if(indexPath.section==0)
    {
        switch (indexPath.row) {
            case 0:
                self.cellType=NotificationCellMaplyNews;
                title=MPLocalizedString(@"maply_news", nil);
                break;
            case 1:
                self.cellType=NotificationCellMessage;
                title=MPLocalizedString(@"message", nil);
                break;
            case 2:
                self.cellType=NotificationCellFriendRequest;
                title=MPLocalizedString(@"friend_request", nil);
                break;
            case 3:
                self.cellType=NotificationCellSound;
                title=MPLocalizedString(@"sound", nil);
                break;
                
            default:
                break;
        }
    }
    else if(indexPath.section==1)
    {
        switch (indexPath.row) {
            case 0:
                self.cellType=NotificationCellLiveRequest;
                title=MPLocalizedString(@"live_request", nil);
                break;
            case 1:
                self.cellType=NotificationCellLiveUpdate;
                title=MPLocalizedString(@"friend_updates", nil);
                break;
            case 2:
                self.cellType=NotificationCellFriendNearby;
                title=MPLocalizedString(@"live_friends_nearby", nil);
                break;
                          
            default:
                break;
        }
        
    }
    
    return title;
}
-(void)hideLastCellSperatorIfNeeded:(NSInteger)row_count index:(NSInteger)index
{
    if(index==(row_count-1))
        _constFooterLeading.constant=0;
    else
        _constFooterLeading.constant=16;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
