//
//  JourneyRestAPI.h
//  Maply
//
//  Created by admin on 2/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "BaseRestAPI.h"

@interface JourneyRestAPI : BaseRestAPI
- (AFHTTPRequestOperation*)GETAction:(APIRestAction)action
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;


- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)action
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)action
                           endDynamic:(NSString*)path
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

- (AFHTTPRequestOperation*)GETAction:(APIRestAction)action
                          endDynamic:(NSString*)path
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

- (AFHTTPRequestOperation*)POSTAction:(APIRestAction)task
            constructingBodyWithBlock:(void (^)(id<AFMultipartFormData> formData))block
                              success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                              failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

- (AFHTTPRequestOperation*)PUTAction:(APIRestAction)action
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

- (AFHTTPRequestOperation*)DELETEAction:(APIRestAction)action
                             endDynamic:(NSString*)person_id
                                success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                                failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;

- (AFHTTPRequestOperation*)PUTAction:(APIRestAction)action
                          endDynamic:(NSString*)path
                             success:(void (^)(AFHTTPRequestOperation* operation, id responseObject))success
                             failure:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failure;
@end
