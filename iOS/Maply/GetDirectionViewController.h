//
//  GetDirectionViewController.h
//  Maply
//
//  Created by admin on 2/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Journey;
@class Message;

@interface GetDirectionViewController : UIViewController
@property(nonatomic,strong) Journey *objJourney;
@property(nonatomic,strong) Message *objMessage;
@end
