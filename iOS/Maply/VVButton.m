//
//  NewVivinoButton.m
//  VivinoV2
//
//  Created by Pradip Parkhe on 7/29/15.
//
//

#import "VVButton.h"
#import <Masonry/Masonry.h>

@implementation VVButton

- (id)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    
    return self;
}

-(void)awakeFromNib
{
    [self setupUI];
}

- (void)setupUI
{
    self.backgroundColor=PICKER_COLOR_PINK;
    self.layer.cornerRadius=self.bounds.size.width/2;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth=1;
    self.layer.borderColor=[UIColor whiteColor].CGColor;
}

-(void)setColor:(UIColor *)color
{
    _color=color;
    self.backgroundColor=color;
}








@end
