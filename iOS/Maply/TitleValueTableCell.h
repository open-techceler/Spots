//
//  TitleValueTableCell.h
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 11/23/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@interface TitleValueTableCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *lblTitle,*lblValue;
@property(nonatomic,weak) IBOutlet UIImageView *imgArrow;
@property(nonatomic,weak) IBOutlet BaseImageViewWithData *imgUser;

-(void)setTitle:(NSString*)title value:(NSString*)value;
-(void)hideLastCellSperatorIfNeeded:(NSInteger)row_count index:(NSInteger)index;
@end
