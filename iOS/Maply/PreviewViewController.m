//
//  PreviewViewController.m
//  Maply
//
//  Created by admin on 3/22/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PreviewViewController.h"
#import "BaseImageViewWithData.h"

@interface PreviewViewController ()
@property (nonatomic,strong)IBOutlet BaseImageViewWithData *imageView;

@end

@implementation PreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setUpNavigationBar];
    // Do any additional setup after loading the view from its nib.
    self.imageView.image=self.image;
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
}

-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"preview", nil);
    [self.Nav setBackButtonAsLeftButton];
    [self.Nav setRightButtonWithImageName:nil withTitle:MPLocalizedString(@"done", nil) font:FONT_DONE_BUTTON];
    [self.Nav.btnRight addTarget:self action:@selector(clickDone)forControlEvents:UIControlEventTouchUpInside];
    
     [self.Nav setNavigationLayout:NO];

}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // start the camera
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)clickDone
{
    [self.previewDelegate clickDone:self.image];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)dealloc
{
    self.previewDelegate=nil;
}


@end
