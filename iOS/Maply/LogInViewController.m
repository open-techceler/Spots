//
//  LogInViewController.m
//  Maply
//
//  Created by admin on 2/9/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "LogInViewController.h"
#import "FacebookManager.h"
#import "UserNameViewController.h"
#import "OnboardingViewController.h"
#import "NotificationEnableViewController.h"
#import "LocationEnableViewController.h"
#import "UserAPI.h"
#import "User.h"
#import "SSError.h"
#import <FBSDKLoginKit/FBSDKLoginManager.h>
#import "RNLoadingButton.h"
#import "NotificationDefinitions.h"
#import "LocationClass.h"
#import "StevenXMPPHelper.h"

@interface LogInViewController ()<LocationClassDelegate, OnboardingViewControllerDelegate, LocationEnableViewControllerDelegate>
@property(nonatomic,weak)IBOutlet UILabel *lblWhereYou;
@property(nonatomic,weak)IBOutlet RNLoadingButton *btnFacebook;

@end

@implementation LogInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setFontAndColor];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideLoader) name:kHideProgreesViewForFBSessionStateClosedNotification object:nil];
    
    [self showOnboardingView];
}

- (void) showOnboardingView {
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"firstopen"]) {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"firstopen"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        OnboardingViewController *onboardingView =[[OnboardingViewController alloc] initWithNibName:@"OnboardingViewController" bundle:nil];
        onboardingView.delegate = self;
        [self presentViewController:onboardingView animated:YES completion:nil];
    } else {
        [self checkEnableNotification];
    }
}

- (void) checkEnableNotification {
    if (![UIAppDelegate checkPushNotificationSetting]) {
        NotificationEnableViewController* viewCon = [[NotificationEnableViewController alloc] initWithNibName:@"NotificationEnableViewController" bundle:nil];
        [self presentViewController:viewCon animated:YES completion:nil];
    }
}

- (void) dismissOnboardingViewCon:(OnboardingViewController *)viewCon {
    [viewCon dismissViewControllerAnimated:YES completion:^(void) {
        [self checkEnableNotification];
    }];
}

-(void)setFontAndColor
{
    self.lblWhereYou.font=FONT_AVENIR_PRO_REGULAR_H8;
    self.lblWhereYou.text=MPLocalizedString(@"where_you_friends", nil);
    
    [self.btnFacebook  setActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge forState:UIControlStateNormal];
}

-(void)getUserLocation
{
    [self startLoadingActivity];
    if([LocationClass sharedLoctionManager].currentLocation==nil){
        
        [[LocationClass sharedLoctionManager] setDelegate:self];
        [[LocationClass sharedLoctionManager] startFetchingLocation];
    }
    else
    {
          [self clickFacebookLogin];
    }
}

-(void) locationFound:(CLLocation*)location
{
    [[LocationClass sharedLoctionManager] setDelegate:nil];
     [self clickFacebookLogin];
}

-(void) failedToFetchLocation:(NSString*)str_error
{
    [[LocationClass sharedLoctionManager] setDelegate:nil];
   // [ToastMessage showErrorMessageAppTitleWithMessage:str_error];
    [self stopLoadingActivity];
    [self clickFacebookLogin];
}

-(void)hideLoader
{
    self.btnFacebook.loading=NO;
}

-(IBAction)clickLogin:(id)sender
{
    if(self.btnFacebook.loading==YES)
        return;
    
    if ([UIAppDelegate checkLocationServicePermission] == 1) {
        [self getUserLocation];
    } else {
        [self showLocationEnableViewCon];
    }
}

- (void) showLocationEnableViewCon {
    LocationEnableViewController* viewCon = [[LocationEnableViewController alloc] initWithNibName:@"LocationEnableViewController" bundle:nil];
    viewCon.delegate = self;
    [self presentViewController:viewCon animated:YES completion:nil];
}

- (void) enabledLocationService:(LocationEnableViewController *)viewCon withFlag:(bool)bFlag {
    [viewCon dismissViewControllerAnimated:YES completion:^(void) {
        if (bFlag)
            [self clickFacebookLogin];
    }];
}

/*-(void)getFacebookFriends
{
    
    if (![FBSDKAccessToken currentAccessToken])
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:@[@"public_profile", @"email", @"user_friends"]  handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                MPLog(@"Process error");
               // self.btnFbLoader.loading = NO;
            } else if (result.isCancelled) {
                MPLog(@"Cancelled");
               // self.btnFbLoader.loading = NO;
            } else {
                MPLog(@"Logged in");
                
                if ([result.grantedPermissions containsObject:@"email"]) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self getFacebookFriendsAndUpload];
                    });
                }
                else {
                    MPLog(@"No grantedPermissions");
                   // self.btnFbLoader.loading = NO;
                }
            }
        }];
    }
    else
    {
        MPLog(@"USer already Logib with FB");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self getFacebookFriendsAndUpload];
        });
    }
    
}*/

/*-(void)saveFbUserData
{
    SSLog(@"saveFbUserData");
   [FacebookManager saveFbUserInfoWithSuccess:^{
        __block NSMutableArray *lsTemp = [NSMutableArray new];
        [FacebookManager getFacebookFriendsWithSuccess:^(NSMutableArray *lsFriends) {
            lsTemp = lsFriends;
            SSLog(@"lsTemp==>%@",lsTemp);
            [self uploadSocialFriendsWith:SocialComeFromFacebook withFriendIds:lsTemp];
        } failure:^(NSError *error) {
            self.btnFbLoader.loading = NO;
        }];
    } failure:^(NSError *error) {
        SSLog(@"error==>%@",error);
        self.btnFbLoader.loading = NO;
    }];
}*/

-(void)clickFacebookLogin
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
     self.btnFacebook.loading=YES;
    if (![FacebookManager isFBSessionValid])
    {
        [[FacebookManager sharedInstance] createfacebookinstance:FacebookPermissionTypeEmail success:^{
            MPLog(@"facebook successfully ");
            
            [self signUpOnServer];
            
        } failure:^{
            MPLog(@"facebook got error ");
            //[self.hud hide:YES];
            
             self.btnFacebook.loading=NO;
        }];
    }
    else
    {
        MPLog(@"facebook already login");
        [self signUpOnServer];
        
    }
}

-(void)getFacebookFriendsAndUpload
{
    __block NSMutableArray *lsTemp = [NSMutableArray new];
    [FacebookManager getFacebookFriendsWithSuccess:^(NSMutableArray *lsFriends) {
        lsTemp = lsFriends;
        MPLog(@"lsTemp==>%@",lsTemp);
        [self uploadFacebookFriendsOnServer:lsTemp];
    } failure:^(NSError *error) {
        //self.btnFbLoader.loading = NO;
    }];
}

-(void)uploadFacebookFriendsOnServer:(NSMutableArray *)lsTemp
{
    if(lsTemp.count)
    {
        [[UserAPI new]uploadFaceookFriends:lsTemp success:^(BOOL status) {
            
        } failure:^(NSError *error) {
            
        }];
    }
    
}

-(void)signUpOnServer
{
    [[UserAPI new]logInWithFacebookSuccess:^(User *obj) {
        
        [[StevenXMPPHelper sharedInstance]
         registerUserWithJID:[NSString stringWithFormat:@"%d@%@", (int)obj.user_id, XmppVHOST]
         andPassword:@"1"
         withHostName:@"47.89.40.246"
         completion:nil];
        
        [self getFacebookFriendsAndUpload];
        [UIAppDelegate initMainAppFlow];
        self.btnFacebook.loading=NO;
        
    } failure:^(NSError *error) {
        [ToastMessage showErrorMessageAppTitleWithMessage:[SSError getErrorMessage:error]];
        self.btnFacebook.loading=NO;

    }];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
