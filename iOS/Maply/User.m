//
//  User.m
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 06/10/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import "User.h"
#import "VVBaseUserDefaults.h"

@implementation User

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:[NSString stringWithFormat:@"%ld",(long)self.user_id] forKey:@"user_id"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.image forKey:@"image"];
    [encoder encodeObject:self.facebook_id forKey:@"facebook_id"];
    [encoder encodeObject:self.status forKey:@"status"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.user_id = [[decoder decodeObjectForKey:@"user_id"]integerValue];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.username = [decoder decodeObjectForKey:@"username"];
        self.image = [decoder decodeObjectForKey:@"image"];
        self.facebook_id= [decoder decodeObjectForKey:@"facebook_id"];
        self.status= [decoder decodeObjectForKey:@"status"];

    }
    return self;
}


+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // override this in the model if property names in JSON don't match model
    NSDictionary *mapping = [NSDictionary mtl_identityPropertyMapWithModel:self];
    return [mapping mtl_dictionaryByAddingEntriesFromDictionary:@{@"user_id":@"id"}];
}

+(void)setAsCurrentUser:(User*)obj
{
    [VVBaseUserDefaults saveCustomObject:obj key:@"user"];
}
+(User*)currentUser
{
    return [VVBaseUserDefaults loadCustomObjectWithKey:@"user"];
}

@end
