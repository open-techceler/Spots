//
//  NewVivinoButton.h
//  VivinoV2
//
//  Created by Pradip Parkhe on 7/29/15.
//
//

#import <UIKit/UIKit.h>

typedef enum {
    VVButtonStylePrice,
} VVButtonStyle;

@interface VVButton : UIButton

@property(nonatomic,assign)UIColor *color;

@end
