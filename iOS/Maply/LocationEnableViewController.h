//
//  LocationEnableViewController.h
//  Maply
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LocationEnableViewControllerDelegate;
@protocol LocationEnableViewControllerDelegateForSkip;

@interface LocationEnableViewController : UIViewController

@property (weak, nonatomic) id<LocationEnableViewControllerDelegate> delegate;
@property (weak, nonatomic) id<LocationEnableViewControllerDelegateForSkip> delegateForSkip;

@property (nonatomic, assign) bool bShowSkipButton;

@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UIButton *btnAskMe;

- (IBAction)askMe:(id)sender;

- (void) checkLocationService;

- (IBAction)actionSkip:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnSkip;

@end

@protocol LocationEnableViewControllerDelegate <NSObject>

- (void) enabledLocationService:(LocationEnableViewController *) viewCon withFlag:(bool) bFlag;

@end

@protocol LocationEnableViewControllerDelegateForSkip <NSObject>

- (void) tappedLocationSkipButton;

@end
