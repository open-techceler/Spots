//
//  MessageListWrapper.h
//  Maply
//
//  Created by admin on 2/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MTLModelModified.h"

@interface MessageListWrapper : MTLModelModified
@property(nonatomic,strong)NSArray *list;

@end
