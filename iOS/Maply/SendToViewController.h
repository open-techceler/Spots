//
//  SendToViewController.h
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCRecorder.h"
@class FSVenue;

@interface SendToViewController : MyNavigationViewController

@property(nonatomic,strong)FSVenue *objVenue;
@property(nonatomic,strong)UIImage *finalSnap,*bannerImg;
@property(nonatomic,strong)NSURL *videoUrl,*gifUrl;
@property(nonatomic,strong)NSString *imageText;
@property (strong, nonatomic) SCRecordSession *recordSession;
@property (strong, nonatomic) SCFilter *selectedFilter;

@property (nonatomic) CameraMode camera_mode;


@end
