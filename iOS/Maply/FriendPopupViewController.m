//
//  FriendPopupViewController.m
//  Maply
//
//  Created by Admin on 28/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "FriendPopupViewController.h"
#import "FriendDetailView.h"

@interface FriendPopupViewController () <UIScrollViewDelegate, FriendDetailViewDelegate>
{
    bool bLoadedView;
    int nTotalPages;
    
    NSMutableArray* arraySubViews;
}

@end

@implementation FriendPopupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    bLoadedView = false;
    
    self.imgOverlay.hidden = true;
    self.viewCanvas.hidden = true;
    
    UISwipeGestureRecognizer* gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(slideDown)];
    gesture.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:gesture];
}

- (void) slideDown {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(dismissFriendPopupViewCon:)])
        [self.delegate dismissFriendPopupViewCon:self];
}

- (void) viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (bLoadedView)
        return;
    
    bLoadedView = true;
    
    [self makeUI];
}

- (void) makeUI {
    self.pageControl.hidden = true;
    self.btnPrev.hidden = true;
    self.btnNext.hidden = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.viewCanvas.layer.cornerRadius = 16.f;

    self.constraintViewBottom.constant = -1 * CGRectGetHeight(self.viewCanvas.frame);
    [self.view layoutIfNeeded];
    
    self.viewCanvas.hidden = false;
    
    [UIView animateWithDuration:0.4f animations:^(void) {
        self.constraintViewBottom.constant = 6.f;
        [self.view layoutIfNeeded];
    }];

    [self makeScrollView];
}

- (void) makeScrollView {
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = TRUE;
    
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
    
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
    float fScrollViewHeight = CGRectGetHeight(self.scrollView.bounds);
    float fScrollViewWidth = CGRectGetWidth(self.scrollView.bounds);
    
    NSDictionary *metrics = @{@"width":[NSNumber numberWithFloat:fScrollViewWidth], @"height":[NSNumber numberWithFloat:fScrollViewHeight], @"padding":[NSNumber numberWithFloat:0.f]};
    NSMutableDictionary *subviews = [[NSMutableDictionary alloc] init];
    NSString* strConstraints = @"";
    NSString* strSubViewName = @"";
    
    nTotalPages = (int)self.arrayTotalFriends.count;
    self.pageControl.numberOfPages = nTotalPages;
    
    if (nTotalPages <= 1) {
        self.pageControl.hidden = true;
        self.btnPrev.hidden = true;
        self.btnNext.hidden = true;
    } else {
        self.pageControl.hidden = false;
        self.btnPrev.hidden = false;
        self.btnNext.hidden = false;
    }
    
    self.constraintContentViewWidth.constant = nTotalPages * fScrollViewWidth;
    [self.contentView layoutIfNeeded];
    
    if (nTotalPages > 0)
    {
        for (int nIdx = 0; nIdx < nTotalPages; nIdx++)
        {
            UIView* curView = nil;
            
            strSubViewName = [NSString stringWithFormat:@"subview%d", nIdx + 1];
            
            FriendDetailView* subView = (FriendDetailView *)[[[NSBundle mainBundle] loadNibNamed:@"FriendDetailView" owner:self options:nil] objectAtIndex:0];
            subView.frame = CGRectMake(0, 0, fScrollViewWidth, fScrollViewHeight);
            subView.delegate = self;

            [subView loadUserInfo:self.arrayTotalFriends[nIdx]];
            
            curView = subView;
            
            subView.translatesAutoresizingMaskIntoConstraints = NO;
            [self.contentView addSubview:subView];
            
            [subView setNeedsLayout];
            [subView layoutIfNeeded];

            if (nIdx == 0) {
                strConstraints = [strConstraints stringByAppendingString:[NSString stringWithFormat:@"H:|[%@(width)]", strSubViewName]];
            } else if (nIdx < nTotalPages - 1) {
                strConstraints = [strConstraints stringByAppendingString:[NSString stringWithFormat:@"-padding-[%@(width)]", strSubViewName]];
            } else if (nIdx == nTotalPages - 1)
            {
                strConstraints = [strConstraints stringByAppendingString:[NSString stringWithFormat:@"-padding-[%@(width)]|", strSubViewName]];
            }
            
            [subviews setValue:subView forKey:strSubViewName];
            
            [arraySubViews addObject:subView];
        }
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:strConstraints options:0 metrics:metrics views:subviews]];
        
        for (NSString* strViewName in subviews.allKeys)
        {
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"V:|[%@(height)]|", strViewName] options:0 metrics:metrics views:subviews]];
        }
    }
    
    [self performSelector:@selector(showSelectedFriend) withObject:nil afterDelay:0.2f];
}

- (void) showSelectedFriend {
    float fScrollViewHeight = CGRectGetHeight(self.scrollView.bounds);
    float fScrollViewWidth = CGRectGetWidth(self.scrollView.bounds);

    if (self.nSelectedFriendIdx > 0)
        [self.scrollView scrollRectToVisible:CGRectMake(self.nSelectedFriendIdx * fScrollViewWidth, 0.f, fScrollViewWidth, fScrollViewHeight) animated:NO];
}

- (void) updateSelectedFriend {
    float fScrollViewHeight = CGRectGetHeight(self.scrollView.bounds);
    float fScrollViewWidth = CGRectGetWidth(self.scrollView.bounds);
    
    if (self.nSelectedFriendIdx > 0)
        [self.scrollView scrollRectToVisible:CGRectMake(self.nSelectedFriendIdx * fScrollViewWidth, 0.f, fScrollViewWidth, fScrollViewHeight) animated:YES];
}

- (void) sendHey:(FriendDetailView *)friendView {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(sendHeyToFriend:withFriendInfo:)])
        [self.delegate sendHeyToFriend:self withFriendInfo:friendView.friendObj];
}

- (void) sendMessage:(FriendDetailView *)friendView {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(sendMessageToFriend:withFriendInfo:)])
        [self.delegate sendMessageToFriend:self withFriendInfo:friendView.friendObj];
}

- (void) sendMeetUp:(FriendDetailView *)friendView {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(sendMeetUpToFriend:withFriendInfo:)])
        [self.delegate sendMeetUpToFriend:self withFriendInfo:friendView.friendObj];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - scrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    if (page != self.pageControl.currentPage) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(selectFriendUser:withFriendInfo:)])
            [self.delegate selectFriendUser:self withFriendInfo:self.arrayTotalFriends[page]];
    }
    
    self.pageControl.currentPage = page;
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(dismissFriendPopupViewCon:)])
        [self.delegate dismissFriendPopupViewCon:self];
}

- (IBAction)actionPrev:(id)sender {
    float fScrollViewHeight = CGRectGetHeight(self.scrollView.bounds);
    float fScrollViewWidth = CGRectGetWidth(self.scrollView.bounds);

    NSInteger nCurPageIdx = self.pageControl.currentPage;
    nCurPageIdx--;
    
    CGFloat fNextPos = 0.f;
    if (nCurPageIdx < 0) {
        fNextPos = (nTotalPages - 1) * fScrollViewWidth;
    } else {
        fNextPos = nCurPageIdx * fScrollViewWidth;
    }
    
    [self.scrollView scrollRectToVisible:CGRectMake(fNextPos, 0.f, fScrollViewWidth, fScrollViewHeight) animated:YES];
}

- (IBAction)actionNext:(id)sender {
    float fScrollViewHeight = CGRectGetHeight(self.scrollView.bounds);
    float fScrollViewWidth = CGRectGetWidth(self.scrollView.bounds);
    
    NSInteger nCurPageIdx = self.pageControl.currentPage;
    nCurPageIdx++;
    
    CGFloat fNextPos = 0.f;
    if (nCurPageIdx >= nTotalPages) {
        fNextPos = 0;
    } else {
        fNextPos = nCurPageIdx * fScrollViewWidth;
    }
    
    [self.scrollView scrollRectToVisible:CGRectMake(fNextPos, 0.f, fScrollViewWidth, fScrollViewHeight) animated:YES];
}

@end
