

#import <Foundation/Foundation.h>

@interface NotificationDefinitions : NSObject

extern NSString * const kLocationUpdateNotification;

extern NSString * const kLocationErrorNotification;

extern NSString * const kHideProgressViewOnFBLoginFailedNotification;

extern NSString * const kHideProgreesViewForFBSessionStateClosedNotification;

extern NSString * const kLocationResetSetCurrentLocationLock;

extern NSString * const kReloadJourneyListNotification;
extern NSString * const kReloadMessageListNotification;
extern NSString * const kReloadNearByListNotification;
extern NSString * const kReloadLiveFriendsListNotification;
extern NSString * const kpopToMapViewNotification;
extern NSString * const kcheckLocationServiceNotification;
extern NSString * const kreloadMyJourneyTabNotification;
extern NSString * const kUIApplicationDidBecomeActive;
extern NSString * const kUIApplicationGoOnline;

extern NSString * const kMessageIsDestructed;

extern NSString * const kSubbPromotionVCLoadingDone;
@end
