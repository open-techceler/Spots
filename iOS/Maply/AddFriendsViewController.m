//
//  AddFriendsViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "AddFriendsViewController.h"
#import "FacebookManager.h"
#import "BlockedCell.h"
#import "UserAPI.h"
#import "FriendsListWrapper.h"
#import "Friend.h"
#import "InviteFriendsViewController.h"
#import "AddFriendCell.h"

@interface AddFriendsViewController () <AddFriendCellDelegate>

@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,weak)IBOutlet UIView *vwSearch;
@property(nonatomic,weak)IBOutlet UISearchBar *searchBar;
@property(nonatomic,strong) NSMutableArray *arrayList,*arrayActions;

@property(nonatomic)BOOL server_request_inprogress;
@property(nonatomic)NSInteger next_offset,total_count;
@property (nonatomic,strong)UIActivityIndicatorView *activityLoadMore;
@property(nonatomic,weak)IBOutlet UILabel *lblNoRecordFound;

@property BOOL isSearching;
@end

@implementation AddFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setFontAndColor];
    [self setUpNavigationBar];
    
    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    [self initLoaderActivity];
    
    self.arrayList=[NSMutableArray new];
    self.arrayActions=[NSMutableArray new];
    
    self.searchBar.barTintColor = [UIColor clearColor];
    self.searchBar.backgroundImage = [UIImage new];

    self.tableView.tableHeaderView=self.vwSearch;
    
    self.tableView.hidden=YES;
    
    [self startLoadingActivity];
    
//    [self refreshFriendsList];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
     UIAppDelegate.mainController.swipeEnabled=NO;
    [self getFacebookFriendsAndUpload];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    UIAppDelegate.mainController.swipeEnabled=YES;
}

-(void)refreshFriendsList
{
    self.next_offset=0;
    self.tableView.hidden=YES;
    [self startLoadingActivity];
    [self getFriendsList];
}

-(void)setUpNavigationBar
{
    self.Nav.backgroundColor = [UIColor whiteColor];

    self.Nav.lblTitle.text=MPLocalizedString(@"add_friends", nil);
    
    if (self.bFirstSetup) {
        [self.Nav setNextButtonRightButton];
        [self.Nav.btnRight addTarget:self action:@selector(clickNext:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [self.Nav setBackButtonAsLeftButton];
        [self.Nav.btnLeft addTarget:self action:@selector(clickBack) forControlEvents:UIControlEventTouchUpInside];
    }

    [self.Nav setNavigationLayout:NO];
}

-(void)clickBack
{
    //UIAppDelegate.mainController.currentIndex=kFriendsMainViewController;
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG;
    
    self.searchBar.placeholder=MPLocalizedString(@"search_by_username", nil);
    
    _lblNoRecordFound.hidden=YES;
    _lblNoRecordFound.text=MPLocalizedString(@"no_friends_found", nil);
    
    UITextField *searchField = [self.searchBar valueForKey:@"_searchField"];
    [searchField setFont:FONT_AVENIR_MEDIUM_H3];
}

-(void)updateUserActionsOnServer{
    if(self.arrayActions.count)
    {
        [[UserAPI new]updateUserAction:self.arrayActions success:^(FriendsListWrapper *objWrapper) {
            [self.arrayActions removeAllObjects];
        
        } failure:^(NSError *error) {
        
        }];
    }
}

-(void)getFriendsList
{
    self.server_request_inprogress=YES;

    //[[UserAPI new] getFriends:0 keyWord:@"" success:^(FriendsListWrapper *objWrapper) {
    [[UserAPI new]getFriendsList:self.next_offset keyWord:self.searchBar.text success:^(FriendsListWrapper *objWrapper) {
        
        if(self.next_offset==0)
            [self.arrayList removeAllObjects];
        
        self.tableView.hidden=NO;
        [self.tableView reloadData];
        
        self.next_offset=objWrapper.next_offset;
        self.total_count=objWrapper.count;
        
        self.server_request_inprogress=NO;
        [self stopLoadingActivity];
        [self setActivityInTableFooter:NO];
        [self.searchBar resignFirstResponder];
        
        /*if(self.arrayList.count || self.isSearching==YES){
            self.tableView.hidden=NO;
            _lblNoRecordFound.hidden=YES;
        }
        else{
            self.tableView.hidden=YES;
            _lblNoRecordFound.hidden=NO;
        }*/



    } failure:^(NSError *error) {
        self.server_request_inprogress=NO;
        [self stopLoadingActivity];
        [self setActivityInTableFooter:NO];
        [self showOfflineView:YES error:error];

    }];
}

-(IBAction)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)clickNext:(id)sender {
    InviteFriendsViewController* viewCon = [[InviteFriendsViewController alloc] initWithNibName:@"InviteFriendsViewController" bundle:nil];
    [self.navigationController pushViewController:viewCon animated:YES];
}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        [self refreshFriendsList];
    }
 
}

#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(self.arrayList.count)
        return 30;
    return 0;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vw =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 30)];
    vw.backgroundColor=[UIColor whiteColor];
    
    UILabel *lbl =[[UILabel alloc] initWithFrame:CGRectMake(16, 10, SCREEN_SIZE.width-16, 20)];
    lbl.backgroundColor=[UIColor whiteColor];
    lbl.textColor=UICOLOR_LIGHT_TEXT;
    lbl.font= FONT_HEADER_TITLE;// FONT_AVENIR_PRO_DEMI_H1;
    lbl.textAlignment=NSTextAlignmentCenter;//NSTextAlignmentLeft;
    if(self.isSearching)
         lbl.text=[NSString stringWithFormat:MPLocalizedString(@"xxx_matches", nil),self.arrayList.count];
    else
        lbl.text=[NSString stringWithFormat:MPLocalizedString(@"xxx_facebook_friends_on_maply", nil),self.arrayList.count];
    [vw addSubview:lbl];
    
    return vw;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger nTotalRows = (int)self.arrayList.count / 3;
    if (self.arrayList.count % 3 > 0) {
        nTotalRows++;
    }

    return nTotalRows;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat fTableViewWidth = CGRectGetWidth(self.tableView.bounds);
    
    return fTableViewWidth * 306.f / 600.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString* MyIdentifier = @"AddFriendCell";
    AddFriendCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"AddFriendCell" owner:self options:nil] objectAtIndex:0];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    cell.delegate = self;
    
    NSMutableArray* arrayList = [[NSMutableArray alloc] init];

    int nStartIdx = 3 * (int)(indexPath.row);
    int nEndIdx = nStartIdx + 3;
    
    for (int nFriendIdx = nStartIdx; nFriendIdx < nEndIdx; nFriendIdx++) {
        if (nFriendIdx >= self.arrayList.count)
            continue;
        
        [arrayList addObject:self.arrayList[nFriendIdx]];
    }

    [cell loadUsers:arrayList];
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];

    return cell;
    
}

- (void) tappedAddButton:(AddFriendCell *)cell friend:(Friend *)obj {
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
 
    if([obj.status_type isEqualToString:kStatusTypeRequest] || [obj.status_type isEqualToString:kStatusTypeFriend])
        obj.status_type=kStatusTypeNone;
    else
        obj.status_type=kStatusTypeRequest;
    
    NSArray *resultArray = [self.arrayActions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"friend_id = %ld",obj.friend_id]];
    if(resultArray.count)
    {
        Friend *objAdded=[resultArray firstObject];
        objAdded.status_type=obj.status_type;
    }
    else
    {
        Friend *objSelected=[Friend new];
        objSelected.friend_id=obj.friend_id;
        objSelected.status_type=obj.status_type;
        [self.arrayActions addObject:objSelected];
        
    }
    [self.tableView reloadData];
    
    [self updateUserActionsOnServer];

}

-(void)clickAdd:(id)sender
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    UIButton *btn=(UIButton *)sender;
    Friend *obj=[self.arrayList objectAtIndex:btn.tag];
    
    if([obj.status_type isEqualToString:kStatusTypeRequest] || [obj.status_type isEqualToString:kStatusTypeFriend])
        obj.status_type=kStatusTypeNone;
    else
        obj.status_type=kStatusTypeRequest;
    
    NSArray *resultArray = [self.arrayActions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"friend_id = %ld",obj.friend_id]];
    if(resultArray.count)
    {
        Friend *objAdded=[resultArray firstObject];
        objAdded.status_type=obj.status_type;
    }
    else
    {
        Friend *objSelected=[Friend new];
        objSelected.friend_id=obj.friend_id;
        objSelected.status_type=obj.status_type;
        [self.arrayActions addObject:objSelected];

    }
    [self.tableView reloadData];
    
    [self updateUserActionsOnServer];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchBar.text.length==0)
    {
        if(self.isSearching)
        {
            self.isSearching=NO;
            self.next_offset=0;
            [self getFriendsList];
        }
    }
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    self.isSearching=YES;
    self.next_offset=0;
    [self getFriendsList];
    [searchBar resignFirstResponder];
}

#pragma mark -- Load More methods
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if([self.searchBar isFirstResponder])
        [self.searchBar resignFirstResponder];
    
    if (self.server_request_inprogress == NO && self.next_offset!=-1) {
        CGPoint offset = self.tableView.contentOffset;
        CGRect bounds = self.tableView.bounds;
        CGSize size = self.tableView.contentSize;
        
        UIEdgeInsets inset = self.tableView.contentInset;
        
        float y = offset.y + bounds.size.height - inset.bottom;
        float h = size.height;
        
        CGFloat reload_distance = 50;
        
        if (y + reload_distance > h ) {
            [self setActivityInTableFooter:YES];
            self.server_request_inprogress = YES;
            
            double delayInSeconds = 0.5f;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self getFriendsList];
            });
            
            return;
        }
    }
}

-(void)addLoadMoreActivityasFooter
{
    self.activityLoadMore =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityLoadMore.hidden=YES;
    self.activityLoadMore.frame = CGRectMake(0, 0, SCREEN_SIZE.width, 44);
    self.tableView.tableFooterView=self.activityLoadMore;
}

-(void)setActivityInTableFooter:(BOOL)show
{
    if(show )//&& self.lsDoctors.next_offset!=-1)
    {
        self.activityLoadMore.hidden=NO;
        [self.activityLoadMore startAnimating];
        self.tableView.tableFooterView=self.activityLoadMore;
    }
    else
    {
        self.activityLoadMore.hidden=YES;
        [self.activityLoadMore stopAnimating];
        self.tableView.tableFooterView=nil;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getFacebookFriendsAndUpload
{
    __block NSMutableArray *lsTemp = [NSMutableArray new];
    [FacebookManager getFacebookFriendsWithSuccess:^(NSMutableArray *lsFriends) {
        lsTemp = lsFriends;
        MPLog(@"lsTemp==>%@",lsTemp);
        [self uploadFacebookFriendsOnServer:lsTemp];
    } failure:^(NSError *error) {
        //self.btnFbLoader.loading = NO;
    }];
}

-(void)uploadFacebookFriendsOnServer:(NSMutableArray *)lsTemp
{
    if(lsTemp.count)
    {
        [[UserAPI new]uploadFaceookFriends:lsTemp success:^(BOOL status) {
            [self refreshFriendsList];
        } failure:^(NSError *error) {
            
        }];
    } else {
        [self refreshFriendsList];
    }
    
}

@end
