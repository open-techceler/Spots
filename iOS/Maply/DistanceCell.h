//
//  DistanceCell.h
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Notification;
@interface DistanceCell : UITableViewCell
@property(nonatomic,weak) IBOutlet UILabel *lblTitle,*lblValue;
@property(nonatomic,weak) IBOutlet UISlider *slider;
@property(nonatomic,strong) Notification *objNotification;

-(void)configureCellWithObj:(Notification *)obj;

@end
