//
//  SelectLanguageViewController.h
//  VivinoV2
//
//  Created by Admin on 18/04/14.
//
//

#import <UIKit/UIKit.h>
#import "MyNavigationViewController.h"


@protocol SelectLanguageDelegate
-(void) selectLanguageSuccessfully:(NSInteger)selctedLanguage;
@end


@interface SelectLanguageViewController : MyNavigationViewController

@property(nonatomic,weak)    id<SelectLanguageDelegate>delegate;
@property(nonatomic,assign)    NSInteger comefrom;

@end
