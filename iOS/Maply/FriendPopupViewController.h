//
//  FriendPopupViewController.h
//  Maply
//
//  Created by Admin on 28/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Friend;

@protocol FriendPopupViewControllerDelegate;

@interface FriendPopupViewController : UIViewController

@property (strong, nonatomic) NSArray* arrayTotalFriends;
@property (assign, nonatomic) NSInteger nSelectedFriendIdx;

@property (nonatomic, weak) id<FriendPopupViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imgOverlay;

@property (weak, nonatomic) IBOutlet UIView *viewCanvas;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewBottom;

@property (weak, nonatomic) IBOutlet UIButton *btnPrev;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

- (IBAction)actionPrev:(id)sender;
- (IBAction)actionNext:(id)sender;

- (void) updateSelectedFriend;

@end

@protocol FriendPopupViewControllerDelegate <NSObject>

- (void) dismissFriendPopupViewCon:(FriendPopupViewController *) viewCon;

- (void) sendHeyToFriend:(FriendPopupViewController *) viewCon withFriendInfo:(Friend *) friendObj;
- (void) sendMessageToFriend:(FriendPopupViewController *) viewCon withFriendInfo:(Friend *) friendObj;
- (void) sendMeetUpToFriend:(FriendPopupViewController *) viewCon withFriendInfo:(Friend *) friendObj;
- (void) selectFriendUser:(FriendPopupViewController *) viewCon withFriendInfo:(Friend *) friendObj;

@end
