//
//  XMPPHelper.h
//  FindTalents
//
//  Created by steven on 28/6/2016.
//  Copyright © 2016 steven. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"

static NSString *const muticast_module = @"multicast.steven.com";

typedef void(^ChatConnectCompletionHandler)(XMPPStream* stream, DDXMLElement* error);
typedef void(^ChatAuthCompletionHandler)(XMPPStream* stream, DDXMLElement* error);
typedef void(^ChatRegisterCompletionBlock)(XMPPStream* stream, DDXMLElement* error);
typedef void(^ChatDidSendMessageCompletionHandler)(XMPPStream* stream, XMPPMessage* message);

@protocol StevenXMPPHelperDelegate

@optional
-(void)xmppHelper:(XMPPStream*)stream didReceiveMessage:(NSString*)message;
-(void)xmppHelper:(XMPPStream *)stream didReceiveFlashMessage:(NSDictionary *)data;

@end

@interface StevenXMPPHelper : NSObject<XMPPStreamDelegate, XMPPReconnectDelegate>

+ (StevenXMPPHelper*)sharedInstance;

@property(nonatomic, strong) XMPPStream* xmppStream;
@property(nonatomic, weak) id<StevenXMPPHelperDelegate>delegate;

@property(nonatomic, strong) XMPPMessageArchivingCoreDataStorage* xmppMessageStorage;
@property(nonatomic, strong) XMPPMessageArchiving* xmppMessageArchiving;

@property(nonatomic, strong) XMPPRosterCoreDataStorage* xmppRosterStorage;
@property(nonatomic, strong) XMPPRoster* xmppRoster;

-(void)registerUserWithJID:(NSString*)jid andPassword: (NSString*)pw withHostName: (NSString*)hostname completion: (void(^)(XMPPStream* stream, DDXMLElement* error))block;

-(void)connectWithJid:(NSString*)jid password:(NSString*)password hostname:(NSString*)hostname connectionHandler:(ChatConnectCompletionHandler)connection authenticationHandler:(ChatAuthCompletionHandler)authentication;
-(void)disconnect;
-(BOOL)isConnected;
-(NSMutableArray*)loadArchivedMessagesFrom:(NSString*)jid;
-(NSMutableArray*)loadArchivedMessagesFrom:(NSString*)jid isOutgoing:(BOOL)isOutgoing;

-(void)sendMessage:(NSString*)message to:(NSString*)receiverJid completion:(ChatDidSendMessageCompletionHandler)block;
-(void)sendFlashMessage:(NSDictionary*)data to:(NSString*)receiverJid completion:(ChatDidSendMessageCompletionHandler)block;
-(void)sendMultcastMesssage:(NSString*)message to:(NSArray*)receiverJids completion:(ChatDidSendMessageCompletionHandler)block;
-(void)sendMuticastFlashMessage:(NSString*)message to:(NSArray*)receiverJids completion:(ChatDidSendMessageCompletionHandler)block;

@end
