//
//  MessagesViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessageCell.h"
#import "JourneyDetailsViewController.h"
#import "UserAPI.h"
#import "MessageListWrapper.h"
#import "Message.h"
#import "User.h"
#import "Journey.h"
#import "JourneyListWrapper.h"
#import "NotificationDefinitions.h"
#import "FormatterCollection.h"
#import "SentCell.h"
#import "EGORefreshTableHeaderView.h"
#import "CameraMainViewController.h"

@interface MessagesViewController ()<EGORefreshTableHeaderDelegate, MessageCellDelegate, SentCellDelegate>
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,strong) JourneyListWrapper *objWrapper;
//@property(nonatomic,strong) UIRefreshControl *refreshControl;
@property(nonatomic,strong) EGORefreshTableHeaderView *refreshHeaderView;
@property BOOL reloading;
//  Reloading var should really be your tableviews datasource
//  Putting it here for demo purposes

@property (nonatomic, strong) UITableViewCell* loadingCell;

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setFontAndColor];
    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];
    
    [self initLoaderActivityWithFrame: CGRectMake((SCREEN_SIZE.width/2.0)-40,(SCREEN_SIZE.height/2.0)-(40+kNavBarHeight+kScopeBarHeight+kStatusBarHeight), 80, 80)];
    
    [self initEmptyDataLabelWithFrame: CGRectMake(40,NAVIGATION_HEIGHT + 30.f,SCREEN_SIZE.width-80.f,SCREEN_SIZE.height-2*(NAVIGATION_HEIGHT+120.f))];
    //[self initEmptyDataLabelWithFrame:CGRectMake(0,(SCREEN_SIZE.height/2.0)-(NAVIGATION_HEIGHT+kScopeBarHeight),SCREEN_SIZE.width,30)];
    
    [self startLoadingActivity];
    self.tableView.hidden=YES;
    
    double delayInSeconds = 0.2f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self loadMessagesFromServer];
    });
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshMessages) name:kReloadMessageListNotification object:nil];
   
    //_refreshControl = [[UIRefreshControl alloc]init];
   // [self.tableView addSubview:_refreshControl];
   // [_refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    if (_refreshHeaderView == nil) {
        
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - 60, SCREEN_SIZE.width, 60)];
        view.delegate = self;
        [self.tableView addSubview:view];
        _refreshHeaderView = view;
    }
}
#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
     _reloading = YES;
    [self refreshTable];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    return _reloading; // should return if data source model is reloading
    
}

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)endRefreshing{
    //  model should call this when its done loading
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    
}

- (void)refreshTable {
    //TODO: refresh your data
    [self loadMessagesFromServer];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG;
}

-(void)refreshMessages
{
    if(self.objWrapper.list.count==0)
    {
        [self startLoadingActivity];
        self.tableView.hidden=YES;
    }
    [self loadMessagesFromServer];
}

-(void)loadMessagesFromServer
{
    [self showOfflineView:NO error:nil];
    if(self.comefrom==MessagesComeFromDirect)
        [self getDirectMessages];
    else
        [self getSentMessages];
}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        
        self.tableView.hidden=YES;
        [self startLoadingActivity];
        [self loadMessagesFromServer];
        
    }
}

-(void)getDirectMessages
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

        [[UserAPI new]getDirectMessages:0 success:^(JourneyListWrapper *objWrapper) {
            self.tableView.hidden=NO;
            self.objWrapper=objWrapper;
            [self.tableView reloadData];
            [self stopLoadingActivity];
            [self endRefreshing];
            
            NSInteger unread_count=0;
            for (Journey *objJ in self.objWrapper.list) {
                unread_count=unread_count+objJ.count;
            }
            
            [self.delegate setBadgeForTab:0 value:unread_count];
            
            if(self.objWrapper.list.count==0)
                [self showEmptyDataImageViewWithImage:[UIImage imageNamed:@"icon_empty_moments_direct"]];
                //[self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_message_found", nil)];

        } failure:^(NSError *error) {
            [self stopLoadingActivity];
            [self showOfflineView:YES error:error];
            [self endRefreshing];
        }];
    });
}

-(void)getSentMessages
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[UserAPI new]getSentMessages:0 success:^(JourneyListWrapper *objWrapper) {
            self.objWrapper=objWrapper;
            [self.tableView reloadData];
           
            self.tableView.hidden=NO;
            [self stopLoadingActivity];
            [self endRefreshing];
            
            if(self.objWrapper.list.count==0)
                [self showEmptyDataImageViewWithImage:[UIImage imageNamed:@"icon_empty_moments_sent"]];
//                [self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_message_found", nil)];
            
        } failure:^(NSError *error) {
            [self stopLoadingActivity];
            [self showOfflineView:YES error:error];
            [self endRefreshing];
        }];
    });
    
}

#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.objWrapper.list.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   //"longitude":73.86488839991787,"latitude":18.49967418668279,
    
    if(self.comefrom==MessagesComeFromDirect)
    {
        static NSString* MyIdentifier = @"MessageCell";
        MessageCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil] objectAtIndex:0];
        }
        
        Journey *obj=[self.objWrapper.list objectAtIndex:indexPath.row];
        cell.lblName.text=obj.user.name.capitalizedString;
        
        NSString *timeago=[FormatterCollection utc_timeAgoWithUTCDate:obj.created_date];
        if(![Utility isEmpty:obj.user.location])
            cell.lblAddress.text=[NSString stringWithFormat:@"%@  %@",obj.user.location,timeago];
        else if(![Utility isEmpty:obj.created_date])
            cell.lblAddress.text=timeago;
            
        
        [cell.imgUser getImageWithURL:obj.user.image comefrom:ImageViewComfromUser];
        
        if(obj.count==0){
            cell.btnCount.hidden=YES;
            cell.imgPhoto.hidden=NO;
            cell.imgSaved.hidden=YES;
            if(obj.is_saved == YES) {
             [cell.imgPhoto getImageWithURL:obj.image comefrom:ImageViewComfromUser];
              cell.imgPhoto.contentMode = UIViewContentModeScaleAspectFill;
              cell.imgSaved.hidden=NO;
            } else if(obj.expired == NO) {
             [cell.imgPhoto getImageWithURL:obj.image comefrom:ImageViewComfromUser];
            }
        }
        else
        {
            cell.btnCount.hidden=NO;
            cell.imgPhoto.hidden=YES;
            cell.imgSaved.hidden=YES;
            [cell.btnCount setTitle:[NSString stringWithFormat:@"%ld",(long)obj.count] forState:UIControlStateNormal];
        }
        
        cell.delegate = self;
        return cell;
    }
    else
    {
        static NSString* MyIdentifier = @"SentCell";
        SentCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"SentCell" owner:self options:nil] objectAtIndex:0];
        }
        
        Journey *obj=[self.objWrapper.list objectAtIndex:indexPath.row];
        
        cell.lblName.text=obj.user.name.capitalizedString;
        cell.lblDelivered.text=nil;
        [cell.imgUser getImageWithURL:obj.user.image comefrom:ImageViewComfromUser];
        [cell.btnStatus setImage:[UIImage imageNamed:@"radio"] forState:UIControlStateNormal];
        cell.btnStatus.selected=NO;
        cell.imgPhoto.hidden = YES;
        NSString *msgStatus=nil;
        if(![Utility isEmpty:obj.created_date])
        {
            NSString *timeago=[FormatterCollection utc_timeAgoWithUTCDate:obj.created_date];
            msgStatus =[NSString stringWithFormat:MPLocalizedString(@"delivered_xxx", nil),timeago];
            cell.btnStatus.selected=YES;
        }
        if(![Utility isEmpty:obj.modified_date])
        {
            NSString *timeago=[FormatterCollection utc_timeAgoWithUTCDate:obj.modified_date];
            msgStatus=[NSString stringWithFormat:MPLocalizedString(@"opened_xxx", nil),timeago];
            cell.btnStatus.selected=NO;
        }
        cell.lblDelivered.text=msgStatus;
//        if(obj.expired == NO)
//        {
//            [cell.btnStatus setImage:[UIImage imageNamed:@"24"] forState:UIControlStateNormal];
//            [cell.imgPhoto getImageWithURL:obj.image comefrom:ImageViewComfromUser];
//            cell.imgPhoto.contentMode = UIViewContentModeScaleAspectFill;
//            cell.imgPhoto.hidden = NO;
//        }
        
        cell.delegate = self;
        return cell;
        
    }
    return nil;
}

#pragma mark - MessageCellDelegate
-(void)messageCell:(MessageCell *)cell doubleTap:(UITapGestureRecognizer *)doubleTap {
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForCell:cell] animated:YES];
    Journey *obj=[self.objWrapper.list objectAtIndex:[self.tableView indexPathForCell:cell].row];
    
    CameraMainViewController *cnt=[[CameraMainViewController alloc]initWithNibName:@"CameraMainViewController" bundle:nil];
    cnt.comefrom= CameraMainComeFromReplyUser;
    cnt.reply_userId=obj.user.user_id;
    cnt.reply_userImgUrl = obj.user.image;
    UINavigationController *modalVC=[[UINavigationController alloc]initWithRootViewController:cnt];
    modalVC.navigationBarHidden=YES;
    
    [UIAppDelegate.window.rootViewController presentViewController:modalVC animated:NO completion:nil];
}

-(void)messageCell:(MessageCell *)cell singleTap:(UITapGestureRecognizer *)singleTap {
    
    [self showPreviewLoader:cell];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForCell:cell] animated:YES];
    Journey *obj=[self.objWrapper.list objectAtIndex:[self.tableView indexPathForCell:cell].row];
    if(self.comefrom==MessagesComeFromDirect && obj.count==0 && obj.is_saved == NO && obj.expired == YES){
        [self hidePreviewLoader];
        return;
    }
    JourneyDetailsViewController *cnt=[[JourneyDetailsViewController alloc]initWithNibName:@"JourneyDetailsViewController" bundle:nil];
    if(self.comefrom==MessagesComeFromDirect)
        cnt.comefrom=JourneyDetailsComeFromDirect;
    else
        cnt.comefrom=JourneyDetailsComeFromSent;

    cnt.objJourney=obj;
    cnt.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    cnt.view.hidden = YES;
    cnt.delegate = self;
    [self.delegate presentViewController:cnt];
    [self performSelector:@selector(refreshTable) withObject:nil afterDelay:70];
}

#pragma mark - SentCellDelegate
-(void)sentCell:(SentCell *)cell doubleTap:(UITapGestureRecognizer *)doubleTap {
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForCell:cell] animated:YES];
    Journey *obj=[self.objWrapper.list objectAtIndex:[self.tableView indexPathForCell:cell].row];
    
    CameraMainViewController *cnt=[[CameraMainViewController alloc]initWithNibName:@"CameraMainViewController" bundle:nil];
    cnt.comefrom= CameraMainComeFromReplyUser;
    cnt.reply_userId=obj.user.user_id;
    cnt.reply_userImgUrl = obj.user.image;
    UINavigationController *modalVC=[[UINavigationController alloc]initWithRootViewController:cnt];
    modalVC.navigationBarHidden=YES;
    
    [UIAppDelegate.window.rootViewController presentViewController:modalVC animated:NO completion:nil];
}

-(void)sentCell:(SentCell *)cell singleTap:(UITapGestureRecognizer *)singleTap {
    
    [self showPreviewLoader:cell];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForCell:cell] animated:YES];
    
    Journey *obj=[self.objWrapper.list objectAtIndex:[self.tableView indexPathForCell:cell].row];
    if(obj.expired == YES){
        [self hidePreviewLoader];
        return;
    }
    JourneyDetailsViewController *cnt=[[JourneyDetailsViewController alloc]initWithNibName:@"JourneyDetailsViewController" bundle:nil];
    if(self.comefrom==MessagesComeFromDirect)
        cnt.comefrom=JourneyDetailsComeFromDirect;
    else
        cnt.comefrom=JourneyDetailsComeFromSent;
    
    cnt.objJourney=obj;
    cnt.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    cnt.view.hidden = YES;
    cnt.delegate = self;
    [self.delegate presentViewController:cnt];
}

-(void)showPreviewLoader:(UITableViewCell*)cell {
    _loadingCell = cell;
    if ([cell isKindOfClass:[MessageCell class]]) {
        MessageCell* directCell = (MessageCell*)cell;
        [directCell startActivity];
    }
    else if ([cell isKindOfClass:[SentCell class]]) {
        SentCell* sentCell = (SentCell*)cell;
        [sentCell startActivity];
    }
}

-(void)hidePreviewLoader{
    if ([_loadingCell isKindOfClass:[MessageCell class]]) {
        MessageCell* directCell = (MessageCell*)_loadingCell;
        [directCell stopActivity];
    }
    else if ([_loadingCell isKindOfClass:[SentCell class]]) {
        SentCell* sentCell = (SentCell*)_loadingCell;
        [sentCell stopActivity];
    }
    _loadingCell = nil;
}

#pragma mark - JourneyDetailViewControllerDelegate
-(void)didDeleteJourneyAtIndex:(NSInteger)index {
    
}

-(void)didDoneLoading:(JourneyDetailsViewController*)detailVC {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self hidePreviewLoader];
        detailVC.view.hidden = NO;
    });
}

-(void)didClosed:(JourneyDetailsViewController *)detailVC {
    [self refreshTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
