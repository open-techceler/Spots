//
//  MiniToLargeViewAnimator.m
//  DraggableViewControllerDemo
//
//  Created by saiday on 11/19/15.
//  Copyright © 2015 saiday. All rights reserved.
//

#import "MiniToLargeViewAnimator.h"

static NSTimeInterval kAnimationDuration = .4f;

@implementation MiniToLargeViewAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return kAnimationDuration;
}

- (void)animatePresentingInContext:(id<UIViewControllerContextTransitioning>)transitionContext toVC:(UIViewController *)toVC fromVC:(UIViewController *)fromVC
{
    CGRect fromVCRect = [transitionContext initialFrameForViewController:fromVC];
    CGRect toVCRect = fromVCRect;
    toVCRect.origin.y = toVCRect.size.height - self.initialY;
    
    toVC.view.frame = toVCRect;
    UIView *container = [transitionContext containerView];
    
    [container addSubview:fromVC.view];
    [container addSubview:toVC.view];
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        toVC.view.frame = fromVCRect;
    } completion:^(BOOL finished) {
        if ([transitionContext transitionWasCancelled]) {
            [transitionContext completeTransition:NO];
        } else {
            [transitionContext completeTransition:YES];
        }
    }];
}

//- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
//    
//}

- (void)animateDismissingInContext:(id<UIViewControllerContextTransitioning>)transitionContext toVC:(UIViewController *)toVC fromVC:(UIViewController *)fromVC
{
    UIViewController *_fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//    UIViewController *_toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *_fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
    UIView *_toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    
    CGRect fromVCRect = [transitionContext initialFrameForViewController:_fromVC];
    fromVCRect.origin.y = fromVCRect.size.height - self.initialY;
  
    UIView *container = [transitionContext containerView];
    [container addSubview:_toView];
    [container addSubview:_fromView];
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        _fromView.frame = fromVCRect;
    } completion:^(BOOL finished) {
        if ([transitionContext transitionWasCancelled]) {
            [transitionContext completeTransition:NO];
            [_toView removeFromSuperview];
        } else {
            [transitionContext completeTransition:YES];
        }
    }];
}


@end
