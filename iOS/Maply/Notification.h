//
//  Notification.h
//  Maply
//
//  Created by admin on 2/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MTLModelModified.h"

@interface Notification : MTLModelModified

@property(nonatomic)BOOL message_noti,maply_news,friend_request,live_request,sound;
@property(nonatomic)BOOL live_update,llive_friends_nearby;
@property(nonatomic)NSInteger maximum_distance;
/*"id" : 2,
"user_id" : 2,
"message": true,
"maply_news": true,
"friend_request": true,
"live_request": false,
"live_update": true,
"llive_friends_nearby": true,
"maximum_distance": 2*/
@end
