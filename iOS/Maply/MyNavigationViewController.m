//
//  MyNavigationViewController.m
//  VivinoV2
//
//  Created by Admin on 24/07/13.
//
//

#import "MyNavigationViewController.h"
#import "NoInternetView.h"
#import "Utility.h"
#import "LocationView.h"
#import "UIImage+GIF.h"

@interface MyNavigationViewController ()
{
   
}
@property (nonatomic,strong)UIImageView *imgLoader;
@property (nonatomic,strong)UIActivityIndicatorView *activityLoader;
@property(nonatomic,strong) UIButton *explictbackBtn;
@property(nonatomic,strong) UILabel *lblNoRecord;
@property (nonatomic,strong)UIImageView *imgNoRecord;
@property(nonatomic,strong) LocationView *vwLocation;

@end

@implementation MyNavigationViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    NavigationView *vwnav =[[NavigationView alloc] init];
    vwnav.frame=CGRectMake(0, 0,SCREEN_SIZE.width, 44);
    self.Nav=vwnav;
   
   
   self.navigationItem.leftBarButtonItem = nil;
   
    self.navigationItem.titleView=self.Nav;

    
    _isControllerPopout=NO;
    //
    
    
    self.navigationItem.title=@"";
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
            self.edgesForExtendedLayout = UIRectEdgeNone;
    
    //VVLog(@"*********Class name:==>%@",self.class);
    
    
}

/**
 *  Returns CGRect that is centered in the navigation bar
 *
 *  @return CGRect
 */
- (CGRect)navigationBarCenteredRect
{
    // http://stackoverflow.com/questions/6452716/convertpointtoview-in-landscape-mode-giving-wrong-values
    CGPoint offset = [self.Nav convertPoint:self.Nav.bounds.origin toView:self.view];
    CGFloat offsetleft = offset.x;
    CGFloat offsetright = self.Nav.bounds.origin.x + offsetleft + self.Nav.bounds.size.width - self.view.bounds.size.width;
    CGRect rect = CGRectMake(0, 0, self.Nav.bounds.size.width - offsetright, self.Nav.bounds.size.height);
    return rect;
}

-(void)addAnExplicitBackButton
{
    //https://tickets.vivino.com/issues/6265
    //as on iOS7 , self.navigationItem.titleView has around 30 pixel padding from left, so back button added on Nav does nit take the touch event in this area .
    // so we have added an explicit back button over here.
    _explictbackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    _explictbackBtn.frame= CGRectMake(0, 0, 45, 44);
    
    [self.navigationController.navigationBar addSubview:_explictbackBtn];
    
}
-(void)removeExplicitBackButton
{
    [_explictbackBtn removeFromSuperview];
    _explictbackBtn= nil;
}

-(void)hideExplicitBackButton
{
    _explictbackBtn.hidden = YES;
}

-(void)showExplicitBackButton
{
    _explictbackBtn.hidden = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
       
   
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];

    
    if (self.parentViewController == nil) {
        
        _isControllerPopout=YES;
     } else {
        _isControllerPopout=NO;
      }
}

-(void)initOfflineView
{
    [self initOfflineViewWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width,SCREEN_SIZE.height)];
    
}
#pragma marks --Offline View functions
-(void)initOfflineViewWithFrame:(CGRect)frame
{
    _vwOffline=[[NoInternetView alloc] initWithFrame:frame];
     [_vwOffline drawSubviewsWithType:ConnectivityCommon];
    //  [_vwOffline.btnRetry addTarget:self action:@selector(clickretry:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_vwOffline];
    _vwOffline.hidden=YES;
}
-(void)showOfflineView:(BOOL)show error:(NSError*)error
{
    if(show)
    {
        
        [_vwOffline showConnectivityMessage:error];
        _vwOffline.hidden=NO;
    }
    else
    {
        _vwOffline.hidden=YES;
    }
        
}

#pragma marks --Activity View functions
-(void)initLoaderActivityWithFrame:(CGRect)frame
{
    _imgLoader=[[UIImageView alloc]init];
    _imgLoader.frame=CGRectMake((SCREEN_SIZE.width/2.0)-40,frame.origin.y, 80, 80);
    [self.view addSubview:_imgLoader];
   
    /*_activityLoader = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityLoader.frame = frame;
    [self.view addSubview:_activityLoader];
    _activityLoader.hidden=YES;*/
}
-(void)initLoaderActivity
{
    [self initLoaderActivityWithFrame: CGRectMake((SCREEN_SIZE.width/2.0)-40,(SCREEN_SIZE.height/2.0)-(40+kNavBarHeight+kStatusBarHeight), 80, 80)];
}

-(void)initEmptyDataLabelWithFrame:(CGRect)frame
{
    _lblNoRecord = [[UILabel alloc] initWithFrame:frame];
    _lblNoRecord.font=FONT_AVENIR_PRO_DEMI_H5;
    _lblNoRecord.textColor=UICOLOR_DARK_TEXT;
    _lblNoRecord.textAlignment=NSTextAlignmentCenter;
    [self.view addSubview:_lblNoRecord];
    _lblNoRecord.hidden=YES;
    
    _imgNoRecord = [[UIImageView alloc] initWithFrame:frame];
    _imgNoRecord.backgroundColor = [UIColor clearColor];
    _imgNoRecord.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:_imgNoRecord];
    _imgNoRecord.hidden=YES;
}

-(void)initEmptyDataLabel
{
//    [self initEmptyDataLabelWithFrame: CGRectMake(0,SCREEN_SIZE.height/2.0-NAVIGATION_HEIGHT,SCREEN_SIZE.width,30)];
    [self initEmptyDataLabelWithFrame: CGRectMake(40,NAVIGATION_HEIGHT + 30.f,SCREEN_SIZE.width-80.f,SCREEN_SIZE.height-2*(NAVIGATION_HEIGHT+120.f))];

}

-(void)showEmptyDataLabelWithMsg:(NSString *)message
{
    _lblNoRecord.text=message;
    _lblNoRecord.hidden=NO;
}

- (void) showEmptyDataImageViewWithImage:(UIImage *) image {
    _imgNoRecord.image = image;
    _imgNoRecord.hidden = NO;
}

-(void)startLoadingActivity
{
    if(_imgLoader.image==nil)
        _imgLoader.image= [UIImage sd_animatedGIFNamed:@"ripple"];
    _imgLoader.hidden=NO;
    
    /*_activityLoader.hidden =NO;
    [_activityLoader startAnimating];*/
    _lblNoRecord.hidden=YES;
    _imgNoRecord.hidden=YES;
    
}
-(void)stopLoadingActivity
{
    _imgLoader.image= nil;
    _imgLoader.hidden=YES;
    _lblNoRecord.hidden=YES;
    _imgNoRecord.hidden=YES;
   /* _activityLoader.hidden =YES;
    [_activityLoader stopAnimating];*/
}

-(void)hideLocationView
{
    if(_vwLocation.superview)
     [_vwLocation removeFromSuperview];
}
-(void)showLocationView
{
    [self hideLocationView];
   
    if(!_vwLocation)
    {
       // _vwLocation = [[[NSBundle mainBundle] loadNibNamed:@"LocationView" owner:[LocationView class] options:nil] objectAtIndex:0];

        _vwLocation=[[LocationView alloc]init];
        _vwLocation.frame=CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
    }
    _vwLocation.backgroundColor=[UIColor whiteColor];
    [UIAppDelegate.window addSubview:_vwLocation];
    [UIAppDelegate.window bringSubviewToFront:_vwLocation];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    self.navigationItem.titleView=nil;
    self.Nav = nil;
    self.explictbackBtn=nil;
    
}

@end
