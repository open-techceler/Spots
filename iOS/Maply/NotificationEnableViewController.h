//
//  NotificationEnableViewController.h
//  Maply
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NotificationEnableViewControllerDelegateForSkip;

@interface NotificationEnableViewController : UIViewController

@property (weak, nonatomic) id<NotificationEnableViewControllerDelegateForSkip> delegateForSkip;

@property (nonatomic, assign) bool bShowSkipButton;

- (IBAction)askMe:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSkip;
- (IBAction)actionSkip:(id)sender;

@end

@protocol NotificationEnableViewControllerDelegateForSkip <NSObject>

- (void) tappedNotificationSkipButton;

@end
