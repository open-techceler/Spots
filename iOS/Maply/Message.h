//
//  Message.h
//  Maply
//
//  Created by admin on 2/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MTLModelModified.h"
@class User;

@interface Message : MTLModelModified

@property(nonatomic)NSInteger message_id;
@property(nonatomic,strong)NSString *text,*image,*type,*media;
@property(nonatomic,strong)NSString *location_name,*location_address;
@property(nonatomic)double latitude,longitude;
@property(nonatomic,strong)User *user;
@property(nonatomic)double seenTimeStamp;
@property BOOL isDistructed,is_saved;

//my journey
@property(nonatomic,strong)NSString *banner_image;
@property(nonatomic)NSInteger view_count;
@property(nonatomic,strong)NSString *created_date;

@end
