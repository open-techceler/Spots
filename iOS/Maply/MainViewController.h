//
//  MainViewController.h
//  Maply
//
//  Created by admin on 5/3/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewViewController.h"

@interface MainViewController : UIViewController

@property (nonatomic, assign) NSUInteger currentIndex;
@property (nonatomic, assign) BOOL swipeEnabled;
@property NSInteger currentPageIndex;
-(void)popToMapView;
-(HomeViewViewController *)HomeViewInstance;

@end
