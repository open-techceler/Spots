//
//  GetDirectionViewController.m
//  Maply
//
//  Created by admin on 2/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "GetDirectionViewController.h"
#import <MapKit/MapKit.h>
#import "BaseImageViewWithData.h"
#import "Journey.h"
#import "User.h"
#import "Message.h"
#import "MyLocation.h"
#import "DXAnnotationView.h"
#import "DXAnnotationSettings.h"
#import "LocationClass.h"
#import "NSObject+SimpleJson.h"
#import "DDAnnotation.h"

@interface GetDirectionViewController ()
@property(nonatomic,strong)IBOutlet MKMapView *mapView;
@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UIView *vwContainer;
@property(nonatomic,weak)IBOutlet UIButton *btnGetDirection;
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgUser;

@property (nonatomic, weak) IBOutlet UILabel *lblCallTitle,*lblCallAddress;

@end

@implementation GetDirectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setFontAndColor];
    [self setUpNavigationBar];

    

}

-(void)setUpNavigationBar
{
    self.lblTitle.text=self.objJourney.user.name;

}

-(void)setFontAndColor
{
    self.view.backgroundColor=[[UIColor blackColor] colorWithAlphaComponent:.5];
    
    self.vwContainer.layer.masksToBounds=YES;
    self.vwContainer.layer.cornerRadius=5.0;
    
    self.btnGetDirection.layer.masksToBounds=YES;
    self.btnGetDirection.layer.cornerRadius=5.0;
    
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    self.imgUser.clipsToBounds = YES;
    
    self.lblTitle.font=FONT_AVENIR_PRO_DEMI_H4;
    self.lblTitle.textColor=UICOLOR_DARK_TEXT;
    self.btnGetDirection.titleLabel.font=FONT_AVENIR_PRO_DEMI_H4;
    
    [self.btnGetDirection setTitle:MPLocalizedString(@"get_directions", nil).uppercaseString forState:UIControlStateNormal];
    [self.imgUser getImageWithURL:self.objJourney.user.image comefrom:ImageViewComfromUser];

    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self addMessageToMap];
    });
 

}

-(IBAction)clickBack:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)clickDirection:(id)sender
{
    double myLat=[LocationClass sharedLoctionManager].currentLocation.coordinate.latitude;
    double myLng=[LocationClass sharedLoctionManager].currentLocation.coordinate.longitude;
    
    NSString* addr = [NSString stringWithFormat:@"http://maps.apple.com/maps?daddr=%1.6f,%1.6f&saddr=%1.6f,%1.6f", self.objMessage.latitude,self.objMessage.longitude,myLat,myLng];
    
    NSURL* url = [[NSURL alloc] initWithString:[addr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [[UIApplication sharedApplication] openURL:url];
  //  [self LoadMapRoute];
}

- (void)addMessageToMap {
    
        // Get business coordinate
        double latitude = self.objMessage.latitude;
        double longitude = self.objMessage.longitude;
   
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(coordinate, 800, 800);
        [self.mapView setRegion:[self.mapView regionThatFits:region] animated:NO];
        
        // Get business name
        NSString * businessName =  self.objMessage.location_name;
        
        // Get business address
        NSString * businessAddress = self.objMessage.location_address;
        
        // Create loaction and call method to add it on map
        MyLocation *annotation = [[MyLocation alloc] initWithName:businessName address:businessAddress coordinate:coordinate index:0];
        [_mapView addAnnotation:annotation];
}

#pragma mark - MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
   
}
-(void)fillDataFor:(UIView *)calloutView
{
    self.lblCallTitle.font=[UIFont fontWithName:AvenirNextLTProDemi size:10.0f];
    self.lblCallAddress.font=[UIFont fontWithName:AvenirNextLTProDemi size:10.0f];
    
    self.lblCallTitle.textColor=UICOLOR_DARK_TEXT;
    self.lblCallAddress.textColor=UICOLOR_LIGHT_TEXT;
    
    //calloutView.backgroundColor=[UIColor whiteColor];
    
    self.lblCallTitle.text=self.objMessage.location_name;
    self.lblCallAddress.text=self.objMessage.location_address;
    
    [calloutView bringSubviewToFront:self.lblCallTitle];
    [calloutView bringSubviewToFront:self.lblCallAddress];
    
    float required_width=[calloutView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].width+15;
    CGRect rct=calloutView.frame;
    rct.size.width=MAX(required_width, 111);
    calloutView.frame=rct;

   
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MyLocation class]]) {
        
        UIImageView *pinView = nil;
        
        UIView *calloutView = nil;
        
        DXAnnotationView *annotationView = (DXAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:NSStringFromClass([DXAnnotationView class])];
        if (!annotationView) {
            pinView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fill"]];
            calloutView = [[[NSBundle mainBundle] loadNibNamed:@"myView" owner:self options:nil] firstObject];
            
            MyLocation * obj_MyLocation = annotation;
            calloutView.tag=[[obj_MyLocation index] integerValue];
            
            [self fillDataFor:calloutView];
            
            DXAnnotationSettings *newSettings = [[DXAnnotationSettings alloc] init];
            newSettings.calloutOffset = 5.0f;
            newSettings.shouldRoundifyCallout = NO;
            newSettings.calloutCornerRadius = 0.0f;
            newSettings.shouldAddCalloutBorder = NO;
            newSettings.calloutBorderColor = [UIColor clearColor];
            newSettings.calloutBorderWidth = 0.0;
            newSettings.animationType = DXCalloutAnimationNone;
            newSettings.animationDuration = 0.15;
            
            annotationView = [[DXAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:NSStringFromClass([DXAnnotationView class])
                                                                  pinView:pinView
                                                              calloutView:calloutView
                                                                 settings:newSettings];
            [annotationView showCalloutView];
        }else {
            MKAnnotationView *pinView = nil;
            static NSString *defaultPinID = @"com.cabwala.pin";
            pinView = (MKAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
            if ( pinView == nil )
                pinView = [[MKAnnotationView alloc]
                           initWithAnnotation:annotation reuseIdentifier:defaultPinID];
            DDAnnotation *ddannotation = (DDAnnotation*)annotation;
            if([ddannotation.index isEqualToString:@"source"])
            {
                pinView.image = [UIImage imageNamed:@"fill"];
                
            }
            else
            {
                pinView.image = [UIImage imageNamed:@"fill"];    //as suggested by Squatch
                
            }
            return pinView;
            //Changing PinView's image to test the recycle
            // pinView = (UIImageView *)annotationView.pinView;
            //pinView.image = [UIImage imageNamed:@"car-blue-icon"];
        }
        
        
        return annotationView;
    }
    return nil;

}
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [mapView deselectAnnotation:view.annotation animated:YES];

}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    //DDAnnotation *annotationView = (DDAnnotation*)view.annotation;
    
}
//-------------------------------------
// ************* Map Route******************
//-------------------------------------

-(void)LoadMapRoute
{
    if (self.mapView.annotations.count > 0) {
        [self.mapView removeAnnotations:[self.mapView annotations]];
    }
    double myLat=[LocationClass sharedLoctionManager].currentLocation.coordinate.latitude;
    double myLng=[LocationClass sharedLoctionManager].currentLocation.coordinate.longitude;
    
    CLLocationCoordinate2D sourceCoordinate=  CLLocationCoordinate2DMake(myLat,myLng);
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.04, 0.04);
    MKCoordinateRegion region;
    region.span = span;
    region.center=sourceCoordinate;
    
    /* if(![self.objBookingDetails hasValidDestinationCoordinate])
     {
     DDAnnotation *point=[[DDAnnotation alloc] initWithCoordinate:sourceCoordinate addressDictionary:nil];
     // point.title =[_routeName objectAtIndex:0];
     point.index=@"source";
     [self.mapView addAnnotation:point];
     
     [self.mapView setRegion:region animated:YES];
     return;
     }*/
    
    NSString *origin=[NSString stringWithFormat:@"%f,%f",myLat,myLng];
    
    double msg_latitude = self.objMessage.latitude;
    double msg_longitude = self.objMessage.longitude;
    
    // NSString *destination=[NSString stringWithFormat:@"%f,%f",myLat+1,myLng+1];
    NSString *destination=[NSString stringWithFormat:@"%f,%f",msg_latitude,msg_longitude];
    
    NSURL *url = [self getGoogleDistanceUrlWithSourceCoordinate:origin destinationCoordinate:destination];
    MPLog(@"google url %@",url);
    
    NSDictionary *data1 = [[[NSData alloc] initWithContentsOfURL:url] JSONValue];
    MPLog(@"data1 %@",data1);
    
    NSArray *routes = [data1 objectForKey:@"routes"];
    
    if(routes.count==0)
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"direction_not_found", nil)];
        [self.mapView setRegion:region animated:YES];
        return;
    }
    else
    {
        
        NSDictionary *firstRoute = [routes firstObject];
        NSDictionary *leg =  [[firstRoute objectForKey:@"legs"] firstObject];
        NSArray *steps = [leg objectForKey:@"steps"];
        
        int stepIndex = 0;
        CLLocationCoordinate2D stepCoordinates[[steps count]+1 ];
        
        for (NSDictionary *step in steps)
        {
            
            NSDictionary *start_location = [step objectForKey:@"start_location"];
            double latitude = [[start_location objectForKey:@"lat"] doubleValue];
            double longitude = [[start_location objectForKey:@"lng"] doubleValue];
            stepCoordinates[stepIndex] = CLLocationCoordinate2DMake(latitude, longitude);
            
            if (stepIndex==0)
            {
                DDAnnotation *point=[[DDAnnotation alloc] initWithCoordinate:stepCoordinates[stepIndex] addressDictionary:nil];
                /// point.title =[_routeName objectAtIndex:0];
                point.index=@"source";
                [self.mapView addAnnotation:point];
            }
            if (stepIndex==[steps count]-1)
            {
                stepIndex++;
                NSDictionary *end_location = [step objectForKey:@"end_location"];
                double latitude = [[end_location objectForKey:@"lat"] doubleValue];
                double longitude = [[end_location objectForKey:@"lng"] doubleValue];
                stepCoordinates[stepIndex] = CLLocationCoordinate2DMake(latitude, longitude);
                
                DDAnnotation *point=[[DDAnnotation alloc] initWithCoordinate:stepCoordinates[stepIndex] addressDictionary:nil];
                point.index=@"des";
                /// point.title =[_routeName objectAtIndex:1];
                
                [self.mapView addAnnotation:point];
                
            }
            stepIndex++;
        }
        MKPolyline *polyLine = [MKPolyline polylineWithCoordinates:stepCoordinates count: stepIndex];
        [self.mapView addOverlay:polyLine];
        [self.mapView setRegion:region animated:YES];
    }
    
    
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineRenderer *polylineView = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor blueColor];
    polylineView.lineWidth = 4;
    
    return polylineView;
}

-(NSURL*)getGoogleDistanceUrlWithSourceCoordinate:(NSString*)sco destinationCoordinate:(NSString*)dco
{
    NSString *url=  [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&sensor=true", sco,dco];
    
    return  [NSURL URLWithString:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
