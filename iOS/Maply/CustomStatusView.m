//
//  CustomStatusView.m
//  Maply
//
//  Created by admin on 4/29/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "CustomStatusView.h"

@implementation CustomStatusView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)init
{
    self = [super init];
    if (self) {
        [self setupUI];
    }
    return self;
}

-(void)awakeFromNib
{
    [self setupUI];
}

- (void)setupUI
{
    self.arrSessions=[NSMutableArray new];
    self.backgroundColor=[UIColor whiteColor];
}

-(void)startNewSession
{
    float x_value_for_new_session=_lastSession.frame.origin.x+_lastSession.frame.size.width;
    if(_lastSession!=nil)
        x_value_for_new_session+=1;
    
    _currentStatus=[[UIView alloc]init];
    _currentStatus.backgroundColor=UICOLOR_PROGRESS_BAR;
    _currentStatus.frame=CGRectMake(x_value_for_new_session, 0, 0, self.frame.size.height);
    [self addSubview:_currentStatus];
    [self.arrSessions addObject:_currentStatus];
}

-(void)updateSessionValue:(float)width
{
   // NSLog(@"width-self.frame.size.width>>%f",width-self.frame.size.width);
    
    CGRect frame =  _currentStatus.frame;
    frame.size.width = width-_currentStatus.frame.origin.x;
    _currentStatus.frame = frame;
    
    CGRect frame1 =  self.frame;
    frame1.size.width = width;
    self.frame = frame1;
}

-(void)removeLastSession
{
    CGRect frame1 =  self.frame;
    frame1.size.width = self.frame.size.width-(self.lastSession.frame.size.width+1);
    self.frame = frame1;
    
    [self.lastSession removeFromSuperview];
    [self.arrSessions removeLastObject];
    _lastSession=[self.arrSessions lastObject];
}

-(void)stopSession
{
//    CGRect frame1 =  self.frame;
//    frame1.size.width = frame1.size.width+1;
//    self.frame = frame1;
    _lastSession=_currentStatus;
}

@end
