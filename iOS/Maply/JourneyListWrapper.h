//
//  JourneyListWrapper.h
//  Maply
//
//  Created by admin on 2/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MTLModelModified.h"

@interface JourneyListWrapper : MTLModelModified
@property(nonatomic,strong)NSArray *list;
@property(nonatomic,strong)NSArray *my_journey;

@end
