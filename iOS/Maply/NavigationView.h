//
//  NavigationView.h
//  VivinoV2
//
//  Created by Admin on 24/07/13.
//
//

#import <UIKit/UIKit.h>
#import "RNLoadingButton.h"

@interface NavigationView : UIView

@property (nonatomic,strong) RNLoadingButton *btnLeft,*btnRight;
@property (nonatomic,strong) UILabel *lblTitle;
@property (nonatomic,strong) UIActivityIndicatorView *activityView;
-(void)setNavigationLayout:(BOOL)isModelView;
-(void)setNavigationLayoutXXX:(BOOL)isModelView;
-(void)addSubtitleWithText:(NSString*)subTitle withFont:(UIFont*)fontSize;
-(void)showActivity:(BOOL)animated;

-(void)makeRightButtonTextTwolines;
-(void)applyLineBreakMode:(NSLineBreakMode)lineBreakMode;
-(void)setBackButtonAsLeftButton;
-(void)setRightButtonWithImageName:(NSString*)imageName withTitle:(NSString*)title font:(UIFont*)font;
-(void)setLeftButtonWithImageName:(NSString*)imageName withTitle:(NSString*)title font:(UIFont*)font;

-(void)setWhiteBackButtonAsLeftButton;
-(void)setNextButtonRightButton;
-(void)setRightButtonWithTitle:(NSString *) title font:(UIFont *) font;

@end
