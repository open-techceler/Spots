//
//  FriendsMainViewController.m
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "FriendsMainViewController.h"
#import "FriendsViewController.h"
#import "AddFriendsViewController.h"
#import "RequestViewController.h"
#import "CarbonKit.h"
#import "UserAPI.h"
#import "MFSideMenu.h"

@interface FriendsMainViewController ()<FriendsViewDelegate,RequestViewDelegate,
CarbonTabSwipeNavigationDelegate>
{
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}
@property (nonatomic,strong) FriendsViewController *friendsViewController;
@property (nonatomic,strong) RequestViewController *requestViewController;


@end

@implementation FriendsMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setFontAndColor];
    [self setUpNavigationBar];
    
   // dispatch_async(dispatch_get_main_queue(), ^{
        self.friendsViewController=[[FriendsViewController alloc]initWithNibName:@"FriendsViewController" bundle:nil];
       // self.friendsViewController.delegate=self;
        self.friendsViewController.view.frame=CGRectMake(0, 30, SCREEN_SIZE.width, SCREEN_SIZE.height-30);
        
        self.requestViewController=[[RequestViewController alloc]initWithNibName:@"RequestViewController" bundle:nil];
        self.requestViewController.delegate=self;
        self.requestViewController.view.frame=CGRectMake(0, 30, SCREEN_SIZE.width, SCREEN_SIZE.height-30);
        
       /* [self.view addSubview:self.friendsViewController.view];
        [self.view addSubview:self.requestViewController.view];
        
        self.friendsViewController.view.hidden=YES;
        self.requestViewController.view.hidden=YES;
        [self clickTabButton:self.btnFriends];*/

    //});
    items = @[MPLocalizedString(@"friends", nil),
              MPLocalizedString(@"request", nil)];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    [self style];
    
    
}

-(void)refreshFriendsList
{
    [self.friendsViewController reloadFriendList];
    [self.requestViewController reloadFriendList];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self updateUserActionsOnServer];
    [self resetTabToFriends];
}

-(void)updateUserActionsOnServer{
    if( self.requestViewController.arrayActions.count || self.friendsViewController.arrayActions.count)
    {
        NSMutableArray *arr=[NSMutableArray new];
         if(self.requestViewController.arrayActions.count)
             [arr addObjectsFromArray:self.requestViewController.arrayActions];
        if( self.friendsViewController.arrayActions.count)
            [arr addObjectsFromArray:self.friendsViewController.arrayActions];
        
        [[UserAPI new]updateUserAction:arr success:^(FriendsListWrapper *objWrapper) {
            [self.requestViewController.arrayActions removeAllObjects];
            [self.friendsViewController.arrayActions removeAllObjects];
           
        } failure:^(NSError *error) {
            
        }];
    }
  
}


- (void)style {
    
    UIColor *color = [UIColor  blackColor];
    [carbonTabSwipeNavigation setTabBarHeight:kScopeBarHeight];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.clipsToBounds = YES;
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor clearColor]];
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setIndicatorHeight:3];
    [carbonTabSwipeNavigation.carbonSegmentedControl.indicator setImage:[UIImage imageNamed:@"line-1"]];
    carbonTabSwipeNavigation.carbonSegmentedControl.indicator.contentMode=UIViewContentModeScaleToFill;
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:SCREEN_SIZE.width/2 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:SCREEN_SIZE.width/2 forSegmentAtIndex:1];

    
    UIView *vwSep=[[UIView alloc]initWithFrame:CGRectMake(SCREEN_SIZE.width/2,(kScopeBarHeight-18)/2, 1, 18)];
    vwSep.backgroundColor=UICOLOR_SEPERATOR;
    [carbonTabSwipeNavigation.carbonSegmentedControl addSubview:vwSep];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:color
                                        font:FONT_SEGMENT_TITLE];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:FONT_SEGMENT_TITLE];
    
    carbonTabSwipeNavigation.carbonSegmentedControl.layer.borderWidth=1;
    carbonTabSwipeNavigation.carbonSegmentedControl.layer.borderColor=UICOLOR_SEPERATOR.CGColor;
    carbonTabSwipeNavigation.carbonTabSwipeScrollView.scrollEnabled=NO;

    for (UIScrollView *view in carbonTabSwipeNavigation.pageViewController.view.subviews) {
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.bounces = NO;
            view.scrollEnabled = NO;
        }
    }
}

-(void)setBadgeForTab:(NSInteger)tabIndex value:(NSInteger)value
{
    UIView *vwSelected=[carbonTabSwipeNavigation.carbonSegmentedControl.segments objectAtIndex:tabIndex];

    UIButton *lblBadge=[vwSelected viewWithTag:1000];
    if(lblBadge==nil)
    {
        lblBadge=[UIButton buttonWithType:UIButtonTypeCustom];
        lblBadge.frame=CGRectMake([self getXOfButtonTitle:tabIndex]-20,(kScopeBarHeight/2)-7, 14, 14);
        lblBadge.tag=1000;
        lblBadge.backgroundColor=PICKER_COLOR_PINK;
      //  [lblBadge setTitleEdgeInsets:UIEdgeInsetsMake(1, 1, 0, 0)];
        lblBadge.titleLabel.textColor=[UIColor whiteColor];
        lblBadge.titleLabel.font=FONT_SEGEMENT_BADGE;
        lblBadge.layer.cornerRadius=4.0;
        lblBadge.clipsToBounds = YES;
        [vwSelected addSubview:lblBadge];

    }
    else
        lblBadge.frame=CGRectMake([self getXOfButtonTitle:tabIndex]-20, (kScopeBarHeight/2)-7, 14, 14);
    
    
    if(value)
    {
        lblBadge.hidden=NO;
         [lblBadge setTitle:[NSString stringWithFormat:@"%ld",(long)value] forState:UIControlStateNormal];
    }
    else
        lblBadge.hidden=YES;
}

-(NSInteger)getXOfButtonTitle:(NSInteger)tabIndex
{
    NSString *tabTitle=(tabIndex==0?MPLocalizedString(@"friends", nil) :MPLocalizedString(@"request", nil));
    NSInteger total_width=SCREEN_SIZE.width/2;
    CGSize boundingSize = CGSizeMake(total_width, CGFLOAT_MAX);
    CGSize requiredSize = [tabTitle VVSizeWithFont:FONT_AVENIR_PRO_DEMI_H1 constrainedToSize:boundingSize lineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat label_x=(total_width/2)-(requiredSize.width/2);
    return label_x;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton=YES;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    [self refreshFriendsList];
}

-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"friends", nil);
    
    [self.Nav setLeftButtonWithImageName:@"globe" withTitle:nil font:nil];
    [self.Nav.btnLeft addTarget:self action:@selector(clickBack:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.Nav setRightButtonWithImageName:@"addf" withTitle:nil font:nil];
    [self.Nav.btnRight addTarget:self action:@selector(clickAdd:)forControlEvents:UIControlEventTouchUpInside];
    [self.Nav setNavigationLayout:YES];
    
}

-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
}

-(IBAction)clickBack:(id)sender
{
    UIAppDelegate.mainController.currentIndex=kJourneysViewController;

   // [self.navigationController popViewControllerAnimated:YES];

}

-(IBAction)clickAdd:(id)sender
{
    AddFriendsViewController *cnt=[[AddFriendsViewController alloc]initWithNibName:@"AddFriendsViewController" bundle:nil];
    [self.navigationController pushViewController:cnt animated:YES];
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)resetTabToFriends
{
        if(carbonTabSwipeNavigation != nil && carbonTabSwipeNavigation.currentTabIndex!=0)
            carbonTabSwipeNavigation.currentTabIndex=0;
    
}


# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return self.friendsViewController;
            
        case 1:
            return self.requestViewController;
            
            
        default:
            return self.friendsViewController;
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    if(carbonTabSwipeNavigation.currentTabIndex!=0 && index == 0) {
        [self.friendsViewController reloadFriendList];
    }
    if (carbonTabSwipeNavigation.currentTabIndex != 1 && index == 1){
        [self.requestViewController reloadFriendList];
    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", (unsigned long)index);
    
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

@end
