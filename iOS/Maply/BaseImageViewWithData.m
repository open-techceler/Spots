

#import "BaseImageViewWithData.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <Masonry/Masonry.h>
#import "UIImage+GIF.h"


static UIImage* cabPlaceHolder;
static UIImage* userPlaceHolder;
static UIImage* cabProviderPlaceHolder;
@interface BaseImageViewWithData()

@property (nonatomic,strong) UIImage *placeholder;
@property (nonatomic) ImageViewComfrom comefrom;
//@property (nonatomic,strong) UIActivityIndicatorView *activity;
@property (nonatomic,strong)UIImageView *imgLoader;

@end


@implementation BaseImageViewWithData

#pragma mark - Initialize
+ (void)initialize
{
    if(self == [BaseImageViewWithData class])
    {
        cabPlaceHolder = [UIImage imageNamed:kCabPlaceHolderImage];
        userPlaceHolder = [UIImage imageNamed:kUserPlaceHolderImage];
        cabProviderPlaceHolder = [UIImage imageNamed:kCabProviderPlaceHolderImage];
    }
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor=[UIColor whiteColor];
}

-(void)addBigImageLoader
{
    if(_imgLoader==nil)
    {
        _imgLoader=[[UIImageView alloc]init];
        [self addSubview:_imgLoader];
        _imgLoader.hidden = YES;
        
        [_imgLoader mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            make.width.equalTo(@80);
            make.height.equalTo(@80);
        }];
    }
 
}

#pragma mark - Get Images Methods
- (void) getImageWithURL:(NSString*)stringURL  comefrom:(ImageViewComfrom)comefrom
{
    /*if(self.activity==nil)
    {
        _activity=[[UIActivityIndicatorView alloc]init];
        _activity.activityIndicatorViewStyle=UIActivityIndicatorViewStyleGray;
        [self addSubview:_activity];
        
        [_activity mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.centerY.equalTo(self.mas_centerY);
            
        }];
    }*/
    self.comefrom=comefrom;

    if(_imgLoader==nil)
    {
        _imgLoader=[[UIImageView alloc]init];
        [self addSubview:_imgLoader];
        
        if(_comefrom==ImageViewComfromUser)
        {
            [_imgLoader mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.mas_centerX);
                make.centerY.equalTo(self.mas_centerY);
                make.width.equalTo(@40);
                make.height.equalTo(@40);
            }];
        }
        else if(_comefrom==ImageViewComfromMoment)
        {
            [_imgLoader mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.mas_centerX);
                make.centerY.equalTo(self.mas_centerY);
                make.width.equalTo(@80);
                make.height.equalTo(@80);
            }];
        }
    }

    [self setPlaceHolder];
    
    [self loadImageWithURL:stringURL success:nil failure:nil];
}

- (void) getImageWithURL:(NSString*)stringURL
                comefrom:(ImageViewComfrom)comefrom
                 success:(void (^)())success
                 failure:(void (^)())failure
{
    self.comefrom=comefrom;
    
    if(_imgLoader==nil)
    {
        _imgLoader=[[UIImageView alloc]init];
        [self addSubview:_imgLoader];
        
        if(_comefrom==ImageViewComfromUser)
        {
            [_imgLoader mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.mas_centerX);
                make.centerY.equalTo(self.mas_centerY);
                make.width.equalTo(@40);
                make.height.equalTo(@40);
            }];
        }
        else if(_comefrom==ImageViewComfromMoment)
        {
            [_imgLoader mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(self.mas_centerX);
                make.centerY.equalTo(self.mas_centerY);
                make.width.equalTo(@80);
                make.height.equalTo(@80);
            }];
        }
    }
    
    [self setPlaceHolder];
    [self loadImageWithURL:stringURL success:^{
        if(success)
            success();
    } failure:^{
            if(failure)
                failure();
    }];

}

-(void)loadImageWithURL:(NSString*)stringURL
                success:(void (^)())success
                failure:(void (^)())failure
{
    if ([stringURL isEmpty] == YES || stringURL == nil) {
        self.image = _placeholder;
        return;
    }
    [self startActivity];

     // [_activity startAnimating];
    NSURL* url;
     url = [NSURL URLWithString:stringURL];
    
    
    //VVLog(@"url==>%@",url);
    
   __weak BaseImageViewWithData* weakSelf = self;
    [self sd_setImageWithURL:url
            placeholderImage:_placeholder
                   completed:^(UIImage* image, NSError* error, SDImageCacheType cacheType,NSURL *originalImageURL) {
                        [self stopActivity];
                      // [_activity stopAnimating];
                       if(image) {
                         
                          [UIView transitionWithView:weakSelf
                                             duration:0.2f
                                              options:UIViewAnimationOptionTransitionCrossDissolve
                                           animations:^{
                                               
                                               [weakSelf setImage:image];
                                           
                                           }
                                           completion:NULL];
                           
                           if(success)
                               success();
                       }
                   }];
}

#pragma mark - Custum Methods

- (void)cancelDownload
{
    [self sd_cancelCurrentImageLoad];
}

-(void)startActivity
{
    if(_imgLoader==nil)
    {
        [self addBigImageLoader];
    }
    _imgLoader.image= [UIImage sd_animatedGIFNamed:@"ripple"];
    [self bringSubviewToFront:_imgLoader];
    _imgLoader.hidden = NO;
}

-(void)stopActivity
{
    _imgLoader.image=nil;
    _imgLoader.hidden = YES;

}

- (void) setPlaceHolder
{
    self.clipsToBounds=YES;
   
    if(_comefrom==ImageViewComfromUser)
    {
        _placeholder =userPlaceHolder;
        self.contentMode = UIViewContentModeScaleAspectFill;
    }
    else if(_comefrom == ImageViewComfromMoment)
    {
        _placeholder =userPlaceHolder;
        self.contentMode = UIViewContentModeScaleAspectFill;
    }
}


-(void)makeImageRoundWithRadius:(CGFloat)radius border:(CGFloat)border color:(UIColor*)color
{
    CALayer *imageLayer = self.layer;
    [imageLayer setCornerRadius:radius];
    [imageLayer setBorderWidth:border];
    [imageLayer setBorderColor:color.CGColor];
    [imageLayer setMasksToBounds:YES];

}

-(void)dealloc
{
    MPLog(@"BaseImageViewWithData");
}
@end
