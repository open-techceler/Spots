#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyLocation : NSObject <MKAnnotation> {
    NSString *_name;
    NSString *_address;
    NSString *_index;
    CLLocationCoordinate2D _coordinate;
    
//    CLLocationDegrees _latitude;
//    CLLocationDegrees _longitude;

}

@property (copy) NSString *name;
@property (copy) NSString *address;
@property (copy) NSString *index;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate index:(NSString*)index;

//- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;

@end
