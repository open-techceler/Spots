//
//  LanguageManager.m
//  VivinoV2
//
//  Created by Admin on 17/04/14.
//
//

#import <objc/runtime.h>
#import "LanguageManager.h"

NSString* const kUserPreferedLanguageKey = @"userPreferedLanguage";
NSString* const kUserPreferedLocaleKey = @"userPreferedLocale";

#define LANGUAGE_ENGLISH_CODE @"en"
#define LANGUAGE_ENGLISH_LOCALE_CODE @"en_US"

#define LANGUAGE_GERMAN_CODE @"de"
#define LANGUAGE_GERMAN_LOCALE_CODE @"de_DE"

typedef NS_ENUM(NSInteger, LanguageManagerLanguage) {
    English = 0,
    German = 1
};

@interface LanguageManager ()

@property (nonatomic, strong) NSBundle* bundle;
@property (nonatomic, strong) NSArray* supportedLanguages;

@end

@implementation LanguageManager

+ (NSString*)get:(NSString*)key alter:(NSString*)alternate
{
    return [[LanguageManager sharedInstance] get:key
                                           alter:alternate];
}

+ (NSInteger)getPerferedLanguageInInteger
{
    return [[LanguageManager sharedInstance] getPerferedLanguageInInteger];
}

+ (NSString*)getLanguageNameWithCode:(NSInteger)code
{
    return [[LanguageManager sharedInstance] getLanguageNameWithCode:code];
}

+ (NSString*)nativeLanguageNameForLocale:(NSString*)isoIdentifier
{
    return [[LanguageManager sharedInstance] nativeLanguageNameForLocale:isoIdentifier];
}

+ (NSString*)getPerferedLocaleCode
{
    return [[LanguageManager sharedInstance] getPerferedLocaleCode];
}

+ (NSLocale*)getPerferedLocale
{
    return [[LanguageManager sharedInstance] getPerferedLocale];
}

+ (NSString*)getPerferedLanguage
{
    return [[LanguageManager sharedInstance] getPerferedLanguage];
}

+ (NSString*)getPerferedLanguageForServer
{
    return [[LanguageManager sharedInstance] getPerferedLanguageForServer];
}

+ (NSString*)getPluralizedString:(NSString*)key
                      withNumber:(CGFloat)n
                           alter:(NSString*)alternate
{
    return [[LanguageManager sharedInstance] getPluralizedString:key
                                                      withNumber:n
                                                           alter:alternate];
}

+ (NSString*)getLocalizeCountryNameWithCode:(NSString*)countryCode
{
    return [[LanguageManager sharedInstance] getLocalizeCountryNameWithCode:countryCode];
}

+ (NSString*)getLocalizeCurrencyNameWithCode:(NSString*)currencyCode
{
    return [[LanguageManager sharedInstance] getLocalizeCurrencyNameWithCode:currencyCode];
}

+ (UIImage*)getLocalizedImage:(NSString*)imageName
{
    return [[LanguageManager sharedInstance] getLocalizedImage:imageName];
}

+ (NSLocale*)getCurrentLocale
{
    return [[LanguageManager sharedInstance] getCurrentLocale];
}

+ (void)setUserPerferedLanguage:(NSString*)language
                     withLocale:(NSString*)locale
{
    return [[LanguageManager sharedInstance] setUserPerferedLanguage:language
                                                          withLocale:locale];
}

+ (void)setLanguageWithIntType:(NSInteger)language_value
{
    return [[LanguageManager sharedInstance] setLanguageWithIntType:language_value];
}

+ (void)setLanguageAsEnglish
{
    return [[LanguageManager sharedInstance] setLanguageAsEnglish];
}

+ (void)setLanguageAsGerman
{
    return [[LanguageManager sharedInstance] setLanguageAsGerman];
}


+ (LanguageManager*)sharedInstance
{
    static LanguageManager* sharedInstance = nil;
    static dispatch_once_t onceToken;

    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });

    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self) {

        _supportedLanguages = @[
            LANGUAGE_ENGLISH_CODE,
            LANGUAGE_GERMAN_CODE
        ];

        [self setUserPerferedLanguage:[self getPerferedLanguage]
                           withLocale:[self getPerferedLocaleCode]];
    }

    return self;
}

- (NSString*)get:(NSString*)key alter:(NSString*)alternate
{
    return [_bundle localizedStringForKey:key
                                    value:alternate
                                    table:nil];
}

- (NSString*)getPluralizedString:(NSString*)key
                      withNumber:(CGFloat)n
                           alter:(NSString*)alternate
{

    /*return [_bundle pluralizedStringWithKey:key
                               defaultValue:@""
                                      table:@""
                                pluralValue:n
                            forLocalization:[LanguageManager getPerferedLanguage]];*/
    return nil;
}

- (void)setUserPerferedLanguage:(NSString*)language
                     withLocale:(NSString*)locale
{
    if (language == nil) {
        [self setDefaultLanguage];
    } else {
        [self setLanguageInUserDefaultsWithLangaueCode:language
                                        withLocaleCode:locale];
    }
}

- (void)setLanguageWithIntType:(NSInteger)language_value
{
    switch (language_value) {
    case English:
        [self setLanguageAsEnglish];
        break;
        
    case German:
        [self setLanguageAsGerman];
        break;

    default:
        break;
    }
}

- (void)setLanguageInUserDefaultsWithLangaueCode:(NSString*)language
                                  withLocaleCode:(NSString*)locale
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSArray* languages = [NSArray arrayWithObject:language];
    [[NSUserDefaults standardUserDefaults] setObject:languages
                                              forKey:@"AppleLanguages"];

    [defaults setObject:language
                 forKey:kUserPreferedLanguageKey];
    [defaults setObject:locale
                 forKey:kUserPreferedLocaleKey];
    [defaults synchronize];
   NSString* path =
        [[NSBundle mainBundle] pathForResource:language
                                        ofType:@"lproj"];
    _bundle = [NSBundle bundleWithPath:path];
}

- (void)setDefaultLanguage
{
    // need to set default lanague
    NSString* language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString* locale = @"";
    if (![_supportedLanguages
            containsObject:language]) // set default langaue as english
    {
        language = LANGUAGE_ENGLISH_CODE;
        locale = LANGUAGE_ENGLISH_LOCALE_CODE;
    } else {

        if ([language isEqualToString:LANGUAGE_GERMAN_CODE]) {
            language = LANGUAGE_GERMAN_CODE;
            locale = LANGUAGE_GERMAN_LOCALE_CODE;

        }
        else {
            language = LANGUAGE_ENGLISH_CODE;
            locale = LANGUAGE_ENGLISH_LOCALE_CODE;
        }
    }

    [self setLanguageInUserDefaultsWithLangaueCode:language
                                    withLocaleCode:locale];
}

- (void)setLanguageAsEnglish
{
    [self setUserPerferedLanguage:LANGUAGE_ENGLISH_CODE
                       withLocale:LANGUAGE_ENGLISH_LOCALE_CODE];
}


- (void)setLanguageAsGerman
{
    [self setUserPerferedLanguage:LANGUAGE_GERMAN_CODE
                       withLocale:LANGUAGE_GERMAN_LOCALE_CODE];
}

- (NSString*)getPerferedLanguage
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:kUserPreferedLanguageKey];
}

- (NSString*)getPerferedLocaleCode
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    return [defaults objectForKey:kUserPreferedLocaleKey];
}

- (NSLocale*)getPerferedLocale
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString* isoIdentifier = [defaults objectForKey:kUserPreferedLocaleKey];
    return [[NSLocale alloc] initWithLocaleIdentifier:isoIdentifier];
}

- (NSString*)getPerferedLanguageForServer
{
    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
    NSString *val= [[[defaults objectForKey:kUserPreferedLanguageKey] componentsSeparatedByString:@"-"] firstObject];
    if (val == nil)
    {
        [self setLanguageWithIntType:0];
       return [self getPerferedLanguage];
    }
    else
    {
        return val;
    }
}

- (NSLocale*)getCurrentLocale
{
    return [[NSLocale alloc]
        initWithLocaleIdentifier:[LanguageManager getPerferedLocaleCode]];
}

- (NSString*)getLocalizeCountryNameWithCode:(NSString*)countryCode
{
    NSLocale* usLocale = [self getCurrentLocale];
    return [usLocale displayNameForKey:NSLocaleCountryCode
                                 value:countryCode];
}

- (NSString*)getLocalizeCurrencyNameWithCode:(NSString*)currencyCode
{
    NSLocale* usLocale = [self getCurrentLocale];

    return [[usLocale displayNameForKey:NSLocaleCurrencyCode
                                  value:currencyCode] capitalizedStringWithLocale:usLocale];
}

- (NSInteger)getPerferedLanguageInInteger
{
    NSInteger language_int_value = English;
    NSString* language = [self getPerferedLanguage];
    if ([language isEqualToString:LANGUAGE_ENGLISH_CODE])
        language_int_value = English;
    else if ([language isEqualToString:LANGUAGE_GERMAN_CODE])
        language_int_value = German;
   
  
    return language_int_value;
}

- (NSString*)getLanguageNameWithCode:(NSInteger)code
{
    NSString* languageName = @"";
    NSString* language = [self getPerferedLanguage];
    if ([language isEqualToString:LANGUAGE_ENGLISH_CODE])
        languageName = MPLocalizedString(@"English", nil);
   else if ([language isEqualToString:LANGUAGE_GERMAN_CODE])
        languageName = MPLocalizedString(@"German", nil);
      return languageName;
}

- (NSString*)nativeLanguageNameForLocale:(NSString*)isoIdentifier
{
    NSLocale* locale = [[NSLocale alloc] initWithLocaleIdentifier:isoIdentifier];
    return [[locale displayNameForKey:NSLocaleIdentifier
                                value:isoIdentifier]
        capitalizedStringWithLocale:locale];
}

- (UIImage*)getLocalizedImage:(NSString*)imageName
{
  
    return [UIImage imageWithContentsOfFile:[_bundle pathForResource:imageName
                                                              ofType:@"png"]];
}

+ (BOOL)isEnglishLanguage
{
    return [[LanguageManager getPerferedLanguage] isEqualToString:LANGUAGE_ENGLISH_CODE];
}

@end
