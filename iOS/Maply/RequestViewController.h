//
//  RequestViewController.h
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MyNavigationViewController.h"
@protocol  RequestViewDelegate <NSObject>
//-(void)pushController:(UIViewController *)cnt;
//-(void)presentViewController:(UIViewController *)cnt;
-(void)setBadgeForTab:(NSInteger)tabIndex value:(NSInteger)value;

@end

@interface RequestViewController : MyNavigationViewController
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSMutableArray *arrayActions;
@property(nonatomic,weak) id<RequestViewDelegate> delegate;
-(void)reloadFriendList;

@end
