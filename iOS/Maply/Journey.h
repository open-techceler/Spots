//
//  Journey.h
//  Maply
//
//  Created by admin on 2/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MTLModelModified.h"
@class User;

@interface Journey : MTLModelModified

@property(nonatomic,strong)NSString *name,*image;
@property(nonatomic)NSInteger journey_id,moments;
@property(nonatomic,strong)User *user;
@property(nonatomic)BOOL is_new, is_saved, expired;

@property(nonatomic)NSInteger count;

@property(nonatomic,strong)NSString *created_date,*modified_date;
@property(nonatomic,strong)NSArray *moment;

/*
 "id": 1,
	"user": {
 "id": 5,
 "username": "tes",
 "image": "https://s3.eu-central-1.amazonaws.com/oscar-app-media/1455612542448image.jpeg",
 "name": "Roshan Mahajan"
	},
	"name": "Нагадим в США",
	"image": "1456220856006image.jpeg",
	"moments": 1,
	"is_new": true
 
 */
@end
