//
//  SendToCell.h
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@interface SendToCell : UITableViewCell
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgUser;
@property(nonatomic,weak)IBOutlet UILabel *lblName;
@property(nonatomic,weak)IBOutlet UIButton *btnSelect;

@end
