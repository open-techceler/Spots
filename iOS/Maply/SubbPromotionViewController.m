//
//  SubbPromotionViewController.m
//  Beetot
//
//  Created by admin on 1/6/16.
//  Copyright (c) 2016 Rishi. All rights reserved.
//

#import "SubbPromotionViewController.h"
#import "Message.h"
#import "BaseImageViewWithData.h"
#import "MomentsManager.h"
#import "VVBaseUserDefaults.h"
#import "NotificationDefinitions.h"

@import AVFoundation;

#define VIDEO @"VIDEO"
#define IMAGE @"IMAGE"
#define GIF @"GIF"

#define SeenTimeStampKey(messageID) [NSString stringWithFormat:@"message_%d", (int)messageID]

@interface SubbPromotionViewController ()
@property(nonatomic,strong)IBOutlet BaseImageViewWithData *imageView,*gifImageView;
@property (strong, nonatomic) AVPlayer *avPlayer;
@property (strong, nonatomic) AVPlayerLayer *avPlayerLayer;
@property(nonatomic,strong)IBOutlet UIButton *btnTimer;
@property(nonatomic,strong)IBOutlet UILabel *lblDistruct;
@property (nonatomic, strong) NSTimer* timer;
@property double remaining_sec;
@property int appearedTimes;
@property bool isLoadedDone;
@end

@implementation SubbPromotionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initLoaderActivityWithFrame:CGRectMake((SCREEN_SIZE.width/2.0)-10,(SCREEN_SIZE.height/2.0)-40, 20, 20)];

    self.view.backgroundColor=UICOLOR_APP_BG;
    self.btnTimer.layer.cornerRadius=3.0;
    self.btnTimer.layer.borderColor=PICKER_COLOR_PINK.CGColor;
    self.btnTimer.layer.borderWidth=2.0;

    self.btnTimer.titleLabel.text = @"";
    self.btnTimer.titleLabel.font = FONT_AVENIR_PRO_DEMI_H3;
    
    self.btnTimer.titleLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.btnTimer.titleLabel.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    
    self.btnTimer.titleLabel.layer.shadowRadius = 5.0;
    self.btnTimer.titleLabel.layer.shadowOpacity =0.6;
    self.btnTimer.titleLabel.layer.masksToBounds = NO;
    self.btnTimer.titleLabel.layer.shouldRasterize = YES;
    
    self.lblDistruct.text = @"Moment is distructed";
    self.lblDistruct.hidden = YES;
    self.btnTimer.hidden = YES;
    
    _appearedTimes = 0;
    _isLoadedDone = NO;
    
    [self setData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //@added by steven
    _appearedTimes ++;
//    if (self.objMessage.seenTimeStamp != 0) {
//        if (appearedTimes > 1) {
//            [self setSeenTiming];
//            return;
//        }
//    }
    if (_isLoadedDone) {
        [self setSeenTiming];
        return;
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self stopTimer];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [self.avPlayer pause];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if(self.avPlayer && self.objMessage.isDistructed == NO)
    {
        if(self.avPlayer.rate==0)//play only if not playing
            [self.avPlayer play];
    }
}
-(void)startTimer
{
   
    double current = [[NSDate new] timeIntervalSince1970];
    double diff = current - self.objMessage.seenTimeStamp;
    
    double max_duration = 60;
    _remaining_sec = max_duration-diff;

    MPLog(@"seenTimeStamp>>>%f",self.objMessage.seenTimeStamp);
    MPLog(@"current>>>%f",current);
    MPLog(@"_remaining_sec>>>%f",_remaining_sec);
    if(_remaining_sec < 60  && _remaining_sec >0)
    {
        if (!_timer) {
            self.btnTimer.hidden = NO;
            _timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                      target:self
                                                    selector:@selector(timerFired:)
                                                    userInfo:nil
                                                     repeats:YES];
        }
    }
    else
    {
        [self momentIsDistructed];
        [self stopTimer];
    }
}

- (void)stopTimer
{
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    _timer = nil;
}

- (void)timerFired:(NSTimer*)timer
{
    _remaining_sec=_remaining_sec-1;
    
    if(_remaining_sec >=0)
    {
        [self.btnTimer setTitle:[NSString stringWithFormat:@"%.f",_remaining_sec] forState:UIControlStateNormal];
    }
    else
    {
        [self stopTimer];
        [self momentIsDistructed];
    }
}

-(void)momentIsDistructed
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [[MomentsManager sharedInstance] markMomentAsRead:self.objMessage.message_id];
        self.objMessage.isDistructed = YES;
        self.imageView.hidden =YES;
        self.avPlayerLayer.hidden =YES;
        self.btnTimer.hidden =YES;
//        self.lblDistruct.hidden =NO;
        if(self.avPlayer)
            [self.avPlayer pause];
        
        //@added by steven
        NSDictionary* userInfo = @{@"message_id": [NSNumber numberWithInteger:self.objMessage.message_id], @"page_index": [NSNumber numberWithInteger:self.pageIndex]};
        [[NSNotificationCenter defaultCenter] postNotificationName:kMessageIsDestructed object:nil userInfo:userInfo];
        //@added by steven
   });
}

-(int)getSeenTimeStamp {
    return [VVBaseUserDefaults getDoubleForKey:SeenTimeStampKey(self.objMessage.message_id)];
}

-(void)setSeenTimeWithCurrentTimeStamp {
    [VVBaseUserDefaults setDouble:[[NSDate date] timeIntervalSince1970] forKey: SeenTimeStampKey(self.objMessage.message_id)];
}

-(void)setSeenTiming
{
    if(self.comefrom == JourneyDetailsComeFromDirect)
    {
        if(self.objMessage.is_saved)
        {
            self.objMessage.isDistructed = NO;
            [self stopTimer];
            self.btnTimer.hidden =YES;

        }
        else if(self.objMessage.isDistructed == NO)
        {
//            if(self.objMessage.seenTimeStamp == 0)
//                self.objMessage.seenTimeStamp = [[NSDate date] timeIntervalSince1970];
            
            //@added by steven
            if ([self getSeenTimeStamp] == 0) {
                [self setSeenTimeWithCurrentTimeStamp];
            }
            self.objMessage.seenTimeStamp = [self getSeenTimeStamp];
            
            
            //@added by steven
            
            [self startTimer];
        }
        else
        {
            [self momentIsDistructed];
        }

    }
}

-(void)setObjMessage:(Message *)objMessage
{
    if(objMessage==nil) //load more....
    {
        [self startLoadingActivity];
    }
    else
    {
         [self stopLoadingActivity];
        _objMessage=objMessage;
        
        [self setData];
    }

}
-(void)setData
{
    if(self.objMessage.isDistructed == YES)
        return;
    
    self.lblDistruct.hidden =YES;

     if([self.objMessage.type isEqualToString:IMAGE])
     {
         self.imageView.hidden=NO;
//         [self.imageView getImageWithURL:self.objMessage.image comefrom:ImageViewComfromMoment];
         [self.imageView getImageWithURL:self.objMessage.image comefrom:ImageViewComfromMoment success:^{
             _isLoadedDone = YES;
             [[NSNotificationCenter defaultCenter] postNotificationName:kSubbPromotionVCLoadingDone object:nil];
             
             if (_appearedTimes > 0) {
                 [self setSeenTiming];
             }
         } failure:nil];
     }
     else if([self.objMessage.type isEqualToString:VIDEO])
     {
         self.imageView.hidden=NO;
         if(self.avPlayerLayer)
             [self.avPlayerLayer removeFromSuperlayer];
         
         self.avPlayer = [AVPlayer playerWithURL:[NSURL URLWithString:self.objMessage.media]];
         self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
         
         self.avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
         self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
         
         [[NSNotificationCenter defaultCenter] addObserver:self
                                                  selector:@selector(playerItemDidReachEnd:)
                                                      name:AVPlayerItemDidPlayToEndTimeNotification
                                                    object:[self.avPlayer currentItem]];
         
         self.avPlayerLayer.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
         // [self.view.layer insertSublayer:self.avPlayerLayer atIndex:0];
         
         [self.view.layer addSublayer:self.avPlayerLayer];
         [self.avPlayer play];
         
        /* [self.imageView getImageWithURL:self.objPromotion.image comefrom:ImageViewComfromUser];
         self.imageView.backgroundColor=[UIColor clearColor];
         [self.view bringSubviewToFront:self.imageView];
          */
         
         [self.imageView startActivity];
         [self.avPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];

     }
     else if([self.objMessage.type isEqualToString:GIF])
     {
         self.imageView.hidden=NO;
         [self.imageView getImageWithURL:self.objMessage.image comefrom:ImageViewComfromMoment];

         //[self.gifImageView getImageWithURL:self.objMessage.media comefrom:ImageViewComfromMoment];
         
         [self.gifImageView getImageWithURL:self.objMessage.media comefrom:ImageViewComfromMoment success:^{
             _isLoadedDone = YES;
             [[NSNotificationCenter defaultCenter] postNotificationName:kSubbPromotionVCLoadingDone object:nil];
             if (_appearedTimes > 0) {
                [self setSeenTiming];
             }
         } failure:nil];
         
         self.imageView.backgroundColor=[UIColor clearColor];
         [self.view bringSubviewToFront:self.imageView];
     }
    [self.view bringSubviewToFront:self.btnTimer];

  /*  if([Utility isEmpty:self.objPromotion.image])
    {
        self.lblMessage.hidden=NO;
        self.imageView.hidden=YES;
        self.lblMessage.text=self.objPromotion.text;
    }
    else
    {
        if([self.objPromotion.type isEqualToString:VIDEO])
        {
            self.imageView.hidden=NO;
           
            self.avPlayer = [AVPlayer playerWithURL:[NSURL URLWithString:self.objPromotion.media]];
            self.avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
            
            self.avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
            self.avPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(playerItemDidReachEnd:)
                                                         name:AVPlayerItemDidPlayToEndTimeNotification
                                                       object:[self.avPlayer currentItem]];
            
            self.avPlayerLayer.frame = CGRectMake(0, 0, SCREEN_SIZE.width, SCREEN_SIZE.height);
           // [self.view.layer insertSublayer:self.avPlayerLayer atIndex:0];
            [self.view.layer addSublayer:self.avPlayerLayer];
            [self.avPlayer play];
            
            
             [self.imageView startActivity];
             [self.avPlayer addObserver:self forKeyPath:@"status" options:0 context:nil];
            
             [self.imageView getImageWithURL:self.objPromotion.image comefrom:ImageViewComfromUser];
             self.view.backgroundColor=[UIColor redColor];
             self.imageView.backgroundColor=[UIColor clearColor];
             [self.view bringSubviewToFront:self.imageView];

        }
        else
        {
            self.lblMessage.hidden=YES;
            self.imageView.hidden=NO;
            [self.imageView getImageWithURL:self.objPromotion.image comefrom:ImageViewComfromUser];
        }
    }*/

}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object
                        change:(NSDictionary *)change context:(void *)context {
    if (object == self.avPlayer && [keyPath isEqualToString:@"status"]) {
        if (self.avPlayer.status == AVPlayerStatusReadyToPlay) {
            [self.imageView stopActivity];
            [self.avPlayer removeObserver:self forKeyPath:@"status"];
            //@added by steven
            _isLoadedDone = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:kSubbPromotionVCLoadingDone object:nil];
            
            if (_appearedTimes > 0) {
                [self setSeenTiming];
            }
        } else if (self.avPlayer.status == AVPlayerStatusFailed) {
            // something went wrong. player.error should contain some information
        }
    }
}
- (void)playerItemDidReachEnd:(NSNotification *)notification {
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
-(void)dealloc
{
    MPLog(@"SubbPromotionViewController --- Dealloc");

    [[NSNotificationCenter defaultCenter]removeObserver:self];
    [self stopTimer];
    
    @try{
        [self.avPlayer removeObserver:self forKeyPath:@"status"];
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
}
@end
