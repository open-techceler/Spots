#ifndef Maply_FontsAndColors_h
#define Maply_FontsAndColors_h

/* Font: Avenir-Medium
 Font: Avenir-HeavyOblique
 Font: Avenir-Book
 Font: Avenir-Light
 Font: Avenir-Roman
 Font: Avenir-BookOblique
 Font: Avenir-Black
 Font: Avenir-MediumOblique
 Font: Avenir-BlackOblique
 Font: Avenir-Heavy
 Font: Avenir-LightOblique
 Font: Avenir-Oblique*/


#define HelveticaNeueMedium        @"HelveticaNeue-Medium"
#define HelveticaNeueLight        @"HelveticaNeue-Light"
#define HelveticaNeueRegular       @"HelveticaNeue"



#define AvenirMedium        @"Avenir-Medium"
#define AvenirLight        @"Avenir-Light"
#define AvenirRoman        @"Avenir-Roman"
#define AvenirBlack       @"Avenir-Black"
#define AvenirBook      @"Avenir-Book"

#define AvenirNextLTProRegular        @"AvenirNextLTPro-Regular"
#define AvenirNextLTProDemi        @"AvenirNextLTPro-Demi"

#define FONT_AVENIR_MEDIUM_H08   [UIFont fontWithName:AvenirMedium size:8.0f]
#define FONT_AVENIR_MEDIUM_H00   [UIFont fontWithName:AvenirMedium size:9.0f]
#define FONT_AVENIR_MEDIUM_H0      [UIFont fontWithName:AvenirMedium size:10.0f]
#define FONT_AVENIR_MEDIUM_H1      [UIFont fontWithName:AvenirMedium size:11.0f]
#define FONT_AVENIR_MEDIUM_H2      [UIFont fontWithName:AvenirMedium size:12.0f]
#define FONT_AVENIR_MEDIUM_H3      [UIFont fontWithName:AvenirMedium size:13.0f]
#define FONT_AVENIR_MEDIUM_H4      [UIFont fontWithName:AvenirMedium size:14.0f]
#define FONT_AVENIR_MEDIUM_H5      [UIFont fontWithName:AvenirMedium size:15.0f]
#define FONT_AVENIR_MEDIUM_H6      [UIFont fontWithName:AvenirMedium size:16.0f]
#define FONT_AVENIR_MEDIUM_H7      [UIFont fontWithName:AvenirMedium size:17.0f]

#define FONT_AVENIR_LIGHT_H09      [UIFont fontWithName:AvenirLight size:9.0f]
#define FONT_AVENIR_LIGHT_H0      [UIFont fontWithName:AvenirLight size:10.0f]
#define FONT_AVENIR_LIGHT_H1      [UIFont fontWithName:AvenirLight size:11.0f]
#define FONT_AVENIR_LIGHT_H3      [UIFont fontWithName:AvenirLight size:13.0f]
#define FONT_AVENIR_LIGHT_H7      [UIFont fontWithName:AvenirLight size:17.0f]
#define FONT_AVENIR_LIGHT_H20      [UIFont fontWithName:AvenirLight size:20.0f]

#define FONT_AVENIR_ROMAN_H09      [UIFont fontWithName:AvenirRoman size:9.0f]
#define FONT_AVENIR_ROMAN_H0      [UIFont fontWithName:AvenirRoman size:10.0f]
#define FONT_AVENIR_ROMAN_H1      [UIFont fontWithName:AvenirRoman size:11.0f]
#define FONT_AVENIR_ROMAN_H2      [UIFont fontWithName:AvenirRoman size:12.0f]
#define FONT_AVENIR_ROMAN_H3      [UIFont fontWithName:AvenirRoman size:13.0f]
#define FONT_AVENIR_ROMAN_H4      [UIFont fontWithName:AvenirRoman size:14.0f]
#define FONT_AVENIR_ROMAN_H5      [UIFont fontWithName:AvenirRoman size:15.0f]

#define FONT_AVENIR_ROMAN_H9      [UIFont fontWithName:AvenirRoman size:19.0f]
#define FONT_AVENIR_ROMAN_H15  [UIFont fontWithName:AvenirRoman size:25.0f]


#define FONT_AVENIR_BLACK_H00      [UIFont fontWithName:AvenirBlack size:9.0f]
#define FONT_AVENIR_BLACK_H0      [UIFont fontWithName:AvenirBlack size:10.0f]
#define FONT_AVENIR_BLACK_H1      [UIFont fontWithName:AvenirBlack size:20.0f]

#define FONT_HELVETICA_MEDIUM_H1      [UIFont fontWithName:HelveticaNeueMedium size:11.0f]
#define FONT_HELVETICA_STRONG_H1       [UIFont fontWithName:HelveticaNeueMedium size:13.0f]

#define FONT_HELVETICA_LIGHT_H08      [UIFont fontWithName:HelveticaNeueLight size:8.0f]
#define FONT_HELVETICA_LIGHT_H1      [UIFont fontWithName:HelveticaNeueLight size:11.0f]
#define FONT_HELVETICA_LIGHT_H2      [UIFont fontWithName:HelveticaNeueLight size:12.0f]

#define FONT_HELVETICA_REGULAR_H3      [UIFont fontWithName:HelveticaNeueRegular size:13.0f]


// Font for fields Without Translaion
#define FONT_AVENIR_PRO_DEMI_H08   [UIFont fontWithName:AvenirNextLTProDemi size:8.0f]
#define FONT_AVENIR_PRO_DEMI_H00   [UIFont fontWithName:AvenirNextLTProDemi size:9.0f]
#define FONT_AVENIR_PRO_DEMI_H0      [UIFont fontWithName:AvenirNextLTProDemi size:10.0f]
#define FONT_AVENIR_PRO_DEMI_H1      [UIFont fontWithName:AvenirNextLTProDemi size:11.0f]
#define FONT_AVENIR_PRO_DEMI_H2      [UIFont fontWithName:AvenirNextLTProDemi size:12.0f]
#define FONT_AVENIR_PRO_DEMI_H3      [UIFont fontWithName:AvenirNextLTProDemi size:13.0f]
#define FONT_AVENIR_PRO_DEMI_H4      [UIFont fontWithName:AvenirNextLTProDemi size:14.0f]
#define FONT_AVENIR_PRO_DEMI_H5      [UIFont fontWithName:AvenirNextLTProDemi size:15.0f]
#define FONT_AVENIR_PRO_DEMI_H6      [UIFont fontWithName:AvenirNextLTProDemi size:14.0f]
#define FONT_AVENIR_PRO_DEMI_H7      [UIFont fontWithName:AvenirNextLTProDemi size:17.0f]
#define FONT_AVENIR_PRO_DEMI_H8      [UIFont fontWithName:AvenirNextLTProDemi size:18.0f]
#define FONT_AVENIR_PRO_DEMI_H10     [UIFont fontWithName:AvenirNextLTProDemi size:20.0f]
#define FONT_AVENIR_PRO_DEMI_H12     [UIFont fontWithName:AvenirNextLTProDemi size:22.0f]
#define FONT_AVENIR_PRO_DEMI_H14     [UIFont fontWithName:AvenirNextLTProDemi size:24.0f]

// Font for fields Without Translaion
#define FONT_AVENIR_PRO_REGULAR_H07   [UIFont fontWithName:AvenirNextLTProRegular size:7.0f]
#define FONT_AVENIR_PRO_REGULAR_H08   [UIFont fontWithName:AvenirNextLTProRegular size:8.0f]
#define FONT_AVENIR_PRO_REGULAR_H00   [UIFont fontWithName:AvenirNextLTProRegular size:9.0f]
#define FONT_AVENIR_PRO_REGULAR_H0      [UIFont fontWithName:AvenirNextLTProRegular size:10.0f]
#define FONT_AVENIR_PRO_REGULAR_H1      [UIFont fontWithName:AvenirNextLTProRegular size:11.0f]
#define FONT_AVENIR_PRO_REGULAR_H2      [UIFont fontWithName:AvenirNextLTProRegular size:12.0f]
#define FONT_AVENIR_PRO_REGULAR_H3      [UIFont fontWithName:AvenirNextLTProRegular size:13.0f]
#define FONT_AVENIR_PRO_REGULAR_H4      [UIFont fontWithName:AvenirNextLTProRegular size:14.0f]
#define FONT_AVENIR_PRO_REGULAR_H5      [UIFont fontWithName:AvenirNextLTProRegular size:15.0f]
#define FONT_AVENIR_PRO_REGULAR_H6      [UIFont fontWithName:AvenirNextLTProRegular size:16.0f]
#define FONT_AVENIR_PRO_REGULAR_H7      [UIFont fontWithName:AvenirNextLTProRegular size:17.0f]
#define FONT_AVENIR_PRO_REGULAR_H8      [UIFont fontWithName:AvenirNextLTProRegular size:18.0f]
#define FONT_AVENIR_PRO_REGULAR_H10     [UIFont fontWithName:AvenirNextLTProRegular size:20.0f]
#define FONT_AVENIR_PRO_REGULAR_H12     [UIFont fontWithName:AvenirNextLTProRegular size:22.0f]
#define FONT_AVENIR_PRO_REGULAR_H14     [UIFont fontWithName:AvenirNextLTProRegular size:24.0f]

#define FONT_AVENIR_BOOK_H12     [UIFont fontWithName:AvenirBook size:12.0f]
#define FONT_AVENIR_BOOK_H15     [UIFont fontWithName:AvenirBook size:15.0f]
#define FONT_AVENIR_BOOK_H17     [UIFont fontWithName:AvenirBook size:17.0f]
#define FONT_AVENIR_BOOK_H32     [UIFont fontWithName:AvenirBook size:32.0f]


#define FONT_NAV_TITLE FONT_AVENIR_MEDIUM_H7
#define FONT_DONE_BUTTON [UIFont fontWithName:AvenirBook size:14.0f]

#define FONT_SETTING_TITLE  FONT_AVENIR_MEDIUM_H4 //FONT_AVENIR_PRO_DEMI_H3
#define FONT_NOTIFICATION_TITLE  FONT_AVENIR_MEDIUM_H4 //FONT_AVENIR_PRO_DEMI_H3
#define FONT_MESSAGE_TITLE  FONT_AVENIR_MEDIUM_H4 //   FONT_AVENIR_MEDIUM_H1;

#define FONT_HEADER_TITLE FONT_HELVETICA_LIGHT_H2

#define FONT_SEGMENT_TITLE FONT_HELVETICA_STRONG_H1//FONT_AVENIR_PRO_DEMI_H1

#define FONT_SEGEMENT_BADGE FONT_HELVETICA_LIGHT_H08

//base css for app
#define UICOLOR_GLOBAL_BG [UIColor whiteColor]
#define UICOLOR_WHITE [UIColor whiteColor]
#define UICOLOR_BLACK [UIColor blackColor]
#define UICOLOR_NAVIGATION_BG [UIColor colorWithHexValue:0x50b952]
#define UICOLOR_NAVIGATION_TITLE [UIColor colorWithHexValue:0x00000]
#define UICOLOR_NAVIGATION_SUBTITLE [UIColor colorWithHexValue:0x666666]


#define BLACK_BUTTON_FONT FONT_Next_LTPro_Medium_H12
#define BREAKUP_TABLE_FONT FONT_Next_LTPro_H5
#define MAIN_BUTTON_FONT FONT_Next_LTPro_Medium_H12
#define TXTFLD_FONT FONT_Next_LTPro_H8

#define NAV_TITLE_FONT FONT_Next_LTPro_H12
#define LEFT_MENU_TITLE_FONT FONT_Next_LTPro_Medium_H8
#define LBL_SMALLTXT_FONT FONT_Next_LTPro_H2




//Book button
#define UICOLOR_BOOK_BUTTON [UIColor colorWithHexValue:0xfe6905]



#define UICOLOR_SEPERATOR [UIColor colorWithRed:242/255.0f green:242/255.0f blue:242/255.0f alpha:1.0f]
#define UICOLOR_NAV_TITLE [UIColor colorWithRed:254/255.0f green:77/255.0f blue:93/255.0f alpha:1.0f]
#define UICOLOR_APP_BG_GREY [UIColor colorWithRed:247/255.0f green:247/255.0f blue:247/255.0f alpha:1.0f]

#define UICOLOR_APP_BG [UIColor whiteColor]

#define PICKER_COLOR_PINK [UIColor colorWithRed:252/255.0f green:78/255.0f blue:97/255.0f alpha:1.0f]
#define PICKER_COLOR_BLACK [UIColor colorWithRed:3/255.0f green:3/255.0f blue:3/255.0f alpha:1.0f]
#define PICKER_COLOR_WHITE [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0f]
#define PICKER_COLOR_BLUE [UIColor colorWithRed:29/255.0f green:163/255.0f blue:252/255.0f alpha:1.0f]
#define PICKER_COLOR_GREEN [UIColor colorWithRed:132/255.0f green:189/255.0f blue:96/255.0f alpha:1.0f]
#define PICKER_COLOR_YELLOW [UIColor colorWithRed:253/255.0f green:187/255.0f blue:72/255.0f alpha:1.0f]

#define UICOLOR_DARK_TEXT [UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]
#define UICOLOR_LIGHT_TEXT [UIColor colorWithRed:102/255.0f green:102/255.0f blue:102/255.0f alpha:1.0f]
#define UICOLOR_SELECTED_CELL  [UIColor colorWithRed:250/255.0f green:250/255.0f blue:250/255.0f alpha:1.0f]

#define UICOLOR_NOTIFICATION_TITLE [UIColor colorWithRed:86/255.0f green:86/255.0f blue:86/255.0f alpha:1.0f]
#define UICOLOR_LOCATION_TITLE [UIColor colorWithRed:54/255.0f green:54/255.0f blue:54/255.0f alpha:1.0f]

#define UICOLOR_PROGRESS_BAR [UIColor colorWithRed:212/255.0f green:40/255.0f blue:40/255.0f alpha:1.0f]


#define FONT_IMAGE_TEXT FONT_AVENIR_PRO_DEMI_H2
#endif
