//
//  CameraMainViewController.h
//  oscar
//
//  Created by Rishi on 24/12/15.
//  Copyright © 2015 Rishi. All rights reserved.
//

@protocol  CameraDelegate <NSObject>
-(void)didSelectImage:(UIImage *)img;
@end

@interface CameraMainViewController : MyNavigationViewController

@property (nonatomic) CameraMode camera_mode;
@property (nonatomic) CameraMainComeFrom comefrom;
@property (nonatomic) NSInteger reply_userId;
@property (nonatomic, strong) NSString* reply_userImgUrl;
@property(nonatomic,weak) id<CameraDelegate> camaraDelegate;


@end
