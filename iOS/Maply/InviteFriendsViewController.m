//
//  InviteFriendsViewController.m
//  Maply
//
//  Created by Admin on 01/10/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "InviteFriendsViewController.h"
#import "AppDelegate.h"
#import <MessageUI/MessageUI.h>
#import "FacebookManager.h"

#define SHARE_IMAGE_URL     @"https://www.mydomain.com/my_invite_image.jpg"
#define SHARE_APP_LINK      @"https://itunes.apple.com/dk/app/spots/id1162120056"
#define SHARE_TITLE         @"Download Spots!"
#define SHARE_DESCRIPTION   @"Spots er din nye genvej til at finde ud af, hvad dine venner laver! Download pÂ LINK ìî. https://itunes.apple.com/dk/app/spots/id1162120056"

@interface InviteFriendsViewController () <MFMessageComposeViewControllerDelegate, FBSDKAppInviteDialogDelegate>
{
    FBSDKSendButton *fbSendbutton;
}

@end

@implementation InviteFriendsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnDone.hidden = self.bHideDoneButton;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self addFacebookSendButton];
}

- (void) addFacebookSendButton {
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:SHARE_APP_LINK];
    content.contentTitle = SHARE_TITLE;
    content.contentDescription = SHARE_DESCRIPTION;
    content.imageURL = [NSURL URLWithString:SHARE_IMAGE_URL];
    
    fbSendbutton = [[FBSDKSendButton alloc] init];
    fbSendbutton.shareContent = content;
    
    if (fbSendbutton.isHidden) {
        NSLog(@"Is hidden");
    } else {
        [self.view insertSubview:fbSendbutton belowSubview:self.btnFBSendButton];
        
        fbSendbutton.frame = CGRectMake(0, 0, 100, 20);
        fbSendbutton.center = self.btnFBSendButton.center;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actionInviteSMS:(id)sender {
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    if([MFMessageComposeViewController canSendText])
    {
        controller.subject = SHARE_TITLE;
        controller.body = SHARE_DESCRIPTION;
        controller.messageComposeDelegate = self;
        
        /*
        NSData* attachment = UIImageJPEGRepresentation([UIImage imageNamed:@"promotion.jpg"], 0.8f);
        [controller addAttachmentData:attachment typeIdentifier:@"public.data" filename:@"image.jpeg"];
         */
        [self presentViewController:controller animated:YES completion:nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultCancelled:
            NSLog(@"Cancelled");
            break;
        case MessageComposeResultFailed:
            NSLog(@"Failed to send message");
            break;
        case MessageComposeResultSent:
            NSLog(@"Sent message successfully!");
            break;
        default:
            break;
    }

    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionInviteFacebook:(id)sender {
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:SHARE_APP_LINK];
    //optionally set previewImageURL
    content.appInvitePreviewImageURL = [NSURL URLWithString:SHARE_IMAGE_URL];

    // Present the dialog. Assumes self is a view controller
    // which implements the protocol `FBSDKAppInviteDialogDelegate`.
    [FBSDKAppInviteDialog showFromViewController:self
                                     withContent:content
                                        delegate:self];
}

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results {
    
}

- (void)appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error {
    
}

- (IBAction)actionInviteMessagener:(id)sender {
    [fbSendbutton sendActionsForControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)actionBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)actionDone:(id)sender {
    [UIAppDelegate initMainAppFlow];
}

@end
