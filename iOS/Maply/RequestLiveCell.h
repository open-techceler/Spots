//
//  RequestLiveCell.h
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@interface RequestLiveCell : UITableViewCell

@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgUser;
@property(nonatomic,weak)IBOutlet UILabel *lblName,*lblWaiting;
@property(nonatomic,weak)IBOutlet UIButton *btnEdit,*btnBlock,*btnDelete,*btnRequestLive;
@property(nonatomic,weak)IBOutlet UIView *vwBlock ;

-(void)setUpDisableLiveButton;
-(void)setUpRequstLiveButton;

@end
