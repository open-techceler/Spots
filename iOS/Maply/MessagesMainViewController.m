//
//  MessagesMainViewController.m
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MessagesMainViewController.h"
#import "SettingsViewController.h"
#import "MessagesViewController.h"
#import "CarbonKit.h"
#import "LGSideMenuController.h"
#import "MFSideMenu.h"

@interface MessagesMainViewController ()
<CarbonTabSwipeNavigationDelegate,MessagesViewDelegate>
{
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}

@property (nonatomic,weak)IBOutlet UIView *vwScope;
@property (nonatomic,weak)IBOutlet UIButton *btnDirect,*btnSent;
@property (nonatomic,strong) MessagesViewController *directViewController,*sentViewController;
@property (nonatomic) BOOL isViewingMoments;


@end

@implementation MessagesMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setFontAndColor];
    [self setUpNavigationBar];
    
    //https://trello.com/c/9v3lQNMq
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetTabToDirect:) name:MFSideMenuStateNotificationEvent object:nil];
    
   /* dispatch_async(dispatch_get_main_queue(), ^{
        self.directViewController=[[MessagesViewController alloc]initWithNibName:@"MessagesViewController" bundle:nil];
        self.directViewController.view.frame=CGRectMake(0, 30, SCREEN_SIZE.width, SCREEN_SIZE.height-30);
        
        self.sentViewController=[[MessagesViewController alloc]initWithNibName:@"MessagesViewController" bundle:nil];
        self.sentViewController.view.frame=CGRectMake(0, 32, SCREEN_SIZE.width, SCREEN_SIZE.height-32);
        
        [self.view addSubview:self.directViewController.view];
        [self.view addSubview:self.sentViewController.view];
        
        self.directViewController.view.hidden=YES;
        self.sentViewController.view.hidden=YES;
        [self clickTabButton:self.btnDirect];
        
    });*/
    
    self.directViewController=[[MessagesViewController alloc]initWithNibName:@"MessagesViewController" bundle:nil];
    self.directViewController.comefrom=MessagesComeFromDirect;
    self.directViewController.delegate=self;
    self.directViewController.view.frame=CGRectMake(0, 30, SCREEN_SIZE.width, SCREEN_SIZE.height-30);
    
    self.sentViewController=[[MessagesViewController alloc]initWithNibName:@"MessagesViewController" bundle:nil];
    self.sentViewController.comefrom=MessagesComeFromSent;
    self.sentViewController.delegate=self;
    self.sentViewController.view.frame=CGRectMake(0, 32, SCREEN_SIZE.width, SCREEN_SIZE.height-32);
    
    items = @[MPLocalizedString(@"direct", nil),
              MPLocalizedString(@"sent", nil)];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    [self style];
}

-(void)resetTabToDirect:(NSNotification *)noti
{
    NSDictionary *dict=noti.userInfo;
    MFSideMenuStateEvent eventtype = [[dict objectForKey:@"eventType"] intValue];
    if(eventtype==MFSideMenuStateEventMenuDidClose)
    {
        if(carbonTabSwipeNavigation.currentTabIndex!=0)
            carbonTabSwipeNavigation.currentTabIndex=0;
    }

}

- (void)style {
    
    UIColor *color = [UIColor  blackColor];
    [carbonTabSwipeNavigation setTabBarHeight:kScopeBarHeight];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.clipsToBounds = YES;
    [carbonTabSwipeNavigation setIndicatorColor:[UIColor clearColor]];
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setIndicatorHeight:3];
    [carbonTabSwipeNavigation.carbonSegmentedControl.indicator setImage:[UIImage imageNamed:@"line-1"]];
    carbonTabSwipeNavigation.carbonSegmentedControl.indicator.contentMode=UIViewContentModeScaleToFill;
    
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:SCREEN_SIZE.width/2 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:SCREEN_SIZE.width/2 forSegmentAtIndex:1];
    
    UIView *vwSep=[[UIView alloc]initWithFrame:CGRectMake(SCREEN_SIZE.width/2,(kScopeBarHeight-18)/2, 1, 18)];
    vwSep.backgroundColor=UICOLOR_SEPERATOR;
    [carbonTabSwipeNavigation.carbonSegmentedControl addSubview:vwSep];
    
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:color
                                        font:FONT_SEGMENT_TITLE];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:FONT_SEGMENT_TITLE];
    
    carbonTabSwipeNavigation.carbonSegmentedControl.layer.borderWidth=1;
    carbonTabSwipeNavigation.carbonSegmentedControl.layer.borderColor=UICOLOR_SEPERATOR.CGColor;
    carbonTabSwipeNavigation.carbonTabSwipeScrollView.scrollEnabled=NO;
    for (UIScrollView *view in carbonTabSwipeNavigation.pageViewController.view.subviews) {
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            view.bounces = NO;
            view.scrollEnabled = NO;
        }
    }
}

-(void)setBadgeForTab:(NSInteger)tabIndex value:(NSInteger)value
{
    UIView *vwSelected=[carbonTabSwipeNavigation.carbonSegmentedControl.segments objectAtIndex:tabIndex];
    
    UIButton *lblBadge=[vwSelected viewWithTag:1000];
    if(lblBadge==nil)
    {
        lblBadge=[UIButton buttonWithType:UIButtonTypeCustom];
        lblBadge.frame=CGRectMake([self getXOfButtonTitle:tabIndex]-20, (kScopeBarHeight/2)-7, 14, 14);
        lblBadge.tag=1000;
        lblBadge.backgroundColor=PICKER_COLOR_PINK;
       // [lblBadge setTitleEdgeInsets:UIEdgeInsetsMake(1, 1, 0, 0)];
        lblBadge.titleLabel.textColor=[UIColor whiteColor];
        lblBadge.titleLabel.font=FONT_SEGEMENT_BADGE;
        lblBadge.layer.cornerRadius=4.0;
        lblBadge.clipsToBounds = YES;
        [vwSelected addSubview:lblBadge];
        
    }
    else
        lblBadge.frame=CGRectMake([self getXOfButtonTitle:tabIndex]-20,(kScopeBarHeight/2)-7, 14, 14);
    
    
    if(value)
    {
        lblBadge.hidden=NO;
        [lblBadge setTitle:[NSString stringWithFormat:@"%ld",(long)value] forState:UIControlStateNormal];
    }
    else
        lblBadge.hidden=YES;
    
}

-(NSInteger)getXOfButtonTitle:(NSInteger)tabIndex
{
    NSString *tabTitle=(tabIndex==0?MPLocalizedString(@"direct", nil) :MPLocalizedString(@"sent", nil));

    NSInteger total_width=SCREEN_SIZE.width/2;
    CGSize boundingSize = CGSizeMake(total_width, CGFLOAT_MAX);
    CGSize requiredSize = [tabTitle VVSizeWithFont:FONT_AVENIR_PRO_DEMI_H1 constrainedToSize:boundingSize lineBreakMode:NSLineBreakByWordWrapping];
    
    CGFloat label_x=(total_width/2)-(requiredSize.width/2);
    return label_x;
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;
    //[kLGSideMenuController setLeftViewSwipeGestureEnabled:NO];
}
-(void)viewDidDisappear:(BOOL)animated
{
    if(carbonTabSwipeNavigation.currentTabIndex!=0 && !_isViewingMoments)
        carbonTabSwipeNavigation.currentTabIndex=0;
    else
        _isViewingMoments=NO;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   /* self.navigationItem.hidesBackButton=YES;
    [[self navigationController] setNavigationBarHidden:NO animated:NO];*/
  
    UIAppDelegate.mainController.swipeEnabled=YES;
    self.menuContainerViewController.panMode =  MFSideMenuPanModeDefault;
    [self refreshTableData:0];
    //[kLGSideMenuController setLeftViewSwipeGestureEnabled:YES];

}

-(IBAction)clickTabButton:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if(btn.tag==1)
    {
        self.btnDirect.selected=YES;
        self.btnSent.selected=NO;
        
        self.directViewController.view.hidden=NO;
        self.sentViewController.view.hidden=YES;
    }
    else{
        self.btnDirect.selected=NO;
        self.btnSent.selected=YES;
        
        self.directViewController.view.hidden=YES;
        self.sentViewController.view.hidden=NO;
    }
}

-(void)refreshTableData:(NSUInteger)index {
    if(index == 0) {
        [self.directViewController refreshTable];
    } else {
        [self.sentViewController refreshTable];
    }
}

-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"moments", nil);
    
    [self.Nav setLeftButtonWithImageName:@"setings" withTitle:nil font:nil];
    [self.Nav.btnLeft addTarget:self action:@selector(clickSetting:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.Nav setRightButtonWithImageName:@"map" withTitle:nil font:nil];
    [self.Nav.btnRight addTarget:self action:@selector(clickAdd:)forControlEvents:UIControlEventTouchUpInside];
    [self.Nav setNavigationLayout:YES];
    
}

-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    
    self.btnDirect.titleLabel.font=FONT_AVENIR_PRO_DEMI_H1;
    self.btnSent.titleLabel.font=FONT_AVENIR_PRO_DEMI_H1;
    
    [self.btnDirect setTitle:MPLocalizedString(@"direct", nil) forState:UIControlStateNormal];
    [self.btnSent setTitle:MPLocalizedString(@"sent", nil) forState:UIControlStateNormal];

    
}

-(IBAction)clickSetting:(id)sender
{
    dispatch_async(dispatch_get_main_queue(), ^(void){
        UIAppDelegate.mainController.swipeEnabled=NO;

        SettingsViewController *cnt=[[SettingsViewController alloc]initWithNibName:@"SettingsViewController" bundle:nil];
        [self slideLayerInDirection:kCATransitionFromTop andPush:cnt];
    });
    
}
-(void) slideLayerInDirection:(NSString *)direction andPush:(UIViewController *)dstVC {
    
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromBottom;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] pushViewController:dstVC animated:NO];
  
}
-(IBAction)clickAdd:(id)sender
{
    UIAppDelegate.mainController.currentIndex=kHomeViewViewController;

   // [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];

   // [kLGSideMenuController hideLeftViewAnimated:YES completionHandler:nil];

}
# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
              return self.directViewController;
            
        case 1:
            return self.sentViewController;

            
        default:
            return [[MessagesViewController alloc]initWithNibName:@"MessagesViewController" bundle:nil];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", (unsigned long)index);
    [self refreshTableData:index];
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}


-(void)pushController:(UIViewController *)cnt
{
    
}
-(void)presentViewController:(UIViewController *)cnt
{
    _isViewingMoments=YES;
    //UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:cnt];
    [UIAppDelegate.window.rootViewController presentViewController:cnt animated:NO completion:nil];

}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
