//
//  HomeViewViewController.h
//  Maply
//
//  Created by admin on 2/9/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LGSideMenuController.h"
#import "MyNavigationViewController.h"
#import "LocationTrackEngine.h"

@class CustomMapView;

@interface HomeViewViewController : MyNavigationViewController<LocationTrackEngineDelegate>


-(void)handlePushNotification:(NSDictionary *)userInfo;
@end
