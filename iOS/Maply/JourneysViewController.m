//
//  JourneysViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "JourneysViewController.h"
#import "JourneyCell.h"
#import "MessageCell.h"
#import "FriendsMainViewController.h"
#import "LGSideMenuController.h"
#import "UserAPI.h"
#import "JourneyListWrapper.h"
#import "Journey.h"
#import "User.h"
#import "MFSideMenu.h"
#import "JourneyDetailsViewController.h"
#import "NotificationDefinitions.h"
#import "FormatterCollection.h"
#import "SLExpandableTableView.h"
#import "Moment.h"
#import "MyJourneySubCell.h"
#import "FacebookManager.h"
#import "Message.h"
#import "EGORefreshTableHeaderView.h"


@interface JourneysViewController ()<UIViewControllerTransitioningDelegate,JourneyDetailsDelegate,EGORefreshTableHeaderDelegate>

@property(nonatomic,strong)IBOutlet UIView *vwAllJourney;
@property(nonatomic,strong)IBOutlet UILabel *lblAllJourney;
@property(nonatomic)NSInteger next_offset;

@property(nonatomic)BOOL server_request_inprogress;
@property (nonatomic,strong)UIActivityIndicatorView *activityLoadMore;
@property(nonatomic,weak)IBOutlet UILabel *lblNoRecordFound;
@property(nonatomic,strong) NSMutableArray *arrayList;
@property(nonatomic,strong) NSArray *arrNewJourneys,*arrAllJourneys;
@property(nonatomic,strong)NSMutableArray *arrMyJourney;

@property(nonatomic,strong)JourneyCell *loadingCell;
@property(nonatomic,strong)JourneyCell *myJourneyCell;

@property(nonatomic,strong)IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableIndexSet *expandedSections;

@property(nonatomic,strong) EGORefreshTableHeaderView *refreshHeaderView;
@property BOOL reloading;

@end

@implementation JourneysViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (!_expandedSections)
    {
        _expandedSections = [[NSMutableIndexSet alloc] init];
    }
    
    [self initLoaderActivity];
    [self initEmptyDataLabel];
    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];

    self.arrayList=[NSMutableArray new];
    
    [self setUpNavigationBar];
    [self setFontAndColor];
    
    double delayInSeconds = 0.2f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self loadJourney];
    });
  
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshJourney) name:kReloadJourneyListNotification object:nil];
    
    
    if (_refreshHeaderView == nil) {
        
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - 60, SCREEN_SIZE.width, 60)];
        view.delegate = self;
        [self.tableView addSubview:view];
        _refreshHeaderView = view;
    }
    
}

- (void)refreshTable {
    //TODO: refresh your data
    //[_refreshControl endRefreshing];
     self.next_offset=0;
     [self getJourneyFromServer];
}

-(void)loadJourney
{
    self.next_offset=0;
    self.tableView.hidden=YES;
    [self startLoadingActivity];
//    [self getJourneyFromServer];
    
}


-(void)refreshJourney
{
    self.next_offset=0;
    if(self.arrayList.count==0)
    {
        self.tableView.hidden=YES;
        [self startLoadingActivity];
    }
    [self getJourneyFromServer];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    self.menuContainerViewController.panMode = MFSideMenuPanModeNone;

   // [kLGSideMenuController setRightViewSwipeGestureEnabled:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
 
    UIAppDelegate.mainController.swipeEnabled=YES;
    self.menuContainerViewController.panMode =  MFSideMenuPanModeDefault;

    //[kLGSideMenuController setRightViewSwipeGestureEnabled:YES];
    
}



-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self refreshTable];
}


-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG;
    
    self.lblAllJourney.text=MPLocalizedString(@"all_journeys", nil);
    self.lblAllJourney.font=FONT_AVENIR_PRO_REGULAR_H4;
    
}

-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"journeys", nil);
    
    [self.Nav setLeftButtonWithImageName:@"map" withTitle:nil font:nil];
    [self.Nav.btnLeft addTarget:self action:@selector(clickLocation:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.Nav setRightButtonWithImageName:@"group" withTitle:nil font:nil];
    [self.Nav.btnRight addTarget:self action:@selector(clickFriends:)forControlEvents:UIControlEventTouchUpInside];
    
    [self.Nav setNavigationLayout:YES];
    
}

#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    _reloading = YES;
    [self refreshTable];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view{
    return _reloading; // should return if data source model is reloading
    
}

- (void)endRefreshing{
    //  model should call this when its done loading
    _reloading = NO;
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    
}

-(void)getJourneyFromServer
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

    self.server_request_inprogress=YES;

    [[UserAPI new]getJourneys:self.next_offset success:^(JourneyListWrapper *objWrapper) {
        if(self.next_offset==0)
        {
            [self.arrayList removeAllObjects];
            [self.expandedSections removeAllIndexes];

        }
        
        self.next_offset=objWrapper.next_offset;
        
        self.arrMyJourney = (NSMutableArray *)objWrapper.my_journey;
        
        [self.arrayList addObjectsFromArray:objWrapper.list];
        [self filterArray];
        
        self.server_request_inprogress=NO;
        [self stopLoadingActivity];
        
        [self.tableView reloadData];
        [self endRefreshing];
        
        if(self.arrayList.count==0 && self.arrMyJourney.count==0)
        {
            self.tableView.hidden=NO;
            //[self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_journey_found", nil)];
            [self showEmptyDataImageViewWithImage:[UIImage imageNamed:@"icon_no_journey"]];

        }
        else
            self.tableView.hidden=NO;

        
    } failure:^(NSError *error) {
        
        self.server_request_inprogress=NO;
        self.tableView.hidden=YES;
        [self stopLoadingActivity];
        [self showOfflineView:YES error:error];
        [self endRefreshing];
    }];
    });
}

-(void)filterArray
{
   self.arrNewJourneys= [self.arrayList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"is_new =%ld",true]];
   self.arrAllJourneys= [self.arrayList filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"is_new !=%ld",true]];

}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        self.next_offset=0;
        self.tableView.hidden=YES;
        [self startLoadingActivity];
        [self getJourneyFromServer];
    }
}



-(IBAction)clickLocation:(id)sender
{
    UIAppDelegate.mainController.currentIndex=kHomeViewViewController;
 //   [self.menuContainerViewController setMenuState:MFSideMenuStateClosed completion:^{}];
    //[kLGSideMenuController hideRightViewAnimated:YES completionHandler:nil];

}

-(IBAction)clickFriends:(id)sender
{
    //UIAppDelegate.mainController.swipeEnabled=NO;
    UIAppDelegate.mainController.currentIndex=kFriendsMainViewController;

   /* FriendsMainViewController *cnt=[[FriendsMainViewController alloc]initWithNibName:@"FriendsMainViewController" bundle:nil];
    [self.navigationController pushViewController:cnt animated:YES];*/
}

#pragma mark - SLExpandableTableViewDatasource
- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if(section==[self totalSections]-2)
        return NO;
    else if(section==[self totalSections]-1)
        return NO;
    
    return YES;
}


//- (UITableViewCell<UIExpandingTableViewCell> *)tableView:(SLExpandableTableView *)tableView expandingCellForSection:(NSInteger)section
//{
//    static NSString* MyIdentifier = @"JourneyCell";
//    JourneyCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
//    
//    if (cell == nil) {
//        cell = [[[NSBundle mainBundle] loadNibNamed:@"JourneyCell" owner:self options:nil] objectAtIndex:0];
//    }
//    
//    
//    return cell;
//}



#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self totalSections];
}

-(NSInteger)totalSections
{
    return  (self.arrMyJourney.count?1:0) + 2;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    if(section==[self totalSections]-1 &&  self.arrAllJourneys.count)
//        return 52;
//    
//    return JOURNEY_BANNER_HEIGHT;
//}
//
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    
//   /* UIView *vw =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 30)];
//    vw.backgroundColor=[UIColor whiteColor];
//    
//    UILabel *lbl =[[UILabel alloc] initWithFrame:CGRectMake(16, 10, SCREEN_SIZE.width-16, 20)];
//    lbl.textColor=[UIColor lightGrayColor];
//    lbl.font=FONT_AVENIR_PRO_DEMI_H2;
//    lbl.textAlignment=NSTextAlignmentLeft;
//    lbl.text=[NSString stringWithFormat:MPLocalizedString(@"friends", nil),8 ];
//    [vw addSubview:lbl];*/
//    
//    return _vwAllJourney;
//}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==[self totalSections]-2)
        return self.arrNewJourneys.count;
    else if(section==[self totalSections]-1)
        return self.arrAllJourneys.count;
    else if([self tableView:tableView canCollapseSection:section])
    {
        if ([_expandedSections containsIndex:section])
        {
            return self.arrMyJourney.count+1; // return rows when expanded
        }
        
        return 1; // only top row showing
    }
    
    return (self.arrMyJourney.count?1:0);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            return JOURNEY_BANNER_HEIGHT;
        }
        else if([_expandedSections containsIndex:indexPath.section])
        {
            return 50;
        }
    }
    
    if(indexPath.section==[self totalSections]-2)
        return JOURNEY_BANNER_HEIGHT;
    
    return 60 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==[self totalSections]-1)
    {
        static NSString* MyIdentifier = @"MessageCell";
        MessageCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"MessageCell" owner:self options:nil] objectAtIndex:0];
        }
        
        Journey *obj=[self.arrAllJourneys objectAtIndex:indexPath.row];
        cell.lblName.text=obj.user.name.capitalizedString;
        
        NSString *timeago=[FormatterCollection utc_timeAgoWithUTCDate:obj.created_date];
        if(![Utility isEmpty:obj.user.location])
            cell.lblAddress.text=[NSString stringWithFormat:@"%@  %@",obj.user.location,timeago];
        else if(![Utility isEmpty:obj.created_date])
            cell.lblAddress.text=timeago;
        
        [cell.imgPhoto getImageWithURL:obj.image comefrom:ImageViewComfromUser];
        [cell.imgUser getImageWithURL:obj.user.image comefrom:ImageViewComfromUser];
        
        cell.btnCount.hidden=YES;
        return cell;
        
    }
    else if(indexPath.section==[self totalSections]-2)
    {
        static NSString* MyIdentifier = @"JourneyCell";
        JourneyCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"JourneyCell" owner:self options:nil] objectAtIndex:0];
        }
        cell.btnMyJourney.hidden= YES;
        cell.imgArrow.hidden= YES;

        Journey *obj=[self.arrNewJourneys objectAtIndex:indexPath.row];
        
        cell.lblTitle.text=obj.name;
        cell.lblName.text=obj.user.name.capitalizedString;
        //∙• •
        NSString *timeago=[FormatterCollection utc_timeAgoWithUTCDate:obj.created_date];
        if(![Utility isEmpty:obj.created_date])
            cell.lblMoments.text=[NSString stringWithFormat:@"%ld moments ∙ %@",(long)obj.moments,timeago];
        else
            cell.lblMoments.text=[NSString stringWithFormat:@"%ld moments",(long)obj.moments];
        
        [cell.imgBanner getImageWithURL:obj.image comefrom:ImageViewComfromUser];
        [cell.imgUser getImageWithURL:obj.user.image comefrom:ImageViewComfromUser];
        return cell;
        
    }
    else if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            static NSString* MyIdentifier = @"JourneyCell";
            JourneyCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
            
            if (cell == nil) {
                cell = [[[NSBundle mainBundle] loadNibNamed:@"JourneyCell" owner:self options:nil] objectAtIndex:0];
            }
            cell.btnMyJourney.hidden= NO;
            cell.imgArrow.hidden= NO;
            [cell.btnMyJourney addTarget:self action:@selector(clickMyJourney:) forControlEvents:UIControlEventTouchUpInside];
            
            Message *obj=[self.arrMyJourney objectAtIndex:indexPath.row];
            
            cell.lblTitle.text=obj.location_name;
            cell.lblName.text=MPLocalizedString(@"my_journey", nil);
            //∙• •
            NSString *timeago=[FormatterCollection utc_timeAgoWithUTCDate:obj.created_date];
            if(![Utility isEmpty:obj.created_date])
                cell.lblMoments.text=[NSString stringWithFormat:@"%ld moments ∙ %@",(long)self.arrMyJourney.count,timeago];
            else
                cell.lblMoments.text=[NSString stringWithFormat:@"%ld moments",(long)self.arrMyJourney.count];
            
            [cell.imgBanner getImageWithURL:obj.banner_image comefrom:ImageViewComfromUser];

            User *objUser=[User currentUser];
            [cell.imgUser getImageWithURL:objUser.image comefrom:ImageViewComfromUser];
            
            //@added by steven
            _myJourneyCell = cell;
            
            return cell;
        }
        else
        {
            static NSString* MyIdentifier = @"MyJourneySubCell";
            MyJourneySubCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
            
            if (cell == nil) {
                cell = [[[NSBundle mainBundle] loadNibNamed:@"MyJourneySubCell" owner:self options:nil] objectAtIndex:0];
            }
       
            Message *obj=[self.arrMyJourney objectAtIndex:indexPath.row-1];
           
            cell.lblName.text=obj.text;
//TODO for view count functionality
//            cell.lblCount.text = [NSString stringWithFormat:@"%ld",(long)obj.view_count];
            [cell.imgPhoto getImageWithURL:obj.banner_image comefrom:ImageViewComfromUser];
            
            NSString *timeago=[FormatterCollection utc_timeAgoWithUTCDate:obj.created_date];
            if(![Utility isEmpty:obj.location_name])
                cell.lblAddress.text=[NSString stringWithFormat:@"%@  %@",obj.location_name,timeago];
            else if(![Utility isEmpty:obj.created_date])
                cell.lblAddress.text=timeago;
            
            return cell;
          
        }
    
    }
    return nil;
    
}

-(void)clickMyJourney:(id)sender
{
    [self showPreviewLoader:_myJourneyCell];
    
    JourneyDetailsViewController *cnt=[[JourneyDetailsViewController alloc]initWithNibName:@"JourneyDetailsViewController" bundle:nil];
    cnt.comefrom=JourneyDetailsComeFromJourney;
    cnt.arrMyJourney= [NSMutableArray arrayWithArray:[[self.arrMyJourney reverseObjectEnumerator] allObjects]];
    cnt.delegate=self;
    [cnt setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    cnt.view.hidden = true;
    [UIAppDelegate.window.rootViewController presentViewController:cnt animated:NO completion:nil];
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"indexPath>>%@",indexPath);
    Journey *obj=nil;
    BOOL isMyJourney = NO;
    if(indexPath.section==[self totalSections]-1)
    {
        obj=[self.arrAllJourneys objectAtIndex:indexPath.row];
    }
    else if(indexPath.section==[self totalSections]-2)
    {
        obj=[self.arrNewJourneys objectAtIndex:indexPath.row];
    }
    else if([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            // only first row toggles exapand/collapse

            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [_expandedSections containsIndex:section];
            NSInteger rows;
            
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [_expandedSections removeIndex:section];
                
            }
            else
            {
                [_expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i
                                                               inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
               
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
               
            }
            return;
        }
        else
        {
            isMyJourney = YES;
           // obj=[self.arrMyJourney objectAtIndex:indexPath.section];
        }
      
    }

    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    //@added by steven
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    if ([selectedCell isKindOfClass:[JourneyCell class]]) {
        [self showPreviewLoader:(JourneyCell*)selectedCell];
    }
    else if ([selectedCell isKindOfClass:[MyJourneySubCell class]]){
        [self showPreviewLoader:_myJourneyCell];
    }
    
    JourneyDetailsViewController *cnt=[[JourneyDetailsViewController alloc]initWithNibName:@"JourneyDetailsViewController" bundle:nil];
    cnt.comefrom=JourneyDetailsComeFromJourney;
    cnt.objJourney=obj;
    if(isMyJourney)
    {
        cnt.arrMyJourney = [NSMutableArray arrayWithObjects:self.arrMyJourney[indexPath.row - 1], nil];
    }
    cnt.delegate=self;
    [cnt setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    cnt.view.hidden = YES;
    [UIAppDelegate.window.rootViewController presentViewController:cnt animated:NO completion:nil];
}

-(void)showPreviewLoader:(JourneyCell*)cell {
    _loadingCell = cell;
    [cell showImagePreviewLoader];
}

-(void)hidePreviewLoader {
    [_loadingCell hideImagePreviewLoader];
    _loadingCell = nil;
}

#pragma mark - JourneyDetailViewController Delegate

-(void)didDeleteJourneyAtIndex:(NSInteger)index
{
    [self.arrMyJourney removeObjectAtIndex:index];
    [self.tableView reloadData];
    
    if(self.arrayList.count==0 && self.arrMyJourney.count==0)
    {
        //[self showEmptyDataLabelWithMsg:MPLocalizedString(@"no_journey_found", nil)];
        [self showEmptyDataImageViewWithImage:[UIImage imageNamed:@"icon_no_journey"]];
    }
}

-(void)didDoneLoading:(JourneyDetailsViewController *)detailVC {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        detailVC.view.hidden = NO;
        [self hidePreviewLoader];
    });
}

#pragma mark -- Load More methods

#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];

    if (self.server_request_inprogress == NO && self.next_offset!=-1) {
        CGPoint offset = self.tableView.contentOffset;
        CGRect bounds = self.tableView.bounds;
        CGSize size = self.tableView.contentSize;
        
        UIEdgeInsets inset = self.tableView.contentInset;
        
        float y = offset.y + bounds.size.height - inset.bottom;
        float h = size.height;
        
        CGFloat reload_distance = 50;
        
        if (y + reload_distance > h ) {
            [self setActivityInTableFooter:YES];
            self.server_request_inprogress = YES;
            
            double delayInSeconds = 0.5f;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [self getJourneyFromServer];
            });
            
            return;
        }
    }
}

-(void)addLoadMoreActivityasFooter
{
    self.activityLoadMore =[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityLoadMore.hidden=YES;
    self.activityLoadMore.frame = CGRectMake(0, 0, SCREEN_SIZE.width, 44);
    self.tableView.tableFooterView=self.activityLoadMore;
}

-(void)setActivityInTableFooter:(BOOL)show
{
    if(show )//&& self.lsDoctors.next_offset!=-1)
    {
        self.activityLoadMore.hidden=NO;
        [self.activityLoadMore startAnimating];
        self.tableView.tableFooterView=self.activityLoadMore;
    }
    else
    {
        self.activityLoadMore.hidden=YES;
        [self.activityLoadMore stopAnimating];
        self.tableView.tableFooterView=nil;
    }
}
-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
