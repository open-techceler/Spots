//
//  NotificationDefinitions.m
//  
//
//  Created by Ajay Dalvi on 26/05/15.
//
//

#import "NotificationDefinitions.h"

@implementation NotificationDefinitions


NSString * const kLocationUpdateNotification = @"LocationUpdateNotification";

NSString * const kLocationErrorNotification  = @"LocationErrorNotification";

NSString * const kHideProgressViewOnFBLoginFailedNotification = @"HideProgressViewOnFBLoginFailedNotification";

NSString * const kHideProgreesViewForFBSessionStateClosedNotification = @"HideProgreesViewForFBSessionStateClosedNotification";


NSString * const kLocationResetSetCurrentLocationLock  = @"LocationErrorNotification";


NSString * const kReloadJourneyListNotification  = @"ReloadJourneyListNotification";
NSString * const kReloadMessageListNotification  = @"ReloadMessageListNotification";
NSString * const kReloadNearByListNotification  = @"ReloadNearByListNotification";
NSString * const kReloadLiveFriendsListNotification  = @"ReloadLiveFriendsListNotification";
NSString * const kpopToMapViewNotification  = @"PopToMapViewNotification";
NSString * const kcheckLocationServiceNotification  = @"CheckLocationServiceNotification";
NSString * const kreloadMyJourneyTabNotification  = @"ReloadMyJourneyTabNotification";
NSString * const kUIApplicationDidBecomeActive = @"UIApplicationDidBecomeActive";
NSString * const kUIApplicationGoOnline = @"UIApplicationGoOnline";

NSString * const kMessageIsDestructed = @"MessageIsDestructed";

NSString * const kSubbPromotionVCLoadingDone = @"SubbPromotionViewControllerLoadingDone";

@end
