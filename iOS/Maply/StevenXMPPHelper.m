//
//  XMPPHelper.m
//  FindTalents
//
//  Created by steven on 28/6/2016.
//  Copyright © 2016 steven. All rights reserved.
//

#import "StevenXMPPHelper.h"

@implementation StevenXMPPHelper

ChatRegisterCompletionBlock   xmppRegisterCompletionBlock;
ChatConnectCompletionHandler  xmppConnectCompletionBlock;
ChatAuthCompletionHandler     xmppAuthCompletionBlock;
ChatDidSendMessageCompletionHandler xmppDidSendMessgageCompletionBlock;
XMPPReconnect* xmppReconnect;

+ (StevenXMPPHelper*)sharedInstance
{
    static StevenXMPPHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[StevenXMPPHelper alloc] init];
    });
    
    return sharedInstance;
}

NSString *pswd;
BOOL gonnaRegister = NO;

-(void)registerUserWithJID:(NSString*)jid andPassword: (NSString*)pw withHostName: (NSString*)hostname completion: (ChatRegisterCompletionBlock)block{
    
    if (block)
        xmppRegisterCompletionBlock = block;
    
    gonnaRegister = YES;
    NSError* error;
    XMPPStream *xmppStream = [[XMPPStream alloc] init];
    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    xmppStream.myJID = [XMPPJID jidWithString:jid];
    xmppStream.hostName = hostname;
    xmppStream.hostPort = 5222;
    
    pswd = pw;
    [xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error];
    
}

-(void)setupStreamWithHostName:(NSString*)hostname databaseFileName_suffix:(NSString*)suffix {
    
    _xmppStream = [[XMPPStream alloc] init];
    #if !TARGET_IPHONE_SIMULATOR
        // Want xmpp to run in the background?
        //
        // P.S. - The simulator doesn't support backgrounding yet.
        //        When you try to set the associated property on the simulator, it simply fails.
        //        And when you background an app on the simulator,
        //        it just queues network traffic til the app is foregrounded again.
        //        We are patiently waiting for a fix from Apple.
        //        If you do enableBackgroundingOnSocket on the simulator,
        //        you will simply see an error message from the xmpp stack when it fails to set the property.
    _xmppStream.enableBackgroundingOnSocket = true;
    #endif
    
    //Setup reconnect
    xmppReconnect = [[XMPPReconnect alloc] init];
    xmppReconnect.autoReconnect = true;
    xmppReconnect.reconnectDelay = 3;
    [xmppReconnect addDelegate:self delegateQueue:dispatch_get_main_queue()];
    [xmppReconnect activate:_xmppStream];
    
    //Setup message archiving
    _xmppMessageStorage = [[XMPPMessageArchivingCoreDataStorage alloc] initWithDatabaseFilename:[NSString stringWithFormat:@"%@_messageArchive", suffix] storeOptions:nil];
    if(_xmppMessageStorage != nil) {
        _xmppMessageArchiving = [[XMPPMessageArchiving alloc] initWithMessageArchivingStorage:_xmppMessageStorage];
        _xmppMessageArchiving.clientSideMessageArchivingOnly = YES;
        [_xmppMessageArchiving activate:_xmppStream];
        [_xmppMessageArchiving addDelegate:self delegateQueue:dispatch_get_main_queue()];
    }
//    //Setup Roster
//    _xmppRosterStorage = [[XMPPRosterCoreDataStorage alloc] initWithDatabaseFilename:[NSString stringWithFormat:@"%@_roster", suffix] storeOptions:nil];
//    _xmppRoster = [[XMPPRoster alloc] initWithRosterStorage:_xmppRosterStorage];
//    _xmppRoster.autoFetchRoster = YES;
//    _xmppRoster.autoAcceptKnownPresenceSubscriptionRequests = YES;
//    _xm
    
    //Add delegate for xmppStream
    [_xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    _xmppStream.hostName = hostname;
    _xmppStream.hostPort = 5222;
}

dispatch_semaphore_t semaphore = nil;
-(void)connectWithJid:(NSString*)jid password:(NSString*)password hostname:(NSString*)hostname connectionHandler:(ChatConnectCompletionHandler)connection authenticationHandler:(ChatAuthCompletionHandler)authentication {
    
    semaphore = dispatch_semaphore_create(0);
    dispatch_async(dispatch_queue_create([@"connect_to_ejabberd" UTF8String], DISPATCH_QUEUE_SERIAL), ^{
        if (gonnaRegister) {
            dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        }
        [self disconnect];
        
        [self setupStreamWithHostName:hostname databaseFileName_suffix:jid];
        
        xmppConnectCompletionBlock = connection;
        xmppAuthCompletionBlock = authentication;
        
        pswd = password;
        _xmppStream.myJID = [XMPPJID jidWithString:jid];
        
        NSError* error;
        [_xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error];
        NSLog(@"error ==> %@", error);
    });
}

-(BOOL)isConnected {
    if (_xmppStream != nil) {
        return _xmppStream.isConnected;
    }
    return NO;
}

-(void)disconnect {
    if (![self isConnected]) {
        return;
    }
    
    [self goOffline];
    [_xmppStream removeDelegate:self];
//    [_xmppRoster removeDelegate:self];
    
    [xmppReconnect deactivate];
    [_xmppStream disconnect];
    
    _xmppStream = nil;
    xmppReconnect = nil;
    _xmppMessageArchiving = nil;
    _xmppMessageStorage = nil;
}

#pragma mark - Utility
-(void)goOffline {
    XMPPPresence* presence = [[XMPPPresence alloc] initWithType:@"unavailable"];
    [_xmppStream sendElement:presence];
}

-(void)goOnline {
    XMPPPresence* presence = [[XMPPPresence alloc] init];
    [_xmppStream sendElement:presence];
}

-(void)sendMessage:(NSString*)message to:(NSString*)receiverJid completion:(ChatDidSendMessageCompletionHandler)block {
    
    if (![self isConnected]) {
        if (block)
            block(nil, nil);
        return;
    }
    
    NSString* messageId = _xmppStream.generateUUID;
    DDXMLElement* body = [DDXMLElement elementWithName:@"body"];
    [body setStringValue:message];
    DDXMLElement* completeMsg = [DDXMLElement elementWithName:@"message"];
    [completeMsg addAttributeWithName:@"id" stringValue:messageId];
    [completeMsg addAttributeWithName:@"type" stringValue:@"chat"];
    [completeMsg addAttributeWithName:@"to" stringValue:receiverJid];
    [completeMsg addChild:body];
    
    if (block)
        xmppDidSendMessgageCompletionBlock = block;
    [_xmppStream sendElement:completeMsg];
}

-(BOOL)isFlashMessage:(XMPPMessage*)message {
    return ([message elementForName:@"stevencustommsg"] != nil);
}

-(NSMutableDictionary*)parseFlashMessage:(XMPPMessage*)message {
    if ([self isFlashMessage:message]) {
        NSArray* children = [message children];
        NSMutableDictionary* info = [[NSMutableDictionary alloc] init];
        for (DDXMLNode* child_node in children) {
            [info setObject:[child_node stringValue] forKey:[child_node name]];
        }
        return info;
    }
    else {
        return nil;
    }
}

-(void)sendFlashMessage:(NSDictionary*)data to:(NSString*)receiverJid completion:(ChatDidSendMessageCompletionHandler)block {
    
    if (![self isConnected]) {
        if (block)
            block(nil, nil);
        return;
    }
    
    NSString* messageId = _xmppStream.generateUUID;
    DDXMLElement* customMsgMark = [DDXMLElement elementWithName:@"stevencustommsg"];
    [customMsgMark setStringValue:@"true"];
    
    DDXMLElement* completeMsg = [DDXMLElement elementWithName:@"message"];
    [completeMsg addAttributeWithName:@"id" stringValue:messageId];
    [completeMsg addAttributeWithName:@"type" stringValue:@"chat"];
    [completeMsg addAttributeWithName:@"to" stringValue:receiverJid];
    [completeMsg addAttributeWithName:@"from" stringValue:_xmppStream.myJID.bare];
    [completeMsg addChild:customMsgMark];
    
    [data enumerateKeysAndObjectsUsingBlock:^(NSString* key, NSString* content, BOOL * _Nonnull stop) {
        DDXMLElement* customData = [DDXMLElement elementWithName:key];
        [customData setStringValue:content];
        [completeMsg addChild:customData];
    }];
    
    if (block)
        xmppDidSendMessgageCompletionBlock = block;
    [_xmppStream sendElement:completeMsg];
}

-(void)sendMultcastMesssage:(NSString*)message to:(NSArray*)receiverJids completion:(ChatDidSendMessageCompletionHandler)block {
    if (![self isConnected]) {
        if (block)
            block(nil, nil);
        return;
    }
    
    if (block)
        xmppDidSendMessgageCompletionBlock = block;
    
    [_xmppStream sendElement:[XMPPMessage multicastMessageWithBody:message to:receiverJids module:muticast_module]];
}

-(void)sendMuticastFlashMessage:(NSDictionary*)data to:(NSArray*)receiverJids completion:(ChatDidSendMessageCompletionHandler)block {
    if (![self isConnected]) {
        if (block)
            block(nil, nil);
        return;
    }
    
    if (block)
        xmppDidSendMessgageCompletionBlock = block;
    
    [_xmppStream sendElement:[XMPPMessage multicaseFlashMessageWithData:data to:receiverJids module:muticast_module]];
}

-(NSMutableArray*)loadArchivedMessagesFrom:(NSString*)jid {
    if (!self.isConnected) {
        return nil;
    }
    NSManagedObjectContext* moc = self.xmppMessageStorage.mainThreadManagedObjectContext;
    NSEntityDescription* entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    
    NSSortDescriptor* sd1 = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    NSArray* sortDescriptors = @[sd1];
    NSString* predicateFormat = @"bareJidStr = %@";
    NSPredicate* predicate = [NSPredicate predicateWithFormat:predicateFormat, jid];
    NSMutableArray* retrievedMessages = [[NSMutableArray alloc] init];
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    request.predicate = predicate;
    request.sortDescriptors = sortDescriptors;
    request.entity = entityDescription;
    
    NSError* error;
    NSArray* results = [moc executeFetchRequest:request error:&error];
    
    for (XMPPMessageArchiving_Message_CoreDataObject* message in results) {
        NSError *error;
        DDXMLElement *element = [[DDXMLElement alloc] initWithXMLString:message.messageStr error:&error];
        if (error) {
            continue;
        }
        
        if ([[element attributeStringValueForName:@"type"] isEqualToString:@"error"]) {
            continue;
        }
        
        [retrievedMessages addObject:message.body];
    }
    
    return retrievedMessages;
}

-(NSMutableArray*)loadArchivedMessagesFrom:(NSString*)jid isOutgoing:(BOOL)isOutgoing {
    if (!self.isConnected) {
        return nil;
    }
    NSManagedObjectContext* moc = self.xmppMessageStorage.mainThreadManagedObjectContext;
    NSEntityDescription* entityDescription = [NSEntityDescription entityForName:@"XMPPMessageArchiving_Message_CoreDataObject" inManagedObjectContext:moc];
    
    NSSortDescriptor* sd1 = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES];
    NSArray* sortDescriptors = @[sd1];
    NSString* predicateFormat = @"bareJidStr = %@";
    NSPredicate* predicate = [NSPredicate predicateWithFormat:predicateFormat, jid];
    NSMutableArray* retrievedMessages = [[NSMutableArray alloc] init];
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    request.predicate = predicate;
    request.sortDescriptors = sortDescriptors;
    request.entity = entityDescription;
    
    NSError* error;
    NSArray* results = [moc executeFetchRequest:request error:&error];
    
    for (XMPPMessageArchiving_Message_CoreDataObject* message in results) {
        NSError *error;
        DDXMLElement *element = [[DDXMLElement alloc] initWithXMLString:message.messageStr error:&error];
        if (error) {
            continue;
        }
        
        //when the oppenent's offline queue is fulled and for some other reason, the type of message is contain error, and it is stored in the message storage
        if ([[element attributeStringValueForName:@"type"] isEqualToString:@"error"]) {
            continue;
        }
        
        if ([[element attributeStringValueForName:@"to"] isEqualToString:jid]) {
            if (isOutgoing) {
                [retrievedMessages addObject:message.body];
            }
        }
        else {
            if (!isOutgoing) {
                [retrievedMessages addObject:message.body];
            }
        }
    }
    
    return retrievedMessages;
}

#pragma mark - XMPPStreamConnect/Register Delegate Handler
-(void)xmppStreamDidConnect:(XMPPStream *)sender {
    __block NSError *error;
    if (gonnaRegister) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [sender registerWithPassword:pswd error:&error];
        });
    }
    else {
        if (xmppConnectCompletionBlock)
            xmppConnectCompletionBlock(sender, nil);
        
        NSError *error;
        [_xmppStream authenticateWithPassword:pswd error:&error];
    }
}

-(void)xmppStreamDidRegister:(XMPPStream *)sender {
    gonnaRegister = NO;
    if (xmppRegisterCompletionBlock)
        xmppRegisterCompletionBlock(sender, nil);
    
    if (semaphore)
        dispatch_semaphore_signal(semaphore);
}

-(void)xmppStream:(XMPPStream *)sender didNotRegister:(DDXMLElement *)error {
    gonnaRegister = NO;
    if (xmppRegisterCompletionBlock)
        xmppRegisterCompletionBlock(sender, error);
    
    if (semaphore)
        dispatch_semaphore_signal(semaphore);
}

-(void)xmppStreamDidAuthenticate:(XMPPStream *)sender {
    [self goOnline];
    if (xmppAuthCompletionBlock)
        xmppAuthCompletionBlock(sender, nil);
}

-(void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(DDXMLElement *)error {
    if (xmppAuthCompletionBlock)
        xmppAuthCompletionBlock(sender, error);
}

#pragma mark - XMPPReconnectDelegate Handler
-(void)xmppReconnect:(XMPPReconnect *)sender didDetectAccidentalDisconnect:(SCNetworkConnectionFlags)connectionFlags {
    [xmppReconnect manualStart];
}
-(BOOL)xmppReconnect:(XMPPReconnect *)sender shouldAttemptAutoReconnect:(SCNetworkConnectionFlags)connectionFlags {
    return YES;
}

#pragma mark - XMPPStreamMessgae Delegate Handler
-(void)xmppStream:(XMPPStream *)sender didSendMessage:(XMPPMessage *)message {
    if (xmppDidSendMessgageCompletionBlock)
        xmppDidSendMessgageCompletionBlock(sender, message);
}

-(void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    if ([self isFlashMessage:message]) {
        NSDictionary* dic = [self parseFlashMessage:message];
        [_delegate xmppHelper:sender didReceiveFlashMessage:[[NSDictionary alloc] initWithDictionary:dic]];
    }
    else {
        if ([message isChatMessageWithBody]) {
            [_delegate xmppHelper:sender didReceiveMessage:message.body];
        }
    }
}

@end
