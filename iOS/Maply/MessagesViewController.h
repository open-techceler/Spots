//
//  MessagesViewController.h
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JourneyDetailsViewController.h"

@protocol  MessagesViewDelegate <NSObject>
-(void)pushController:(UIViewController *)cnt;
-(void)presentViewController:(UIViewController *)cnt;
-(void)setBadgeForTab:(NSInteger)tabIndex value:(NSInteger)value;

@end

@interface MessagesViewController : MyNavigationViewController<JourneyDetailsDelegate>

@property(nonatomic,weak) id<MessagesViewDelegate> delegate;
@property(nonatomic) MessagesComeFrom comefrom;
- (void)refreshTable;
@end
