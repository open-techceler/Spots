//
//  InviteFriendsViewController.h
//  Maply
//
//  Created by Admin on 01/10/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendsViewController : MyNavigationViewController

@property (nonatomic, assign) BOOL bHideDoneButton;

@property (weak, nonatomic) IBOutlet UIButton *btnFBSendButton;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

- (IBAction)actionInviteSMS:(id)sender;
- (IBAction)actionInviteFacebook:(id)sender;
- (IBAction)actionInviteMessagener:(id)sender;

@end
