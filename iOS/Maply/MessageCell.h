//
//  MessageCell.h
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@class MessageCell;
@protocol MessageCellDelegate <NSObject>
-(void)messageCell:(MessageCell*)cell doubleTap:(UITapGestureRecognizer*)doubleTap;
-(void)messageCell:(MessageCell*)cell singleTap:(UITapGestureRecognizer*)singleTap;
@end

@interface MessageCell : UITableViewCell
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgUser,*imgPhoto;
@property(nonatomic,weak)IBOutlet UIImageView *imgSaved;
@property(nonatomic,weak)IBOutlet UILabel *lblName,*lblAddress;
@property(nonatomic,weak)IBOutlet UIButton *btnCount;
@property (weak, nonatomic) IBOutlet UIView *viewPreviewLoader;

@property(nonatomic, weak) id<MessageCellDelegate> delegate;

-(void)startActivity;
-(void)stopActivity;

@end
