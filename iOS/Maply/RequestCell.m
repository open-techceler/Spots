//
//  RequestCell.m
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "RequestCell.h"

@implementation RequestCell

- (void)awakeFromNib {
    // Initialization code
    self.lblName.font=FONT_MESSAGE_TITLE;//FONT_AVENIR_PRO_DEMI_H0;
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    self.imgUser.clipsToBounds = YES;
    
    self.lblName.text=@"";
    [self.btnAdd setTitle:MPLocalizedString(@"accept", nil) forState:UIControlStateNormal];
    [self.btnBlock setTitle:MPLocalizedString(@"decline", nil) forState:UIControlStateNormal];
    
    self.btnAdd.titleLabel.font=FONT_AVENIR_MEDIUM_H00; //FONT_AVENIR_PRO_REGULAR_H00;
    self.btnBlock.titleLabel.font=FONT_AVENIR_MEDIUM_H00; //FONT_AVENIR_PRO_REGULAR_H00;


    self.lblName.textColor=UICOLOR_DARK_TEXT;
    
    self.lblHours.font=FONT_AVENIR_MEDIUM_H00;
    self.lblHours.textColor=UICOLOR_NAV_TITLE;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
