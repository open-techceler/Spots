//
//  DistortionCell.h
//  Maply
//
//  Created by admin on 6/27/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseImageViewWithData.h"

@interface DistortionCell : UITableViewCell
@property(nonatomic,weak)IBOutlet BaseImageViewWithData *imgBanner;
@property(nonatomic,weak)IBOutlet UILabel *lblTitle,*lblDetails;
@end
