//
//  Notification.m
//  Maply
//
//  Created by admin on 2/12/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "Notification.h"

@implementation Notification

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // override this in the model if property names in JSON don't match model
    NSDictionary *mapping = [NSDictionary mtl_identityPropertyMapWithModel:self];
    return [mapping mtl_dictionaryByAddingEntriesFromDictionary:@{@"message_noti":@"message"}];
}
@end
