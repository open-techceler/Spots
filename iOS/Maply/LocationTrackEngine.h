//
//  LocationTrackEngine.h
//  Maply
//
//  Created by steven on 22/7/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StevenXMPPHelper.h"
#import "INTULocationManager.h"

#define FOREGROUND_TRACK_INTERVAL  5 //in seconds
#define BACKGROUND_TRACK_INTERVAL  300

static NSString *const XmppVHOST = @"steven.com";
static float const AllowDeltaInLatiLongi = 0.000045f;    //means 4m
static float const MiniumDistanceFilterInMeter = 4.0f;      //means 4meters

/**
 *LocationTrackEngine to implment location algorithm using xmppProtocal
 so in the old algorithm, every 1 min, the device upload his position to the server, and at that time receive nearByFriends list from the server.
 so, with this algorithm, accurate position for friend in realtime can not be determined(because every 1 min, updated position is deliveried, and do nothing in the background task), with the http protocal, saving battery issue can not be resolved.
 so in this new algorithm, every time the user updates his location(more than 1 sec), the user send his location(along with his user_id) to all friends, and each friends receive this message, and parse the user is in his region, and if so, then update his location in realtime.
 xmppProtocal supports the background socket communication, and well designed for the saving battery, so all the issued occured in the http protocal can be solved
 */
@protocol LocationTrackEngineDelegate

-(void)didReceiveUpdatedLocation:(CLLocation*)location fromUserId:(NSInteger)user_id;
-(void)broadcastToFriendsWithMyLocation:(CLLocation*)location;

@end

@interface LocationTrackEngine : NSObject<StevenXMPPHelperDelegate>

@property (nonatomic, weak) id<LocationTrackEngineDelegate> delegate;
@property (nonatomic, strong) NSTimer* timer;
@property (atomic, strong) NSMutableDictionary* locationTrackTable;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (nonatomic) double track_interval_in_seconds;

+(LocationTrackEngine*)sharedInstance;
-(void)sendMyLocation:(CLLocation*)location andMyUserId:(NSInteger)user_id to:(NSString*)jid;
-(void)broadcastWithMyLocation:(CLLocation*)locationInfo andMyUserId:(NSInteger)userId toFriendIds:(NSArray*)friendIds;
-(void)startLocationTrackEngine;
-(void)stopLocationTrackEngine;

-(CLLocation*)lastLocationWithFriendId:(NSInteger)friendId;
@end
