//
//  NotificationEnableViewController.m
//  Maply
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import "NotificationEnableViewController.h"

@interface NotificationEnableViewController ()

@end

@implementation NotificationEnableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnSkip.hidden = !self.bShowSkipButton;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationAuthorized) name:@"NotificationAuthorized" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationFailed) name:@"NotificationFailed" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) notificationAuthorized {
    if (self.delegateForSkip != nil && [self.delegateForSkip respondsToSelector:@selector(tappedNotificationSkipButton)])
        [self.delegateForSkip tappedNotificationSkipButton];
}

- (void) notificationFailed {
    if (self.delegateForSkip != nil && [self.delegateForSkip respondsToSelector:@selector(tappedNotificationSkipButton)])
        [self.delegateForSkip tappedNotificationSkipButton];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)askMe:(id)sender {
    [UIAppDelegate registerUserNotificationSetting];
    
    if (!self.bShowSkipButton)
        [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionSkip:(id)sender {
    if (self.delegateForSkip != nil && [self.delegateForSkip respondsToSelector:@selector(tappedNotificationSkipButton)])
        [self.delegateForSkip tappedNotificationSkipButton];
}

@end
