#import "MyLocation.h"

@interface MyLocation()

//@property (nonatomic) CLLocationDegrees latitude;
//@property (nonatomic) CLLocationDegrees longitude;

@end

@implementation MyLocation
@synthesize name = _name;
@synthesize address = _address;
@synthesize coordinate = _coordinate;
@synthesize index = _index;
//@synthesize latitude = _latitude;
//@synthesize longitude = _longitude;

- (id)initWithName:(NSString*)name address:(NSString*)address coordinate:(CLLocationCoordinate2D)coordinate index:(NSString*)index {
    if ((self = [super init])) {
        _name = [name copy];
        _address = [address copy];
        _coordinate = coordinate;
        _index = [index copy];
    }
    return self;
}

- (NSString *)title {
    if ([_name isKindOfClass:[NSNull class]]) 
        return @"Unknown charge";
    else
        return _name;
}

- (NSString *)subtitle {
    return _address;
}

- (NSString *)storeIndex {
    return _index;
}
/*
- (CLLocationCoordinate2D)coordinate {
    
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = self.latitude;
    coordinate.longitude = self.longitude;
    
    return coordinate;
}

- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    
    self.latitude = newCoordinate.latitude;
    self.longitude = newCoordinate.longitude;
}
*/



@end