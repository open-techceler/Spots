//
//  JourneyCell.m
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "JourneyCell.h"

@implementation JourneyCell

- (void)awakeFromNib {
    // Initialization code
    
    self.lblTitle.font=FONT_AVENIR_ROMAN_H15  ;//FONT_AVENIR_ROMAN_H9;
    self.lblMoments.font=FONT_AVENIR_LIGHT_H3 ;//FONT_AVENIR_LIGHT_H0;
    self.lblName.font= FONT_AVENIR_MEDIUM_H3 ;//FONT_AVENIR_MEDIUM_H1;
  //  self.lblTitle.font=FONT_AVENIR_PRO_DEMI_H10;
   // self.lblMoments.font=FONT_AVENIR_PRO_DEMI_H1;
   // self.lblName.font=FONT_AVENIR_PRO_DEMI_H3;
    
    self.lblTitle.textColor=[UIColor whiteColor];
    self.lblMoments.textColor=[UIColor whiteColor];
    self.lblName.textColor=[UIColor whiteColor];
    
    //self.contentView.backgroundColor=[UIColor lightGrayColor];
    
    self.imgUser.layer.borderColor=[UIColor whiteColor].CGColor;
    self.imgUser.layer.borderWidth=1.0;
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    self.imgUser.clipsToBounds = YES;
    
    self.lblTitle.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.lblTitle.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    
    self.lblTitle.layer.shadowRadius = 5.0;
    self.lblTitle.layer.shadowOpacity =0.6;
    self.lblTitle.layer.masksToBounds = NO;
    self.lblTitle.layer.shouldRasterize = YES;
   
    self.lblMoments.highlightedTextColor = [UIColor whiteColor];
    
    self.imgPreviewLoader.alpha = 0.5;
    self.imgPreviewLoader.hidden = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)showImagePreviewLoader {
    self.imgPreviewLoader.hidden = NO;
    [self.imgPreviewLoader startActivity];
}

- (void)hideImagePreviewLoader {
    self.imgPreviewLoader.hidden = YES;
    [self.imgPreviewLoader stopActivity];
}

@end
