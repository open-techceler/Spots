//
//  MyNavigationController.m
//  Maply
//
//  Created by admin on 6/6/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MyNavigationController.h"
#import "MiniToLargeViewAnimator.h"
#import "MiniToLargeViewInteractive.h"
@interface MyNavigationController () <UIViewControllerTransitioningDelegate>
@property (nonatomic) BOOL disableInteractivePlayerTransitioning;
@property (nonatomic) MiniToLargeViewInteractive *dismissInteractor;

@end

@implementation MyNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //self.transitioningDelegate = self;
    
    //for drag to dismiss view
    
    self.disableInteractivePlayerTransitioning = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if(self.dismissInteractor==nil)
    {
        self.transitioningDelegate = self;
        self.dismissInteractor = [[MiniToLargeViewInteractive alloc] init];
        UIViewController *cnt=[self.viewControllers firstObject];
        if(cnt)
            [self.dismissInteractor attachToViewController:self withView:cnt.view presentViewController:nil];
    }
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.transitioningDelegate=nil;
    self.dismissInteractor=nil;
}

#pragma mark - transitioningDelegate
- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    MiniToLargeViewAnimator *animator = [[MiniToLargeViewAnimator alloc] init];
    animator.initialY = 0;
    animator.transitionType = ModalAnimatedTransitioningTypeDismiss;
    return animator;
}

- (id<UIViewControllerInteractiveTransitioning>)interactionControllerForDismissal:(id<UIViewControllerAnimatedTransitioning>)animator
{
    if (self.disableInteractivePlayerTransitioning) {
        return nil;
    }
    return self.dismissInteractor;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc{
    MPLog(@"MyNavigationViewController--- Dealloc");
    self.transitioningDelegate = nil;
    self.dismissInteractor=nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
