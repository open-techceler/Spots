//
//  Moment.h
//  Maply
//
//  Created by admin on 6/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MTLModelModified.h"

@interface Moment : MTLModelModified
@property(nonatomic,strong)NSString *text,*location_name,*image;
@property(nonatomic)NSInteger moment_id;
@property(nonatomic)NSInteger view_count;
@property(nonatomic,strong)NSString *created_date;


@end
