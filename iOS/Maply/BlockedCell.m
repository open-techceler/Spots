//
//  BlockedCell.m
//  Maply
//
//  Created by admin on 2/11/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "BlockedCell.h"

@implementation BlockedCell

- (void)awakeFromNib {
    // Initialization code
    self.lblFullName.font=FONT_MESSAGE_TITLE;//FONT_AVENIR_PRO_DEMI_H0;
    self.lblUserName.font=FONT_AVENIR_PRO_REGULAR_H00;
    
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    self.imgUser.clipsToBounds = YES;
    
    self.btnBlock.titleLabel.font=FONT_AVENIR_MEDIUM_H00;
    //[UIFont fontWithName:AvenirNextLTProDemi size:8.0f];
    self.lblFullName.textColor=UICOLOR_DARK_TEXT;
    self.lblUserName.textColor=UICOLOR_LIGHT_TEXT;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
