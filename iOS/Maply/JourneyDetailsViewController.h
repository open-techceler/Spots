//
//  JourneyDetailsViewController.h
//  Maply
//
//  Created by admin on 2/24/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MyNavigationViewController.h"
#import "JourneysViewController.h"
@class  Journey;
@class JourneyDetailsViewController;

@protocol  JourneyDetailsDelegate <NSObject>
-(void)didDeleteJourneyAtIndex:(NSInteger)index;
-(void)didDoneLoading:(JourneyDetailsViewController*)detailVC;
@optional
-(void)didClosed:(JourneyDetailsViewController*)detailVC;
@end
@interface JourneyDetailsViewController : MyNavigationViewController

@property(nonatomic,strong) Journey *objJourney;
@property(nonatomic) NSInteger position;
@property (nonatomic)JourneyDetailsComeFrom comefrom;
@property(nonatomic,strong) NSMutableArray *arrMyJourney;
@property(nonatomic,weak) id<JourneyDetailsDelegate> delegate;


@end
