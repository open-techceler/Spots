//
//  LocationView.h
//  Maply
//
//  Created by admin on 5/20/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationView : UIView
@property(nonatomic,weak)IBOutlet UILabel *lblWhere,*lblInOrder,*lblGoto;
@property(nonatomic,weak)IBOutlet UILabel *lblWherePop,*lblInOrderPop;
@property(nonatomic,weak)IBOutlet UIButton *btnSettings;
@property(nonatomic,weak)IBOutlet UIView *vwPop;

@property (nonatomic, strong)LocationView *tempXib;

@end
