//
//  OnboardingViewController.h
//  Maply
//
//  Created by Admin on 26/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OnboardingViewControllerDelegate;

@interface OnboardingViewController : UIViewController

@property (nonatomic, weak) id<OnboardingViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewWidth;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@protocol OnboardingViewControllerDelegate <NSObject>

- (void) dismissOnboardingViewCon:(OnboardingViewController *) viewCon;

@end
