//
//  SendToCell.m
//  Maply
//
//  Created by admin on 2/15/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SendToCell.h"

@implementation SendToCell

- (void)awakeFromNib {
    // Initialization code
    
    self.lblName.font=FONT_MESSAGE_TITLE;
    self.imgUser.layer.cornerRadius=self.imgUser.frame.size.width / 2;
    self.imgUser.clipsToBounds = YES;
    
    self.lblName.textColor=UICOLOR_DARK_TEXT;
    self.btnSelect.userInteractionEnabled=NO;


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
