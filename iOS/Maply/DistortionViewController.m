//
//  DistortionViewController.m
//  Maply
//
//  Created by admin on 6/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "DistortionViewController.h"
#import "JourneyCell.h"
#import "UserAPI.h"
#import "Journey.h"
#import "User.h"
#import "FormatterCollection.h"
#import "DistortionCell.h"

@interface DistortionViewController ()
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@end

@implementation DistortionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initLoaderActivity];
    [self initEmptyDataLabel];
    [self initOfflineView];
    
    [self setUpNavigationBar];
    [self setFontAndColor];
}


-(void)viewWillDisappear:(BOOL)animated
{
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG_GREY;
}

-(void)setUpNavigationBar
{
    self.Nav.lblTitle.attributedText = [self getAttributedString];
    [self.Nav setBackButtonAsLeftButton];
    [self.Nav.btnLeft addTarget:self action:@selector(clickBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.Nav setNavigationLayout:YES];
    
}

-(NSMutableAttributedString *)getAttributedString
{
    NSString *location=self.objFriend.location;
    NSString *full_str=[NSString stringWithFormat:@"%@\n%@",self.objFriend.title,location];
    
    NSMutableAttributedString* att_string = [[NSMutableAttributedString alloc] initWithString:full_str];
    NSRange atRange = [full_str rangeOfString:location];
    
    if (atRange.location != NSNotFound) {
        [att_string addAttribute:NSFontAttributeName value:FONT_AVENIR_MEDIUM_H00 range:atRange];
    }
    return att_string;
}

-(IBAction)clickBack:(id)sender
{
    [self dismissViewControllerAnimated:NO completion:nil];
}


#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 128;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString* MyIdentifier = @"DistortionCell";
        DistortionCell * cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"DistortionCell" owner:self options:nil] objectAtIndex:0];
        }
    
        cell.lblTitle.text=self.objFriend.name;
        cell.lblDetails.text=self.objFriend.event_description;
        [cell.imgBanner getImageWithURL:self.objFriend.image comefrom:ImageViewComfromUser];
        return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
//    if(![UIAppDelegate isInternetAvailable])
//    {
//        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
//        return;
//    }
}

#pragma mark -- Load More methods

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
