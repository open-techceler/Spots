//
//  SettingsViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SettingsViewController.h"
#import "TitleValueTableCell.h"
#import "AddInfoCell.h"
#import "NotificationsViewController.h"
#import "BlockedUserViewController.h"
#import "SelectLanguageViewController.h"
#import "User.h"
#import "UserAPI.h"
#import "UIImage+Resize.h"
#import "UIImage+FixOrientation.h"
#import <MessageUI/MessageUI.h>
#import "CameraMainViewController.h"
#import "AddFriendsViewController.h"
#import "FacebookManager.h"
#import "StevenXMPPHelper.h"
#import "InviteFriendsViewController.h"

@interface SettingsViewController ()<UITextFieldDelegate,UIActionSheetDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate,MFMailComposeViewControllerDelegate,CameraDelegate,UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UIView *vwSignOut;
@property(nonatomic,weak)IBOutlet UIButton *btnSignOut;
@property(nonatomic,strong) User *objUser;
@property(nonatomic) BOOL needToPicutreAtServer;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    self.navigationItem.hidesBackButton = YES;
    [self initLoaderActivity];
    [self startLoadingActivity];
    [self setFontAndColor];
    [self setUpNavigationBar];
    self.tableView.hidden=YES;
    //https://trello.com/c/oQ7Do1C5
    double delayInSeconds = 0.1f;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        self.objUser=[User currentUser];
        
        self.tableView.delegate=self;
        self.tableView.dataSource=self;
        
        [self.tableView reloadData];
        [self stopLoadingActivity];
        
        self.tableView.hidden=NO;
        
        [self getProfilePictureFromFb];
        
    });
    
    self.tableView.tableFooterView=self.vwSignOut;

    
}

-(void)getProfilePictureFromFb
{
    if([VVBaseUserDefaults isProfilePictureChanged]==NO)
    {
        [[FacebookManager sharedInstance]getFacebookProfilePicture:^(NSString *url) {
            if([self.objUser.image isEqualToString:url]==NO)
            {
                _needToPicutreAtServer=YES;
                self.objUser.image=url;
                User *obj=[User currentUser];
                obj.image=url;
                [User setAsCurrentUser:obj];
                [self.tableView reloadData];
            }
        } failure:^{
        
        }];
    }
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    User *obj=[User currentUser];
    if(![obj.name isEqualToString:self.objUser.name]
       || ![obj.email isEqualToString:self.objUser.email]
       ||_needToPicutreAtServer)
    {
        [self updateUserProfile];
    }
}

-(void)updateUserProfile
{
    if([UIAppDelegate isInternetAvailable]){
        [[UserAPI new]updateUserProfile:self.objUser success:^(BOOL status) {
        
        } failure:^(NSError *error) {
        }];
    }
}
-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"settings", nil);
    [self.Nav setBackButtonAsLeftButton];
    [self.Nav.btnLeft addTarget:self action:@selector(clickBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.Nav setNavigationLayout:YES];
    
}
-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG_GREY;
    self.tableView.backgroundColor=UICOLOR_APP_BG_GREY;
    
    self.lblTitle.font=FONT_NAV_TITLE;
    self.lblTitle.textColor=UICOLOR_NAV_TITLE;
    self.lblTitle.text=MPLocalizedString(@"settings", nil);
    
    self.btnSignOut.titleLabel.font=FONT_SETTING_TITLE;
    [self.btnSignOut setTitleColor:[UIColor colorWithRed:20/255.0f green:34/255.0f blue:62/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [self.btnSignOut setTitle:MPLocalizedString(@"sign_out", nil) forState:UIControlStateNormal];
 
}

-(IBAction)clickBack:(id)sender
{
    //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopTop];
    //[self.navigationController popViewControllerAnimated:YES];
    CATransition* transition = [CATransition animation];
    transition.duration = 0.5;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromTop;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [[self navigationController] popViewControllerAnimated:NO];
}

-(IBAction)clickSignOut:(id)sender
{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:MPLocalizedString(@"do_you_want_to_logout", nil) delegate:self cancelButtonTitle:MPLocalizedString(@"cancel", nil) destructiveButtonTitle:MPLocalizedString(@"yes", nil) otherButtonTitles:nil];
    [action showInView:self.view];

}
#pragma mark -- UIActionSheet
// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex!=actionSheet.cancelButtonIndex)
    {
        [Utility logoutFromApp];
        [[StevenXMPPHelper sharedInstance] disconnect];
    }
}
#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 38;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vw =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 38)];
    vw.backgroundColor=UICOLOR_APP_BG_GREY;
    
    UILabel *lbl =[[UILabel alloc] initWithFrame:CGRectMake(17, 16, SCREEN_SIZE.width-17, 20)];
    lbl.backgroundColor=[UIColor clearColor];
    lbl.font=FONT_HELVETICA_REGULAR_H3;//FONT_AVENIR_PRO_DEMI_H2;
    lbl.textAlignment=NSTextAlignmentLeft;
    lbl.textColor=UICOLOR_LIGHT_TEXT;
    lbl.text=[[self getTitleForSection:section] uppercaseString];
    [vw addSubview:lbl];
    
    return vw;
}

-(NSString *)getTitleForSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return MPLocalizedString(@"account_details", nil);
            break;
        case 1:
            return MPLocalizedString(@"app", nil);
            break;
            
        default:
            break;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return 4;
    else if(section==1)
        return 6;
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==0&& (indexPath.row==0||indexPath.row==1))
    {
        
        static NSString* MyIdentifier = @"AddInfoCell";
        AddInfoCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"AddInfoCell" owner:self options:nil] objectAtIndex:0];
            cell.txtField.delegate=self;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        cell.txtField.tag=indexPath.row;
        [cell.txtField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

        cell.lblTitle.text=[self getTitleForIndexPath:indexPath];
        cell.txtField.placeholder=[self getTitleForIndexPath:indexPath];
        
        User *obj =[User currentUser];
        if(indexPath.row==0)
            cell.txtField.text=obj.name;
        else
            cell.txtField.text=obj.email;


       
         return cell;
    }
    else
    {
        static NSString* MyIdentifier = @"TitleValueTableCell";
        TitleValueTableCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"TitleValueTableCell" owner:self options:nil] objectAtIndex:0];
        }
        cell.lblTitle.text=[self getTitleForIndexPath:indexPath];
        if(indexPath.section==1 && indexPath.row==0)
        {
            cell.lblValue.text=@"English";
        }
        else
            cell.lblValue.text=nil;
        if(indexPath.section==0 && indexPath.row==2) //@profile picture
        {
            cell.imgUser.hidden=NO;
            [cell.imgUser getImageWithURL:self.objUser.image comefrom:ImageViewComfromUser];
            
        }
        else
        {
             cell.imgUser.hidden=YES;
        }
        
        [cell hideLastCellSperatorIfNeeded:[self.tableView numberOfRowsInSection:indexPath.section] index:indexPath.row];

         return cell;
    }
    
    return nil;
   
}

-(NSString *)getTitleForIndexPath:(NSIndexPath *)indexPath
{

    NSString *title=nil;
    if(indexPath.section==0)
    {
        switch (indexPath.row) {
            case 0:
                title=MPLocalizedString(@"full_name", nil);
                break;
            case 1:
                title=MPLocalizedString(@"email", nil);
                break;
            case 2:
                title=MPLocalizedString(@"profile_picture", nil);
                break;
            case 3:
                title=MPLocalizedString(@"notifications", nil);
                break;
          
                
            default:
                break;
        }
    }
    else if(indexPath.section==1)
    {
        switch (indexPath.row) {
            case 0:
                title=MPLocalizedString(@"language", nil);
                break;
            case 1:
                title=MPLocalizedString(@"find_friends", nil);
                break;
            case 2:
                title=MPLocalizedString(@"invite_friends", nil);
                break;
            case 3:
                title=MPLocalizedString(@"blocked_users", nil);
                break;
            case 4:
                title=MPLocalizedString(@"support", nil);
                break;
            case 5:
                title=MPLocalizedString(@"rate_heat", nil);
                break;
                
            default:
                break;
        }

    }
    
    return title;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if(indexPath.section==0)
    {
        if(indexPath.row==2)//@click the profile picture
        {
            [self clickAddPhoto:nil];
        }
        else if(indexPath.row==3)
        {
            NotificationsViewController *cnt=[[NotificationsViewController alloc] initWithNibName:@"NotificationsViewController" bundle:nil];
            [self.navigationController pushViewController:cnt animated:YES];
            
        }
       
    }
    else
    {
        if(indexPath.row==0)
        {
            SelectLanguageViewController *cnt=[[SelectLanguageViewController alloc] initWithNibName:@"SelectLanguageViewController" bundle:nil];
            [self.navigationController pushViewController:cnt animated:YES];
        }
        else if(indexPath.row==1)
        {
            AddFriendsViewController *cnt=[[AddFriendsViewController alloc]initWithNibName:@"AddFriendsViewController" bundle:nil];
            [self.navigationController pushViewController:cnt animated:YES];
        }
        else if(indexPath.row==2)
        {
            InviteFriendsViewController *cnt=[[InviteFriendsViewController alloc]initWithNibName:@"InviteFriendsViewController" bundle:nil];
            cnt.bHideDoneButton = YES;
            [self.navigationController pushViewController:cnt animated:YES];
        }
        else if(indexPath.row==3)
        {
            BlockedUserViewController *cnt=[[BlockedUserViewController alloc] initWithNibName:@"BlockedUserViewController" bundle:nil];
            [self.navigationController pushViewController:cnt animated:YES];

        }
        else if(indexPath.row==4)
        {
            [self ShowEmailApp];
        }
        else if(indexPath.row==5)
        {
            
        }

    }

}

- (void)ShowEmailApp
{
    [UIAppDelegate applyNativeInterfaceApperance];
    MFMailComposeViewController* mail = [[MFMailComposeViewController alloc] init];
    mail.mailComposeDelegate = self;
    
    if ([MFMailComposeViewController canSendMail]) {
        
       // [mail setSubject:VVLocalizedString(@"i_have_wishlist", nil)];
        //[mail setMessageBody:sharingtext isHTML:YES];
        
        //Present the mail view controller
        [UIAppDelegate.window.rootViewController presentViewController:mail animated:YES completion:nil];
    }
    else {
        //VVLog(@"no mail account set up");
        [UIAppDelegate applyGlobalInterfaceAppearance];
        
        [UIAppDelegate.window.rootViewController  dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    
    [UIAppDelegate applyGlobalInterfaceAppearance];
    if (result == MFMailComposeResultFailed) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:MPLocalizedString(@"message_failed", nil) message:MPLocalizedString(@"your_email_failed_to_send", nil) delegate:self cancelButtonTitle:MPLocalizedString(@"ok_capital", nil) otherButtonTitles:nil];
        [alert show];
    }
    else if (result == MFMailComposeResultSent) {
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:MPLocalizedString(@"message_sent", nil) message:MPLocalizedString(@"your_email_sent_successfully", nil) delegate:self cancelButtonTitle:MPLocalizedString(@"ok_capital", nil) otherButtonTitles:nil];
        [alert show];
    }
    
    [UIAppDelegate.window.rootViewController  dismissViewControllerAnimated:YES completion:nil];
}

// Called when a button is clicked. The view will be automatically dismissed after this call returns
- (void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [UIAppDelegate.window.rootViewController  dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)clickAddPhoto:(id)sender{
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:MPLocalizedString(@"add_photo", nil)
                                                                         message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:MPLocalizedString(@"cancel", nil) style:UIAlertActionStyleCancel
                                                  handler:^(UIAlertAction *action) {
                                                      [self dismissViewControllerAnimated:YES completion:nil];
                                                  }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:MPLocalizedString(@"take_a_photo", nil) style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action) {
                                                      [self takePhoto];
                                                  }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:MPLocalizedString(@"add_from_library", nil) style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction *action) {
                                                      [self takePhotoFromLibrary];
                                                  }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:MPLocalizedString(@"fetch_from_facebook", nil)
                                                    style:UIAlertActionStyleDefault
                                                  handler:^(UIAlertAction * _Nonnull action) {
                                                      [[FacebookManager sharedInstance]getFacebookProfilePicture:^(NSString *url) {
                                                          if([self.objUser.image isEqualToString:url]==NO)
                                                          {
                                                              _needToPicutreAtServer=YES;
                                                              self.objUser.image=url;
                                                              User *obj=[User currentUser];
                                                              obj.image=url;
                                                              [User setAsCurrentUser:obj];
                                                              
                                                              //get profile picture cell
                                                              NSIndexPath *profileCellPath = [NSIndexPath indexPathForRow:2 inSection:0];
                                                              TitleValueTableCell* cell = [self.tableView cellForRowAtIndexPath:profileCellPath];
                                                              [cell.imgUser getImageWithURL:self.objUser.image comefrom:ImageViewComfromUser];
                                                          }
                                                      } failure:^{
                                                          
                                                      }];
    }]];
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

- (void)takePhoto
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (!([UIImagePickerController  isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])) {
            UIAlertView* myAlert = [[UIAlertView alloc]
                                    initWithTitle:TITLE
                                    message:MPLocalizedString(@"camera_not_supported", nil)
                                    delegate:nil
                                    cancelButtonTitle:nil
                                    otherButtonTitles:MPLocalizedString(@"ok_capital", nil), nil];
            [myAlert show];
        } else {
            
            CameraMainViewController *cnt=[[CameraMainViewController alloc]initWithNibName:@"CameraMainViewController" bundle:nil];
            cnt.camaraDelegate=self;
            cnt.comefrom=CameraMainComeFromSetting;
            
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:cnt];
            nav.navigationBarHidden=YES;
            [UIAppDelegate.window.rootViewController presentViewController:nav animated:YES completion:nil];
            
           /* UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
            [imagePickerController  setSourceType:UIImagePickerControllerSourceTypeCamera];
            [imagePickerController setDelegate:self];
            [self.navigationController presentViewController:imagePickerController
                                                    animated:YES
                                                  completion:nil];*/
        }
    });
}

- (void)takePhotoFromLibrary
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIImagePickerController* imagePickerController = [[UIImagePickerController alloc] init];
        [imagePickerController setDelegate:self];
        [UIAppDelegate.window.rootViewController presentViewController:imagePickerController
                                                animated:YES
                                              completion:nil];
    });
}

#pragma mark UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary*)info
{
    UIImage *imgtemp = [info[UIImagePickerControllerOriginalImage] fixOrientation];
    imgtemp = [imgtemp resizedImage:CGSizeMake(480, 640) interpolationQuality:kCGInterpolationHigh];

    [UIAppDelegate.window.rootViewController  dismissViewControllerAnimated:YES completion:nil];
    
    [self uploadKidPhoto:imgtemp];
}

-(void)uploadKidPhoto:(UIImage *)image
{
    if(![UIAppDelegate isInternetAvailable])
    {
        [ToastMessage showErrorMessageAppTitleWithMessage:MPLocalizedString(@"no_internet_message", nil)];
        return;
    }
    
    [[UserAPI new]updateUserPhotoWithPhoto:image success:^(NSString *image_url) {
        self.objUser.image=image_url;
        User *obj=[User currentUser];
        obj.image=image_url;
        [User setAsCurrentUser:obj];

        [self.tableView reloadData];
        
        [VVBaseUserDefaults markProfilePictureChanged];
        
    } failure:^(NSError *error) {
        
    }];
}

-(void)didSelectImage:(UIImage *)img
{
    img = [img resizedImage:CGSizeMake(480, 640) interpolationQuality:kCGInterpolationHigh];
    [self uploadKidPhoto:img];
}



- (void)imagePickerControllerDidCancel:(UIImagePickerController*)picker
{
    [UIAppDelegate.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.tag==1)
        return NO;
    return YES;
}

- (BOOL)textFieldDidChange:(UITextField *)textField
{
    NSString *value = textField.text;
    if(textField.tag==0)
    {
        self.objUser.name=value;
    }
    else{
        self.objUser.email=value;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
