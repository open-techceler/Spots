//
//  User.h
//  MediPock
//
//  Created by Sphinx Solution Pvt Ltd on 06/10/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import "MTLModelModified.h"

@interface User : MTLModelModified
@property(nonatomic)NSInteger user_id;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *email;
@property(nonatomic,strong)NSString *username;
@property(nonatomic,strong)NSString *image;
@property(nonatomic,strong)NSString *facebook_id;

@property(nonatomic,strong)NSString *location;


+(void)setAsCurrentUser:(User*)obj;
+(User*)currentUser;

@end