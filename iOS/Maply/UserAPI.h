//
//  UserProfileAPI.h
//  
//
//  Created by Admin on 28/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
@class User;
@class Notification;
@class FriendsListWrapper;
@class FSVenue;
@class JourneyListWrapper;
@class MessageListWrapper;
@interface UserAPI : NSObject

-(void)logInWithFacebookSuccess:(void (^)(User *obj))success
                        failure:(void (^)(NSError* error))failure;

-(void)saveNotificationSetting:(Notification*)obj
                       Success:(void (^)(User *obj))success
                       failure:(void (^)(NSError* error))failure;

-(void)getNotificationSettingSuccess:(void (^)(Notification *obj))success
                             failure:(void (^)(NSError* error))failure;

-(void)updateUsernameOnServer:(NSString*)username
                      success:(void (^)(BOOL status))success
                      failure:(void (^)(NSError* error))failure;

-(void)checkUsername:(NSString*)username
             success:(void (^)(BOOL status))success
             failure:(void (^)(NSError* error))failure;

-(void)uploadFaceookFriends:(NSMutableArray *)array
                    success:(void (^)(BOOL status))success
                    failure:(void (^)(NSError* error))failure;

-(void) updateUserPhotoWithPhoto:(UIImage *)imgPhoto
                         success:(void (^)(NSString *image_url))success
                         failure:(void (^)(NSError* error))failure;

-(void)updateUserProfile:(User *)objUser
                   success:(void (^)(BOOL status))success
                   failure:(void (^)(NSError* error))failure;

-(void)getFriendsList:(NSInteger)offset
              keyWord:(NSString *)keyword
              success:(void (^)(FriendsListWrapper *objWrapper))success
              failure:(void (^)(NSError* error))failure;

-(void)updateUserAction:(NSMutableArray  *)lsActions
                success:(void (^)(FriendsListWrapper *objWrapper))success
                failure:(void (^)(NSError* error))failure;

-(void)getBlockedFriendsList:(NSInteger)offset
                     success:(void (^)(FriendsListWrapper *objWrapper))success
                     failure:(void (^)(NSError* error))failure;

-(void)updateUserStatusSuccess: (NSNumber*)offlineHours success:(void (^)(BOOL status))success
                       failure:(void (^)(NSError* error))failure;

-(void)getFriendsRequest:(NSInteger)offset
                 success:(void (^)(FriendsListWrapper *objWrapper))success
                 failure:(void (^)(NSError* error))failure;

-(void)getFriends:(NSInteger)offset
          keyWord:(NSString *)keyword
          success:(void (^)(FriendsListWrapper *objWrapper))success
          failure:(void (^)(NSError* error))failure;


-(void) uploadSnapToServer:(UIImage *)imgPhoto
            withBannerImag:(UIImage *)bannerImg
                  withText:(NSString *)text
              withVideoUrl:(NSURL *)videoUrl
               isMyJourney:(BOOL)isMyJourney
                 withVenue:(FSVenue *)objVenue
                cameraMode:(CameraMode)cameraMode
                   success:(void (^)(NSInteger uploaded_id))success
                   failure:(void (^)(NSError* error))failure;

-(void)getSendToFriendSuccess:(void (^)(FriendsListWrapper *objWrapper))success
                      failure:(void (^)(NSError* error))failure;

-(void)sendMessageToFriends:(NSMutableArray *)array
                  withMsgId:(NSInteger)msgId
                    success:(void (^)(BOOL status))success
                    failure:(void (^)(NSError* error))failure;

-(void)getJourneys:(NSInteger)offset
           success:(void (^)(JourneyListWrapper *objWrapper))success
           failure:(void (^)(NSError* error))failure;

-(void)getNearbyUsers:(NSInteger)offset
              success:(void (^)(FriendsListWrapper *objWrapper))success
              failure:(void (^)(NSError* error))failure;

-(void) replyToFriendWithImage:(UIImage *)imgPhoto
                  withFriendId:(NSInteger)friend_id
                     withVenue:(FSVenue *)objVenue
                      withText:(NSString *)text
                       success:(void (^)(NSString *image_url))success
                       failure:(void (^)(NSError* error))failure;

-(void)getMessagesCount:(NSInteger)offset
                success:(void (^)(NSInteger msgCount))success
                failure:(void (^)(NSError* error))failure;

-(void)getJourneysDetailsWithUserId:(NSInteger)userId
                            withOffset:(NSInteger)offset
                               success:(void (^)(MessageListWrapper *objWrapper))success
                               failure:(void (^)(NSError* error))failure;

-(void)getDirectMessages:(NSInteger)offset
                 success:(void (^)(JourneyListWrapper *objWrapper))success
                 failure:(void (^)(NSError* error))failure;

-(void)getSentMessages:(NSInteger)offset
               success:(void (^)(JourneyListWrapper *objWrapper))success
               failure:(void (^)(NSError* error))failure;

-(void)getChatOfFriend:(NSInteger)friend_id
            withOffset:(NSInteger)offset
          withComeFrom:(JourneyDetailsComeFrom)comefrom
               success:(void (^)(MessageListWrapper *objWrapper))success
               failure:(void (^)(NSError* error))failure;

-(void)sendPushToken:(NSString *)push_token
             success:(void (^)(MessageListWrapper *objWrapper))success
             failure:(void (^)(NSError* error))failure;


-(void)updateUserLocation:(CLPlacemark *)placemark
                  success:(void (^)(MessageListWrapper *objWrapper))success
                  failure:(void (^)(NSError* error))failure;

-(void)markMomentAsRead:(NSMutableArray *)array
              withMsgId:(NSInteger)msgId
                success:(void (^)(BOOL status))success
                failure:(void (^)(NSError* error))failure;

-(void)deleteMyJourneyWithMsgId:(NSInteger)msgId
                               success:(void (^)(BOOL status))success
                               failure:(void (^)(NSError* error))failure;

-(void)getEventsAndMyJourneySuccess:(void (^)(FriendsListWrapper *objWrapper))success
                            failure:(void (^)(NSError* error))failure;

-(void)saveMomentFor24HWithMsgId:(NSInteger)msgId
                         success:(void (^)(BOOL status))success
                         failure:(void (^)(NSError* error))failure;
@end
