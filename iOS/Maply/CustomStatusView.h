//
//  CustomStatusView.h
//  Maply
//
//  Created by admin on 4/29/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomStatusView : UIView
@property (nonatomic,strong)UIView *currentStatus;
@property (nonatomic,strong)UIView *lastSession;
@property (nonatomic,strong)NSMutableArray *arrSessions;

-(void)startNewSession;
-(void)updateSessionValue:(float)width;
-(void)stopSession;

-(void)removeLastSession;
@end
