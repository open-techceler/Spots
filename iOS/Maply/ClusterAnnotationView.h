//
//  ClusterAnnotationView.h
//  CCHMapClusterController Example iOS
//
//  Created by Hoefele, Claus(choefele) on 09.01.14.
//  Copyright (c) 2014 Claus Höfele. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface ClusterAnnotationView : MKAnnotationView

@property (nonatomic, copy) NSString *title;
@property (nonatomic) NSUInteger count,badge_count;
@property (nonatomic,strong) NSString *imgUrl;
@property (nonatomic, strong) NSString *type;

- (void) showHaloAnimation:(BOOL) bShow;

@end
