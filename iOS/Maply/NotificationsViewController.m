//
//  NotificationsViewController.m
//  Maply
//
//  Created by admin on 2/10/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "NotificationsViewController.h"
#import "NotificationCell.h"
#import "DistanceCell.h"
#import "Notification.h"
#import "UserAPI.h"
#import "NoInternetView.h"

@interface NotificationsViewController ()
@property(nonatomic,weak)IBOutlet UITableView *tableView;
@property(nonatomic,weak)IBOutlet UILabel *lblTitle;
@property(nonatomic,weak)IBOutlet UIButton *btnSave;
@property(nonatomic,strong) Notification *objNotification;

@end

@implementation NotificationsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self setFontAndColor];
    [self setUpNavigationBar];
    [self initLoaderActivity];
    [self initOfflineView];
    [self.vwOffline.btnRetry addTarget:self action:@selector(clickRetry:) forControlEvents:UIControlEventTouchUpInside];

    
    
    self.objNotification=[Notification new];
    
    [self getNotificationSetting];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [self saveNotificationSetting];

}

-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"notifications", nil);
    [self.Nav setBackButtonAsLeftButton];
    [self.Nav.btnLeft addTarget:self action:@selector(clickBack:) forControlEvents:UIControlEventTouchUpInside];

    [self.Nav setNavigationLayout:NO];
    
    //Avenir Book 13.4
    //save
   
    
}
-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG;
    self.tableView.backgroundColor=UICOLOR_APP_BG;
    
    self.lblTitle.font=FONT_NAV_TITLE;
    self.lblTitle.textColor=UICOLOR_NAV_TITLE;
    self.lblTitle.text=MPLocalizedString(@"notifications", nil);
    
    self.btnSave.titleLabel.font=FONT_DONE_BUTTON;
    [self.btnSave setTitle:MPLocalizedString(@"save", nil) forState:UIControlStateNormal];
    
    self.tableView.scrollEnabled=NO;
}

-(void)getNotificationSetting
{
    [self startLoadingActivity];
    self.tableView.hidden=YES;
    
    [[UserAPI new]getNotificationSettingSuccess:^(Notification *obj) {
        self.objNotification=obj;
        [self.tableView reloadData];
        
        self.tableView.hidden=NO;
        [self stopLoadingActivity];
    } failure:^(NSError *error) {
         [self stopLoadingActivity];
         [self showOfflineView:YES error:error];
    }];
}

-(void)saveNotificationSetting
{
    [[UserAPI new]saveNotificationSetting:self.objNotification Success:^(User *obj) {
        
    }  failure:^(NSError *error) {
        
    }];
}

-(void)clickRetry:(id)sender
{
    if([UIAppDelegate isInternetAvailable])
    {
        [self showOfflineView:NO error:nil];
        [self getNotificationSetting];
    }
}

-(IBAction)clickBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)clickSave:(id)sender
{
}

#pragma mark UITableView Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section==0)
        return 40;
    
    return 15;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *vw =[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_SIZE.width, 40)];
    vw.backgroundColor=UICOLOR_APP_BG_GREY;
    if(section==0)
    {
        UILabel *lbl =[[UILabel alloc] initWithFrame:CGRectMake(17, 19, SCREEN_SIZE.width-17, 15)];
        lbl.backgroundColor=[UIColor clearColor];
        lbl.font=FONT_HELVETICA_REGULAR_H3;//FONT_AVENIR_PRO_DEMI_H2;
        lbl.textAlignment=NSTextAlignmentLeft;
        lbl.textColor= UICOLOR_LIGHT_TEXT;//UICOLOR_DARK_TEXT;

        lbl.text=[[self getTitleForSection:section] uppercaseString];
        [vw addSubview:lbl];
    }
    
    return vw;
}

-(NSString *)getTitleForSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return MPLocalizedString(@"push_notifications", nil);
            break;
            
        default:
            break;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==0)
        return 4;
    else if(section==1)
        return 2;
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==1&& indexPath.row==3)
        return 68;
    return 38;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section==1&& indexPath.row==3)
    {
        
        static NSString* MyIdentifier = @"DistanceCell";
        DistanceCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"DistanceCell" owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        [cell configureCellWithObj:self.objNotification];
        
        return cell;
    }
    else
    {
        static NSString* MyIdentifier = @"NotificationCell";
        NotificationCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
        
        if (cell == nil) {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil] objectAtIndex:0];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [cell configureCellWithObj:self.objNotification indexPath:indexPath];
        [cell hideLastCellSperatorIfNeeded:[self.tableView numberOfRowsInSection:indexPath.section] index:indexPath.row];
        
        return cell;
    }
    
    return nil;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
