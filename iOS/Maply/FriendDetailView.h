//
//  FriendDetailView.h
//  Maply
//
//  Created by Admin on 28/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Friend;

@protocol FriendDetailViewDelegate;

@interface FriendDetailView : UIView

@property (weak, nonatomic) id<FriendDetailViewDelegate> delegate;

@property (nonatomic, strong) Friend* friendObj;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;

@property (weak, nonatomic) IBOutlet UIView *viewBattery;
@property (weak, nonatomic) IBOutlet UILabel *lblBatteryStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmptyBattery;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBatteryCapacity;
@property (weak, nonatomic) IBOutlet UIImageView *imgBatterCharge;
@property (weak, nonatomic) IBOutlet UIImageView *imgBatteryNoCharge;

@property (weak, nonatomic) IBOutlet UIButton *btnHey;
@property (weak, nonatomic) IBOutlet UIButton *btnMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnMeetUp;

@property (weak, nonatomic) IBOutlet UIButton *btnMsgHey;
@property (weak, nonatomic) IBOutlet UIButton *btnMsgMeet;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeyWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintMessageWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintMeetUpWidth;

- (IBAction)actionHey:(id)sender;
- (IBAction)actionMessage:(id)sender;
- (IBAction)actionMeetUp:(id)sender;

- (void) loadUserInfo:(Friend *) obj;

@end

@protocol FriendDetailViewDelegate <NSObject>

- (void) sendHey:(FriendDetailView *) friendView;
- (void) sendMessage:(FriendDetailView *) friendView;
- (void) sendMeetUp:(FriendDetailView *) friendView;

@end
