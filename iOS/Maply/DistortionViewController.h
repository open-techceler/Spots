//
//  DistortionViewController.h
//  Maply
//
//  Created by admin on 6/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MyNavigationViewController.h"
#import "Friend.h"

@interface DistortionViewController : MyNavigationViewController

@property(nonatomic,strong) Friend *objFriend;
@end
