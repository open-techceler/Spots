//
//  TWMessageBarManagerStyleSheet.h
//  MediPock
//
//  Created by anand mahajan on 12/11/15.
//  Copyright (c) 2015 Sphinx. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TWMessageBarManagerStyleSheet : NSObject<TWMessageBarStyleSheet>
+ (TWMessageBarManagerStyleSheet *)styleSheet;

@end
