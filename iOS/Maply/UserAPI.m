//
//  UserProfileAPI.m
//  
//
//  Created by Admin on 28/06/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserAPI.h"
#import "UserRestAPi.h"
#import "User.h"
#import "FacebookManager.h"
#import "Notification.h"
#import "FriendsListWrapper.h"
#import "Friend.h"
#import "FSVenue.h"
#import "MessagesRestAPI.h"
#import "JourneyRestAPI.h"
#import "JourneyListWrapper.h"
#import "LocationClass.h"
#import "Message.h"
#import "MessageListWrapper.h"
#import "APNRegistrationHelpers.h"
#import "UIImage+Resize.h"
#import "MomentRestAPI.h"


@implementation UserAPI

-(void)logInWithFacebookSuccess:(void (^)(User *obj))success
                failure:(void (^)(NSError* error))failure

{
    
    /*
     "facebook_id" : "1353252345",
     "email" : "roshan@sphinx-solution.com",
     "image" : "http://www.sphinx-solution.com/wp-content/themes/enfold/images/layout/new-logox-sphinx.png",
     "device_type" : "Ios",
     "device_token" : "sdfjasklfajsdlfjsldfas",
     "name" : "Roshan Mahajan"
     */
    
     if([Utility isEmpty:[FacebookManager facebookId]])
     {
         [ToastMessage showErrorMessageAppTitleWithMessage:@"Facebook logIn error!"];
         return;

     }
    
    NSMutableDictionary *params =[NSMutableDictionary new];
    if(![Utility isEmpty:[FacebookManager facebookEmail]])
        [params setObject:[FacebookManager facebookEmail] forKey:@"email"];
    
    if(![Utility isEmpty:[FacebookManager facebookFullName]])
        [params setObject:[FacebookManager facebookFullName] forKey:@"name"];
    
    if(![Utility isEmpty:[FacebookManager facebookGender]])
        [params setObject:[FacebookManager facebookGender] forKey:@"gender"];
    
    if(![Utility isEmpty:[FacebookManager facebookBirthday]])
        [params setObject:[FacebookManager facebookBirthday] forKey:@"birthday"];
    
    [params setObject:[FacebookManager facebookId]  forKey:@"facebook_id"];
    
    if(![Utility isEmpty:[FacebookManager facebookImageUrl]])
       [params setObject:[FacebookManager facebookImageUrl] forKey:@"image"];
    
      [params setObject:@"Ios" forKey:@"device_type"];
    
     if(![Utility isEmpty:[APNRegistrationHelpers deviceTokenString]])
       [params setObject:[APNRegistrationHelpers deviceTokenString] forKey:@"device_token"];
    
    double lat=[LocationClass sharedLoctionManager].currentLocation.coordinate.latitude;
    double lng=[LocationClass sharedLoctionManager].currentLocation.coordinate.longitude;
    [params setObject:[NSNumber numberWithDouble:lat] forKey:@"latitude"];
    [params setObject:[NSNumber numberWithDouble:lng] forKey:@"longitude"];

    
    UserRestAPI *api =[[UserRestAPI alloc] initWithParams:params];
    [api POSTAction:APIRestUserLogin success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dict=(NSDictionary *)responseObject;
        User *obj=[User instanceFromDict:dict];
        if(obj)
        {
            [User setAsCurrentUser:obj];
            success (obj);
        }
        else
            failure (nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failure)
            failure(error);
    }];
}

-(void)checkUsername:(NSString*)username
                      success:(void (^)(BOOL status))success
                      failure:(void (^)(NSError* error))failure

{
    UserRestAPI *api =[UserRestAPI new];
    [api GETAction:APIRestUserCheckUsername endDynamic:username  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success (YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failure)
            failure(error);
    }];
}

-(void)uploadFaceookFriends:(NSMutableArray *)array
             success:(void (^)(BOOL status))success
             failure:(void (^)(NSError* error))failure

{
    User *objUser=[User currentUser];
    NSLog(@"array>>%@",array);
    UserRestAPI *api =[[UserRestAPI alloc]initWithArray:array];
    
    [api POSTAction:APIRestUserUploadFacebookFriends endDynamic:[NSString stringWithFormat:@"%ld",(long)objUser.user_id]
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
        success (YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failure)
            failure(error);
    }];
}

-(void)updateUsernameOnServer:(NSString*)username
                      success:(void (^)(BOOL status))success
                             failure:(void (^)(NSError* error))failure

{
   /*
    {
    "id" : 2,
    "username" : "Roshan"
    }
    */
    
    NSMutableDictionary *params =[NSMutableDictionary new];
    
    User *objUser=[User currentUser];
    [params setObject:[NSString stringWithFormat:@"%ld",(long)objUser.user_id] forKey:@"id"];
    [params setObject:username forKey:@"username"];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    [api PUTAction:APIRestUserUpdateUsername  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        objUser.username=username;
        [User setAsCurrentUser:objUser];

        success (YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failure)
            failure(error);
    }];
}

-(void)getNotificationSettingSuccess:(void (^)(Notification *obj))success
                       failure:(void (^)(NSError* error))failure

{
    User *objUser=[User currentUser];
    
    UserRestAPI *api =[UserRestAPI new];
    [api GETAction:APIRestUserNotification endDynamic:[NSString stringWithFormat:@"%ld",(long)objUser.user_id] success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *dict=(NSDictionary *)responseObject;
        Notification *obj=[Notification instanceFromDict:dict];
        success (obj);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failure)
            failure(error);
    }];
}


-(void)saveNotificationSetting:(Notification*)obj
                               Success:(void (^)(User *obj))success
                        failure:(void (^)(NSError* error))failure

{
    /*
     "id" : 2,
     "user_id" : 2,
     "message": true,
     "maply_news": true,
     "friend_request": true,
     "live_request": false,
     "live_update": true,
     "llive_friends_nearby": true,
     "maximum_distance": 2 */
    
    // [params setObject:[[FacebookManager sharedInstance] getFBAccessToken] forKey:@"access_token"];
    
    NSMutableDictionary *params =[NSMutableDictionary new];
    
    
    User *objUser=[User currentUser];
    [params setObject:[NSString stringWithFormat:@"%ld",(long)objUser.user_id] forKey:@"user_id"];
    
    [params setObject:[NSNumber numberWithBool:obj.message_noti] forKey:@"message"];
    [params setObject:[NSNumber numberWithBool:obj.maply_news] forKey:@"maply_news"];
    [params setObject:[NSNumber numberWithBool:obj.friend_request] forKey:@"friend_request"];
    [params setObject:[NSNumber numberWithBool:obj.live_request] forKey:@"live_request"];
    [params setObject:[NSNumber numberWithBool:obj.live_update] forKey:@"live_update"];
    [params setObject:[NSNumber numberWithBool:obj.llive_friends_nearby] forKey:@"llive_friends_nearby"];
    [params setObject:[NSString stringWithFormat:@"%ld",(long)obj.maximum_distance] forKey:@"maximum_distance"];
    [params setObject:[NSNumber numberWithBool:obj.sound] forKey:@"sound"];

    
    
    UserRestAPI *api =[[UserRestAPI alloc] initWithParams:params];
    [api PUTAction:APIRestUserNotification success:^(AFHTTPRequestOperation *operation, id responseObject) {
       // NSDictionary *dict=(NSDictionary *)responseObject;
        success (nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failure)
            failure(error);
    }];
}

-(void) updateUserPhotoWithPhoto:(UIImage *)imgPhoto
                        success:(void (^)(NSString *image_url))success
                        failure:(void (^)(NSError* error))failure
{
    
    NSMutableDictionary *params=[NSMutableDictionary new];
    [params setObject:[VVBaseUserDefaults userId] forKey:@"id"];
    
    UserRestAPI* api = [[UserRestAPI alloc] initWithParams:params];
    
    [api POSTAction:APIRestUserUploadUserImage
        constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
        {
    
            if(imgPhoto){
                NSData *imageData = UIImageJPEGRepresentation(imgPhoto,0.7);
                [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.jpeg" mimeType:@"image/jpeg"];
            }
        }
        success:^(AFHTTPRequestOperation *operation, id responseObject)
        {
                NSDictionary *dict=(NSDictionary *)responseObject;
                success ([dict objectForKey:@"image"]);
            
            }
            failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                MPLog(@"error>>%@",error);
                failure(error);
            }];
}

-(void)updateUserProfile:(User *)objUser
                      success:(void (^)(BOOL status))success
                      failure:(void (^)(NSError* error))failure

{
    /*
     {
     "id" : 2,
     "username" : "Roshan",
     "email" : "roshan@sphinx-solution.com",
     "name" : "Roshan Mahajan"
     }
     */
    
    if([Utility isEmpty:objUser.name] || [Utility isEmpty:objUser.email] || [VVBaseUserDefaults userId].integerValue==0)
        return;
    
    NSMutableDictionary *params =[NSMutableDictionary new];
    
    [params setObject:[VVBaseUserDefaults userId] forKey:@"id"];
    [params setObject:objUser.name forKey:@"name"];
    [params setObject:objUser.email forKey:@"email"];
    if(![Utility isEmpty:objUser.image])
        [params setObject:objUser.image forKey:@"image"];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    [api PUTAction:APIRestUserUpdateUsername  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        User *obj=[User currentUser];
        obj.name=objUser.name;
        obj.email=objUser.email;
        
        [User setAsCurrentUser:obj];
        
        success (YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failure)
            failure(error);
    }];
}

-(void)getFriendsList:(NSInteger)offset
              keyWord:(NSString *)keyword
                    success:(void (^)(FriendsListWrapper *objWrapper))success
                    failure:(void (^)(NSError* error))failure

{
    NSMutableDictionary *params=[NSMutableDictionary new];
    if(![Utility isEmpty:keyword])
        [params setObject:keyword forKey:@"keyword"];
    NSString *endPath=[NSString stringWithFormat:@"%@/%ld",[VVBaseUserDefaults userId],(long)offset];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    
    [api GETAction:APIRestUserSearchFriends endDynamic:endPath
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 NSDictionary *dict=(NSDictionary *)responseObject;
                FriendsListWrapper *obj=[FriendsListWrapper instanceFromDict:dict];
                success (obj);
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if(failure)
                    failure(error);
            }];
}

-(void)updateUserAction:(NSMutableArray  *)lsActions
              success:(void (^)(FriendsListWrapper *objWrapper))success
              failure:(void (^)(NSError* error))failure

{
    /*
     {{base_url}}/users/3/action
     {
     "user_id" : 2
     "action" : Block
     }
     Add
     Live
     Accept
     Decline
     */
    
    //NSMutableDictionary *params=[NSMutableDictionary new];
   
    NSMutableArray *array=[NSMutableArray new];
    
    for (Friend *obj in lsActions)
    {
        NSDictionary *dict=@{@"action":obj.status_type,
                             @"user_id":[NSNumber numberWithInteger:obj.friend_id],
                             @"time":([Utility isEmpty:obj.time]?@"":obj.time)
                             };
        [array addObject:dict];
    }
    
   // if(![Utility isEmpty:action])
     // [params setObject:action forKey:@"action"];
    
    //[params setObject:[NSNumber numberWithInteger:friend_id] forKey:@"user_id"];
    
  //  NSString *endPath=[NSString stringWithFormat:@"%@/%@",[VVBaseUserDefaults userId],@"action"];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithArray:array];
    
    [api PUTAction:APIRestUserAction endDynamic:[NSString stringWithFormat:@"%@",[VVBaseUserDefaults userId]]
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               FriendsListWrapper *obj=[FriendsListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)getBlockedFriendsList:(NSInteger)offset
              success:(void (^)(FriendsListWrapper *objWrapper))success
              failure:(void (^)(NSError* error))failure

{
    NSMutableDictionary *params=[NSMutableDictionary new];
    NSString *endPath=[NSString stringWithFormat:@"%@/block/%ld",[VVBaseUserDefaults userId],(long)offset];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    
    [api GETAction:APIRestUserBlock endDynamic:endPath
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               FriendsListWrapper *obj=[FriendsListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)updateUserStatusSuccess: (NSNumber*)offlineHours success:(void (^)(BOOL status))success
                 failure:(void (^)(NSError* error))failure

{
    /*
     {
     "id" : 2,
     "username" : "Roshan",
     "email" : "roshan@sphinx-solution.com",
     "name" : "Roshan Mahajan"
     Live, Away
     }
     */
     User *obj=[User currentUser];
    
    NSMutableDictionary *params =[NSMutableDictionary new];
    NSString *status=nil;
    if([obj.status isEqualToString:@"Away"]) {
        status=@"Live";
    } else {
        status=@"Away";
        [params setObject:offlineHours forKey:@"offline_hours"];
    }

    [params setObject:[VVBaseUserDefaults userId] forKey:@"id"];
    [params setObject:status forKey:@"status"];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    [api PUTAction:APIRestUserUpdateUsername  success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        User *obj=[User currentUser];
        obj.status=status;
        [User setAsCurrentUser:obj];
        
        success (YES);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if(failure)
            failure(error);
    }];
}

-(void)getFriendsRequest:(NSInteger)offset
              success:(void (^)(FriendsListWrapper *objWrapper))success
              failure:(void (^)(NSError* error))failure

{
    NSString *endPath=[NSString stringWithFormat:@"%@/requests",[VVBaseUserDefaults userId]];

    UserRestAPI *api =[UserRestAPI new];
    
    [api GETAction:APIRestUserRequests endDynamic:endPath
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               FriendsListWrapper *obj=[FriendsListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)getFriends:(NSInteger)offset
                    keyWord:(NSString *)keyword
                 success:(void (^)(FriendsListWrapper *objWrapper))success
                 failure:(void (^)(NSError* error))failure

{
    NSMutableDictionary *params=[NSMutableDictionary new];
    if(![Utility isEmpty:keyword])
        [params setObject:keyword forKey:@"keyword"];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    NSString *endPath=[NSString stringWithFormat:@"%@/friends/%ld",[VVBaseUserDefaults userId],(long)offset];

    [api GETAction:APIRestUserFriends endDynamic:endPath
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
                FriendsListWrapper *obj=[FriendsListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void) uploadSnapToServer:(UIImage *)imgPhoto
            withBannerImag:(UIImage *)bannerImg
                  withText:(NSString *)text
              withVideoUrl:(NSURL *)videoUrl
            isMyJourney:(BOOL)isMyJourney
                      withVenue:(FSVenue *)objVenue
                        cameraMode:(CameraMode)cameraMode
                        success:(void (^)(NSInteger uploaded_id))success
                        failure:(void (^)(NSError* error))failure
{
    
    NSMutableDictionary *params=[NSMutableDictionary new];
    [params setObject:[VVBaseUserDefaults userId] forKey:@"user_id"];
    
    [params setObject:[NSNumber numberWithBool:isMyJourney] forKey:@"is_jounery"];

    if(![Utility isEmpty:objVenue.venueId])
          [params setObject:objVenue.venueId forKey:@"location_id"];
    
    if(![Utility isEmpty:objVenue.name])
        [params setObject:objVenue.name forKey:@"location_name"];
    
    if(![Utility isEmpty:objVenue.location.address])
        [params setObject:objVenue.location.address forKey:@"address"];
    
    if(![Utility isEmpty:text])
        [params setObject:text forKey:@"text"];
    
      MPLog(@"objVenue.location.coordinate.latitude>>%f",objVenue.location.coordinate.latitude);
    MPLog(@"objVenue.location.coordinate.longitude>>%f",objVenue.location.coordinate.longitude);

    [params setObject:[NSNumber numberWithDouble:objVenue.location.coordinate.latitude] forKey:@"latitude"];
    [params setObject:[NSNumber numberWithDouble:objVenue.location.coordinate.longitude] forKey:@"longitude"];
    
    
    MessagesRestAPI* api = [[MessagesRestAPI alloc] initWithParams:params];
    
    [api POSTAction:APIRestMessages
        constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            
            if(imgPhoto){
               // UIImage * resizedImg = [imgPhoto resizedImage:CGSizeMake(320, 480) interpolationQuality:kCGInterpolationMedium];
               // NSData *imageData = UIImageJPEGRepresentation(imgPhoto,0.7);
                if(cameraMode==CameraModeVideo || cameraMode==CameraModeGIF)
                {
                    NSData *imageData = UIImagePNGRepresentation(imgPhoto);
                    [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.png" mimeType:@"image/png"];
                }
                else
                {
                   NSData *imageData = UIImageJPEGRepresentation(imgPhoto,0.7);
                    [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.jpeg" mimeType:@"image/jpeg"];
                }
            }
            
            if(bannerImg){
//                NSInteger max_width=414;
//                UIImage * resizedImg = [bannerImg resizedImage:CGSizeMake(max_width, max_width) interpolationQuality:kCGInterpolationMedium];
                
                //https://trello.com/c/PXjJg3Lb/121-the-pictures-on-my-journey-seems-streched
                UIImage * resizedImg = [Utility bannerImageFrom:bannerImg];
                
                NSData *imageData = UIImageJPEGRepresentation(resizedImg,0.7);
                if(imageData!=nil)
                {
                    [formData appendPartWithFileData:imageData name:@"banner_image" fileName:@"banner_image.jpeg" mimeType:@"image/jpeg"];
                }
            }
            
            if(cameraMode==CameraModeVideo || cameraMode==CameraModeGIF)
            {
                if(![Utility isEmpty:[NSString stringWithFormat:@"%@",videoUrl]]){
                    NSData *videoData = [NSData dataWithContentsOfURL:videoUrl];
                    [formData appendPartWithFileData:videoData name:@"video" fileName:@"video.mov" mimeType:@"video/quicktime"];

                }
            }
            
            /*if(cameraMode==CameraModeGIF)
            {
                if(![Utility isEmpty:[NSString stringWithFormat:@"%@",videoUrl]]){
                    NSData *videoData = [NSData dataWithContentsOfURL:videoUrl];
                    [formData appendPartWithFileData:videoData name:@"video" fileName:@"image.gif" mimeType:@"image/gif"];
                }

            }*/
            
        }
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSDictionary *dict=(NSDictionary *)responseObject;
                MPLog(@"dict>>%@",dict);
                
                if (success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        success([[dict objectForKey:@"id"]integerValue]);
                    });
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                MPLog(@"error>>%@",error);
                if (failure) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        failure(error);
                    });
                }
            }];
}

-(void)getSendToFriendSuccess:(void (^)(FriendsListWrapper *objWrapper))success
          failure:(void (^)(NSError* error))failure

{
    
    UserRestAPI *api =[UserRestAPI new];
    NSString *endPath=[NSString stringWithFormat:@"%@/friends",[VVBaseUserDefaults userId]];
    
    [api GETAction:APIRestUserFriends endDynamic:endPath
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               FriendsListWrapper *obj=[FriendsListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)sendMessageToFriends:(NSMutableArray *)array
                  withMsgId:(NSInteger)msgId
                    success:(void (^)(BOOL status))success
                    failure:(void (^)(NSError* error))failure

{
    NSLog(@"array>>%@",array);
    MessagesRestAPI *api =[[MessagesRestAPI alloc]initWithArray:array];

    [api POSTAction:APIRestMessagesSend endDynamic:[NSString stringWithFormat:@"%ld",(long)msgId]
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                if (success) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        success (YES);
                    });
                }
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if(failure) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        failure(error);
                    });
                }
            }];
}


-(void)getJourneys:(NSInteger)offset
          success:(void (^)(JourneyListWrapper *objWrapper))success
          failure:(void (^)(NSError* error))failure

{
    
    JourneyRestAPI *api =[JourneyRestAPI new];
    NSString *endPath=[NSString stringWithFormat:@"%@/%ld",[VVBaseUserDefaults userId],(long)offset];
    [api GETAction:APIRestJourney endDynamic:endPath
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               JourneyListWrapper *obj=[JourneyListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)getNearbyUsers:(NSInteger)offset
           success:(void (^)(FriendsListWrapper *objWrapper))success
           failure:(void (^)(NSError* error))failure

{
    double lat=[LocationClass sharedLoctionManager].currentLocation.coordinate.latitude;
    double lng=[LocationClass sharedLoctionManager].currentLocation.coordinate.longitude;
    
    NSMutableDictionary *params=[NSMutableDictionary new];
    [params setObject:[NSNumber numberWithDouble:lat] forKey:@"latitude"];
    [params setObject:[NSNumber numberWithDouble:lng] forKey:@"longitude"];

    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    [api GETAction:APIRestUserNearby endDynamic:[NSString stringWithFormat:@"%@",[VVBaseUserDefaults userId]]
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               FriendsListWrapper *obj=[FriendsListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void) replyToFriendWithImage:(UIImage *)imgPhoto
                     withFriendId:(NSInteger)friend_id
                        withVenue:(FSVenue *)objVenue
                         withText:(NSString *)text
                          success:(void (^)(NSString *image_url))success
                          failure:(void (^)(NSError* error))failure
{
    /*
     location_id"
     latitude"
     "longitude"
     "location_name"
     "address"
     "user_id"
     "text"
     */
        
    NSMutableDictionary *params=[NSMutableDictionary new];
    [params setObject:[NSNumber numberWithInteger:friend_id] forKey:@"user_id"];
    
    
    if(![Utility isEmpty:objVenue.venueId])
    {
        [params setObject:objVenue.venueId forKey:@"location_id"];
        [params setObject:[NSNumber numberWithDouble:objVenue.location.coordinate.latitude] forKey:@"latitude"];
        [params setObject:[NSNumber numberWithDouble:objVenue.location.coordinate.longitude] forKey:@"longitude"];
        
        if(![Utility isEmpty:objVenue.name])
            [params setObject:objVenue.name forKey:@"location_name"];
        
        if(![Utility isEmpty:objVenue.location.address])
            [params setObject:objVenue.location.address forKey:@"address"];
    }
    
    if(![Utility isEmpty:text])
        [params setObject:text forKey:@"text"];
    
        MessagesRestAPI* api = [[MessagesRestAPI alloc] initWithParams:params];
        
        [api POSTAction:APIRestMessages
           endDynamic:[NSString stringWithFormat:@"%@",[VVBaseUserDefaults userId]]
            constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
            {
             
             if(imgPhoto){
                 NSData *imageData = UIImageJPEGRepresentation(imgPhoto,0.7);
                 [formData appendPartWithFileData:imageData name:@"image" fileName:@"image.jpeg" mimeType:@"image/jpeg"];
             }
            }
                success:^(AFHTTPRequestOperation *operation, id responseObject)
                {
                    //NSDictionary *dict=(NSDictionary *)responseObject;
                    if (success) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            success (nil);
                        });
                    }
             
                }
                failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                    MPLog(@"error>>%@",error);
                    if (failure) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            failure(error);
                        });
                    }
                }];
}

-(void)getMessagesCount:(NSInteger)offset
           success:(void (^)(NSInteger msgCount))success
           failure:(void (^)(NSError* error))failure

{
    MessagesRestAPI *api =[MessagesRestAPI new];
    [api GETAction:APIRestMessagesUnread endDynamic:[NSString stringWithFormat:@"%@",[VVBaseUserDefaults userId]]
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               NSInteger count=[[dict objectForKey:@"count"]integerValue];
               success (count);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)getJourneysDetailsWithUserId:(NSInteger)userId
            withOffset:(NSInteger)offset
           success:(void (^)(MessageListWrapper *objWrapper))success
           failure:(void (^)(NSError* error))failure

{
    ///journey/{journeyId}/view/{userId}/{offset}
    JourneyRestAPI *api =[JourneyRestAPI new];
  //  NSString *endPath=[NSString stringWithFormat:@"%ld/%ld",(long)journeyId,(long)offset];

    NSString *endPath=[NSString stringWithFormat:@"%ld/view/%@/%ld",(long)userId,[VVBaseUserDefaults userId],(long)offset];
    [api GETAction:APIRestJourneyView endDynamic:endPath
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               MessageListWrapper *obj=[MessageListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)getDirectMessages:(NSInteger)offset
                success:(void (^)(JourneyListWrapper *objWrapper))success
                failure:(void (^)(NSError* error))failure

{
    
    MessagesRestAPI *api =[MessagesRestAPI new];
    [api GETAction:APIRestMessageDirect endDynamic:[NSString stringWithFormat:@"%@",[VVBaseUserDefaults userId]]
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               JourneyListWrapper *obj=[JourneyListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)getSentMessages:(NSInteger)offset
                success:(void (^)(JourneyListWrapper *objWrapper))success
                failure:(void (^)(NSError* error))failure

{
    
    MessagesRestAPI *api =[MessagesRestAPI new];
    [api GETAction:APIRestMessagesSent endDynamic:[NSString stringWithFormat:@"%@",[VVBaseUserDefaults userId]]
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               JourneyListWrapper *obj=[JourneyListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)getChatOfFriend:(NSInteger)friend_id
                            withOffset:(NSInteger)offset
                            withComeFrom:(JourneyDetailsComeFrom)comefrom
                               success:(void (^)(MessageListWrapper *objWrapper))success
                               failure:(void (^)(NSError* error))failure

{
    
    UserRestAPI *api =[UserRestAPI new];
    NSString *endPath=nil;
    if(comefrom==JourneyDetailsComeFromDirect)
        endPath=[NSString stringWithFormat:@"%@/direct/%ld/%ld",[VVBaseUserDefaults userId],(long)friend_id,(long)offset];
    else
        endPath=[NSString stringWithFormat:@"%@/sent/%ld/%ld",[VVBaseUserDefaults userId],(long)friend_id,(long)offset];
    
    [api GETAction:APIRestUserChat endDynamic:endPath
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               MessageListWrapper *obj=[MessageListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)sendPushToken:(NSString *)push_token
               success:(void (^)(MessageListWrapper *objWrapper))success
               failure:(void (^)(NSError* error))failure

{
    NSMutableDictionary *params=[NSMutableDictionary new];
    [params setObject:[VVBaseUserDefaults userId] forKey:@"id"];
    [params setObject:push_token forKey:@"push_token"];
    [params setObject:@"Ios" forKey:@"device_type"];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    [api PUTAction:APIRestUserPushToken
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
              // NSDictionary *dict=(NSDictionary *)responseObject;
               success (nil);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)updateUserLocation:(CLPlacemark *)placemark
             success:(void (^)(MessageListWrapper *objWrapper))success
             failure:(void (^)(NSError* error))failure

{
    /*
     "latitude" :
     "longitude" :
     "location"
     */
    
    NSArray *formatted = [placemark.addressDictionary objectForKey:@"FormattedAddressLines"];
    NSString *full_address=[formatted componentsJoinedByString:@", "];
    
    MPLog(@"full_address %@",full_address);
    MPLog(@"locality %@",placemark.locality);
    MPLog(@"country %@",placemark.country);
    
    NSMutableArray *arr=[NSMutableArray new];
    if(![Utility isEmpty:placemark.locality])
        [arr addObject:placemark.locality];
    if(![Utility isEmpty:placemark.country])
        [arr addObject:placemark.country];
   
    NSString *locationName=[arr componentsJoinedByString:@",  "];
    
    if([Utility isEmpty:locationName] && [Utility isEmpty:full_address])
    {
        failure(nil);
        return;
    }
    
    NSMutableDictionary *params=[NSMutableDictionary new];
    [params setObject:[VVBaseUserDefaults userId] forKey:@"id"];
    if(![Utility isEmpty:locationName])
        [params setObject:locationName forKey:@"location"];
    
    if(![Utility isEmpty:full_address])
        [params setObject:full_address forKey:@"full_address"];
    
    double lat=[LocationClass sharedLoctionManager].currentLocation.coordinate.latitude;
    double lng=[LocationClass sharedLoctionManager].currentLocation.coordinate.longitude;
    [params setObject:[NSNumber numberWithDouble:lat] forKey:@"latitude"];
    [params setObject:[NSNumber numberWithDouble:lng] forKey:@"longitude"];

    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    [api PUTAction:APIRestUserLocation
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               success (nil);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)markMomentAsRead:(NSMutableArray *)array
                  withMsgId:(NSInteger)msgId
                    success:(void (^)(BOOL status))success
                    failure:(void (^)(NSError* error))failure

{
    // base_url + /messages/view/{userId}?ids=1,43,5,6,7,8
    NSString *ids = [array componentsJoinedByString:@","];
    MPLog(@"ids>>%@",ids);
    NSMutableDictionary *params=[NSMutableDictionary new];
    [params setObject:ids forKey:@"ids"];
   
    MessagesRestAPI *api =[[MessagesRestAPI alloc]initWithParams:params];
    
    [api DELETEAction:APIRestMessagesView
           endDynamic:[NSString stringWithFormat:@"%@",[VVBaseUserDefaults userId]]
            success:^(AFHTTPRequestOperation *operation, id responseObject) {
                success (YES);
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
                if(failure)
                    failure(error);
            }];
}

-(void)deleteMyJourneyWithMsgId:(NSInteger)msgId
                success:(void (^)(BOOL status))success
                failure:(void (^)(NSError* error))failure

{
    // base_url + + /moment/{messageId}/user/{userId}
   
    NSString *endPath=nil;
    endPath=[NSString stringWithFormat:@"%ld/user/%@",(long)msgId,[VVBaseUserDefaults userId]];
    
    MomentRestAPI *api =[[MomentRestAPI alloc]init];
    
    [api DELETEAction:APIRestMessagesView
           endDynamic:endPath
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  success (YES);
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  if(failure)
                      failure(error);
              }];
}

-(void)getEventsAndMyJourneySuccess:(void (^)(FriendsListWrapper *objWrapper))success
              failure:(void (^)(NSError* error))failure

{
    //base_url + users/nearby-event/{userId}?latitude=,longitude

    double lat=[LocationClass sharedLoctionManager].currentLocation.coordinate.latitude;
    double lng=[LocationClass sharedLoctionManager].currentLocation.coordinate.longitude;
    
    if(!lat && !lng)
    {
        MPLog(@"Didnt find location");
    }
    
    NSMutableDictionary *params=[NSMutableDictionary new];
    [params setObject:[NSNumber numberWithDouble:lat] forKey:@"latitude"];
    [params setObject:[NSNumber numberWithDouble:lng] forKey:@"longitude"];
    
    UserRestAPI *api =[[UserRestAPI alloc]initWithParams:params];
    [api GETAction:APIRestUserNearbyEvent endDynamic:[NSString stringWithFormat:@"%@",[VVBaseUserDefaults userId]]
           success:^(AFHTTPRequestOperation *operation, id responseObject) {
               NSDictionary *dict=(NSDictionary *)responseObject;
               FriendsListWrapper *obj=[FriendsListWrapper instanceFromDict:dict];
               success (obj);
               
           } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
               
               if(failure)
                   failure(error);
           }];
}

-(void)saveMomentFor24HWithMsgId:(NSInteger)msgId
                success:(void (^)(BOOL status))success
                failure:(void (^)(NSError* error))failure

{
    // base_url + messages/{messageId}/save/{userId}
    NSString *endPath=[NSString stringWithFormat:@"%ld/save/%@",(long)msgId,[VVBaseUserDefaults userId]];

    MessagesRestAPI *api =[[MessagesRestAPI alloc]init];
    
    [api POSTAction:APIRestMessagesSave
           endDynamic:endPath
              success:^(AFHTTPRequestOperation *operation, id responseObject) {
                  success (YES);
                  
              } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                  
                  if(failure)
                      failure(error);
              }];
}

@end
