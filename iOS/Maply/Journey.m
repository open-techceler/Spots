//
//  Journey.m
//  Maply
//
//  Created by admin on 2/23/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "Journey.h"
#import "User.h"
#import "Moment.h"

@implementation Journey
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // override this in the model if property names in JSON don't match model
    NSDictionary *mapping = [NSDictionary mtl_identityPropertyMapWithModel:self];
    return [mapping mtl_dictionaryByAddingEntriesFromDictionary:@{@"journey_id":@"id"}];
}

+ (NSValueTransformer*)userJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[User class]];
}

+ (NSValueTransformer*)momentJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:[Moment class]];
}
@end
