//
//  EditImageViewController.h
//  Maply
//
//  Created by admin on 2/9/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SCRecorder.h"

@interface EditImageViewController : MyNavigationViewController

@property(nonatomic,strong)UIImage *originalImage;
@property(nonatomic,strong)NSURL *videoUrl,*gifUrl;

@property (nonatomic) CameraMainComeFrom comefrom;
@property (nonatomic) NSInteger reply_userId;
@property (nonatomic, strong) NSString* reply_userImgUrl;

@property (strong, nonatomic) SCRecordSession *recordSession;
@property (nonatomic) CameraMode camera_mode;
@property (nonatomic, strong)NSMutableArray *arrayImages;
@end
