
#import "MMDateFormatter.h"

@implementation MMDateFormatter

#pragma mark - <VVFormatterImplementation>

- (NSDateFormatter*)formatterInstance
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    return formatter;
}

- (NSString*)cacheKey
{
    return @"NSDateFormatter";
}

#pragma mark - Date formatting methods

- (NSString*)formatDate:(NSDate*)date format:(NSString*)formatString
{
    NSString* result = [self format:^id(NSDateFormatter* formatter) {
        formatter.dateFormat = formatString;
        NSString *res = [formatter stringFromDate:date];
        return res;
    }];
    return result;
}

- (NSString*)localeAwareFormatDate:(NSDate*)date format:(NSString*)formatString
{
    NSString* result = [self format:^id(NSDateFormatter* formatter) {
        NSString *localizedFormat = [NSDateFormatter dateFormatFromTemplate:formatString options:0 locale:formatter.locale];
        formatter.dateFormat = localizedFormat;
        NSString *res = [formatter stringFromDate:date];
        return res;
    }];
    return result;
}

- (NSDate*)parseDate:(NSString*)dateString format:(NSString*)formatString
{
    NSDate* result = [self format:^id(NSDateFormatter* formatter) {
        formatter.dateFormat = formatString;
        NSDate *res = [formatter dateFromString:dateString];
        return res;
    }];
    return result;
}

- (NSDate*)localeAwareParseDate:(NSString*)dateString format:(NSString*)formatString
{
    NSDate* result = [self format:^id(NSDateFormatter* formatter) {
        NSString *localizedFormat = [NSDateFormatter dateFormatFromTemplate:formatString options:0 locale:formatter.locale];
        formatter.dateFormat = localizedFormat;
        NSDate *res = [formatter dateFromString:dateString];
        return res;
    }];
    return result;
}

#pragma marks -- UTC date functions
-(NSString*)utc_timeAgoWithUTCDate:(NSString*)strDate
{
    NSDate *date =[self utc_dateFromString:strDate format:kDateFormatTypeZFormatted];
    return [self timeAgo:date];
}

-(NSDate *)utc_dateFromString:(NSString *)dateString                   format:(NSString*)format;
{
    
    NSDate *result = [self format:^id(NSDateFormatter* formatter) {
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [formatter setDateFormat:format];
        return [formatter dateFromString:dateString];
    }];
    return result;
    
}

-(NSString*)utc_stringFromDate:(NSDate*)date format:(NSString*)format
{
    NSString *result = [self format:^id(NSDateFormatter* formatter) {
        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [formatter setDateFormat:kDateFormatTypeZFormatted];
        return [formatter stringFromDate:date];
    }];
    return result;
}

#pragma marks -- Helper functions

- (NSString *)timeAgo:(NSDate *)date {
    NSTimeInterval distanceBetweenDates = [date timeIntervalSinceDate:[NSDate date]] * (-1);
    NSInteger distance = (NSInteger)floorf(distanceBetweenDates);
    if (distance <= 0) {
        return MPLocalizedString(@"now", nil);
    }
    else if (distance < SECONDS_IN_A_MINUTE) {
        return  [NSString stringWithFormat:@"%lds",(long)distance];
    }
    else if (distance < SECONDS_IN_A_HOUR) {
        distance = distance / SECONDS_IN_A_MINUTE;
        return   [NSString stringWithFormat:@"%ldm",(long)distance];
    }
    else if (distance < SECONDS_IN_A_DAY) {
        distance = distance / SECONDS_IN_A_HOUR;
        return   [NSString stringWithFormat:@"%ldh",(long)distance];
    }
    else if (distance < SECONDS_IN_A_WEEK) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"EEE"];
        return  [dateFormatter stringFromDate:date];
    }
    else if (distance < SECONDS_IN_A_MONTH_OF_30_DAYS){
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"d MMM"];
        return  [dateFormatter stringFromDate:date];

        /*distance = distance / SECONDS_IN_A_DAY;
        return  [NSString stringWithFormat:@"%ldd",distance];*/
    }
    else if (distance < SECONDS_IN_A_YEAR_OF_MONTH_OF_30_DAYS) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"d MMM"];
        return  [dateFormatter stringFromDate:date];

        /*
        distance = distance / SECONDS_IN_A_MONTH_OF_30_DAYS;
        return   [NSString stringWithFormat:@"%ldmth",distance];*/
    } else {
        distance = distance / SECONDS_IN_A_YEAR_OF_MONTH_OF_30_DAYS;
        return   [NSString stringWithFormat:@"%ldy",(long)distance];;
    }
}

@end
