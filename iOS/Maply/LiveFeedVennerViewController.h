//
//  LiveFeedVennerViewController.h
//  Maply
//
//  Created by Admin on 28/09/2016.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Friend;

@protocol LiveFeedVennerViewControllerDelegate;

@interface LiveFeedVennerViewController : UIViewController

@property (strong, nonatomic) NSArray* arrayTotalFriends;

@property (nonatomic, weak) id<LiveFeedVennerViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imgOverlay;
@property (weak, nonatomic) IBOutlet UIView *viewCanvas;

@property (weak, nonatomic) IBOutlet UILabel *lblLiveStatus;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintViewBottom;

@property (weak, nonatomic) IBOutlet UIButton *btnPrev;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

- (IBAction)actionPrev:(id)sender;
- (IBAction)actionNext:(id)sender;

@end

@protocol LiveFeedVennerViewControllerDelegate <NSObject>

- (void) dismissFeedVennerViewCon:(LiveFeedVennerViewController *) viewCon;
- (void) tappedLiveFriend:(LiveFeedVennerViewController *) viewCon selectedFriend:(Friend *) obj;

@end
