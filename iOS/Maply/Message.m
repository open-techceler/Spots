//
//  Message.m
//  Maply
//
//  Created by admin on 2/25/16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "Message.h"
#import "User.h"

@implementation Message

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    // override this in the model if property names in JSON don't match model
    NSDictionary *mapping = [NSDictionary mtl_identityPropertyMapWithModel:self];
    return [mapping mtl_dictionaryByAddingEntriesFromDictionary:@{@"message_id":@"id"}];
}
+ (NSValueTransformer*)userJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:[User class]];
}
@end
