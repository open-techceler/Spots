//
//  SelectLanguageViewController.m
//  VivinoV2
//
//  Created by Admin on 18/04/14.
//
//

#import "SelectLanguageViewController.h"
#import "CustomListCell.h"

@interface SelectLanguageViewController () {
  IBOutlet UITableView *tblLanguage;
  NSInteger selected_language;
}
@end

@implementation SelectLanguageViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view from its nib.

    [self setFontAndColor];
    [self setUpNavigationBar];
    
    selected_language = [LanguageManager getPerferedLanguageInInteger];
}
-(void)setFontAndColor
{
    self.view.backgroundColor=UICOLOR_APP_BG_GREY;
    tblLanguage.backgroundColor=UICOLOR_APP_BG_GREY;
  
}
-(void)setUpNavigationBar
{
    self.Nav.lblTitle.text=MPLocalizedString(@"select_language", nil);
    [self.Nav setBackButtonAsLeftButton];
    [self.Nav setNavigationLayout:NO];
    
}

- (void)clickCancel:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)clickDone:(id)sender {
  [LanguageManager setLanguageWithIntType:selected_language];

  //[[NSNotificationCenter defaultCenter] postNotificationName:kApplLanguageChangedNotification  object:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return 44;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView
    numberOfRowsInSection:(NSInteger)section {
  return 1;
}

- (CGFloat)tableView:(UITableView *)tableView
    heightForHeaderInSection:(NSInteger)section {
  return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempHeaderView =[UIView new];
    tempHeaderView.backgroundColor=[UIColor clearColor];
   return tempHeaderView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

  CustomListCell *cell = nil;
  NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomListCell"
                                               owner:self
                                             options:nil];

    #ifdef __IPHONE_2_1
        cell = (CustomListCell *)[nib objectAtIndex:0];
    #else
        cell = (CustomListCell *)[nib objectAtIndex:1];
    #endif
 
   
 
  cell.vwSeperator.backgroundColor = UICOLOR_SEPERATOR;
  switch (indexPath.row) {
  case 0: {

    cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"en"];
    break;
  }

  case 1: {
    cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"ca"];
    break;
  }
  case 2: {
    cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"da"];
    break;
  }
  case 3: {
    cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"de"];
    break;
  }
  case 4: {
    cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"es"];
    break;
  }
  case 5: {
    cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"fr"];
    break;
  }
  case 6: {
      cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"it"];
      break;
  }
  case 7: {
      cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"ja"];
      break;
  }
  case 8: {
      cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"nl"];
      break;
  }
  case 9: {
      cell.lblTitle.text = [LanguageManager nativeLanguageNameForLocale:@"pt"];
      break;
  }
  default:
    break;
  }

  if (indexPath.row == selected_language)
       cell.accessoryType=UITableViewCellAccessoryCheckmark;
   // cell.accessoryImageView.hidden = NO;

  return cell;
}

- (void)tableView:(UITableView *)tableView
    didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  selected_language = indexPath.row;
  [tblLanguage reloadData];

  [LanguageManager setLanguageWithIntType:selected_language];
 
    [_delegate selectLanguageSuccessfully:selected_language];

    [self.navigationController popViewControllerAnimated:YES];
}




@end
