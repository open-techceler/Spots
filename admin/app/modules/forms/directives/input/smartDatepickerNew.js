define(['modules/forms/module', 'jquery-ui'], function (module) {
    "use strict";

    return module.registerDirective('smartDatepickerNew', function ($timeout) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, tElement, tAttributes, ngModelCtrl) {

                var stop_timeout;
                return scope.$watch(function () {
                    return ngModelCtrl.$modelValue;
                }, function (name) {

                    stop_timeout = $timeout(function () {
                        console.log(tAttributes);
                        tElement.removeAttr('smartDatepickerNew');

                        var onSelectCallbacks = [];
                        if (tAttributes.minRestrict) {
                            onSelectCallbacks.push(function (selectedDate) {
                                $(tAttributes.minRestrict).datepicker('option', 'minDate', selectedDate);
                            });
                        }
                        if (tAttributes.maxRestrict) {
                            onSelectCallbacks.push(function (selectedDate) {
                                $(tAttributes.maxRestrict).datepicker('option', 'maxDate', selectedDate);
                            });
                        }

                        var options = {
                            prevText: '<i class="fa fa-chevron-left"></i>',
                            nextText: '<i class="fa fa-chevron-right"></i>',
                            onSelect: function (selectedDate) {
                                ngModelCtrl.$setViewValue(selectedDate);
                                angular.forEach(onSelectCallbacks, function (callback) {
                                    callback.call(this, selectedDate);
                                })
                            }
                        };

                        if (tAttributes.minDate) {
                            console.log(tAttributes.minDate);
                            var date1 = tAttributes.minDate.toString().split('/');
                            console.log(date1[0] + " " + date1[1] + "  " + date1[2]);
                            options.minDate = new Date(date1[1] + "/" + date1[0] + "/" + date1[2]);
                            console.log(options.minDate);
                        }


                        //options.maxDate = new Date();

                        if (tAttributes.numberOfMonths)
                            options.numberOfMonths = parseInt(tAttributes.numberOfMonths);

                        if (tAttributes.dateFormat)
                            options.dateFormat = tAttributes.dateFormat;

                        if (tAttributes.defaultDate)
                            options.defaultDate = tAttributes.defaultDate;

                        if (tAttributes.changeMonth)
                            options.changeMonth = tAttributes.changeMonth == "true";


                        tElement.datepicker(options)
                    }, 1000);



                });


            }
        }
    })
});