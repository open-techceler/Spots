define(['setting/module'], function (module) {

    'use strict';

    module.registerController('SettingCtrl', function ($rootScope, $scope, $state, $log, $filter, GetSetting, UpdateSetting) {

        $scope.setting = {
            id: '',
            email: "",
            phone: "",
            fax: ""
        };

        $scope.getData = function ()
        {
            GetSetting.get().$promise.then(function (res_data) {
                $log.info(res_data);
                $scope.id = res_data.id;
                $scope.email = res_data.email;
                $scope.phone = res_data.phone;
                $scope.fax = res_data.fax;
            });

        };


        $scope.saveSetting = function ()
        {
            
            $scope.setting.email = $scope.email;
            $scope.setting.phone = $scope.phone;
            $scope.setting.fax = $scope.fax;

            UpdateSetting.update($scope.setting).$promise.then(function (res_data) {
                $log.info(res_data);
                var alertMsgData = {
                    title: 'Success!',
                    content: "Setting has been Update successfully."
                };
                $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
            });
        }
    });


});
