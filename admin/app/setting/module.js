define(['angular', 'angular-couch-potato', 'angular-ui-router', 'angular-resource'], function (ng, couchPotato) {
    'use strict';

    var module = ng.module('app.setting', ['ui.router', 'ngResource']);

    module.config(function ($stateProvider, $couchPotatoProvider) {
        $stateProvider.state('app.setting', {
            url: '/setting',
            views: {
                "content@app": {
                    controller: 'SettingCtrl',
                    templateUrl: 'app/setting/view/setting.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'setting/controllers/SettingCtrl',
                            //'order/directives/datatables/datatableOrders',
                            'modules/ui/directives/smartJquiDialog',
                            'auth/models/User'])
                    }
                }
            },
            data: {
                title: 'Setting'
            }
        });
    });

    couchPotato.configureApp(module);

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});
