define(['layout/module'], function (module) {

    'use strict';

    module.registerDirective('stateBreadcrumbs', function ($rootScope, $state) {


        return {
            restrict: 'E',
            replace: true,
            template: '<ol class="breadcrumb"><li>Home</li></ol>',
            link: function (scope, element) {

                function setBreadcrumbs(breadcrumbs) {
                    var html = '<li>Home</li>';
                    var fa_icon = "";
                    angular.forEach(breadcrumbs, function (crumb) {
                        //html += '<li>' + crumb + '</li>'

                        switch (crumb) {
                            case "Incoming Orders":
                                fa_icon = "fa-bell";
                                break;
                            case "All Orders":
                                fa_icon = "fa-table";
                                break;
                            case "Food Items":
                                fa_icon = "fa-cutlery";
                                break;
                            case "Food Categories":
                                fa_icon = "fa-cutlery";
                                break;
                            case "Today Menu":
                                fa_icon = "fa-cutlery";
                                break;
                            case "Customers Management":
                                fa_icon = "fa-child";
                                break;
                            case "Employee Management":
                                fa_icon = "fa-group";
                                break;
                            case "Delivery Area & Business Hours":
                                fa_icon = "fa-sliders";
                                break;
                            case "FAQ":
                                fa_icon = "fa-sliders";
                                break;
                            case "ETA":
                                fa_icon = "fa-sliders";
                                break;
                            case "Cart Settings":
                                fa_icon = "fa-sliders";
                                break;


                            default:
                                fa_icon = "fa-home";
                                break;
                        }

                        html = '<li style="font-size:14px; color: #212529;"><i class="fa fa-lg fa-fw ' + fa_icon + '" style="padding-right:14px;"></i>' + crumb + '</li>'
                    });
                    element.html(html)
                }

                function fetchBreadcrumbs(stateName, breadcrunbs) {

                    var state = $state.get(stateName);

                    if (state && state.data && state.data.title && breadcrunbs.indexOf(state.data.title) == -1) {
                        breadcrunbs.unshift(state.data.title)
                    }

                    var parentName = stateName.replace(/.?\w+$/, '');
                    if (parentName) {
                        return fetchBreadcrumbs(parentName, breadcrunbs);
                    } else {
                        return breadcrunbs;
                    }
                }

                function processState(state) {
                    var breadcrumbs;
                    if (state.data && state.data.breadcrumbs) {
                        breadcrumbs = state.data.breadcrumbs;
                    } else {
                        breadcrumbs = fetchBreadcrumbs(state.name, []);
                    }
                    setBreadcrumbs(breadcrumbs);
                }

                processState($state.current);

                $rootScope.$on('$stateChangeStart', function (event, state) {
                    processState(state);
                })
            }
        }
    });
});
