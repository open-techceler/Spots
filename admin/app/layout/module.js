define(['angular',
    'angular-couch-potato',
    'angular-ui-router',
    'auth/module'], function (ng, couchPotato) {

    "use strict";


    var module = ng.module('app.layout', ['ui.router', 'app.auth']);


    couchPotato.configureApp(module);

    module.config(function ($stateProvider, $couchPotatoProvider, $urlRouterProvider, USER_ROLES) {


        $stateProvider
            .state('app', {
                abstract: true,
                views: {
                    root: {
                        templateUrl: 'app/layout/layout.tpl.html',
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                'auth/directives/loginInfo',
                                'modules/graphs/directives/inline/sparklineContainer',
                                'components/inbox/directives/unreadMessagesCount',
                                'components/chat/api/ChatApi',
                                'components/chat/directives/asideChatWidget',
                                'modules/ui/directives/smartJquiDialog'
                            ])
                        }
                    }
                },
                data: {
                  roles: [USER_ROLES.representative, USER_ROLES.admin]
                }
            });
        
        $urlRouterProvider.otherwise('/login');

    });
    
    module.registerController('layoutCtrl', function ($rootScope, $scope, $state, $log) {
        $scope.confirmLogout = function(){
          $('#dialog_status_logout').dialog('open');
        };
        
        $scope.logout = function () {
            window.location.href = '#/logout';
        };
    });

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;

});
