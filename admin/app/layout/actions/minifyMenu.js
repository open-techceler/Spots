define(['layout/module'], function (module) {
    "use strict";

    module.registerDirective('minifyMenu', function () {
        return {
            restrict: 'A',
            link: function (scope, element) {
                var $body = $('body');
                var minifyMenu = function () {
                    if (!$body.hasClass("menu-on-top")) {
                        $body.toggleClass("minified");
                        $body.removeClass("hidden-menu");
                        if ($body.hasClass("minified")) {
                            $('#changeCol').addClass('col-lg-5');
                            $('#changeCol').removeClass('col-lg-6');

                            $('#changeSearchCol').addClass('col-lg-2');
                            $('#changeSearchCol').removeClass('col-lg-1');
                        }
                        else {
                            $('#changeCol').addClass('col-lg-6');
                            $('#changeCol').removeClass('col-lg-5');
                            
                             $('#changeSearchCol').addClass('col-lg-1');
                            $('#changeSearchCol').removeClass('col-lg-2');
                        }
                        $('html').removeClass("hidden-menu-mobile-lock");
                    }
                };

                element.on('click', minifyMenu);
            }
        }
    })

});
