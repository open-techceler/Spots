
define(['admin/module'], function (module) {
    "use strict";

    return module.registerDirective('fileFieldDoctor', function ($rootScope) {
        return {
            require: 'ngModel',
            restrict: 'E',
            link: function (scope, element, attrs, ngModel) {
                //set default bootstrap class
                if (!attrs.class && !attrs.ngClass) {
                    element.addClass('btn');
                }

                var fileField = element.find('input');

                fileField.bind('change', function (event) {

                    var _URL = window.URL || window.webkitURL;
                    var file, img;
                    var imgWidth = 0;
                    var imgHeight = 0;
                    console.log("size .. ", event.target.files[0].size);
                    if ((file = event.target.files[0]) && event.target.files[0].size <= 100000) {
                        img = new Image();
                        img.onload = function () {
                            imgWidth = parseInt(this.width);
                            imgHeight = parseInt(this.height);

                            if (imgWidth <= 60 && imgHeight <= 60)
                            {
                                console.log("width ", imgWidth, "height ", imgHeight, "inside");


                                scope.$evalAsync(function () {
                                    ngModel.$setViewValue(event.target.files[0]);
                                    if (attrs.preview) {
                                        var reader = new FileReader();
                                        var r = new FileReader();
                                        reader.onload = function (e) {
                                            scope.$evalAsync(function () {
                                                scope[attrs.preview] = e.target.result;
                                                //scope.datarow.dis_photo = e.target.result;
                                                //$rootScope.file_str = btoa(e.target.result);

                                                scope.doUpload(e.target.result);


                                            });
                                        };
                                        reader.readAsDataURL(event.target.files[0]);
                                        //console.log(event.target.files[0]);
                                        //reader.readAsBinaryString(event.target.files[0]);

                                        //$rootScope.file_str = r.readAsBinaryString(event.target.files[0]);
                                        //ngModel.$setViewValue(r.readAsBinaryString(event.target.files[0]));
                                        //alert('asd');
                                        //scope.doUpload();

                                        console.log(imgWidth + " x " + imgHeight);
                                        //  scope.doUploadImg(imgWidth, imgHeight);
                                    }
                                });
                            }
                            else {
                                alert("image dimension should be '60 x 60 px'");
                            }
                            console.log("width ", imgWidth, "height ", imgHeight);
                        }
                        img.src = _URL.createObjectURL(file);
                    }
                    else {
                        alert("Image size should be less then 100kb (60 x 60 px)")
                    }

                });

                fileField.bind('click', function (e) {
                    e.stopPropagation();
                });
                element.bind('click', function (e) {
                    e.preventDefault();
                    fileField[0].click()
                });

            },
            template: '<button class="btn btn-primary" type="button"><ng-transclude></ng-transclude><input type="file" accept="image/*"  multiple="false" style="display:none"> Add Photo</button>',
            replace: true,
            transclude: true
        };
    });

});