define(['angular', 'angular-couch-potato', 'angular-ui-router', 'angular-resource'], function (ng, couchPotato) {
    'use strict';

    var module = ng.module('app.admin', ['ui.router', 'ngResource']);

    module.config(function ($stateProvider, $couchPotatoProvider) {
        $stateProvider.state('app.admin', {
            url: '/admin',
            views: {
                "content@app": {
                    controller: 'AdminCtrl',
                    templateUrl: 'app/admin/view/admin.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'admin/controllers/AdminCtrl',
                            //'order/directives/datatables/datatableOrders',
                            'modules/ui/directives/smartJquiDialog',
                            'auth/models/User'])
                    }
                }
            },
            data: {
                title: 'All Admin'
            }
        });

        $stateProvider.state('app.admin.addAdmin', {
            url: '/admin/add',
            views: {
                "content@app": {
                    controller: 'AddAdminCtrl',
                    templateUrl: 'app/admin/view/add_admin.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'admin/controllers/AddAdminCtrl',
                            'admin/directives/passwordVerify',
                            //'speciality/directives/file_field_employee_add',
                            // 'admin/directives/file_field_hospital',
                            //'modules/ui/directives/smartJquiDialog',
                            'auth/models/User',
                                    // 'admin/directives/multiSelect'
                        ])
                    }
                }
            },
            data: {
                title: 'Add Admin'
            }
        });
        $stateProvider.state('app.admin.editAdmin', {
            url: '/admin/edit',
            views: {
                "content@app": {
                    controller: 'EditAdminCtrl',
                    templateUrl: 'app/admin/view/edit_admin.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'admin/controllers/EditAdminCtrl',
                            //'speciality/directives/file_field_employee_add',
                            'admin/directives/passwordVerify',
                            // 'modules/ui/directives/smartJquiDialog',
                            'auth/models/User',
                                    //    'admin/directives/multiSelect'
                        ])
                    }
                }
            },
            data: {
                title: 'Edit Admin'
            }
        });





    });

    couchPotato.configureApp(module);

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});
