define(['admin/module'], function (module) {

    'use strict';
    module.registerController('EditAdminCtrl', function ($rootScope, $scope, $state, $log, $interval, $filter, GetAdminDetails, UpdateAdmin, ChangePassword,SaveEventImage, $window) {
        $log.info('in edit admin controller...');
        //$scope.getData();

        $scope.dataRow = {};
        $scope.passObj = {
            isShowPwd: false
        };
        $scope.getData = function () {
            console.log("before");
            if ($rootScope.rootAdminObjId) {
                $scope.id = $rootScope.rootAdminObjId;

            }
            else {
                $scope.backToAdmin();
            }
            $scope.searchObj = {
            }
            console.log("tasdtyfaysdgfjhsagdfjhsgfsdh");
            GetAdminDetails.get({id: $scope.id}).$promise.then(function (res_data) {
                $log.info(res_data);
                $scope.dataRow = res_data;

            });
        };

        $scope.editAdminInfo = function () {
            $scope.showLoader();
            UpdateAdmin.update($scope.dataRow).$promise.then(function (res_data) {

                $scope.eventId = res_data.id;
                $scope.hideLoader();
                var alertMsgData = {
                    title: 'Success!',
                    content: 'Admin has been updated successfully.'
                };
                $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);

                $state.go("app.admin");

            }, function (error) {
                if (error) {
                    $scope.error = error;
                    var alertMsgData = {
                        title: 'Error!',
                        content: 'Enter valid data'
                    };
                    $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
                    $scope.hideLoader();
                }
            });

        };



        $scope.showLoader = function () {
            $('#loaderbg').show();
        };
        $scope.hideLoader = function () {
            $('#loaderbg').fadeOut('slow');
        };
        $scope.backToAdmin = function () {
            $state.go("app.admin");
        };


        $scope.searchList = function () {
            $scope.searchbyname = $scope.searchName;
            $rootScope.rootAdminUser.searchbyname = $scope.searchName;
            $rootScope.rootAdminUser.rootCurrentPage = $scope.currentPage;
            $log.info($scope.searchbyname);
            $log.info("root search name is", $rootScope.rootAdminUser.searchbyname);
            $scope.currentPage = 1;
            $scope.getData();
        };


$scope.passwordVerify ={}
        $scope.changePassword = function () {
            console.log($rootScope.rootAdminObjId);
            console.log($scope.passwordVerify.password);
            $scope.userObj = {
                    id: $rootScope.rootAdminObjId,
                    password: $scope.passwordVerify.password
                }
              ChangePassword.change($scope.userObj).$promise.then(function (res_data) {

                    $log.info(res_data);

                    if (res_data.id !== null) {
                        var alertMsgData = {
                            title: 'Success!',
                            content: "Password Changed Successfully."
                        };
                        $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
                    }

                });
//            console.log("change password..", $scope.passwordVerify.password, " .." + $("#confirmPassword").val());
//            if ($scope.passwordVerify.password == $("#confirmPassword").val()) {
//                $scope.userObj = {
//                    id: $rootScope.rootUserId,
//                    password: $scope.passwordVerify.password
//                }
//
//                console.log("confirmPassword ... ", $scope.confirmPassword);
//                ChangePassword.change($scope.userObj).$promise.then(function (res_data) {
//
//                    $log.info(res_data);
//
//                    if (res_data.id !== null) {
//                        var alertMsgData = {
//                            title: 'Success!',
//                            content: "Password Successfully Changes."
//                        };
//                        $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
//                    }
//
//                },
//                        function (error) {
//                            if (error) {
//                                $scope.error = error;
//                                var alertMsgData = {
//                                    title: 'Error!',
//                                    content: "Can't change Password."
//                                };
//                                $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
//                            }
//                        });
//            } else {
//
//                var alertMsgData = {
//                    title: 'Error!',
//                    content: "Password Not Match."
//                };
//                $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
//            }
        };

    });
});
