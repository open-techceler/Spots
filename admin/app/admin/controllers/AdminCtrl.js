define(['admin/module'], function (module) {

    'use strict';

    module.registerController('AdminCtrl', function ($rootScope, $scope, $state, $log,GetAllAdmins, $interval, $filter, $window) {
        $log.info('in Admin controller...');
        
        //$scope.getData();
      $scope.adminList = {};
       $scope.currentPage = 1;
        $scope.limit = 10;
        $scope.perPage = 10;
        $scope.noOfPages = 10;
       $scope.searchObj = {};
        $scope.getData = function () {
            
            $scope.searchObj.page = 1;
            GetAllAdmins.get($scope.searchObj).$promise.then(function (res_data) {
                $log.info(res_data);
               $scope.adminList = res_data.list;
                if ($scope.currentPage == 1)
                {
                    $scope.totalItem = res_data.count;
                }
            });
        };

//get another portions of data on page changed
        $scope.pageChanged = function () {
            $log.info($scope.currentPage);
            $scope.getData();
        };

        /************************/
      
        $scope.showDeletePopup = function (id) {
            $rootScope.rootHospitalId = id;
            $('#dialog_status_box_hospital').dialog('open');
        };

        $scope.doDeleteHospital = function () {
            $log.info($rootScope.rootHospitalId);
            DeleteHospital.delete({id: $rootScope.rootHospitalId}).$promise.then(function () {
                var alertMsgData = {
                    title: 'Success!',
                    content: "record has been deleted successfully."
                };
                $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
                $state.go($state.current, {}, {reload: true});
            }, function (error) {
                if (error) {
                    $scope.error = error;
                    var alertMsgData = {
                        title: 'Error!',
                        content: "You can't delete this Hospital."
                    };
                    $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
                }
            });
            /**/
        };
       
        

        $scope.addAdmin = function (){
             $state.go("app.admin.addAdmin");
        };
        
         $scope.searchList = function () {
         
             $scope.searchObj.admin_name = $scope.searchName;
             
             console.log($scope.searchName);
             $scope.getData();
        };
        
         $scope.editAdmin = function (id){
            console.log("view edit fun");
               $rootScope.rootAdminObjId = id;
             $state.go("app.admin.editAdmin");
        };
       
       
    });


});
