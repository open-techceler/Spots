define(['admin/module'], function (module) {

    'use strict';

    module.registerController('AddAdminCtrl', function ($rootScope, $scope, $state, $log, $interval, $filter, SaveAdmin,  $window) {
        $log.info('in add Admin controller...');

        //$scope.getData();
        $scope.hospital = {};
        $scope.hospital.services = [];
        $scope.phone1 = [];
        $scope.phone2 = [];
        $scope.phones = [];
        $scope.hospitalId = "";
        
      $scope.dataRow = {
          
        
      };
        $scope.saveAdminInfo = function () {
          
            console.log($scope.dataRow);
            $scope.dataRow.password=$scope.password;
            SaveAdmin.save($scope.dataRow).$promise.then(function (res_data) {
                $log.info(res_data);
             
                var alertMsgData = {
                    title: 'Success!',
                    content: 'Admin has been added successfully.'
                };
                 $scope.backToAdmin();
                $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);

              

            }, function (error) {
                if (error) {
                    $scope.error = error;
                    var alertMsgData = {
                        title: 'Error!',
                        content:  $scope.error.data.message
                    };
                    $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
                    $scope.backToAdmin();
                    $scope.hideLoader();
                }
            });


        }

        $scope.backToAdmin = function () {
            $state.go("app.admin");
        };
       

        $scope.showLoader = function () {
            $('#loaderbg').show();
        };
        $scope.hideLoader = function () {
            $('#loaderbg').fadeOut('slow');
        };
    

    });



});
