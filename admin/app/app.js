'use strict';

/**
 * @ngdoc overview
 * @name app [smartadminApp]
 * @description
 * # app [smartadminApp]
 *
 * Main module of the application.
 */

define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-animate',
    'angular-bootstrap',
    'smartwidgets',
    'notification',
    'ng-progress',
    'ng-gmap',
    'ckeditor',
    'ng-ckeditor',
    'angular-cookies',
    'angular-filter',
    'ui-sortable',
    //'ng-tags-input'
], function (ng, couchPotato) {

    var app = ng.module('app', [
        'ngSanitize',
        'scs.couch-potato',
        'ngAnimate',
        'ui.router',
        'ui.bootstrap',
        'ui.sortable',
        'ngCkeditor',
       // 'ngTagsInput',
        // App
        'app.auth',
        'app.layout',
      //  'app.speciality',
        
        'app.setting',
        
        'app.admin',
        'app.user',
        'app.event',
//        'app.procedures',
        
        'app.calendar',
        'app.graphs',
        'app.tables',
        'app.forms',
        'app.ui',
        'app.widgets',
        'app.misc',
        'ngProgress',
        'ngMap',
        'ngCookies',
        'angular.filter'
    ]);

    couchPotato.configureApp(app);
    app.config(function (ngProgressProvider) {
        // Default color is firebrick
        ngProgressProvider.setColor('firebrick');
        // Default height is 2px
        ngProgressProvider.setHeight('2px');
    });
    app.config(function ($provide, $httpProvider) {



        // Intercept http calls.
        $provide.factory('ErrorHttpInterceptor', function ($q) {
            var errorCounter = 0;
            function notifyError(rejection) {
                console.log(rejection);
                $.bigBox({
                    title: rejection.status + ' ' + rejection.statusText,
                    content: rejection.data.error_description,
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    number: ++errorCounter,
                    timeout: 6000
                });
                /* $.smallBox({
                 title: rejection.status + ' ' + rejection.statusText,
                 content: "<i class='fa fa-warning'></i> <i>"+rejection.data.error_description+"</i>",
                 color: "#C46A69",
                 iconSmall: "fa shake animated",
                 timeout: 5000
                 });*/
            }

            return {
                // On request failure
                requestError: function (rejection) {
                    // show notification
                    notifyError(rejection);

                    // Return the promise rejection.
                    return $q.reject(rejection);
                },
                // On response failure
                responseError: function (rejection) {
                    //console.info(rejection);
                    // show notification
                    //notifyError(rejection);
                    // Return the promise rejection.
                    return $q.reject(rejection);
                }
            };
        });

        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('ErrorHttpInterceptor');

    });

    app.run(function ($couchPotato, $rootScope, $state, $stateParams, ngProgress) {
        app.lazy = $couchPotato;
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on('triggerErrorAlertPopup', function (event, data) {
            $('.SmallBox').remove();
            $.smallBox({
                title: data.title,
                content: "<i class='fa fa-warning'></i> <i>" + data.content + "</i>",
                color: "#C46A69",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 6000
            });
        });

        $rootScope.$on('triggerSuccessAlertPopup', function (event, data) {
            $('.SmallBox').remove();
            $.smallBox({
                title: data.title,
                content: "<i class='fa fa-check'></i> <i>" + data.content + "</i>",
                color: "#739E73",
                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                timeout: 6000
            });
        });

        $rootScope.$on('triggerShowNgProgress', function (event) {
            ngProgress.start();
        });

        $rootScope.$on('triggerHideNgProgress', function (event) {
            ngProgress.complete();
        });
        // editableOptions.theme = 'bs3';
    });


    return app;
});
