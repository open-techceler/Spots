
define(['user/module'], function (module) {
    "use strict";
    
    return module.registerDirective('fileFieldEmployee', function ($rootScope) {
        return {
            require: 'ngModel',
            restrict: 'E',
            link: function (scope, element, attrs, ngModel) {
                //set default bootstrap class
                if (!attrs.class && !attrs.ngClass) {
                    element.addClass('btn');
                }

                var fileField = element.find('input');

                fileField.bind('change', function (event) {
                    scope.$evalAsync(function () {
                        ngModel.$setViewValue(event.target.files[0]);
                        if (attrs.preview) {
                            var reader = new FileReader();
                            var r = new FileReader();
                            reader.onload = function (e) {
                                scope.$evalAsync(function () {
                                    scope[attrs.preview] = e.target.result;
                                    //scope.datarow.dis_photo = e.target.result;
                                    //$rootScope.file_str = btoa(e.target.result);

                                    scope.doUpload(e.target.result);


                                });
                            };
                            reader.readAsDataURL(event.target.files[0]);
                            //console.log(event.target.files[0]);
                            //reader.readAsBinaryString(event.target.files[0]);

                            //$rootScope.file_str = r.readAsBinaryString(event.target.files[0]);
                            //ngModel.$setViewValue(r.readAsBinaryString(event.target.files[0]));
                            //alert('asd');
                            //scope.doUpload();
                        }
                    });
                });

                fileField.bind('click', function (e) {
                    e.stopPropagation();
                });
                element.bind('click', function (e) {
                    e.preventDefault();
                    fileField[0].click()
                });

            },
            template: '<a class="text-info"><ng-transclude></ng-transclude><input type="file" accept="image/*" style="display:none"> Change Photo</a>',
            replace: true,
            transclude: true
        };
    });

});