define(['angular', 'angular-couch-potato', 'angular-ui-router', 'angular-resource'], function (ng, couchPotato) {
    'use strict';

    var module = ng.module('app.user', ['ui.router', 'ngResource']);

    module.config(function ($stateProvider, $couchPotatoProvider) {
        $stateProvider.state('app.user', {
            url: '/user',
            views: {
                "content@app": {
                    controller: 'UserCtrl',
                    templateUrl: 'app/user/view/user.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'user/controllers/UserCtrl',
                            //'order/directives/datatables/datatableOrders',
                            'modules/ui/directives/smartJquiDialog',
                            'auth/models/User'])
                    }
                }
            },
            data: {
                title: 'All User'
            }
        });

       
        $stateProvider.state('app.user.editUser', {
            url: '/user/edit',
            views: {
                "content@app": {
                    controller: 'EditUserCtrl',
                    templateUrl: 'app/user/view/edit_user.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'user/controllers/EditUserCtrl',
                            //'user/directives/file_field_employee_add',
                             // 'user/directives/file_field_specialities',
                            'modules/ui/directives/smartJquiDialog',
                            'auth/models/User'])
                    }
                }
            },
            data: {
                title: 'Edit User'
           }
       })


    });

    couchPotato.configureApp(module);

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});
