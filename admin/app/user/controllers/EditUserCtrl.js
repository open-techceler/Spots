define(['user/module'], function (module) {

    'use strict';

    module.registerController('EditUserCtrl', function ($rootScope, $scope, $state, $log, $interval, $filter, UpdateUser,ChangePassword, GetAllUsers,GetUserDetails,$window) {
        $log.info('in edit user controller...');

        //$scope.getData();
        $scope.user_obj = {
            
       
        };
          $scope.passObj = {
            isShowPwd: false
        };
       

       $scope.getData = function () {
            console.log("before");
            if ($rootScope.rootUserObjId) {
                
                $scope.id = $rootScope.rootUserObjId;
            }
            else {
                $scope.backToUser();
            }
            $scope.searchObj = {
            }
            console.log("tasdtyfaysdgfjhsagdfjhsgfsdh");
            GetUserDetails.get({id: $scope.id}).$promise.then(function (res_data) {
                $log.info(res_data);
                $scope.user_obj = res_data;
                
                
                
            });
        };

         
         $scope.updateUserInfo = function () {
            $scope.showLoader();
            $scope.user_obj.id = $scope.id;
            
            UpdateUser.update($scope.user_obj).$promise.then(function (res_data) {
             
                $log.info(res_data);
                $scope.userId = res_data.id;
                console.log("user id....... ", $scope.userId);
                $scope.hideLoader();
                var alertMsgData = {
                    title: 'Success!',
                    content: "User has been Update successfully."
                };
                $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
                  $scope.backToUser ();
                  

            },
            
              function (error) {
                if (error) {
                    $scope.error = error;
                    var alertMsgData = {
                        title: 'Error!',
                        content: 'Enter valid data'
                                
                                 
                    };
                    $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
                    $scope.backToUser ();
                     $scope.hideLoader();
                }
            }
             
             );

        };
        
        $scope.passwordVerify ={}
        $scope.changePassword = function () {
            
            console.log("inside change password");
            console.log($rootScope.rootUserObjId);
            console.log($scope.passwordVerify.password);
            $scope.userObj = {
                    id: $rootScope.rootUserObjId,
                    password: $scope.passwordVerify.password
                }
              ChangePassword.change($scope.userObj).$promise.then(function (res_data) {

                    $log.info(res_data);

                    if (res_data.id !== null) {
                        var alertMsgData = {
                            title: 'Success!',
                            content: "Password Changed Successfully."
                        };
                        $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
                    }

                });
                
        };

     
      

        $scope.backToUser = function () {
            $state.go("app.user");
        };

        $scope.doUpload = function () {
            $log.info($scope.uploadFile);
        };

        $scope.showLoader = function () {
            $('#loaderbg').show();
        };
        $scope.hideLoader = function () {
            $('#loaderbg').fadeOut('slow');
        };
        
        $scope.setStatus=function (status){
            
            $scope.user_obj.status=status;
            
        }
    });



});
