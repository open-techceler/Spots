define(['user/module'], function (module) {

    'use strict';

    module.registerController('UserCtrl', function ($rootScope, $scope, $state, $log, $interval, $filter, GetAllUsers, $window) {
        $log.info('in User controller...');

        //$scope.getData();

        $scope.currentPage = 1;
        $scope.limit = 10;
        $scope.perPage = 10;
        $scope.noOfPages = 10;

        $scope.userList = {};
        $scope.searchObj = {};
        $scope.getData = function () {

            $scope.searchObj.page = 1;
            GetAllUsers.get($scope.searchObj).$promise.then(function (res_data) {
                $log.info(res_data);
                $scope.userList = res_data.list;
                if ($scope.currentPage == 1)
                {
                    $scope.totalItem = res_data.count;
                }
            });
        };

        $scope.pageChanged = function () {
            $log.info($scope.currentPage);
             $scope.getData();
        };
        
        $scope.searchList = function () {

            $scope.searchObj.name = $scope.searchName;

            console.log($scope.searchName);
            $scope.getData();
        };
        $scope.editUser = function (id) {
            console.log("view edit fun");
            $rootScope.rootUserObjId = id;
            $state.go("app.user.editUser");
        };


    });


});
