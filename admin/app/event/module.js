define(['angular', 'angular-couch-potato', 'angular-ui-router', 'angular-resource'], function (ng, couchPotato) {
    'use strict';

    var module = ng.module('app.event', ['ui.router', 'ngResource']);

    module.config(function ($stateProvider, $couchPotatoProvider) {
        $stateProvider.state('app.event', {
            url: '/event',
            views: {
                "content@app": {
                    controller: 'EventCtrl',
                    templateUrl: 'app/event/view/event.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'event/controllers/EventCtrl',
                            //'order/directives/datatables/datatableOrders',
                            'modules/ui/directives/smartJquiDialog',
                            'auth/models/User'])
                    }
                }
            },
            data: {
                title: 'All Events'
            }
        });

       $stateProvider.state('app.event.addEvent', {
            url: '/event/add',
            views: {
                "content@app": {
                    controller: 'AddEventCtrl',
                    templateUrl: 'app/event/view/add_event.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'event/controllers/AddEventCtrl',
                            'event/directives/file_field_country',
                            //'speciality/directives/file_field_employee_add',
                            
                            'auth/models/User'])
                    }
                }
            },
            data: {
                title: 'Add Event'
            }
        });
        $stateProvider.state('app.event.editEvent', {
            url: '/event/edit',
            views: {
                "content@app": {
                    controller: 'EditEventCtrl',
                    templateUrl: 'app/event/view/edit_event.html',
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'event/controllers/EditEventCtrl',
                            //'event/directives/file_field_employee_add',
                              'event/directives/file_field_country',
                          //  'modules/ui/directives/smartJquiDialog',
                            'auth/models/User'])
                    }
                }
            },
            data: {
                title: 'Edit Event'
            }
       })


    });

    couchPotato.configureApp(module);

    module.run(function ($couchPotato) {
        module.lazy = $couchPotato;
    });

    return module;
});
