define(['event/module'], function (module) {

    'use strict';
    module.registerController('AddEventCtrl', function ($rootScope, $scope, $state, $log, $interval, $filter, SaveEvent, SaveEventImage, $window) {
        $log.info('in add event controller...');
        $scope.countryId = "";
        //$scope.getData();

        $scope.travelSafe = true;
        $scope.publicTransport = 'Bad';
        $scope.travelGuide = true;
        $scope.is_featured = false;
        $scope.languageInterpreter = true;
        $scope.event = {
        };

        $scope.dataRow = {};
        $scope.saveEventInfo = function () {
            var fd = new FormData();
            console.log($scope.dataRow);
            SaveEvent.save($scope.dataRow).$promise.then(function (res_data) {
                $log.info(res_data);
                if (res_data) {
                    fd.append('id', res_data.id);
                    fd.append('image', $scope.uploadFileEvent);
                    SaveEventImage.save(fd).$promise.then(function (img_data) {
                        $log.info("uploaded image is..............", img_data);
                    });

                }
                var alertMsgData = {
                    title: 'Success!',
                    content: 'Event has been added successfully.'
                };
                $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);

                $state.go("app.event");

            }, function (error) {
                if (error) {
                    $scope.error = error;
                    var alertMsgData = {
                        title: 'Error!',
                        content: 'Enter valid data'
                    };
                    $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
                }
            });


        }

        $scope.backToEvent = function () {
            $state.go("app.event");
        };
        $scope.doUpload = function () {
            $log.info($scope.uploadFile);
        };

        $scope.showLoader = function () {
            $('#loaderbg').show();
        };
        $scope.hideLoader = function () {
            $('#loaderbg').fadeOut('slow');
        };
    });
});

