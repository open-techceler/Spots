define(['event/module'], function (module) {

    'use strict';

    module.registerController('EventCtrl', function ($rootScope, $scope, $state, $log, $interval, $filter,GetAllEvents,DeleteEventById, $window) {
        $log.info('inside Event controller...');
        
        //$scope.getData();
        $scope.eventList = {};
          $scope.currentPage = 1;
        $scope.limit = 10;
        $scope.perPage = 10;
        $scope.noOfPages = 10;
            $scope.searchObj = {};
        $scope.getData1 = function () {
           
            console.log("tasdtyfaysdgfjhsagdfjhsgfsdh", $scope.searchObj);
            $scope.searchObj.page = 1;
            GetAllEvents.get($scope.searchObj).$promise.then(function (res_data) {
                $log.info(res_data);
               $scope.eventList = res_data.list;
                if ($scope.currentPage == 1)
                {
                    $scope.totalItem = res_data.count;
                }
            });
        };

//get another portions of data on page changed
        $scope.pageChanged = function () {
            $log.info($scope.currentPage);
            $scope.getData();
        };

        /************************/

//        $scope.sortList = function (field) {
//            $scope.sortby = field;
//            if ($scope.order == 'asc')
//            {
//                $scope.order = 'desc';
//            }
//            else
//            {
//                $scope.order = 'asc';
//            }
//            $scope.getData();
//        }
//
//      
        $scope.showDeletePopup = function (id) {
            $rootScope.rootEventObjId1 = id;
            console.log("inside delete popup")
            console.log($rootScope.rootEventObjId1)
           
                       $('#dialog_status_box_country').dialog('open');
        }
        
        
     
        $scope.doDeleteEvent = function () {
           // $scope.showLoader();
           
            DeleteEventById.delete({id: $rootScope.rootEventObjId1 }).$promise.then(function () {
                var alertMsgData = {
                    title: 'Success!',
                    content: "record has been deleted successfully."
                };
 
               $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
                $state.go($state.current, {}, {reload: true});
                 $scope.hideLoader();
            }, function (error) {
                if (error) {
                    $scope.error = error;
                    var alertMsgData = {
                        title: 'Error!',
                        content: "You can't delete this Event."
                    };
                    $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
                    $state.go("app.event");
                     $scope.hideLoader();
                }
                 $scope.hideLoader();
            });
           
        };
        
//
//        $scope.view_country = function (id) {
//            console.log(id);
//            $rootScope.rootCountryId = id;
//            $log.info($rootScope.rootCountryId);
//            $state.go("app.country.editCountry");
//        };
//
//      
//        if (!$rootScope.rootOrderDataList) {
//            $rootScope.rootOrderDataList = {};
//        }

        $scope.addEvent = function (){
             $state.go("app.event.addEvent");
        };
       
        $scope.editEvent = function (id){
            console.log("view edit fun");
               $rootScope.rootEventObjId = id;
             $state.go("app.event.editEvent");
        };
         $scope.searchList = function () {
         
             $scope.searchObj.event_name = $scope.searchName;
             
             console.log($scope.searchName);
             $scope.getData1();
        };
         $scope.showLoader = function () {
            $('#loaderbg').show();
        };
        $scope.hideLoader = function () {
            $('#loaderbg').fadeOut('slow');
        };
        
        
       
    });


});
