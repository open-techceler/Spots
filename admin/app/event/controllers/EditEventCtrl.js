define(['event/module'], function (module) {

    'use strict';
    module.registerController('EditEventCtrl', function ($rootScope, $scope, $state, $log, $interval, $filter, GetEventDetails, UpdateEvent, SaveEventImage,$window) {
        $log.info('in edit event controller...');
        //$scope.getData();

        $scope.eventDetails = {};

        $scope.getData = function () {
            console.log("before");
            if ($rootScope.rootEventObjId) {
                $scope.id = $rootScope.rootEventObjId;
            }
            else {
                $scope.backToEvent();
            }
            $scope.searchObj = {
            }
            console.log("tasdtyfaysdgfjhsagdfjhsgfsdh");
            GetEventDetails.get({id: $scope.id}).$promise.then(function (res_data) {
                $log.info(res_data);
                $scope.eventDetails = res_data;
                $scope.image = res_data.image;
            });
        };

        $scope.editEventInfo = function () {
            $scope.showLoader();
            UpdateEvent.update($scope.eventDetails).$promise.then(function (res_data) {
                 var fd = new FormData();
                  if (res_data) {
                    fd.append('id', res_data.id);
                    fd.append('image', $scope.uploadFileEvent);
                    SaveEventImage.save(fd).$promise.then(function (img_data) {
                        $log.info("uploaded image is..............", img_data);
                    });

                }
                 
                $log.info(res_data);
                $scope.eventId = res_data.id;
                console.log("event id....... ", $scope.eventId);
                $scope.hideLoader();
                var alertMsgData = {
                    title: 'Success!',
                    content: "Event has been Update successfully."
                };
                $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
                 $state.go("app.event");
            },
             function (error) {
                if (error) {
                    $scope.error = error;
                    var alertMsgData = {
                        title: 'Error!',
                        content:  $scope.error.data.message
                    };
                    $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
                   $scope.backToEvent();
                    $scope.hideLoader();
                }
            }
            
            );

        };

        $scope.doUpload = function () {
            $log.info($scope.uploadFile);
        };

        $scope.showLoader = function () {
            $('#loaderbg').show();
        };
        $scope.hideLoader = function () {
            $('#loaderbg').fadeOut('slow');
        };
        $scope.backToEvent = function () {
            $state.go("app.event");
        };
        
        
        $scope.searchList = function () {
             $scope.searchbyname = $scope.searchName;
            $rootScope.rootAdminUser.searchbyname = $scope.searchName;
            $rootScope.rootAdminUser.rootCurrentPage = $scope.currentPage;
            $log.info($scope.searchbyname);
            $log.info("root search name is", $rootScope.rootAdminUser.searchbyname);
            $scope.currentPage = 1;
            $scope.getData();
        };

    });
});
