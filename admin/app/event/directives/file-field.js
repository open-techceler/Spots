
define(['speciality/module'], function (module) {
    "use strict";

    return module.registerDirective('fileFieldEmployee', function ($rootScope) {
        return {
            require: 'ngModel',
            restrict: 'E',
            link: function (scope, element, attrs, ngModel) {
                //set default bootstrap class
                if (!attrs.class && !attrs.ngClass) {
                    element.addClass('btn');
                }

                var fileField = element.find('input');

                fileField.bind('change', function (event) {
                    //alert(event.target.files[0].size);
                    if(event.target.files[0].size <= 500000)
                    {
                        scope.$evalAsync(function () {
                            ngModel.$setViewValue(event.target.files[0]);
                            if (attrs.preview) {
                                var reader = new FileReader();
                                var r = new FileReader();
                                reader.onload = function (e) {
                                    scope.$evalAsync(function () {
                                        scope[attrs.preview] = e.target.result;
                                        //scope.datarow.dis_photo = e.target.result;
                                        $rootScope.file_str = btoa(e.target.result);
                                    });
                                };
                                //reader.readAsDataURL(event.target.files[0]);
                                reader.readAsBinaryString(event.target.files[0]);

                                //$rootScope.file_str = r.readAsBinaryString(event.target.files[0]);
                                //ngModel.$setViewValue(r.readAsBinaryString(event.target.files[0]));
                                //alert('asd');
                                scope.doUpload();
                            }
                        });
                    }
                    else
                    {
                        alert('Photo size could not be more than 500kb');
                        return false;
                    }
                });
                
                fileField.bind('click', function (e) {
                    e.stopPropagation();
                });
                element.bind('click', function (e) {
                    e.preventDefault();
                    fileField[0].click()
                });
            },
            template: '<button class="btn btn-primary" type="button"><ng-transclude></ng-transclude><input type="file" accept="image/*"  multiple="false" style="display:none"> Change Photo</button>',
            replace: true,
            transclude: true
        };
    })
});
