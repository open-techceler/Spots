define(['auth/module', 'auth/services/AuthService'], function (module) {
    "use strict";

    module.registerController('LoginCtrl', function ($rootScope, $scope, $log, $state, AuthService, AUTH_EVENTS, $cookieStore) {
        $scope.error = null;

        var cookie_username = $cookieStore.get('cookie_username');
        var cookie_password = $cookieStore.get('cookie_password');
        if (cookie_username) {
            $scope.credentials = {
                username: cookie_username,
                password: cookie_password
            };
            $scope.remember = true;
        }
        else {
            $scope.credentials = {
                username: '',
                password: ''
            };
            $scope.remember = false;
        }

        $scope.login = function (credentials, remember, isValid) {
            $cookieStore.remove('cookie_username');
            $cookieStore.remove('cookie_password');
            
            if (isValid) {
                if (remember) {
                    $cookieStore.put('cookie_username', $scope.credentials.username);
                    $cookieStore.put('cookie_password', $scope.credentials.password);
                }
                $log.info($scope.credentials);
                return AuthService.login(credentials, remember);
               //$state.go('speciality');
            }
        };

        $rootScope.$on(AUTH_EVENTS.authenticationFailed, function (event, data) {
            var errorMsgData = {
                title: "Bad credentials!",
                content: "Please enter valid email or password"
            };
            $rootScope.$broadcast('triggerErrorAlertPopup', errorMsgData);
            //$state.go('app.orders');
        });

        $rootScope.$on(AUTH_EVENTS.loginFailed, function (event, data) {
            $log.info("In loginFailed function...");
            if (data && data.error_description) {
                $scope.error = data;
            }
        });



    });

});
