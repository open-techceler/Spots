define(['auth/module', 'auth/models/User', 'modules/ui/module'], function (module) {
    "use strict";
    module.registerController('ForgotPasswordCtrl',
            function ($rootScope, $scope, $state, $log, ResetPassword) {
                $scope.forgotCredentials = {};
                $scope.forgotCredentials.email = "";
                $scope.backToLogin = function(){
                    $state.go('login');
                }
                $scope.forgotPassword = function (isValid) {
                    $log.debug('forgot called');
                    if (isValid) {

                        ResetPassword.reset({email:$scope.forgotCredentials.email}).$promise.then(function (res) {
                            // now check response and perform act...
                            $log.debug(res);

                            var alertMsgData = {
                                title: 'Success!',
                                content: "New password has been sent to your email address."
                            };
                            $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
                            $state.go('login');
                        }, function (error) {
                            if (error) {
                                $scope.error = error;
                                var alertMsgData = {
                                    title: 'Error!',
                                    content: "Email address does not exist."
                                };
                                $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);
                            }
                        });

                    }
                };
            });
});