define(['auth/module', 'auth/services/AuthService'], function (module) {
    "use strict";

    module.registerController('DashboardCtrl', function ($rootScope, $scope, $log, $state) {
       console.log("DashboardCtrl");

       $state.go('login');
    });

});
