define(['auth/module', 'auth/models/User', 'auth/services/AuthService'], function (module) {
    "use strict";
    module.registerController('RegisterCtrl',
            function ($rootScope, $scope, $state, $log, Users, AuthService, AUTH_EVENTS) {
                $scope.profile = {
                    '@type': 'representative'
                };
                $rootScope.terms = false;
                $scope.error = null;
                $log.debug('registered RegisterCtrl');

                $scope.register = function (isValid) {
                    $log.debug('register called');
                    if (isValid) {
                        $log.info($scope.profile);
                        // create the user
                        Users.create($scope.profile).$promise.then(function (user) {
                            // now try to login
                            /*return AuthService.login({
                             username: user.username,
                             password: $scope.profile.password
                             }, false);*/
                            var alertMsgData = {
                                title: 'Success!',
                                content: "your account has been created successfully."
                            };
                            $rootScope.$broadcast('triggerSuccessAlertPopup', alertMsgData);
                            $state.go('login');

                        }, function (error) {
                            if (error) {
                                $scope.error = error;
                            }
                        });
                    }
                };

                $scope.iagree = function () {
                    $log.info("In Agree Function...");
                    $rootScope.terms = true;
                };


            });
});