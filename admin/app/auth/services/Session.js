define(['auth/module', 'auth/models/User'], function (module) {

    'use strict';

    // TODO: save session in local storage and delete when logged out or timed out

    return module.registerFactory('Session', function ($window, $rootScope, $q, $log, CurrentUserResource, AUTH_EVENTS) {
        var session, sessionJson = $window.sessionStorage['session'];

        if (sessionJson) {
            // TODO: verify method name
            session = angular.fromJson(sessionJson);
        } else {
            session = {
                user: null,
                userId: null,
                userRoles: ["ROLE_ANONYMOUS"],
                accessToken: null
            };
        }

        var sessionService = {};

        // returns a promise that is resolved when the session is fully configured
        sessionService.create = function (oauth, act_type) {
            console.log("act_type :" + act_type);
            if (session && session.accessToken === oauth.access_token) {
                // session already loaded - return resolved promise
                var deferred = $q.defer();

                deferred.resolve(session);

                //$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, session);

                if (act_type == 'init') {
                    $rootScope.$broadcast(AUTH_EVENTS.authInitSuccess, session);
                }
                else {
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, session);
                }

                return deferred.promise;
            } else {

                if ($window.localStorage['oauth']) {
                    //console.log("Here i am");
                    //console.log($window.sessionStorage['oauth']);
                    var loauth = angular.fromJson($window.localStorage['oauth']);
                    session.userId = loauth.id;
                    session.userRoles = ["ROLE_ANONYMOUS", "ROLE_ADMIN"];
                    session.accessToken = appConfig.api_base_url;
                    session.user = loauth;


                    // save in window's session
                    sessionService.save();

                    //$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, session);
                    if (act_type == 'init') {
                        $rootScope.$broadcast(AUTH_EVENTS.authInitSuccess, session);
                    }
                    else {
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, session);
                    }

                    return session;
                }
                else if ($window.sessionStorage['oauth']) {
                    //console.log("Here i am");
                    //console.log($window.sessionStorage['oauth']);
                    var loauth = angular.fromJson($window.sessionStorage['oauth']);
                    session.userId = loauth.id;
                    session.userRoles = ["ROLE_ANONYMOUS", "ROLE_ADMIN"];
                    session.accessToken = appConfig.api_base_url;
                    session.user = loauth;


                    // save in window's session
                    sessionService.save();

                    //$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, session);
                    if (act_type == 'init') {
                        $rootScope.$broadcast(AUTH_EVENTS.authInitSuccess, session);
                    }
                    else {
                        $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, session);
                    }

                    return session;
                }
               
            }
        };

        sessionService.loggedIn = function () {
            return session.userId != null;
        };

        sessionService.userRoles = function () {
            return session.userRoles;
        }

        sessionService.isUserInRole = function (role) {
            return session.userRoles != null && session.userRoles.indexOf(role) !== -1;
        }

        sessionService.save = function () {
            $window.sessionStorage['session'] = angular.toJson(session);
        }

        sessionService.destroy = function () {
            session.user = null;
            session.userId = null;
            session.userRoles = ["ROLE_ANONYMOUS"];
            session.accessToken = null;
            $window.sessionStorage.removeItem('session');
        };

        return sessionService;
    });

});
