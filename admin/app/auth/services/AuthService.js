define(['auth/module', 'auth/services/Session'], function (module) {

    'use strict';

    // TODO: need to figure out where to put state change handler so it gets loaded originally
    return module.registerFactory('AuthService', function ($window, $rootScope, $resource, $state, $log, Session,
            AUTH_EVENTS) {
        // TODO: configure the API location (and key)
        var oauthResource = $resource(appConfig.api_base_url + '/admins/login', {}, {
            'token': {
                method: 'POST',
                isArray: false,
                /*headers: {
                    'Authorization': 'Basic ' + appConfig.client_auth,
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                }*/
            }
        });

        var oauthResponse = null;
        var authService = {};

        // note: called at the end of this factory function
        authService.init = function () {
            var oauth = $window.sessionStorage['oauth'];
            if (!oauth) {
                oauth = $window.localStorage['oauth'];
            }

            if (oauth) {
                oauthResponse = angular.fromJson(oauth);

                //$log.debug('loaded oauthResponse from storage=' + oauthResponse);

                $rootScope.$broadcast(AUTH_EVENTS.authenticationSuccess, oauthResponse);

                Session.create(oauthResponse,'init');
            } else {
                $log.debug('did not find oauth in local or session storage');
            }
        };

        authService.login = function (credentials, remember) {
            var params = 'user_type=Employer&username=' + credentials.username + '&password=' + credentials.password;
            $rootScope.paramsObj = {};
            //$rootScope.paramsObj.user_type = "Employee";
            $rootScope.paramsObj.username = credentials.username;
            $rootScope.paramsObj.email = credentials.username;
            $rootScope.paramsObj.password = credentials.password;
            //console.log($rootScope.paramsObj);
            // return promise
            return oauthResource.token($rootScope.paramsObj).$promise.then(function (oauth) {
                $log.debug('received oauth=' + oauth + ', remember=' + remember);

                // (re-) init the saved response
                oauthResponse = oauth;

                // remember?
                if (remember === true) {
                    $window.localStorage['oauth'] = angular.toJson(oauthResponse);
                    $log.debug('saved oauth in local storage');
                } else {
                    $window.localStorage.removeItem('oauth');
                }

                // save oauth in session storage
                $window.sessionStorage['oauth'] = angular.toJson(oauthResponse);

                $log.debug('saved oauth in session storage');

                // success
                $rootScope.$broadcast(AUTH_EVENTS.authenticationSuccess, oauthResponse);

                // create our session, passing on current user object
                return Session.create(oauthResponse,'auth');
            }, function (response) {
                // remove on general principal
                oauthResponse = null;
                //$window.sessionStorage.clearItem('oauth');

                // error
                $rootScope.$broadcast(AUTH_EVENTS.authenticationFailed, response.data);

                // return data from the response
                return response.data;
            });
        };

        authService.logout = function () {
            // clear
            $window.sessionStorage.removeItem('oauth');
            Session.destroy();
            if($window.localStorage['oauth'])
            {
                $window.localStorage.removeItem('oauth');
            }
            $rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
        }

        authService.isAuthenticated = function () {
            // TODO: may want to delineate between authenticated and logged in
            return Session.loggedIn();
        };

        authService.isAuthorized = function (authorizedRoles) {
            // TODO: need to check here to auto-login based on session and/or local storage
            if (!angular.isArray(authorizedRoles)) {
                authorizedRoles = [authorizedRoles];
            }

            var roles = Session.userRoles();

            for (var i = 0; i < authorizedRoles.length; i++) {
                if (roles.indexOf(authorizedRoles[i]) !== -1) {
                    return true;
                }
            }

            return false;
        };

        authService.init();

        return authService;
    });

});
