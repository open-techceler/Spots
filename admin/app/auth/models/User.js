define(['auth/module'], function (module) {

    'use strict';

    return module.registerFactory('Users', function ($resource) {
        return $resource(appConfig.api_base_url + '/users', {}, {
            'create': {
                method: 'POST',
                isArray: false,
                headers: {
                    'Authorization': 'Basic ' + appConfig.client_auth,
                    'Content-Type': 'application/json'
                }
            }
        });
    })

            .registerFactory('CurrentUserResource', function ($resource) {
                return $resource(appConfig.api_base_url + '/users/current', {}, {
                    'get': {
                        method: 'GET',
                        isArray: false
                    }
                });
            })

            .registerFactory('ResetPassword', function ($resource) {
                return $resource(appConfig.api_base_url + '/users/reset-password/:email/app', {
                    email: '@email'

                }, {
                    'reset': {
                        method: 'GET',
                        isArray: false,
                    }
                });
            })

            /*=========================XXXXXXXXXXXXXXXXXXXXX API's for User start XXXXXXXXXXXXXXXXXXXXX/*=========================*/

             .registerFactory('GetAllUsers', function ($resource) {
                return $resource(appConfig.api_base_url + '/users/list/:page', {
                    page: '@page'
                }, {
                    'get': {
                        method: 'GET',
                        isArray: false,
                    }
                });
            })
            .registerFactory('GetUserDetails', function ($resource) {
                return $resource(appConfig.api_base_url + '/users/:id', {
                    id: '@id'
                }, {
                    'get': {
                        method: 'GET',
                        isArray: false,
                    }
                });
            })

            .registerFactory('UpdateUser', function ($resource) {
                return $resource(appConfig.api_base_url + '/users', {
                    
                }, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })
            
            .registerFactory('ChangePassword', function ($resource) {
                return $resource(appConfig.api_base_url + '/admins/change-password', {
                    
                }, {
                    'change': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })
          

            
            /*=========================XXXXXXXXXXXXXXXXXXXXX API's for User End XXXXXXXXXXXXXXXXXXXXX/*=========================*/


            /*=========================XXXXXXXXXXXXXXXXXXXXX API's for Country start XXXXXXXXXXXXXXXXXXXXX/*=========================*/

            .registerFactory('GetCountry', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/list', {}, {
                    'get': {
                        method: 'GET',
                        isArray: true,
                    }
                });
            })
            .registerFactory('GetCountryDetails', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/:id', {
                    id: '@id'
                }, {
                    'get': {
                        method: 'GET',
                        isArray: false,
                    }
                });
            })
            .registerFactory('UpdateCountry', function ($resource) {
                return $resource(appConfig.api_base_url + '/country', {}, {
                    'save': {
                        method: 'POST',
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined,
                        }
                    }
                });
            })
            .registerFactory('UpadateTravel', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/travel', {}, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })

            .registerFactory('UpadateAccommodation', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/accommodation', {}, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })
            .registerFactory('UpadateVisa', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/visa', {}, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })
            .registerFactory('UpadateTour', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/tour', {}, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })
            .registerFactory('UpadateCulture', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/culture', {}, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })
            .registerFactory('UpadateQuickTip', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/quick-tip', {}, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })

            .registerFactory('DeleteCountry', function ($resource) {
                return $resource(appConfig.api_base_url + '/country/:id', {
                    id: '@id'
                }, {
                    'delete': {
                        method: 'DELETE',
                        isArray: false,
                    }
                });
            })

            /*=========================XXXXXXXXXXXXXXXXXXXXX API's for Country End XXXXXXXXXXXXXXXXXXXXX/*=========================*/


           
    
    
            /*=========================XXXXXXXXXXXXXXXXXXXXX API's for Event Start XXXXXXXXXXXXXXXXXXXXX/*=========================*/

            .registerFactory('GetAllEvents', function ($resource) {
                return $resource(appConfig.api_base_url + '/events/list/:page', {
                    page: '@page'
                }, {
                    'get': {
                        method: 'GET',
                        isArray: false,
                    }
                });
            })
            .registerFactory('GetEventDetails', function ($resource) {
                return $resource(appConfig.api_base_url + '/events/:id', {
                    id: '@id'
                }, {
                    'get': {
                        method: 'GET',
                        isArray: false,
                    }
                });
            })
            .registerFactory('SaveEvent', function ($resource) {
                return $resource(appConfig.api_base_url + '/events', {
                }, {
                    'save': {
                        method: 'POST',
                        isArray: false,
                    }
                });
            })

            .registerFactory('UpdateEvent', function ($resource) {
                return $resource(appConfig.api_base_url + '/events/:id', {
                    id: '@id'
                }, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })

            .registerFactory('DeleteEventById', function ($resource) {
                return $resource(appConfig.api_base_url + '/events/:id', {
                    id: '@id'
                }, {
                    'delete': {
                        method: 'DELETE',
                        isArray: false,
                    }
                });
            })

            .registerFactory('SaveEventImage', function ($resource) {
                return $resource(appConfig.api_base_url + '/events/image', {}, {
                    'save': {
                        method: 'POST',
                        transformRequest: angular.identity,
                        headers: {
                            'Content-Type': undefined,
                        }
                    }
                });
            })



            /*=========================XXXXXXXXXXXXXXXXXXXXX API's for Event End XXXXXXXXXXXXXXXXXXXXX/*=========================*/

             /*=========================XXXXXXXXXXXXXXXXXXXXX API's for Admin User Start XXXXXXXXXXXXXXXXXXXXX/*=========================*/

            .registerFactory('GetAllAdmins', function ($resource) {
                return $resource(appConfig.api_base_url + '/admins/list/:page', {
                    page: '@page'
                }, {
                    'get': {
                        method: 'GET',
                        isArray: false,
                    }
                });
            })
            .registerFactory('GetAdminDetails', function ($resource) {
                return $resource(appConfig.api_base_url + '/admins/:id', {
                    id: '@id'
                }, {
                    'get': {
                        method: 'GET',
                        isArray: false,
                    }
                });
            })
            .registerFactory('SaveAdmin', function ($resource) {
                return $resource(appConfig.api_base_url + '/admins', {
                }, {
                    'save': {
                        method: 'POST',
                        isArray: false,
                    }
                });
            })

            .registerFactory('UpdateAdmin', function ($resource) {
                return $resource(appConfig.api_base_url + '/admins/:id', {
                    id: '@id'
                }, {
                    'update': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })
            
             .registerFactory('ChangePassword', function ($resource) {
                return $resource(appConfig.api_base_url + '/admins/change-password', {
                    
                }, {
                    'change': {
                        method: 'PUT',
                        isArray: false,
                    }
                });
            })
            

            .registerFactory('DeleteAdminUserById', function ($resource) {
                return $resource(appConfig.api_base_url + '/events/:userId', {
                    userId: '@userId'
                }, {
                    'delete': {
                        method: 'DELETE',
                        isArray: false,
                    }
                });
            })

            



            /*=========================XXXXXXXXXXXXXXXXXXXXX API's for Admin User  End XXXXXXXXXXXXXXXXXXXXX/*=========================*/


});