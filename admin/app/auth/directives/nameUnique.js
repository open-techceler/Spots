define(['auth/module'], function (module) {
    "use strict";

    return module.registerDirective('nameUnique', function ($resource, $timeout, $log, userNameUnique) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var stop_timeout;
                return scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function (name) {
                    $timeout.cancel(stop_timeout);
                    //  var asd = userNameUnique.get();
                    ngModel.$setValidity('unique', true);
                    if (name) {


                        return userNameUnique.get({username: name}).$promise.then(function () {
                            ngModel.$setValidity('unique', true);
                        }, function (response) {
                            ngModel.$setValidity('unique', false);

                        });
                    }

                    //console.log(asd);


                    /*var Model = $resource("/api/" + attrs.nameUnique);
                     
                     stop_timeout = $timeout(function () {
                     Model.query({
                     name: name,
                     }, function (models) {
                     return ngModel.$setValidity('unique', models.length === 0);
                     });
                     }, 200);*/

                });
            }
        };
    })
});
