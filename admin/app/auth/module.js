define(['angular', 'angular-couch-potato', 'angular-ui-router', 'angular-resource'], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.auth', ['ui.router']);

    couchPotato.configureApp(module);

    var authKeys = {
        googleClientId: 'n/a',
        facebookAppId: 'n/a'
    };

    module.config(
            function ($provide, $httpProvider, $stateProvider, $couchPotatoProvider, USER_ROLES) {
                // configure states

                /*$stateProvider.state('speciality', {
                    url: '/speciality',
                    views: {
                        root: {
                            controller: 'SpecialityCtrl',
                            templateUrl: 'app/speciality/view/speciality.html'
                        }
                    },
                    data: {
                        title: 'Speciality',
                        htmlId: 'extr-page',
                        roles: [USER_ROLES.anonymous]
                    }
                });*/

                $stateProvider.state('login', {
                    url: '/login',
                    views: {
                        root: {
                            controller: 'LoginCtrl',
                            templateUrl: 'app/auth/views/login.html'
                        }
                    },
                    data: {
                        title: 'Login',
                        htmlId: 'extr-page',
                        roles: [USER_ROLES.anonymous]
                    },
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'auth/controllers/LoginCtrl'
                                    //'modules/forms/directives/validate/smartValidateForm'
                        ])
                    }
                });

                $stateProvider.state('dashboard', {
                    url: '/dashboard',
                    views: {
                        root: {
                            controller: 'DashboardCtrl',
                            //templateUrl: 'app/auth/views/login.html'
                        }
                    },
                    data: {
                        title: 'Dashboard',
                        htmlId: 'extr-page',
                        roles: [USER_ROLES.anonymous]
                    },
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'auth/controllers/DashboardCtrl'
                                    //'modules/forms/directives/validate/smartValidateForm'
                        ])
                    }
                });

                $stateProvider.state('logout', {
                    url: '/logout',
                    onEnter: function (AuthService) {
                        AuthService.logout();
                    },
                    views: {
                        root: {
                            controller: 'LoginCtrl',
                            templateUrl: 'app/auth/views/login.html'
                        }
                    },
                    data: {
                        title: 'Login',
                        htmlId: 'extr-page',
                        roles: [USER_ROLES.anonymous]
                    },
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'auth/controllers/LoginCtrl',
                            'modules/forms/directives/validate/smartValidateForm'])
                    }
                });

                $stateProvider.state('register', {
                    url: '/register',
                    views: {
                        root: {
                            controller: 'RegisterCtrl',
                            templateUrl: 'app/auth/views/register.html'
                        }
                    },
                    data: {
                        title: 'Register',
                        htmlId: 'extr-page',
                        roles: [USER_ROLES.anonymous]
                    },
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies([
                            'auth/controllers/RegisterCtrl',
                            'auth/directives/passwordVerify',
                            'auth/directives/nameUnique',
                            'auth/directives/emailUnique'
                                    //'modules/forms/directives/validate/smartValidateForm'
                        ])
                    }
                });

                $stateProvider.state('forgotPassword', {
                    url: '/forgot-password',
                    views: {
                        root: {
                            templateUrl: 'app/auth/views/forgot-password.html'
                        }
                    },
                    data: {
                        title: 'Forgot Password',
                        htmlId: 'extr-page',
                        roles: [USER_ROLES.anonymous]
                    },
                    resolve: {
                        deps: $couchPotatoProvider.resolveDependencies(['auth/controllers/ForgotPasswordCtrl'])
                    }
                });

                $stateProvider.state('lock', {
                    url: '/lock',
                    views: {
                        root: {
                            templateUrl: 'app/auth/views/lock.html'
                        }
                    },
                    data: {
                        title: 'Locked Screen',
                        htmlId: 'lock-page',
                        roles: [USER_ROLES.anonymous]
                    }
                });

                // register auth interceptor - must do here to force creation and can't use lazy loading
                $provide.factory('AuthInterceptor', function ($window, $injector, $rootScope, $q, $log, AUTH_EVENTS) {
                    // TODO: move bulk of this code into the AuthService to keep it in one place
                    var interceptor = {
                        request: function (config) {
                            //$log.debug('request called with config = ' + angular.toJson(config));

                            // add oauth headers from localstorage if they are not already there
                            //$log.info('LOCAL INFO...');
                            //$log.info($window.localStorage['oauth']);
                            if ($window.localStorage['oauth'])
                            {
                                $window.sessionStorage['oauth'] = $window.localStorage['oauth'];
                            }

                            var oauthJson = $window.sessionStorage['oauth'];


                            //$log.debug('found oauth in session storage=' + oauthJson);

                            if (oauthJson) {
                                var oauth = angular.fromJson(oauthJson);
                                if (oauth.access_token && !('Authorization' in config.headers)) {
                                    config.headers['Authorization'] = 'Bearer ' + oauth.access_token;
                                    $log.debug('added authorization header=' + config.headers['Authorization']);
                                }
                            }
                            // TODO: else see if credentials are remembered and perform login via promise
                            // - also see which runs first, interceptor or state change handler

                            // TODO: should return promise if doesn't have a valid session?
                            return config;
                        },
                        responseError: function (response) {
                            // $log.debug('responseError called with arg = ' + response);
                            var status = response.status;

                            // TODO: should have a better way to determine the error reason -
                            // oauth says to return 400
                            if (status === 400 && response.data && response.data.error && response.data.error.length > 0) {
                                status = 401;
                            }

                            $rootScope.$broadcast({
                                401: AUTH_EVENTS.notAuthenticated,
                                403: AUTH_EVENTS.notAuthorized,
                                419: AUTH_EVENTS.sessionTimeout,
                                440: AUTH_EVENTS.sessionTimeout
                            }[status], response);

                            return $q.reject(response);
                        }
                    };

                    // $rootScope.$on(AUTH_EVENTS.authenticationSuccess, function(event, oauth) {
                    // header.key = 'Authorization';
                    // header.value = 'Bearer ' + oauth.access_token;
                    // });

                    // TODO: not sure if this is the exact right place for this...
                    $rootScope.$on(AUTH_EVENTS.loginSuccess, function (event, session) {
                        $rootScope.user = session.user;
                        //console.log($rootScope.user);
                        //console.log("inside ---- loginSuccess");
                        //$log.debug('set user in root scope=' + session.user);
                        $('#main').hide();
                        getStatePath();
                    });
                    $rootScope.$on(AUTH_EVENTS.authInitSuccess, function (event, session) {
                        if (session.user) {
                            $rootScope.user = session.user;
                        }
                       // console.log($rootScope.user);
                        //console.log("inside ---- authInitSuccess");
                        //$log.debug('set user in root scope=' + session.user);
                        // $('#main').show();
                        // getStatePath();
                    });


                    $rootScope.$on(AUTH_EVENTS.logoutSuccess, function (event) {
                        window.location.hash = "login";
                    });

                    var getStatePath = function () {
                        // console.log($rootScope.user);

                        //console.log(123123123132123123123123123);
                        if ($rootScope.user) {
                            if ($rootScope.user.id != '')
                            {
                                window.location.hash = "user";
                            }
                            else {
                                window.location.hash = "login";
                            }
                        }
                        else {
                            window.location.hash = "login";
                        }


                        //window.location.hash = "orders";
                    }

                    // $rootScope.$on(AUTH_EVENTS.logoutSuccess, function(event, oauth) {
                    // header.key = null;
                    // header.value = null;
                    // });

                    var notifyAccessDenied = function (rejection) {
                        console.log(rejection);
                        /*$.bigBox({
                         title: 'Access Denied',
                         content: rejection,
                         color: "#C46A69",
                         icon: "fa fa-warning shake animated",
                         // number: ++errorCounter,
                         timeout: 6000
                         });*/

                        var alertMsgData = {
                            title: 'Access Denied',
                            content: rejection
                        };
                        $rootScope.$broadcast('triggerErrorAlertPopup', alertMsgData);


                    };


                    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
                        $rootScope.$broadcast('triggerShowNgProgress');
                        $('.router-animation-loader').remove();
                        $log.debug('toState.url=' + toState.url + ', fromState.url=' + fromState.url);

                        // must use injector due to lazy loading
                        var authService = $injector.get('AuthService');
                        //console.log(toState.data);
                        if (!('data' in toState) || !('roles' in toState.data)) {
                            //$log.info("INNNNNNNNNNN-111111");
                            $rootScope.error = "Access undefined for this state";
                            event.preventDefault();
                            notifyAccessDenied($rootScope.error);
                        } else if (!authService.isAuthorized(toState.data.roles)) {
                            // $log.info("INNNNNNNNNNN-22222");
                            $rootScope.error = "Seems like you tried accessing a route you don't have access to...";
                            event.preventDefault();

                            // TODO: which runs first, interceptor or state change handler?
                            // - will determine where to put remember me authentication

                            // only change the state if there is no previous state
                            if (fromState.url === '^') {
                                //$log.info("INNNNNNNNNNN-3333");
                                var $state = $injector.get('$state');

                                if (authService.isAuthenticated()) {
                                    //$log.info("INNNNNNNNNNN-4444");
                                    //$state.go('app.dashboard');
                                    //window.location.hash = "dashboard";
                                    // getStatePath();
                                } else {
                                    //$log.info("INNNNNNNNNNN-55555");
                                    $rootScope.error = null;
                                    //$state.go('login');
                                    window.location.hash = "login";
                                }
                            } else {
                                //$log.info("INNNNNNNNNNN-66666");
                                notifyAccessDenied($rootScope.error);
                                window.location.hash = "login";
                            }
                        }
                    });

                    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                        $rootScope.$broadcast('triggerHideNgProgress');
                    });

                    return interceptor;
                });

                // configure auth interceptor
                $httpProvider.interceptors.push('AuthInterceptor');

            })

            .constant('authKeys', authKeys)

            .constant('AUTH_EVENTS', {
                authenticationSuccess: 'auth-oauth-success',
                authenticationFailed: 'auth-oauth-failed',
                loginSuccess: 'auth-login-success',
                loginFailed: 'auth-login-failed',
                logoutSuccess: 'auth-logout-success',
                sessionTimeout: 'auth-session-timeout',
                notAuthenticated: 'auth-not-authenticated',
                notAuthorized: 'auth-not-authorized'
            })

            .constant('USER_ROLES', {
                all: '*',
                anonymous: 'ROLE_ANONYMOUS',
                admin: 'ROLE_ADMIN',
                client: 'ROLE_CLIENT_APPLICATION',
                representative: 'ROLE_REPRESENTATIVE'
            })

            .run(function ($couchPotato, $rootScope, $log) {
                module.lazy = $couchPotato;
            });

    return module;
});
