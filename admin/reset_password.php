<?php
if (!isset($_REQUEST['id']) || trim($_REQUEST['id']) == '') {
    echo 'Invalid request';
    die();
}
?>
<html>
    <head>
        <title>Reset Password</title>
        <script src="vendor/jquery/jquery.js"></script>
        <style>
            .div_label
            {
                width: 12%;
                float: left;
            }

            .div_element
            {
                width: 88%;
                float: left;
            }
            
            .div_parent
            {
                float: left;
                padding: 5px;
                width: 100%;
            }
        </style>
    </head>
    <body>
        <div style="width: 100%">
            
            <div class="div_parent" id="alert_div">Enter new password and confirm password</div>
            
            <div class="div_parent">
                <div class="div_label"><label>New Password :</label></div>
                <div class="div_element"><input type="password" name="password" id="password" value="" autocomplete="off"></div>
            </div>

            <div class="div_parent">
                <div class="div_label"><label>Confirm Password :</label></div>
                <div class="div_element"><input type="password" name="confirm_password" id="confirm_password" value="" autocomplete="off"></div>
            </div>

            <div class="div_parent">
                <button type="button" name="reset_btn" id="reset_btn" value="Reset" onclick="reset_password();">Reset</button>
            </div>
        </div>
    </body>
</html>

<script type="text/javascript">
    function reset_password()
    {
        $('#alert_div').html('');
        
        var password = $('#password').val();
        var confirm_password = $('#confirm_password').val();
        
        if(password == '' || confirm_password == '')
        {
            $('#alert_div').css('color','red').html('Please enter new password and confirm password.');
            return false;
        }
        
        if(password != confirm_password)
        {
            $('#alert_div').css('color','red').html('New password and confirm password does not match.');
            return false;
        }
        
        var data = {"id":"<?php echo $_REQUEST['id']; ?>","password":password};
        
        $('#alert_div').css('color','red').html('Please wait.....');
        
        $.ajax({
            method:'post',
            url: 'http://api.delivermint.com/users/change-password',
            data: JSON.stringify(data),
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            success:function(msg)
            {
                if(msg.status)
                {
                    $('#alert_div').css('color','green').html('Password is updated successfully.');
                }
                else
                {
                    $('#alert_div').css('color','red').html('Password is not updated. Please try again.');
                }
            }
        });
    }
</script>